<?php
/*
 * Входные двнные находятся в stdClass $data
 * */
$cat=$data->cat;
$posts=$data->posts;
$tpl->title=NAME;
$tpl->desc="Big collection of Cliparts. Here both free and author's cliparts. Use our search or see the top collections.";
?>
<script type="text/javascript" src="<?=HREF?>/modules/posts/lists/search/tpl/files/js/autocomplite.js"></script>
<div class="container background-white">
	<div class="row margin-vert-30">
		<!-- Main Text -->
		<div class="col-md-12">
			<h2 class="text-center">Welcome to <?=NAME?></h2>
			<p class="text-center">
				What is a clipart?
				<br/>
				Clipart is primarily art, expressed in digital format.
				In our time, this kind of graphics has increased greatly, different types of clipart for offshore interfaces, design of logos, advertising, websites and so on are required.
			</p>
			<p class="text-center">
				Like any images in the digital world, the cliparts are raster and vector.
				Raster images are images that consist of a fixed number of parts and when the image size increases, it will be distorted.
				A vector image is an image that uses a geometric model, a sequence of dots, which means that the image can be printed with the maximum resolution without loss of quality.
			</p>
			<p class="text-center">
				If you want to distribute your clipart for free, then write to us in the form or contact us in any convenient way for you.
				If you want to sell your clipart, we will gladly help you with this, showing your clipart for the audience of our sites.
			</p>
		</div>
		<!-- End Main Text -->
	</div><hr class="margin-vert-40">
	<div class="row">
		<div class="col-md-12 portfolio-group no-padding">
		<?if(!empty($posts)){
			foreach($posts as $p){
				if(!isset($p->imgs)) continue;?>
			<div class="col-md-4 portfolio-item margin-bottom-40 video">
				<div>
					<a href="<?=url::post($p->url)?>">
						<figure>
							<img src="<?=url::imgThumb('600_',$p->imgs[0])?>" alt="<?=$p->title?>">
							<figcaption>
								<h3 class="margin-top-20"><?=$p->title?></h3>
								<span class="glyphicon glyphicon-heart"></span> <?=$p->like?>
								<span style="margin-left: 10px;" class="glyphicon glyphicon-eye-open"></span> <?=$p->statViews?>
							</figcaption>
						</figure>
					</a>
				</div>
			</div>
			<?}
		}?>
		</div>
	</div>
</div><hr class="margin-vert-40">
<div class="container background-white">
	<div class="row padding-vert-40">
		<div class="col-md-12 text-center">
			<h2 class="animate fadeIn text-center">We will help you find what you need</h2>
			<form method="get" action="" class="form-search">
			<div class="input-append">
				<input type="hidden" name="module" value="posts/lists/search">
				<input class="search-input" type="text" name="q" id="name" placeholder="Enter your search term here" autocomplete="off"/> 
				<input type="submit" value="Search" class="special btn btn-primary" />
			</div>
			</form>
		</div>
	</div><hr class="margin-vert-40">
	<div class="row padding-vert-40">
		<div class="col-md-12 text-center">
			<h3>Recent cliparts</h3>
			<div class="portfolio-filter-container margin-top-20">
				<ul class="portfolio-filter">
					<?foreach($data->seealso as $r){?>
					<li>
						<a href="<?=url::post($r->url)?>" class="portfolio-selected btn btn-default"><?=$r->title?></a>
					</li>
					<?}?>
				</ul>
			</div>
		</div>
	</div>
</div>