<!DOCTYPE html>
<!--[if IE 8]> <html lang="en-US" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en-US" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en-US" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
	<!--<![endif]-->
	<head><?=timer(2);?>
		<!-- Title -->
		<title><?=@$data->title?></title>
		<!-- Meta -->
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="<?=@htmlspecialchars($data->desc)?>">
		<meta name="author" content="<?=NAME?>">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<?=@$data->meta?>
		<!-- Favicon -->
		<link href="<?=THEME_HREF?>/template/tpl/files/favicon.ico" rel="shortcut icon">
		<!-- Bootstrap Core CSS -->
		<link rel="stylesheet" href="<?=THEME_HREF?>/template/tpl/files/assets/css/bootstrap.css" rel="stylesheet">
		<!-- Template CSS -->
		<link rel="stylesheet" href="<?=THEME_HREF?>/template/tpl/files/assets/css/animate.css" rel="stylesheet">
		<link rel="stylesheet" href="<?=THEME_HREF?>/template/tpl/files/assets/css/font-awesome.css" rel="stylesheet">
		<link rel="stylesheet" href="<?=THEME_HREF?>/template/tpl/files/assets/css/nexus.css" rel="stylesheet">
		<link rel="stylesheet" href="<?=THEME_HREF?>/template/tpl/files/assets/css/responsive.css" rel="stylesheet">
		<link rel="stylesheet" href="<?=THEME_HREF?>/template/tpl/files/assets/css/custom.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<?=$data->css->theme?>"/>
		<!-- Google Fonts-->
		<link href="http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300" rel="stylesheet" type="text/css">
		<link href="http://fonts.googleapis.com/css?family=PT+Sans" type="text/css" rel="stylesheet">
		<link href="http://fonts.googleapis.com/css?family=Roboto:400,300" rel="stylesheet" type="text/css">
		<?=@$data->headlink?>
		<script type="application/javascript" src="<?=HREF?>/files/js/jquery-2.1.4.min.js"></script>
	</head>
	<body>
		<div id="body-bg">
			<?include $template->inc('template/header.php');?>
			<div id="content"><?=$data->body?></div>
			<?include $template->inc('template/footer.php');?>
			<?=$data->panel?>
			<?include $template->inc('template/stat.php');?>
		</div>
		<!-- End Footer Menu -->
        <!-- JS -->
        <script type="text/javascript" src="<?=THEME_HREF?>/template/tpl/files/assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?=THEME_HREF?>/template/tpl/files/assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?=THEME_HREF?>/template/tpl/files/assets/js/scripts.js"></script>
        <!-- Isotope - Portfolio Sorting -->
        <script type="text/javascript" src="<?=THEME_HREF?>/template/tpl/files/assets/js/jquery.isotope.js"></script>
        <!-- Mobile Menu - Slicknav -->
        <script type="text/javascript" src="<?=THEME_HREF?>/template/tpl/files/assets/js/jquery.slicknav.js"></script>
        <!-- Animate on Scroll-->
        <script type="text/javascript" src="<?=THEME_HREF?>/template/tpl/files/assets/js/jquery.visible.js" charset="utf-8"></script>
        <!-- Sticky Div -->
        <script type="text/javascript" src="<?=THEME_HREF?>/template/tpl/files/assets/js/jquery.sticky.js" charset="utf-8"></script>
        <!-- Slimbox2-->
        <script type="text/javascript" src="<?=THEME_HREF?>/template/tpl/files/assets/js/slimbox2.js" charset="utf-8"></script>
        <!-- Modernizr -->
        <script  type="text/javascript" src="<?=THEME_HREF?>/template/tpl/files/assets/js/modernizr.custom.js"></script>
        <!-- End JS -->
	</body>
</html>