<ul class="social-icons pull-right hidden-xs">
	<li class="social-rss">
		<a href="<?=url::post_rss()?>" target="_blank" title="RSS"></a>
	</li>
	<li class="social-twitter">
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
		<a rel="nofollow" href="https://twitter.com/share" data-lang="en" target="_blank" title="Twitter"></a>
	</li>
	<li class="social-facebook">
		<a rel="nofollow" href="#" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u='+document.location);return false;" target="_blank" title="Facebook"></a>
	</li>
	<li class="social-googleplus">
		<a rel="nofollow" href="#" onclick="javascript:window.open('https://plus.google.com/share?url='+document.location,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" title="GooglePlus"></a>
	</li>
</ul>
<div id="pre-header" class="container" style="height:40px">
</div>
<div id="header">
	<div class="container">
		<div class="row">
			<!-- Logo -->
			<div class="logo">
				<a href="<?=HREF?>" title="home" style="top:46px;padding: 10px 8px;border-right: 3px solid #f5f4f6;border-left: 3px solid #f5f4f6;text-transform: uppercase;font-size: 34px;font-stretch: condensed;color: #f5f4f6;font-family: FontAwesome;">
					<?=NAME?>
				</a>
			</div>
			<!-- End Logo -->
		</div>
	</div>
</div>
<!-- Top Menu -->
<div id="hornav" class="container no-padding">
	<div class="row">
		<div class="col-md-12 no-padding">
			<div class="text-center visible-lg">
				<ul id="hornavmenu" class="nav navbar-nav">
					<li>
						<a href="<?=HREF?>" class="fa-home">Home</a>
					</li>
					<?=$data->userMenu?>
					<li>
						<a href="<?=url::contacts()?>" class="fa-comment">Contact</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- End Top Menu -->
<div id="post_header" class="container" style="height:40px"></div>
<div id="content-top-border" class="container"></div>