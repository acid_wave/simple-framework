<div id="content-bottom-border" class="container"></div>
<div id="base">
    <div class="container padding-vert-30 margin-top-60">
        <div class="row">
            <!-- Contact Details -->
            <div class="col-md-4 margin-bottom-20">
                <h3 class="margin-bottom-10">Contact Details</h3>
                <p>
                    <span class="fa-envelope"></span>
                    <a href="<?=url::contacts()?>" rel="nofollow">contact form</a>
                    <br>
                </p>
                <p>3541 Rosewood Court, Owatonna,
                    <br>Minnesota</p>
            </div>
            <!-- End Contact Details -->
            <!-- Sample Menu -->
            <div class="col-md-3 margin-bottom-20">
                <h3 class="margin-bottom-10">Sample Menu</h3>
                <ul class="menu">
                    <li>
                        <a class="fa-users" href="<?=url::staticPage('terms')?>" rel="nofollow">Terms</a>
                    </li>
                    <li>
                    	<a class="fa-tasks" href="<?=url::staticPage('privacy')?>" rel="nofollow">Privacy Policy</a>
                    </li>
                    <li>
                        <a class="fa-signal" href="<?=url::staticPage('terms')?>#copyright" rel="nofollow">DMCA</a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <!-- End Sample Menu -->
            <div class="col-md-1"></div>
            <!-- Disclaimer -->
            <div class="col-md-3 margin-bottom-20 padding-vert-30 text-center">
                <h3 class="color-gray margin-bottom-10">Join our Newsletter</h3>
                <p>
                    Sign up for our newsletter for all the
                    <br>latest news and information
                </p>
                <input type="email">
                <br>
                <button class="btn btn-primary btn-lg margin-top-20" type="button">Subscribe</button>
            </div>
            <!-- End Disclaimer -->
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Footer Menu -->
<div id="footer">
    <div class="container">
        <div class="row">
            <div id="footermenu" class="col-md-8"></div>
            <div id="copyright" class="col-md-4">
                <p class="pull-right"><?=date('Y')?> <?=NAME?></p>
            </div>
        </div>
    </div>
</div>