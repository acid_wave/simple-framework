<?php
$post=$data->post;
$title=$post->title.(strstr(strtolower($post->title), 'clipart')?'':' clipart');
$tpl->title=$title;
$tpl->desc="{$title} collection. Here are ".count($post->imgs)." cliparts.";

if(($oc=current($data->otherPosts))&&($on=next($data->otherPosts)))
	$tpl->desc.=" And similar cliparts - {$oc->title}, {$on->title}.";
?>
<script type="text/javascript" src="<?=HREF?>/modules/posts/lists/search/tpl/files/js/autocomplite.js"></script>
<div class="container background-white">
	<div class="row">
		<div class="col-md-12 margin-vert-30"><h1><?=$title?></h1></div>
		<div class="col-md-12 portfolio-group no-padding">
		<?if(!empty($data->post->imgs)){
			foreach($data->post->imgs as $img){?>
			<div class="col-md-4 portfolio-item margin-bottom-40 video">
				<div>
					<a href="<?=url::img('post',$img->pid,$img->url)?>">
						<figure>
							<img src="<?=url::image($img->url)?>" alt="<?=$img->title?>">
							<figcaption>
								<h3 class="margin-top-20"><?=$img->gtitle?></h3>
								<span><?=$img->text?></span><br>
								<span class="glyphicon glyphicon-picture"></span> <?="{$img->width}x{$img->height}"?>
								<span style="margin-left: 10px;" class="glyphicon glyphicon-floppy-disk"></span> <?=ceil($img->filesize/1024)?>kb
								<span style="margin-left: 10px;" class="glyphicon glyphicon-eye-open"></span> <?=$img->statViews?>
							</figcaption>
						</figure>
					</a>
				</div>
			</div>
			<?}
		}?>
		</div>
	</div><hr class="margin-vert-40">
</div>
<div class="container background-white">
	<div class="row padding-vert-40">
		<div class="col-md-12 text-center">
			<h2 class="animate fadeIn text-center">Search more cliparts</h2>
			<form method="get" action="" class="form-search">
			<div class="input-append">
				<input type="hidden" name="module" value="posts/lists/search">
				<input class="search-input" type="text" name="q" id="name" placeholder="Enter your search term here" autocomplete="off"/> 
				<input type="submit" value="Search" class="special btn btn-primary" />
			</div>
			</form>
		</div>
	</div><hr class="margin-vert-40">
</div>
<div class="container background-white">
	<div class="row padding-vert-40">
		<div class="col-md-12">
			<div class="row padding-vert-40">
				<div class="col-md-12">
					<div class="portfolio-filter-container margin-top-20">
						<ul class="portfolio-filter">
							<li class="portfolio-filter-label label label-primary">
								Recent cliparts
							</li>
							<?
							reset($data->otherPosts);
							foreach($data->otherPosts as $r){?>
							<li>
								<a href="<?=url::post($r->url)?>" class="portfolio-selected btn btn-default"><?=$r->title?></a>
							</li>
							<?}?>
						</ul>
					</div>
					<div class="portfolio-filter-container margin-top-20">
						<ul class="portfolio-filter">
							<li class="portfolio-filter-label label label-primary">
								See also
							</li>
							<?
							foreach($data->seealso as $r){?>
							<li>
								<a href="<?=url::post($r->url)?>" class="portfolio-selected btn btn-default"><?=$r->title?></a>
							</li>
							<?}?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>