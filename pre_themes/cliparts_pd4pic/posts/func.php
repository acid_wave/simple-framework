<?php
namespace custom_posts;
use module,db,url,cache;

/*
	получает список постов в одной из категорий текущего поста
*/
function relatedByCat($post,$limit=4){
	$tbls=\posts\tables::init();
	$related=array();

	$sql="SELECT post.* FROM `{$tbls->post}` WHERE post.pincid='' && post.published='published' && %s";
	db::query(sprintf($sql,"post.id>'{$post->id}' LIMIT {$limit}"));
	while ($d=db::fetch()) {
		$d->txt=\posts\cutText(strip_tags($d->txt),20);
		$related[$d->id]=$d;
		$limit--;
	}
	if($limit){
		db::query(sprintf($sql,"post.id>0 && post.id!='{$post->id}' LIMIT {$limit}"));
		while ($d=db::fetch()) {
			$d->txt=\posts\cutText(strip_tags($d->txt),20);
			$related[$d->id]=$d;
		}
	}
	\posts\getImages2list($related);

	return $related;
}
?>