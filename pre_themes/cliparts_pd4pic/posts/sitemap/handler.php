<?php
namespace custom_posts_sitemap;
use module,db,url,cache;

class handler extends \posts_sitemap\handler{

	/**
	 * Выводит sitemap в виде HTML страниц по 100 ссылок
	 */
	function sitemapHTML($page,$perpage,$keyword=false){
		$this->template='template';
		$posts=array();

		$limit=($page-1)*$perpage;
		$sql="SELECT %s FROM `post` WHERE `published`='published' && `pincid`=''".($keyword?" && `title` LIKE '%%".db::escape($keyword)."%%'":'')."%s";
		list($count)=db::qrow(sprintf($sql,'COUNT(url)',''));
		db::query(sprintf($sql,
			'`title`,`url`,`datePublish`',
			" LIMIT $limit,$perpage"
		));
		while ($d=db::fetch()) {
			$posts[]=$d;
		}

		return (object)array(
			'posts'=>$posts,
			'keyword'=>$keyword,
			'page'=>$page,
		);
	}
}