<?php
$tpl->title="Map - ".($data->keyword?"{$data->keyword} - ":'')."page {$data->page}"
?>
<div style="width: 80%;margin: auto;">
<h2 style="text-transform: capitalize;margin-bottom: 10px;"><?=$data->keyword?$data->keyword:''?></h2>
<?php
if(isset($data->posts)){?>
	<ul>
	<?foreach ($data->posts as $p) {?>
		<li><a href="<?=url::post($p->url)?>"><?=$p->title?></a></li>
	<?}?>
	</ul>
<?}?>
</div>