<!DOCTYPE HTML>
<html>
	<head><?=timer(2);?>
		<meta charset="utf-8" />
		
		<title><?=@$data->title?></title>
		<meta name="description" content="<?=@htmlspecialchars($data->desc)?>" />
		<?=@$data->meta?>
		<?=@$data->headlink?>

		<link href="<?=HREF?>/favicon.ico" rel="shortcut icon"/>
		<link rel="stylesheet" type="text/css" href="<?=$data->css->admin?>"/>

		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<!--[if lte IE 8]><script src="<?=THEME_HREF?>/template/tpl/files/assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="<?=THEME_HREF?>/template/tpl/files/assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="<?=THEME_HREF?>/template/tpl/files/assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="<?=THEME_HREF?>/template/tpl/files/assets/css/ie8.css" /><![endif]-->
	</head>
	<body>

		<!-- Wrapper -->
			<div id="wrapper">
				<div id="main">
					<div class="inner">
						<?=$data->body?>
					</div>
				</div>
				<!-- Sidebar -->
					<div id="sidebar">
						<div class="inner">

							<!-- Search -->
								<section id="search" class="alt">
									<form method="post" action="#">
										<input type="text" name="query" id="query" placeholder="Search" />
									</form>
								</section>

							<!-- Menu -->
								<nav id="menu">
									<header class="major">
										<h2>Menu</h2>
									</header>
									<ul>
										<li><a href="/">Homepage</a></li>
										<li><a href="/s/generic.html">Generic</a></li>
										<li><a href="/s/elements.html">Elements</a></li>
										<li>
											<span class="opener">Submenu</span>
											<ul>
												<li><a href="#">Lorem Dolor</a></li>
												<li><a href="#">Ipsum Adipiscing</a></li>
												<li><a href="#">Tempus Magna</a></li>
												<li><a href="#">Feugiat Veroeros</a></li>
											</ul>
										</li>
										<li><a href="#">Etiam Dolore</a></li>
										<li><a href="#">Adipiscing</a></li>
										<li>
											<span class="opener">Another Submenu</span>
											<ul>
												<li><a href="#">Lorem Dolor</a></li>
												<li><a href="#">Ipsum Adipiscing</a></li>
												<li><a href="#">Tempus Magna</a></li>
												<li><a href="#">Feugiat Veroeros</a></li>
											</ul>
										</li>
										<li><a href="#">Maximus Erat</a></li>
										<li><a href="#">Sapien Mauris</a></li>
										<li><a href="#">Amet Lacinia</a></li>
									</ul>
								</nav>

							<!-- Section -->
								<section>
									<header class="major">
										<h2>Ante interdum</h2>
									</header>
									<div class="mini-posts">
										<article>
											<a href="#" class="image"><img src="<?=THEME_HREF?>/template/tpl/files/images/pic07.jpg" alt="" /></a>
											<p>Aenean ornare velit lacus, ac varius enim lorem ullamcorper dolore aliquam.</p>
										</article>
										<article>
											<a href="#" class="image"><img src="<?=THEME_HREF?>/template/tpl/files/images/pic08.jpg" alt="" /></a>
											<p>Aenean ornare velit lacus, ac varius enim lorem ullamcorper dolore aliquam.</p>
										</article>
										<article>
											<a href="#" class="image"><img src="<?=THEME_HREF?>/template/tpl/files/images/pic09.jpg" alt="" /></a>
											<p>Aenean ornare velit lacus, ac varius enim lorem ullamcorper dolore aliquam.</p>
										</article>
									</div>
									<ul class="actions">
										<li><a href="#" class="button">More</a></li>
									</ul>
								</section>

							<!-- Section -->
								<section>
									<header class="major">
										<h2>Get in touch</h2>
									</header>
									<p>Sed varius enim lorem ullamcorper dolore aliquam aenean ornare velit lacus, ac varius enim lorem ullamcorper dolore. Proin sed aliquam facilisis ante interdum. Sed nulla amet lorem feugiat tempus aliquam.</p>
									<ul class="contact">
										<li class="fa-envelope-o"><a href="#">information@untitled.tld</a></li>
										<li class="fa-phone">(000) 000-0000</li>
										<li class="fa-home">1234 Somewhere Road #8254<br />
										Nashville, TN 00000-0000</li>
									</ul>
								</section>

							<!-- Footer -->
								<footer id="footer">
									<p class="copyright">&copy; Untitled. All rights reserved. Demo Images: <a href="https://unsplash.com">Unsplash</a>. Design: <a href="https://html5up.net">HTML5 UP</a>.</p>
								</footer>

						</div>
					</div>

			</div>

		<!-- Scripts -->
			<script src="<?=THEME_HREF?>/template/tpl/files/assets/js/jquery.min.js"></script>
			<script src="<?=THEME_HREF?>/template/tpl/files/assets/js/skel.min.js"></script>
			<script src="<?=THEME_HREF?>/template/tpl/files/assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="<?=THEME_HREF?>/template/tpl/files/assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="<?=THEME_HREF?>/template/tpl/files/assets/js/main.js"></script>

			<?=$data->panel?>
			<?include $template->inc('template/stat.php');?>
	</body>
</html> 
