<?php
namespace custom_staticPage;
use module,db,url,cache;

class handler{
	function __construct(){
		$this->template='';#Определяем в какой шаблон будем вписывать
		$this->headers=(object)array();
	}
	function index($url){#Делаем все обработки для вывода данных
		$url=urldecode($url);
		$file1=__DIR__."/tpl/index/{$url}.php";
		$file2=THEME_PATH."staticPage/tpl/index/{$url}.php";
		if(!$url||strstr($url, '/')||(!file_exists($file1) && !file_exists($file2))){
			$this->headers->location=HREF;
			return;
		}
		return (object)array('file'=>"{$url}.php");
	}
}
