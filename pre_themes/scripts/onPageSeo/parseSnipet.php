<?
/*
 * Парсит все скаченные файлы гугла и формирует полный список тайтлов и снипетов картинок
*/
define('SITEOFF',1);#отключаем все кроме инициализации движка
include_once __DIR__.'/../../../index.php';#подключаем движок

$files=scandir($dir=PATH."modules/images/admin/download/daemon/tmp/gglparse.tmp/");
foreach($files as $fl){
	if($fl=='.' or $fl=='..')continue;
	$texts=parse(file_get_contents($dir.$fl));
}
function parse($html){
	preg_match_all('!<div class="rg_meta">(.+?)</div>!is',$html,$m);# дает ссылки на ~100 последних картинок
	$texts=array();
	if(isset($m[0])){
		foreach($m[1] as $k=>$jsonEnc){
			$obj = json_decode($jsonEnc);
			if(!is_object($obj) or empty($obj->s)) continue;
			$texts[]=$obj->s;
		}
	}
	return $texts;

}
