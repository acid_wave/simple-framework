<?
/*
 * Обновляет контент на списке страниц (обновляем только по 1 кейворду на пост)
 * 
 * Вход:
 * 	Файл со списком страниц для обновления (по умолчанию TMP/onPageSeo/updateUrls.txt)
 * Выход:
 * 	Выводит список постов по которым не нашол кеш гугла
*/

define('SITEOFF',1);#отключаем все кроме инициализации движка
include_once __DIR__.'/../../../index.php';#подключаем движок
set_time_limit(0);

@$file=$argv[1];
if(empty($file))$file=TMP."onPageSeo/updateUrls.txt";
#Получаем список кейвордов для которых нужено обновление
	$fl=file($file);
	if(is_array($fl))foreach($fl as $i=>$url){
		$url=trim($url);
		if($url=='')continue;
		list($pid)=db::qrow("SELECT id FROM `post`post WHERE url='$url'");
		list($kid)=db::qrow("SELECT kid FROM `zspec_keyword2post` WHERE pid='$pid'");
		$pid2url[$pid]=$url;
		$pid2kid[$pid]=$kid;
	}
#Обновляем снипеты постов
	$dir=PATH."modules/images/admin/download/daemon/tmp/gglparse.tmp/";
	foreach($pid2kid as $pid=>$kid){
		if(file_exists($dir.$kid)){
			$texts=parse(file_get_contents($dir.$kid));
			shuffle($texts);
			updateImages($pid,$texts);
		}else{
			$noCache[]=$kid;
			unset($pid2url[$pid]);
		}
	}
	print 1;
	if(!empty($noCache)){
		print "list of keywords without cache:\n".implode(",",$noCache)."\n";
	}
#Добавляем новый хеш
	addHash(array_values($pid2url));

print "\nDONE!\n";


#Обновляем тексты картинок
function updateImages($pid,$texts){
	$q=db::query("SELECT id FROM `zspec_imgs` WHERE pid='$pid'");
	while($d=db::fetch($q)){
		$text=current($texts);next($texts);
		db::query("UPDATE `zspec_imgs` SET text='".db::escape($text)."' WHERE id='$d->id' limit 1");
	}
}
#Получаем снипеты из кеша данных спаршенных с гугл картинок
function parse($html){
	preg_match_all('!<div class="rg_meta">(.+?)</div>!is',$html,$m);# дает ссылки на ~100 последних картинок
	$texts=array();
	if(isset($m[0])){
		foreach($m[1] as $k=>$jsonEnc){
			$obj = json_decode($jsonEnc);
			if(!is_object($obj) or empty($obj->s)) continue;
			$texts[]=$obj->s;
		}
	}
	return $texts;

}
#Записываем новый хеш урлам
function addHash($urls){
	#Создаем поле в БД если его нет
	db::query("ALTER TABLE  `post` ADD  `seoHash` VARCHAR( 255 ) NOT NULL;");
	#Записываем хеши
	foreach($urls as $i=>$url){
		if(empty($hash)or $i%1000==0){
			$hashes[]=$hash=md5(mt_rand());
		}
		$hash2url[$hash][]=$url;
		db::query("UPDATE post SET seoHash='$hash' WHERE url='$url' limit 1");
		print "$i ";
	}
	print_r($hashes);
	#Записываем список уникальных хеш
		@mkdir($dir=TMP."onPageSeo");
		file_put_contents("$dir/hashList.txt",implode("\n",$hashes)."\n",FILE_APPEND);
	#Записываем привязку хеш -> url
		@mkdir($dir=TMP."onPageSeo/hash2url");
		foreach($hash2url as $hach=>$urls){
			file_put_contents("$dir/$hach.txt",implode("\n",$urls));
		}
}
