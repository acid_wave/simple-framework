<?php
namespace custom_posts;
use module,db,url,cache;

require_once __DIR__.'/func.php';

class handler extends \posts\handler{
	/*
		Обработчик страницы поста
	*/
	function post($url,$imgfromcookie,$prfxtbl){
		#переключаем таблицы постов
		$tbls=\posts\tables::init($prfxtbl);

		$post=\posts\getPost($url,$this->userControl->access(),$imgfromcookie);
		#Проверяем права на просмотр и редактирование поста
		$accessEdit=\posts\checkAccess($this->userControl,$post->user);
		if(empty($post->url) or ($post->published!='published' && !$accessEdit)){
			\custom_posts\gagBlogOut();
			$this->headers->location=HREF;
			return;
		}

		return array(
			'post'=>$post,
			'prfxtbl'=>$tbls->prfx,
			'keyword'=>$post->keyword,

			# Коментарии
			'comments' => module::exec('posts/comments', 
				array('pid'=>$post->id), 1)->str,
			# Доступы
			'access'=>(object)array(
				'publish'=>$this->userControl->rbac('publishPost'),
				'edit'=>$accessEdit,
			),
			# Перелинковка на другие посты
			'otherPosts'=>relatedByCat($post,16),
			'otherPictures'=>\posts\featuredPhoto(NULL,3)
		);
	}
}
