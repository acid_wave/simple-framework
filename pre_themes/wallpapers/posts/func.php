<?php
namespace custom_posts;
use module,db,url,cache;

/*
	получает список постов в одной из категорий текущего поста
*/
function relatedByCat($post,$limit=4){
	$tbls=\posts\tables::init();
	$related=array();

	$sql="SELECT post.* FROM `{$tbls->post}` WHERE post.pincid='' && post.published='published' && %s";
	db::query(sprintf($sql,"post.id>'{$post->id}' LIMIT {$limit}"));
	while ($d=db::fetch()) {
		$d->txt=\posts\cutText(strip_tags($d->txt),20);
		$related[$d->id]=$d;
		$limit--;
	}
	if($limit){
		db::query(sprintf($sql,"post.id>0 && post.id!='{$post->id}' LIMIT {$limit}"));
		while ($d=db::fetch()) {
			$d->txt=\posts\cutText(strip_tags($d->txt),20);
			$related[$d->id]=$d;
		}
	}
	\posts\getImages2list($related);

	return $related;
}

/*
	выводит статичные файлы заглушки для редиректнутых сайтов
*/
function gagBlogOut(){
	$path=PATH.'blog'.$_SERVER['REQUEST_URI'];
	if(is_dir($path)) {
		$path=preg_replace('!(.+)/$!', '$1', $path);
		$path.='/index.html';
	}

	if(file_exists($path)){
		die(file_get_contents($path));
	}
}
?>