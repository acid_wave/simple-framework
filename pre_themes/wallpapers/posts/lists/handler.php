<?php
namespace custom_posts_lists;
use module,db,url,cache;

require_once THEME_PATH.'posts/func.php';

class handler extends \posts_lists\handler{
	/*
		1. список постов по категории
		2. подключает данные категории (текущей и список подкатегорий)
	*/ 
	function mainList($category,$prfxtbl,$uid,$page,$num,$uri,$popularPostsCount,$excludeCat){
		$tbls=\posts\tables::init($prfxtbl);
		$baseCat=module::exec('category',array('url'=>$category),'data')->data->cat;
		if(!$this->user->id&&$baseCat->id&&$baseCat->count<3) {
			$this->headers->location=HREF; return;
		}

		$accessEditNewsMy=$this->userControl->rbac('editNewsMy');
		$accessPublish=$this->userControl->rbac('publishPost');

		$showRights=\posts_lists\showRights($accessPublish,$accessEditNewsMy,$uid);

		$start=($page-1)*$num;
		$q="SELECT 
				SQL_CALC_FOUND_ROWS post.*, 
				user.name AS authorName, user.mail AS authorMail 
			FROM (
				".\posts_lists\listPostsSubquery($category,$excludeCat,$showRights)."
			) post
			/* присоединяем данные пользователя */
			LEFT JOIN `".PREFIX_SPEC."users` user 
				ON user.id=post.user
			";
		db::query($q." ORDER BY `datePublish` DESC,date DESC LIMIT $start,$num");
		$posts=array();$keywords=array();
		while($d=db::fetch()){
			//Получаем список параметров для шаблона из текста
			$d->data=$d->data!=''?json_decode($d->data):'';
			$d->stxt=\posts_lists\pageBreak($d->txt);
			$d->txt=\posts\cutText($d->txt);
			#set author's name
			$d->authorName=\posts\setAuthorName($d);
			$posts[$d->id]=$d;
			$posts[$d->id]->keyword=&$keywords[$d->kid];
			$posts[$d->id]->funcPanel='';
		}
		list($count)=db::qrow('SELECT FOUND_ROWS()');
		if(empty($posts)&&$category&&!$this->user->id){
			\custom_posts\gagBlogOut();
			$this->headers->location=HREF; return;
		}
		if($this->user->id){
			db::query("SELECT * FROM `keyword` WHERE id IN (".implode(",",array_keys($keywords)).")");
			while($d=db::fetch()){
				$keywords[$d->id]=$d->title;
			}
			foreach($posts as $id=>$d){
				$posts[$id]->funcPanel=module::exec('posts',array('act'=>'editPanel','post'=>$d),1)->str;
			}
		}

		#получаем pin посты
		$pins=\posts_lists\getPins($category,\posts_lists\showRights($accessPublish,$accessEditNewsMy,false,true));

		#получаем картинки
		db::query(
			"SELECT img.*,keyword.title FROM (
				SELECT pid,kid,url,width,height,priority,text FROM `".PREFIX_SPEC."imgs` img
				WHERE `tbl`='{$tbls->post}' && `pid` IN(".
					implode(',',array_merge(
						array_keys($posts),
						array_keys($pins)
						)
					)
				.")
			) img 
			LEFT JOIN `keyword` ON img.kid=keyword.id 
			ORDER BY img.priority DESC");
		while ($d=db::fetch()) {
			$img=new \posts_lists\classImg;
			if(isset($posts[$d->pid]))$posts[$d->pid]->imgs[]=add2obj($img,$d);
			if(isset($pins[$d->pid]))$pins[$d->pid]->imgs[]=add2obj($img,$d);
		}
		#Выводим снипеты картинок если у постов нет текстов
		foreach($posts as $k=>&$pp){
			if($pp->txt==''&&isset($pp->imgs[0]))
				$pp->txt=$pp->imgs[0]->text;
		}
				
		#собираем title постов (3 шт) для description страницы
		$forDesc=array();
		if(!empty($posts)){
			reset($posts);
			while (@++$i<=3) {
				$p=current($posts);
				if(!empty($p)){
					$forDesc[]=$p->title; next($posts);
				}
			}
			reset($posts);
		}
		return array(
			'posts'=>@$posts,
			'prfxtbl'=>$tbls->prfx,
			'pins'=>$pins,
			#получаем данные категорий
			'cat'=>$baseCat,
			'breadCrumbs'=>\posts\getCrumbs($baseCat),
			'topLevelCats'=>module::exec('category',array('act'=>'mlist','tbl'=>$tbls->post,'category'=>$category,'parentId'=>$baseCat->parentId),1)->str,
			'page'=>$page,
			'num'=>$num,
			'paginator'=>module::exec('plugins/paginator',
				array(
					'act'=>((!$category&&$uri!='')||($category&&$uri==''))?'index':'typesmall',
					'page'=>$page,
					'num'=>$num,
					'count'=>$count,
					'uri'=>$uri==''?url::category($category?$category:'main')."%d/":$uri,
					true,
				)
			)->str,
			'count'=>$count,
			'accessPublish'=>$accessPublish,
			'access'=>$this->userControl->access(),
			'accessEditNewsMy'=>$accessEditNewsMy,
			'forDesc'=>$forDesc,
		);
	}
}
