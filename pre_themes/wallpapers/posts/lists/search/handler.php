<?php
namespace custom_posts_lists_search;
use module,db,url;
require_once(module::$path."/posts/handler.php");
/*
 * Должен возвращать:
 * $this->data - объект переменных для вывода в шаблоне
 * $this->headers - объект для изменения заголовков при отдаче
 * $this->act - если есть несколько вариантов ответов
 */
class handler extends \posts_lists_search\handler{
	function index($q,$prfxtbl,$page,$num){
		$data=module::execData('posts/lists/search',array('q'=>$q,'prfxtbl'=>$prfxtbl,'page'=>$page,'num'=>$num),1);
		$post=current($data->posts);
		header("location:".(empty($post)?"/":url::post($post->url)));
		exit;
	}
}
