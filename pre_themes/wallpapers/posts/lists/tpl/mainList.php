<?php
/*
 * Входные данные находятся в stdClass $data
 * */
$cat=$data->cat;
$posts=$data->posts;
$tpl->title=$cat->title.' HD wallpapers'.(@$data->page>1?" - Page {$data->page}":'')." - ".NAME;
$tpl->meta='<meta name="robots" content="noindex,follow" />';
?>
<script type="text/javascript" src="<?=HREF?>/modules/posts/lists/search/tpl/files/js/autocomplite.js"></script>
<div id="our-work">
	<ul>
	<?if(!empty($posts)){
		foreach($posts as $p){
			if(!isset($p->imgs)) continue;?>
		<li><a href="<?=url::post($p->url)?>" title="<?=$p->title?>">
			<div class="img-cover" style="background-image: url('<?=url::imgThumb('600_',$p->imgs[0])?>')">
				<img src="<?=url::imgThumb('600_',$p->imgs[0])?>" alt="<?=$p->title?>">
			</div>
			<div class="overlay">
				<summary>
					<h2><?=$p->imgs[0]->title?></h2>
					<h3><?=$p->imgs[0]->text?></h3>
				</summary>
				<div class="loves"><span><?=$p->like?></span></div>
			</div>
		</a></li>
		<?}
	}?>
	</ul>
</div>
<div id="what-we-do">
	<?=@$data->paginator?>
</div>
<div id="what-we-do">
	<section id="web-design">
		<article>
			<div style="margin: auto;width: 70%;">
				<h2>Search wallpapers</h2>
				<form method="get" action="/">
					<input type="hidden" name="module" value="posts/lists/search">
					<input style="display: inline-block !important; width: 70%;" class="search-input" type="text" name="q" id="name" placeholder="Enter your search term here" autocomplete="off"/> 
					<input type="submit" value="Search" class="special" />
				</form>
			</div>
		</article>
	</section>
</div>
