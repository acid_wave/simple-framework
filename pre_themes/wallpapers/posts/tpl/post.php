<?php
$post=$data->post;
$tpl->title=(isset($post->cats[0])?$post->cats[0]->title.' ':'')."{$post->title} - ".NAME." | 4K Wallpapers";
$tpl->desc="{$post->title} HD wallpapers, desktop and phone wallpapers.".
	(isset($post->cats[0])?" In this {$post->cats[0]->title} collection we have ".count($data->post->imgs)." wallpapers.":'')
	." Also you can share or upload your favorite wallpapers.";
	
$tpl->meta='
	<meta property="og:title" content="'.$post->title.'" />
	<meta property="og:image" content="'.url::image($post->imgs[0]->url).'" />
	<meta property="og:url" content="'.url::post($post->url).'" />
	<meta property="og:type" content="article" />
	<link rel="canonical" href="'.url::post($post->url).'" />
';

$title1=(isset($post->cats[0])?$post->cats[0]->title.' ':'')."{$post->title} | HD Wallpapers";
?>
<?if($data->access->publish){?>
	<script type="application/javascript" src="<?=HREF?>/files/posts/admin/js/published.js"></script>
<?}?>
<div id="pfd-work">
	<div id="work-bio">
		<header>
			<div id="preamble">
				<?=$post->funcPanel?>
				<h1><?=$post->title?></h1>
				<h2></h2>
				<p>In compilation for wallpaper for <?=$post->title?> HD Wallpaeprs, we have <?=count($post->imgs)?> images.</p>
				<?if(isset($post->data->data->tags)){?>
				<p>
					We determined that these pictures can also depict a <?=implode(', ',$post->data->data->tags)?>.
				</p>
				<?}?>
				<p>You can install this wallpaper on your desktop or on your mobile phone and other gadgets that support wallpaper.</p>
				<p><?=$post->txt?></p>
				<?if(!empty($post->cats)){?>
				<p class="breadcrumbs">
				<?foreach ($post->cats as $val) {?>
					<a href="<?=url::category($val->url,$data->prfxtbl)?>"><?=$val->title?></a> |
				<?}?>
				</p>
			<?}?>
			</div>
			<ul id="links">
				<?if(isset($post->cats[0])){?>
				<li>
					<a href="<?=url::category($post->cats[0]->url,$data->prfxtbl)?>" id="home">return</a>
				</li>
				<?}?>
				<li>
					<a id="ilovethis"><span><?=$post->like?></span></a>
				</li>
				<li>
					<a id="circle"><span title="views count"><?=$post->statViews?></span></a>
				</li>
			</ul>
		</header>
	</div>
	<section id="work-visuals">
		<ul>
		<?if(!empty($data->post->imgs)){
			foreach($data->post->imgs as $img){?>
			<li>
				<a href="<?=url::img('post',$img->pid,$img->url)?>" style="float: left;width: 100%;display: block;" title="<?=$title1?>">
					<img src="<?=url::image($img->url)?>" alt="<?=$img->title?>">
				</a>
				<div class="img-title">
					<div>
						<h3><?=$img->title?></h3>
						<span>&nbsp;|&nbsp;<i><?=$img->text?></i></span>
					</div>
				</div>
			</li>
			<?}
		}?>
		</ul>
	</section>
	<br/>
	<?if(!empty($post->prevnext->next)&&!empty($post->prevnext->prev)){?>
	<section style="clear:both;padding: 3em 6em 0 0;">
		<ul id="other-projects">
			<li>
				<a class="previous-work" href="<?=url::post($post->prevnext->prev->url,$data->prfxtbl)?>" rel="prev"><?=$post->prevnext->prev->title?></a>
			</li>
			<li>
				<a class="next-work" href="<?=url::post($post->prevnext->next->url,$data->prfxtbl)?>" rel="next"><?=$post->prevnext->next->title?></a>
			</li>
		</ul>
	</section>
	<?}?>
</div>

<script src="<?=THEME_HREF?>/template/tpl/files/lib/underscore-min.js"></script>  
<script src="<?=THEME_HREF?>/template/tpl/files/lib/jquery-ext.js"></script>