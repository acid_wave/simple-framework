<?php
$tpl->title="Terms of use.";
$tpl->desc="Terms of use. Description how to you may use this website and its content.";
?>
<header>
<div id="preamble">
	<h2><a name="terms">Terms of service</a></h2>
	<h3>1. Service provided</h3>
	<p>You can add photos, reviews and comments for users on our websites. This allows the users to communicate with other people and discuss publications and other items. </p>
	<h3>2. Conditions of user's registration</h3>
	<p>You must be 14 years of age, or older, to become our user.<br />
Approval of administrator is required before the user can post pictures, comments and other content on the Site. After completing the registration, the user has to send the examples of proposed content via the contact form (http://<?=SITE?>/contacts.html).</p>
	<h3>3. Posting Content</h3>
	<p>You are free to post any photos, comments and reviews of your choice on <?=HREF?><br />
Any stuff that you post on the website is referred to as "User content" and you are the one responsible for the content posted. </p>
	<h3>4. How other users and <?=HREF?> can use content </h3>
	<p>When you register, you grant a non-exclusive, royalty-free, transferable, sublicensable, worldwide license to use, store, display, reproduce, distribute and modify your work on <?=HREF?>. The website also has the right to remove or modify any content that may seem to be inappropriate which also includes the content that violates the terms and conditions.<br />
<?=HREF?> may have to "User Content", for example under other licenses.<br /><br />
The administrator or moderator of the site has the authority to block users that were repeatedly charged with copyright offense or other intellectual property rights of others.
	</p>
	<h3>5. User's liabilities</h3>
	<p><?=HREF?> respects the rights of 3rd party content owners so you have to agree not to post any content that violates any law or infringes the right of any person.</p>
	<h3>6. Copyright Policy</h3>
	<p>Copyright policy implemented in <?=HREF?> is in accordance with the Digital Millennium Copyright Act.<br />
	Please read our Copyright Policy.
	</p>
	<h2><a name="copyright">Copyright Policy</a></h2>
	<h3>1. Services provided</h3>
	<p>In accordance with the Digital Millennium Copyright Act of 1998 (http://www.copyright.gov/legislation/dmca.pdf), <?=HREF?> has the right to react to any reports of copyright infringement connected with <?=HREF?> website (the "Site") and that content is sent to the edotirial department of the website by dint of the contact form (<a href="<?=HREF?>/contacts.html" rel="nowfollw">http://<?=SITE?>/contacts.html</a>).</p>
	<p>Both of the following statements are to be included in the body of the Notice</p>
	<p>"I hereby state that I have a good faith belief that the disputed use of the copyrighted material or reference or link to such material is not authorized by the copyright owner, its agent, or the law (e.g., as a fair use)."</p><br />
	<p>"I hereby state that the information in this Notice is accurate and, under penalty of perjury, that I am the owner, or authorized to act on behalf of the owner, of the copyright or of an exclusive right under the copyright that is allegedly infringed."</p>
	<p>Your full legal name is also needed.</p>
</div>
</header>
