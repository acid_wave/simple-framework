<!DOCTYPE html>
<html class="no-js">
<head>
	<!-- Meta info -->
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?=@$data->title?></title>
	<meta name="description" content="<?=@htmlspecialchars($data->desc)?>" />
	<meta name="author" content="<?=NAME?>">
	<meta name="format-detection" content="">
	<?if(defined('FACEBOOKAPPID')){?><meta property="fb:app_id" content="<?=FACEBOOKAPPID?>" /><?}?>
	<?if(defined('FACEBOOKLANG')){?><meta property="og:locale" content="<?=FACEBOOKLANG?>" /><?}?>
	<?=@$data->meta?>
	<?=@$data->headlink?>
	<!-- Styles -->
	<link href="<?=THEME_HREF?>/template/tpl/files/favicon.ico" rel="shortcut icon"/>
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600' rel='stylesheet' type='text/css'>
	<link href="<?=THEME_HREF?>/template/tpl/files/styles/main.css" rel="stylesheet" media="screen, print" type="text/css">
	<script src="<?=THEME_HREF?>/template/tpl/files/lib/modernizr-2.6.2.js"></script>
	<script type="application/javascript" src="<?=HREF?>/files/js/jquery-2.1.4.min.js"></script>
</head>
<body><?=$data->status?>
	<div id="container" style="left: 0px;">
		<section id="content">
		<div id="pfd">
			<a href="/" style="text-decoration: none;font-size:4em;">
				<?=NAME?>
				<img src="<?=THEME_HREF?>/template/tpl/files/images/dot-logo.png" alt="<?=NAME?>">
			</a>
		</div>
		<?=$data->body?>
		</section>
	<?include $template->inc('template/nav.php');?>
	<?include $template->inc('template/footer.php');?>
	</div>
	<style type="text/css">
		.sp{background-color:white;width:100%;position: relative;z-index: 999;opacity: 0.95;}
		.sp a,.specPanel a:visited{color:black;text-decoration:none;}
		.sp a:hover{color:black;text-decoration:underline;}
		.sp ul{display: inline-block;margin: 0;}
		.sp li{float:left;margin:0 3px 0 0;padding: 0 5px;list-style: none outside none;background: none repeat scroll 0 0 #E5E5E5;}
		.sp div{clear:both;}
	</style>
	<?=$data->panel?>
	<?include $template->inc('template/stat.php');?>
</body>
</html>