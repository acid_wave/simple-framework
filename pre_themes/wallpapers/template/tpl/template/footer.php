<footer id="msf">
	<div class="wrapper">
		<ul id="lets-be-social">
			<li>
				<a href="<?=url::staticPage('terms')?>" rel="nofollow">Terms</a>
			</li>
			<li>
				<a href="<?=url::staticPage('privacy')?>" rel="nofollow">Privacy Policy</a>
			</li>
			<li>
				<a href="<?=HREF?>/rss/" rel="nofollow" taget="_blank">RSS</a>
			</li>
		</ul>
		<div id="legal">
			<span><?=NAME?></span>
		</div>
	</div><!-- wrapper -->
</footer><!-- footer -->

<script src="<?=THEME_HREF?>/template/tpl/files/ajax/jquery-1.11.0.min.js"></script>
<script src="<?=THEME_HREF?>/template/tpl/files/scripts/pyaari-main.1.0.js"></script>
<script src="<?=THEME_HREF?>/template/tpl/files/scripts/pyaari-menu.1.0.js"></script>
<script>
$(document).ready(function () {
	PfdMenu._ctor();
});
</script>

<script>
$(document).ready(function(){
   ReadMore.init();
})
</script>