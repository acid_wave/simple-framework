<nav id="toc">
<ul>
	<?=$data->userMenu?>
	<li></li>
	<?foreach($data->category->cats as $cat){?>
	<li><a href="<?=url::category($cat->url)?>"><?=$cat->title?> <small>(<?=$cat->count?>)</small></a></li>
	<?}?>
</ul>
</nav>

<div id="no-script">
<div>
	<p>
		The website is fully responsive and requires Javascript.<br>
		Please enable javascript to use this site without issue.</p>
</div>
</div><!-- no-script -->