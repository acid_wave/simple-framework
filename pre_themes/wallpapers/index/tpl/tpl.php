<?php
$posts=$data->posts;
$tpl->title='4k Wallpapers, Desktop backgrounds - '.NAME;
$tpl->desc="";
?>
<script type="text/javascript" src="<?=HREF?>/modules/posts/lists/search/tpl/files/js/autocomplite.js"></script>
<header>
	<div id="preamble" class="home">
		<div class="preamble">
		<h1>Tips on how to add beauty to your desktop. In the world of computers and mobile phones, which we use every day, every person wants to see on the screen, what pleases him as a good view outside the window.</h1>
		<p class="em" style="display: none;">
			<b>1. Select the correct resolution.</b><br/>
			It is necessary to choose a picture in which the resolution is not less than that of the monitor.
			The screen resolution is indicated in pixels, for example. 1440 x 900. The numbers show how many pixels your monitor can display horizontally and vertically. The resolution also shows what aspect ratio your monitor has, the screen of your phone or tablet
			<br/><br/>
			<b>2. Find the most beautiful wallpaper.</b><br/>
			The number of sites that offer free wallpapers is endless. Fortunately, you have come to the right place, here you will find many beautiful, different categories and different sizes.
			<br/><br/>
			<b>3. Install to your computer</b><br/>
			<br/>
			<b>Windows 10</b><br/>
			Select the Start symbol, then select "Settings"> "Personalization" to select an image, also you can change the color for the "Start", taskbar and other items.
			<br/>
			<br/>
			<b>Windows 7</b><br/>
			Right-click on the desktop and select> Screen Resolution in the menu and find the number next to> Resolution.
			<br/>
			<br/>
			<b>Windows XP</b><br/>
			Right-click on the desktop, select> Properties from the menu, and go to the tab> Preferences. You will see a slider in the section> Screen Resolution in the lower left corner.
		</p>
		<a class="read-more" href="#">Read more +</a></div>
		<p>&nbsp;</p>
	</div>
</header>
<div id="our-work">
<ul>
<?if(!empty($posts)){
	foreach($posts as $p){
		if(!isset($p->imgs)) continue;?>
	<li><a href="<?=url::post($p->url)?>" title="<?=$p->title?>">
		<div class="img-cover" style="background-image: url('<?=url::imgThumb('600_',$p->imgs[0])?>')">
			<img src="<?=url::imgThumb('600_',$p->imgs[0])?>" alt="<?=$p->title?>">
		</div>
		<div class="overlay">
			<summary>
				<h2><?=$p->imgs[0]->title?></h2>
				<h3><?=$p->imgs[0]->text?></h3>
			</summary>
			<div class="loves"><span><?=$p->like?></span></div>
		</div>
	</a></li>
	<?}
}?>
</ul>
</div>
<div id="what-we-do">
	<section id="web-design">
		<article>
			<div style="margin: auto;width: 70%;">
				<h2>Search wallpapers</h2>
				<form method="get" action="/">
					<input type="hidden" name="module" value="posts/lists/search">
					<input style="display: inline-block !important; width: 70%;" class="search-input" type="text" name="q" id="name" placeholder="Enter your search term here" autocomplete="off"/> 
					<input type="submit" value="Search" class="special" />
				</form>
			</div>
		</article>
	</section>
	<section id="web-design">
		<article style="padding-bottom: 0;padding-top: 0;">
			<h2 style="border-top: 1px solid #e4e4e4;">You will be interested</h2>
			<?foreach($data->seealso as $r){?>
				<a style="font-size: <?=mt_rand(1,3)?>em;line-height: 1.5em;margin: 0 0.5em 0 0;" href="<?=url::post($r->url)?>"><?=$r->title?></a>
			<?}?>
		</article>
	</section>
</div>
