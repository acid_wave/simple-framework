<?php
namespace custom_index;
use module,db,url;

/*
 * Должен возвращать:
 * $this->data - объект переменных для вывода в шаблоне
 * $this->headers - объект для изменения заголовков при отдаче
 * $this->act - если есть несколько вариантов ответов
 */
class handler extends \index\handler{
	function index($page,$num){#Делаем все обработки для вывода данных
		$data=module::exec('posts/lists',array('act'=>'mainList','page'=>$page,'num'=>$num,'prfxtbl'=>'blog'),'data')->data;
		$data->seealso=\posts\randomPosts(8);
		return $data;
	}
}
