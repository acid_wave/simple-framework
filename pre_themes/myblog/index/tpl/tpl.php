<?php
/*
 * Входные двнные находятся в stdClass $data
 * */
$cat=$data->cat;
$posts=$data->posts;
$tpl->title=NAME." main.";
$tpl->desc="Discover our website, interesting stories and beautiful media content.";
?>
<?if($data->accessPublish){?>
	<script type="application/javascript" src="<?=HREF?>/files/posts/admin/js/published.js"></script>
<?}?>
<div class="primary col-md-8 col-sm-12 col-xs-12">
	<section class="latest section">
		<div class="section-inner">
			<h2 class="heading">Latest articles</h2>
			<?if(isset($data->access->editNews)){?><span><a href="<?=url::post_adminAdd($cat->url)?>">new post</a></span><?}?>
			<div class="content"><?include $template->inc('index/listposts.php');?></div>
		</div><!--//section-inner-->                 
	</section><!--//section-->
</div>
<div class="secondary col-md-4 col-sm-12 col-xs-12">
	<?=$data->topLevelCats?>
</div>