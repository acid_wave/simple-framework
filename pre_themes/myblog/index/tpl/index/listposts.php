<?if(!empty($posts)){
	$i=0;
	foreach($posts as $p){
		$i++;?>
		<?if($i==1){?>
		<div class="item featured text-center">
			<?=$p->funcPanel?>
			<h3 class="title"><a href="<?=url::post($p->url,$data->prfxtbl)?>"><?=$p->title?></a></h3>
			<p class="summary"></p>
			<div class="desc text-left">                                    
				<p><?=$p->txt?></p>
			</div>
		</div>
		<hr class="divider" />
		<?}else{?>
		<div class="item row">
			<?=$p->funcPanel?>
			<div class="desc col-md-12 col-sm-12 col-xs-12">
				<h3 class="title"><a href="<?=url::post($p->url,$data->prfxtbl)?>"><?=$p->title?></a></h3>
				<p><?=$p->txt?></p>
				<p><a class="more-link" href="<?=url::post($p->url,$data->prfxtbl)?>"><i class="fa fa-external-link"></i> Find out more</a></p>
			</div><!--//desc-->                          
		</div><!--//item-->
		<?}
	}
}?>