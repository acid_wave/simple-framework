<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head><?=timer(2);?>
    <title><?=@$data->title?></title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?=@htmlspecialchars($data->desc)?>">
    <meta name="author" content="<?=NAME?>">
    <?=@$data->meta?>
	<?=@$data->headlink?> 
    <link rel="shortcut icon" href="<?=THEME_HREF?>/template/tpl/files/favicon.ico">  
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,300italic,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'> 
    <!-- Global CSS -->
    <link rel="stylesheet" href="<?=THEME_HREF?>/template/tpl/files/assets/plugins/bootstrap/css/bootstrap.min.css">   
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="<?=THEME_HREF?>/template/tpl/files/assets/plugins/font-awesome/css/font-awesome.css">
    <!-- github acitivity css -->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/octicons/2.0.2/octicons.min.css">
    <link rel="stylesheet" href="http://caseyscarborough.github.io/github-activity/github-activity-0.1.0.min.css">
    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="<?=THEME_HREF?>/template/tpl/files/assets/css/styles.css">
    <link rel="stylesheet" type="text/css" href="<?=$data->css->theme?>"/>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="application/javascript" src="<?=HREF?>/files/js/jquery-2.1.4.min.js"></script>
</head> 

<body>
<?include $template->inc('template/header.php');?>
<div class="container sections-wrapper">
	<div class="row"><?=$data->body?></div>
</div>
<?include $template->inc('template/footer.php');?>
<?include $template->inc('template/stat.php');?>
<?=$data->panel?>
<!-- Javascript -->          
<script type="text/javascript" src="<?=THEME_HREF?>/template/tpl/files/assets/plugins/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?=THEME_HREF?>/template/tpl/files/assets/plugins/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="<?=THEME_HREF?>/template/tpl/files/assets/plugins/bootstrap/js/bootstrap.min.js"></script>    
<script type="text/javascript" src="<?=THEME_HREF?>/template/tpl/files/assets/plugins/jquery-rss/dist/jquery.rss.min.js"></script> 
<!-- github activity plugin -->
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/mustache.js/0.7.2/mustache.min.js"></script>
<script type="text/javascript" src="http://caseyscarborough.github.io/github-activity/github-activity-0.1.0.min.js"></script>
<!-- custom js -->
<script type="text/javascript" src="<?=THEME_HREF?>/template/tpl/files/assets/js/main.js"></script>
</body>
</html>
