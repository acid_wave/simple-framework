<footer class="footer">
	<div class="container text-center">
		<a href="<?=HREF?>/rss/">RSS</a> | <a href="<?=url::staticPage('terms')?>" rel="nofollow">Terms</a> | <a href="<?=url::staticPage('privacy')?>" rel="nofollow">Privacy Policy</a>
	</div><!--//container-->
</footer><!--//footer-->