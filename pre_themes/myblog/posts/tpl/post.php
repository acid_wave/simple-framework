<?php
$post=$data->post;
$tpl->title="{$post->title} - ".NAME;
$tpl->desc=strip_tags($post->shortTxt);
?>
<?if($data->access->publish){?>
	<script type="application/javascript" src="<?=HREF?>/files/posts/admin/js/published.js"></script>
<?}?>

<div class="primary col-md-8 col-sm-12 col-xs-12">
	<section class="experience section">
	<div class="section-inner">
		<?=$post->funcPanel?>
		<h2 class="heading"><?=$post->title?></h2>
		<div class="content">
			<div class="item">
				<div><?=$post->txt?></div>
				<div class="title" style="font-size: 11px">
					<?if(!empty($post->user->mail)){?>
					<a href="<?=url::author($post->user->id).(!empty($data->prfxtbl)?'?prfxtbl=blog':'')?>"><i><?=$post->user->name?></i></a>
					<?}else{?>
					<span class="place"><i><?=$post->user->name?></i></span>
					<?}?> - 
					<span class="year"><?=$post->datePublish?$post->datePublish:$post->date?></span>
				</div>
				<?include $template->inc('post/breadcrumbs.php');?>
			</div>
			<?if(!empty($post->prevnext->next)){?>
			<a class="btn btn-cta-secondary" href="<?=url::post($post->prevnext->next->url,$data->prfxtbl)?>"><?=$post->prevnext->next->title?> <i class="fa fa-chevron-right"></i></a>
			<?}?>
			<?=$data->comments?>		
		</div> 
	</div>                
</section>
</div>
<div class="secondary col-md-4 col-sm-12 col-xs-12">
	<?=$data->topLevelCats?>
	<?if(!empty($post->sources)){?>
	<aside class="info aside section">
		<div class="section-inner">
			<h2 class="heading">Credits</h2>
			<div class="content">
				<ul class="list-unstyled">
				<?foreach(explode("\n", $post->sources) as $s){
					$purl=parse_url($s);?>
					<li><i class="fa fa-link"></i><span class="sr-only">Source:</span><a href="<?=$s?>"><?=$purl['host']?></a></li>
				<?}?>
				</ul>
			</div>
		</div>           
	</aside>
	<?}?>
</div>
