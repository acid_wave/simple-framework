<div class="comments-list">
<?if(!empty($data->comments)){?>
	<div>
		<?foreach($data->comments as $c){?>
		<div class="comments-text">
			<small><?=$c->authorName?>&nbsp;|&nbsp;<?=$c->date?>&nbsp;</small>
			<?if($c->self){?><a class="comment-del" onclick="commentDel(this)" data-id="<?=$c->id?>"></a><?}?>
			<?if($c->approve){?>
				<p><?=$c->text?></p>
			<?}?>
		</div>
		<?}?>
	</div>
<?}?>
<?if(!empty($data->otherComments)){?>
	<hr/><p>Also discuss</p>
	<div class="comments-other">
		<?foreach($data->otherComments as $c){?>
		<div class="comments-text">
			<span><?=$c->title?></span><br/>
			<small><?=$c->authorName?>&nbsp;|&nbsp;<?=$c->date?>&nbsp;</small>
			<?if($c->approve){?>
				<p><?=$c->text?></p>
			<?}else{?>
				<i><a rel="nofollow" href="<?=url::commentsPage($c->url)."#{$c->id}"?>">...on verification, click to read...</a></i>
			<?}?>
		</div>
		<?}?>
	</div>
<?}?>
</div>