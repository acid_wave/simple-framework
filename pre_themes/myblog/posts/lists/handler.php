<?php
namespace custom_posts_lists;
use module,db,url,cache;

class handler extends \posts_lists\handler{
	function mainList($category,$prfxtbl,$uid,$page,$num,$uri,$popularPostsCount,$excludeCat,$disableLimit=false){
		return module::execData('posts/lists',
			[
				'act'=>'mainList',
				'cat'=>$category,
				'prfxtbl'=>$prfxtbl,
				'uid'=>$uid,
				'page'=>$page,
				'num'=>$num,
				'uri'=>$uri,
				'popularPostsCount'=>$popularPostsCount,
				'excludeCat'=>$excludeCat,
				'disableLimit'=>true,
			]
		,true);
	}
}
