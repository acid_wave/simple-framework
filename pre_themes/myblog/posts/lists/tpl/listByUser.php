<?php
/*
 * Входные двнные находятся в stdClass $data
 * */
$posts=$data->posts;
$forDesc=$data->forDesc;
$tpl->title="Posts by {$data->userData->authorName}".($data->page>1?" - Page {$data->page}":'');
$tpl->desc="{$tpl->title}.".(empty($forDesc)?'':' "'.implode('","',$forDesc).'"');
?>
<?if($data->accessPublish){?>
	<script type="application/javascript" src="<?=HREF?>/files/posts/admin/js/published.js"></script>
<?}?>
<div class="primary col-md-8 col-sm-12 col-xs-12">
	<section class="latest section">
		<div class="section-inner">
			<?if(isset($data->access->editNews) && !empty($cat->url)){?><span><a href="<?=url::post_adminAdd($cat->url,$data->prfxtbl)?>">new post</a></span><?}?>
			<h2 class="heading"><?=$data->userData->authorName?> articles</h2>
			<div class="content">
				<?include $template->inc('mainList/listposts.php');?>
				<?=@$data->paginator?>
			</div>
		</div><!--//section-inner-->                 
	</section><!--//section-->
</div>
<div class="secondary col-md-4 col-sm-12 col-xs-12">
	<?=$data->topLevelCats?>
</div>