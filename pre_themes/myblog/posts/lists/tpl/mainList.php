<?php
/*
 * Входные данные находятся в stdClass $data
 * */
$cat=$data->cat;
$posts=$data->posts;
$tpl->title=$cat->title.(@$data->page>1?" - Page {$data->page}":'');
$tpl->desc=$cat->title?"Records for category \"{$cat->title}\".":'';
?>
<?if($data->accessPublish){?>
	<script type="application/javascript" src="<?=HREF?>/files/posts/admin/js/published.js"></script>
<?}?>
<div class="primary col-md-8 col-sm-12 col-xs-12">
	<section class="latest section">
		<?include $template->inc('mainList/breadCrumbs.php');?>
		<div class="section-inner">
			<?if(isset($data->access->editNews) && !empty($cat->url)){?><span><a href="<?=url::post_adminAdd($cat->url,$data->prfxtbl)?>">new post</a></span><?}?>
			<h2 class="heading">Latest articles</h2>
			<?if(!empty($data->pins)){
				include $template->inc('mainList/pinposts.php');
			}?>
			<div class="content">
				<?include $template->inc('mainList/listposts.php');?>
				<?=@$data->paginator?>
			</div>
		</div><!--//section-inner-->                 
	</section><!--//section-->
</div>
<div class="secondary col-md-4 col-sm-12 col-xs-12">
	<?=$data->topLevelCats?>
</div>
