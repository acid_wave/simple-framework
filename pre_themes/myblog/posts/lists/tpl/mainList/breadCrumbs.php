<?if(empty($data->breadCrumbs)) return;?>
<section class="about section">
	<div class="section-inner">
		<a href="<?=HREF?>">HOME</a>
		<?foreach($data->breadCrumbs as $val){
			if($val->url==$cat->url){?>
				<h2 class="heading"><?=$val->title?></h2><?
				continue;
			}?>
			<a href="<?=url::category($val->url,$data->prfxtbl)?>"><?=$val->title?></a>
		<?}?>
	</div>     
</section>