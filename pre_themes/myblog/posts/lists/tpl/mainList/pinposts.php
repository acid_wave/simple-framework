<?foreach($data->pins as $p){?>
	<div class="item featured text-center">
		<?=$p->funcPanel?>
		<h3 class="title"><?=$p->title?></h3>
		<p class="summary"></p>
		<?if(!empty($p->imgs[0])){?>
		<div class="featured-image">
			<a href="<?=url::post($p->url,$data->prfxtbl)?>" title="<?=$p->title?>">
				<img class="img-responsive project-image" src="<?=url::imgThumb('700_',$p->imgs[0])?>" alt="<?=$p->title?>" />
			</a>
		</div>
		<?}?>
		<div class="desc text-left">                                    
			<p><?=$p->txt?></p>
		</div>
	</div>
	<hr class="divider" />
<?}?>