<?if(!empty($posts)){
	foreach($posts as $p){?>
	<div class="item row">
		<?=$p->funcPanel?>
		<div class="desc col-md-12 col-sm-12 col-xs-12">
			<h3 class="title"><a href="<?=url::post($p->url,$data->prfxtbl)?>"><?=$p->title?></a></h3>
			<p><?=$p->txt?></p>
			<p><a class="more-link" href="<?=url::post($p->url,$data->prfxtbl)?>"><i class="fa fa-external-link"></i> Find out more</a></p>
		</div><!--//desc-->                          
	</div><!--//item-->
	<?}
}?>
