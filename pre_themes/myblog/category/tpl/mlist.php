<?if($data->samelevel&&$data->samelevel->cats){?>
<aside class="credits aside section">
	<div class="section-inner">
		<h2 class="heading"><?=$data->samelevel->baseCat->title?></h2>
		<?if($data->edit){?><a href="<?=url::edit(0,'',$data->prfxtbl)?>">add</a><?}?>
		<div class="content">
			<ul class="list-unstyled">
			<?foreach($data->samelevel->cats as $val){?>
				<li>
					<?if($data->samelevel->current==$val->url){?>
						<?=$val->title?>&nbsp;<small title="count of posts">(<?=$val->count?>)</small>
					<?}else{?>
						<a href="<?=url::category($val,$data->prfxtbl)?>"><i class="fa fa-external-link"></i> <?=$val->title?></a>&nbsp;<small title="count of posts">(<?=$val->count?>)</small>
					<?}?>
					<?if(!empty($val->funcPanel)){?><?=$val->funcPanel?><?}?>
				</li>
			<?}?>			
			</ul>		
		</div><!--//content-->
	</div><!--//section-inner-->
</aside><!--//section-->
<?}?>
<?if(!empty($data->cats)){?>
<aside class="credits aside section">
	<div class="section-inner">
		<h2 class="heading">Categories</h2>
		<?if($data->edit){?><a href="<?=url::edit(0,'',$data->prfxtbl)?>">add</a><?}?>
		<div class="content">
			<ul class="list-unstyled">
			<?foreach($data->cats as $val){?>
				<li><a href="<?=url::category($val,$data->prfxtbl)?>"><i class="fa fa-external-link"></i> <?=$val->title?></a>
					<?if(!empty($val->funcPanel)){?><?=$val->funcPanel?><?}?>
				</li>
			<?}?>				
			</ul>		
		</div><!--//content-->
	</div><!--//section-inner-->
</aside><!--//section-->
<?}?>
