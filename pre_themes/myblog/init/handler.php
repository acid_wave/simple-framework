<?php
namespace custom_init;
use module,db,url;
#Используем сторонние модули
require_once(module::$path.'/posts/admin/handler.php');

class handler{
	function __construct(){
		$this->template='template';
		$this->headers=new \stdClass;
		$this->uhandler=module::exec('user',array('easy'=>1),1)->handler;
		$this->user=$this->uhandler->user;
	}
	function index(){
		if(!$this->uhandler->rbac('loadStruct')) die('forbidden');
		$tbl=\posts\tables::init('blog');
		module::exec('posts/admin',array('act'=>'install'),1);
		module::exec('category/admin',array('act'=>'install'),1);
		module::execData('category/admin',array('act'=>'structSave','name'=>'About Me','view'=>'on','disableLimit'=>1));

		$this->headers->location=HREF;
	}
}
