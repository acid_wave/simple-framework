<?php
/*
	Класс управления маршрутами
	примеры создания маршрутов (urls.php):
		route::set('[pregexp](.*?)',array('param1'));
		route::set('[pregexp](.*?)',array('module'=>'posts','param1'));
		route::set('[pregexp](.*?)(.*?)',array('module'=>'posts','act'=>'post','param1','param2'));
*/
class route{
	public static $routeList;
	public static $included=[];
	/*
		Метод установки маршрута
			(string)mask - regular expression
			(array)params - аналог GET параметров
			(int)priority - приоритет проверки, как последовательность выполнения в .htaccess
				0 - самый низкий приоритет
				1 - по умолчанию
				если = 0 - url может попасть под другое правило, если оно существует
				если > 0 - зависит от величины
			Примечание: если существует маршрут для модуля custom, то он не переопределяется основным модулем
	*/
	static function set($mask,$params,$priority=1){
		if(empty($params['module'])) $params['module']='index';
		else $params['module']=str_replace('_', '/', $params['module']);
		if(isset(self::$routeList[$priority][$mask])){
			if(
				substr($params['module'],0,7)!='custom/'&&
				substr(self::$routeList[$priority][$mask]['module'],0,7)=='custom/'
			) return;
		}
		self::$routeList[$priority][$mask]=$params;
	}
	/*
		Загружает установки для роутинга из всх модулей
			подключает файлы urls.php
	*/
	static function load($theme){
		global $urls;		
		# подключение файлов из базовых модулей
		self::urlsInc(PATH.'modules');
		# подключение файлов из модулей основной темы
		if(!empty($theme->defaultThm->name)&&$theme->defaultThm->path)
			self::urlsInc(preg_replace('!(.+)/$!', '$1', $theme->defaultThm->path),$theme->defaultThm->path);
		# подключение файлов из модулей текущей темы (текущей темы поста)
		if(!empty($theme->setThm->name)&&$theme->setThm->name!=$theme->defaultThm->name)
			self::urlsInc(preg_replace('!(.+)/$!', '$1', $theme->setThm->path),$theme->setThm->path);

		foreach (self::$included as $file => $v) {
			include_once $file;
		}

		krsort(self::$routeList);
	}
	/*
		Рекурсивно подключает файлы urls.php в каталогах модулей
	*/
	static function urlsInc($basedir,$themepath=false){		
		$dh=opendir($basedir);
		while ($name=readdir($dh)) {
			if(substr($name,0,1)=='.'||in_array($name, array('files','tmp','tpl'))) continue;
			if(is_dir($path="$basedir/$name")){
				self::urlsInc($path,$themepath);
			}elseif($name=='urls.php'){
				if($themepath){
					$extpath=str_replace($themepath, module::$path.'/', $path);
					if(isset(self::$included[$extpath])){
						unset(self::$included[$extpath]);
					}
				}
				self::$included[$path]=1;
			}
		}
		closedir($dh);
	}
	/*
		Парсит URI строку и получает маршрут
		return 
			(string) obj->module
			(array) obj->params
	*/
	static function get($uri){
		$uri=urldecode($uri);
		$uri=substr($uri,strlen(preg_replace("@^https?://[^/]+@",'',HREF)));
		foreach (self::$routeList as $prior => $routes) {
			foreach ($routes as $mask => $params) {
				if(preg_match("@{$mask}@ui",$uri,$m)){
					array_shift($m);
					$res=new StdClass;
					$res->module=$params['module'];
					$res->params=array();
					unset($params['module']);
					foreach ($params as $key => $v) {
						if(is_numeric($key)){
							if(isset($m[$key])) $res->params[$v]=$m[$key];
							unset($params[$key]);
						}else
							$res->params[$key]=$v;
					}
					break(2);
				}
			}
		}
		return $res;
	}
}

/*
	обрабатывает URL`ы заданные в файлах urls.php
*/
class url{
	static function __callStatic($meth,$args){#Преобразуем урл
		global $urls;
		#Для случаев когда нужна обработка данных в функции урла
		if(function_exists("url_$meth")){
			return call_user_func_array("url_$meth",$args);
		}
		$url=$urls->$meth;
		if(is_array($url)) return url::convert($url[1],$url[0],$args);
		else{
			#Обрабатываем и возвращаем урл
			if(preg_match_all("!\\$[a-z0-9]+!i",$url,$r)){
				return url::convert($url,implode(",",$r[0]),$args);
			}
			return $url;
		}
	}
	static function convert($url,$replace,$args){#Заменяем в строке переменные на нужные параметры
		return str_replace(explode(",",$replace),$args,$url);
	}
}
?>
