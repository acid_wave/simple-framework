<?
class db{
	var $db_id= false;
	var $query_num=0;
	var $query_id=NULL;
	var $error=FALSE; 
	var $error_num=NULL;
	var $last_query=NULL;
	var $errors=0;
	var $save=0; #Управляет сохранением все запросы и скорость их выполнения
	var $queryList=''; # Дапнные по всем запросам
	function __construct($host='',$user='',$pas='',$bd='') {
		global $db;
		$db=$this;
		$db->db_id=mysqli_connect($host,$user,$pas,$bd) or die('db connect error');
		$db->query("SET NAMES utf8");
		register_shutdown_function(array($db,'close'));
		if($this->save)register_shutdown_function(array($db,'show'));
	}
	/*
	 * show:
	 * 	true/1  - показывать ошибки
	 *  false/0 - ничего не показывать
	 *  2		- показывать запрос и ошибки
	 */ 
	static function query($query,$show=false){
		global $db;
		#print "$query<br>";
		if($db->save or in_array($show,array(1,2))){
			$start=microtime(2);
		}
		$db->last_query=$query;
		if(!($db->query_id=mysqli_query($db->db_id,$query))){
			$db->errors=1;
			$db->error=mysqli_error($db->db_id);
			#if($er=mysql_error()){echo "\n<br />$query :\n<br />$er\n<br />".debug_print_backtrace()."\n<br />";die;}
			$db->error_num=mysqli_errno($db->db_id);
			if($show){
				$db->display_error($db->error, $db->error_num, $query);
				$num=10000;
				print "\n<h1>query time: ".(round((microtime(2)-$start)*$num)/$num)."s</h1>\n";
			}
		}else{
			$db->error_num=$db->errors=0;
			$db->error='';
		}
		if($show===2){
			self::display_query($query);
			$num=10000;
			print "\n<h1>query time: ".(round((microtime(2)-$start)*$num)/$num)."s</h1>\n";
		}
		if($db->save){
			$num=10000;
			$db->queryList[]=array($query,round((microtime(2)-$start)*$num)/$num);
		}
		$db->query_num ++;
		return $db->query_id;
	}
	static function &qall($query,$show=-1){
		global $db;
		if($show==-1)$db->query($query);
		else $db->query($query,$show);
		$ar=array();
		while($d=$db->fetch()){
			$ar[]=$d;
		}
		return $ar;
	}
	static function &qrowall($query,$show=-1){
		global $db;
		if($show==-1)$db->query($query);
		else $db->query($query,$show);
		$ar=array();
		while($d=$db->fetchRow()){
			$ar[]=$d;
		}
		return $ar;
	}
	static function qrow($query,$show=-1){
		global $db;
		if($show==-1)$db->query($query);
		else $db->query($query,$show);
		return @mysqli_fetch_row($db->query_id);
	}
	static function qfetch($query,$show=-1){
		global $db;
		if($show==-1)$db->query($query);
		else $db->query($query,$show);
		return @mysqli_fetch_object($db->query_id);
	}
	static function fetch($query_id = ''){
		global $db;
		if ($query_id == '') $query_id = $db->query_id;
		return @mysqli_fetch_object($query_id);
	}
	static function fetchRow($query_id = ''){
		global $db;
		if ($query_id == '') $query_id = $db->query_id;
		list($row)=@mysqli_fetch_row($query_id);
		return $row;
	}
	static function num_rows($query_id = ''){
		global $db;
		if ($query_id == '') $query_id = $db->query_id;
		return @mysqli_num_rows($query_id);
	}
	static function affected(){
		global $db;
		return @mysqli_affected_rows($db->db_id);
	}
	static function insert(){
		global $db;
		return @mysqli_insert_id($db->db_id);
	}
	static function getTbls(){#Получаем список всех таблиц из БД
		$tbls=array();
		db::query("SHOW TABLES");
		while($r=db::fetch_row()){
			$tbls[$r[0]]=1;
		}
		return $tbls;
	}
	static function fields($query_id = ''){
		global $db;
		if ($query_id == '') $query_id = $db->query_id;
		while ($field = @mysqli_fetch_field($query_id)){
			$fields[] = $field;
		}
		return $fields;
	}
	function makeSQL($ar){
		foreach($ar as $k=>$v){
			$str[]="`$k`='$v'";
		}
		return implode(",",$str);
	}
	static function ping(){
		global $db;
		return mysqli_ping($db->db_id);
	}
	static function escape($source){
		global $db;
		if ($db->db_id) return mysqli_real_escape_string ($db->db_id,$source);
		else return mysqli_escape_string($source);
	}
	static function close(){
		global $db;
		@mysqli_close($db->db_id);
	}
	static function show(){
		global $db;
		print "<table>";
		foreach($db->queryList as $q){
			@$sum+=$q[1];
			?>
			<tr>
				<td><?$db->display_query($q[0]);?></td>
				<td>used time:<?=$q[1]?></td>
			</tr>
		<?}
		print "</table><h1>total used time:$sum</h1>";
		print timer(3);
		print "<pre>";print_r($db);
		exit;
	}
	static function error(){
		global $db;
		return $db->error;
	}
	function display_error($error, $error_num, $query = ''){
		echo '<html>
		<head><title>MySQL Fatal Error</title>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
		<style type="text/css">
		body {font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 10px;font-style: normal;color: #000000;}
		</style>
		</head>
		<body>
			<font size="4">MySQL Error!</font><br />------------------------<br /><br />
			<u>The Error returned was:</u><br /><strong>'.$error.'</strong>
			<br /><br /></strong><u>Error Number:</u><br /><strong>'.$error_num.'</strong><br /><br />
			'.self::display_query($query).'
		</body></html>';
		#exit();
	}
	static function display_query($query){?>
		<textarea name="" rows="10" cols="52" wrap="virtual"><?=$query?></textarea><br />
	<?}
	static function save(){
		global $db;
		$db->save=1;
	}
	/**
	 * Генерирует условие WHERE и ORDER для запроса на основе массива
	 * filter[name]=desc
	 * filter[name][uki]='' - `name` = 'uki'
	 * filter[name][10]='>' - `name` > 10
	 * filter[name][uki]='>=' - `name` >= `uki` (сравнение полей между собой)
	 * filter[name1*name2][10]='>=' - `name1`*`name2` >= 10 (сравнение полей между собой+арифметические операции)
	 * @param array $filter 
	 * @param bool $orderOff - отключает ORDER BY в возвращаемой строке
	 * @return string
	 */
	static function filter($filter=[]){
		$sqlPart=(object)['where'=>'','order'=>''];
		foreach ($filter as $key => $d) {
			$key=db::escape($key);
			if(is_array($d)){
				$sort=false;
				$val=db::escape(key($d));
				if(in_array(($opcomp=current($d)),['<','>','>=','<=','=='])){
					if($opcomp=='==') $opcomp='=';
				}else {
					$opcomp=null;
					$val="'$val'";
				}

			}else{
				$sort=strtoupper($d);
				$val=false;
			}
			if($sort!==false&&!in_array($sort,['ASC','DESC'])) $sort='ASC';
			# заковычивание для filter[name1*name2][10]
			$key_quoted='`'.preg_replace('!(\w+)([\*\+\/\-\%])(\w+)!','$1`$2`$3',$key).'`';
			if($val!==false) $sqlFilter[]=$key_quoted.(empty($opcomp)?'=':$opcomp).$val;
			if($sort!==false) $sqlOrder[]="$key_quoted $sort";
		}
		if(!empty($sqlFilter))
			$sqlPart->where='&& '.implode(' && ',$sqlFilter);
		if(!empty($sqlOrder))
			$sqlPart->order="ORDER BY ".implode(', ',$sqlOrder);
		return $sqlPart;
	}
}
