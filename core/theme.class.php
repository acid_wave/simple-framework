<?php
/*
	в шаблонах доступны константы:
	THEME_CURRENT - имя текущей темы (например заданной в посте, иначе совпадает с THEME_DEFAULT)
	THEME_PATH - путь к корню текущей темы (Например: [DOCUMENT_ROOT]/themes/themename/)
	THEME_HREF - host и часть пути текущей темы (Например: http://example.com/themes/themename)

	THEME_DEFAULT - имя темы по умолчанию (заданной в конфиге)
	THEME_DEFAULT_PATH - путь к корню темы по умолчанию
	THEME_DEFAULT_HREF - host и часть пути темы по умолчанию
*/
#темы для поста, default значение константы
if(!defined('THEME_POST_ALLOW'))
	define('THEME_POST_ALLOW', 1);

class theme{
	public $setThm;
	public $defaultThm;

	function __construct($name=false){
		$this->setThm=new StdClass;
		$this->defaultThm=new StdClass;

		if(!defined('THEME_SET')){
			list($mname)=db::qrow("SELECT `value` FROM `".PREFIX_SPEC."config` WHERE `key`='current_theme' LIMIT 1");
			$mname=!empty($mname)?$mname:'';
		}else
			$mname=THEME_SET;
			
		if($name){//Если установлена тема поста или категории
			$this->setThm->name=$name;
			$this->setThm->path=PATH.'themes/'.$name.'/';
			$this->setThm->href=HREF.'/themes/'.$name;
			$this->defaultThm->name=$mname;
			$this->defaultThm->path=PATH.'themes/'.$mname.'/';
			$this->defaultThm->href=HREF.'/themes/'.$mname;
		}else{ 
			if($mname!=''){
				//Если установлена только основная тема
				$this->setThm->path=PATH.'themes/'.$mname.'/';
				$this->setThm->href=HREF.'/themes/'.$mname;
			}else{
				//Если основная тема не установлена
				$this->setThm->path=PATH;
				$this->setThm->href=HREF.'/modules';
			}
			$this->setThm->name=$mname;
			$this->defaultThm=$this->setThm;	
		}
	}
	/*
		механизм отслеживания:
			- нужен для определения темы только для одной страницы
			- должен быть определен модуль который следует отслеживать
	*/
	function tracking($module,$params){
		if(!THEME_POST_ALLOW) return false;
		if($module=='posts'&&!empty($params['url'])){
			$tbl=empty($params['prfxtbl'])?'post':db::escape($params['prfxtbl'])."_post";
			list($theme)=db::qrow("SELECT `theme` FROM `$tbl` WHERE `url`='".db::escape(urldecode($params['url']))."' LIMIT 1");
		}elseif($module=='posts/lists'&&!empty($params['cat'])){
			list($theme)=db::qrow("SELECT `theme` FROM `category` WHERE `url`='".db::escape(urldecode($params['cat']))."' LIMIT 1");
		}
		return empty($theme)?false:$theme;
	}
	/*
		устанавливает константы тем для испольхования в шаблонах
	*/
	function setconsts(){
		define('THEME_CURRENT', $this->setThm->name);
		define('THEME_PATH', $this->setThm->path);
		define('THEME_HREF', $this->setThm->href);
		define('THEME_DEFAULT', $this->defaultThm->name);
		define('THEME_DEFAULT_PATH', $this->defaultThm->path);
		define('THEME_DEFAULT_HREF', $this->defaultThm->href);
	}
	/*
		генерирует ссылку на css для в ставки в HTML шаблон
			+admin см. modules/plugins/css/handler.php
	*/
	function css(){
		$link=new StdClass;
		if($this->defaultThm->name=='' && $this->setThm->name==$this->defaultThm->name){#Если админская тема
			$link->admin=HREF.'/modules/template/tpl/files/style.css';
		}elseif($this->setThm->name==$this->defaultThm->name or $this->defaultThm->name==''){#Если нет темы поста или осноная тема админская
			$link->admin=HREF.'/modules/template/tpl/files/style.css?minus[]='.$this->setThm->name;
			$link->theme=$this->setThm->href.'/template/tpl/files/style.css?theme='.$this->setThm->name;
		}else{#Если есть тема поста
			$link->admin=HREF.'/modules/template/tpl/files/style.css?minus[]='.$this->setThm->name.'&minus[]='.$this->defaultThm->name;
			$link->mtheme=$this->defaultThm->href.'/template/tpl/files/style.css?theme='.$this->defaultThm->name.'&minus[]='.$this->setThm->name;
			$link->theme=$this->setThm->href.'/template/tpl/files/style.css?theme='.$this->setThm->name;
		}
		return $link;
	}
}

/*
	управляет подключением файлов и подшаблонов между темами
*/
class subTemplate{
	function __construct($moduleName,$denycustom=0){
		$this->moduleName=$moduleName;
		$this->denycustom=$denycustom;
	}
	/*
		для подключения файлов подшаблонов
			- либо из шаблона текущей темы, если существует
			- либо из главного шаблона
	*/
	function inc($path){
		if($path[0]=='/') $path=substr($path, 1);
		$path=$this->moduleName.'/tpl/'.$path;
		$mfile=PATH.'modules/'.$path;
		if($this->denycustom||THEME_CURRENT=='') return $mfile;
		
		if(file_exists($file=THEME_PATH.$path)){
			return $file;
		}elseif(file_exists($file=THEME_DEFAULT_PATH.$path)){
			return $file;
		}else{
			$file=$mfile;
		}
		return $file;
	}
	/*
		вычисление пути файлов между темами
	*/
	function path($file=false){
		if($file===false) return;
		return file_exists($tp=THEME_PATH.$file)?$tp:THEME_DEFAULT_PATH.$file;
	}
	/*
		вычисление http ссылки на файлы между темами
	*/
	function href($file=false){
		if($file===false) return;
		return file_exists($tp=THEME_HREF.$file)?$tp:THEME_DEFAULT_HREF.$file;
	}
	/*
		проверяет существует ли custom`ный файл для модуля
	*/
	static function customExist($moduleName,$file){
		if(file_exists($path=THEME_PATH."$moduleName/$file"))
			return $path;
		elseif(file_exists($path=THEME_DEFAULT_PATH."$moduleName/$file"))
			return $path;
		else
			return false;
	}
}
?>
