<?
/*
 * Подключение и выполнение модуля
 * $input - объект переданный извне для управления и вывода данных в модуле
 * $easy =1 - не выводить основной шаблон и не обрабатывать глобальные переменныи
 */
module::$path=PATH."modules";

class module{
	public static $path;
	private static $entry;
	private static $admin;
	/*
		params:
			(string)$moduleName - имя модуля
			(object)$input - входные переменные
			(mixed)easy - Параметры представления данных
				$easy = 0 вывод основного HTML шаблона 
				$easy = 'data' вывод массива данных, без HTML
			(int)$denycustom - запрет на подключение custom модуля
	*/
	static function init($moduleName){
		#default moduleName
		if($moduleName=='') $moduleName='index';
		else $moduleName=str_replace('_', '/', $moduleName);
		#set module path if not set
		if(empty(self::$path)) self::$path=PATH."modules";
		if(!is_dir(self::$path."/$moduleName")&&!subTemplate::customExist($moduleName,'')){
			trigger_error("System error: module::init [\"{$moduleName}\"] is not exists",E_USER_NOTICE);
			return false;
		}
		#set entry point
		if(empty(self::$entry)){
			self::$entry=$moduleName;
			self::$admin=self::isAdminModule($moduleName);
		}
		return $moduleName;
	}
	static function exec($moduleName='',$input=array(),$easy=0,$denycustom=0){
		if(!$moduleName=self::init($moduleName)) return false;
		
		#Обработка входных переменных
		if(!is_object($input)) $input=(object)$input;
		@$input->easy=$easy;
		if(self::$admin)
			$input->denycustom=1;
		else
			$input->denycustom=empty($denycustom)?0:$denycustom;
		$control=self::control($moduleName,$input,$easy);
		#Преобразование данных для вывода
		$input=(object)$input;
		list($handler,$data)=self::handler($moduleName,$control,$input);
		#Вывод из шаблона модуля
		if($easy!=='data'){
			if($easy===0){
				$data->ads=module::exec('ads',array('page'=>$moduleName),'data')->data;
			}
			$tpl=self::view($moduleName,$handler->view,$data,$input->denycustom);
			#Вывод общего шаблона
			if(!empty($handler->template) && $easy===0){
				$str=self::exec($handler->template,$tpl,1,$input->denycustom)->str;
			}else{
				$str=@$tpl->body; $tpl->body='';
			}
			return (object)array(
				'handler'=>$handler,
				'str'=>$str,
				'tpl'=>$tpl,
			);
		}else{
			return (object)array(
				'handler'=>$handler,
				'data'=>$data,
			);
		}
	}
	/*
		псевдоним для exec([modulename],[input],'data')->data
	*/
	static function execData($moduleName='',$input=array(),$denycustom=0){
		$obj=self::exec($moduleName,$input,'data',$denycustom);
		return isset($obj->data)?$obj->data:new stdClass;
	}
	/*
		псевдоним для exec([modulename],[input],1)->str
	*/
	static function execStr($moduleName='',$input=array(),$denycustom=0){
		$obj=self::exec($moduleName,$input,1,$denycustom);
		return isset($obj->str)?$obj->str:new stdClass;
	}
	#Обработка входных переменных
	static function control($moduleName,$input,$easy){
		if(empty($input))$input=(object)array();
		if(!is_object($input))$input=(object)$input;
		
		$moduleNames=self::_buildModuleName($moduleName);
		if(!class_exists($moduleNames['control_class'])){
			if(($path=subTemplate::customExist($moduleName,'input.php'))&&!$input->denycustom){
				include_once($path);
			}elseif(file_exists($path=self::$path."/$moduleName/input.php")){
				include_once($path);
			}
			$moduleNames=self::_buildModuleName($moduleName);
		}
		
		if(class_exists($moduleNames['control_class'])){
			if(empty($easy)){
				$get=add2obj($input,$_GET);
				$get=add2obj($get,$_POST);
				$get=add2obj($get,@$_SESSION);
				$get=add2obj($get,$_COOKIE);
			}else $get=$input;
			$control=new $moduleNames['control_class']($get);
		}else{
			trigger_error("System error: input [class \"{$moduleNames['control_class']}\"] is not exists, check input.php",E_USER_NOTICE);
			$control=(object)array();
		}
		if($control->act===false) $control->act='';
		elseif(empty($control->act) && !empty($input->act)) $control->act=$input->act;
		return $control;
	}
	/*
		Преобразование данных для вывода
		Если существует custom`ный модуль то используется его namespace
	*/
	static function handler($moduleName,$control,$input){
		if(file_exists($path=self::$path."/$moduleName/handler.php")){
			include_once($path);
			$moduleNames=self::_buildModuleName($moduleName);
		}
		if(($path=subTemplate::customExist($moduleName,'handler.php'))&&!$input->denycustom){
			include_once($path);
			$moduleNames=self::_buildModuleName('custom_'.$moduleName);
		}
		if(!empty($moduleNames)){
			if(!class_exists($moduleNames['handler'])){
				trigger_error("System error: Handler [{$moduleNames['handler']}] is not exists ",E_USER_NOTICE);
			}
			$handler=new $moduleNames['handler'];
			#определеям метод контроллера который надо вызвать
			$act=(empty($control->act))?$moduleNames['autoexec_method']:$control->act;
			if(empty($control->data))$control->data=(object)array();
			$data=$control->data;
			$handlerData=[];
			if(method_exists($handler,$act)){
				#enable cache
				if(class_exists('cache')&&property_exists($moduleNames['handler'], 'cache')&&!$input->denycustom){
					if(in_array($act,array_keys($handler->cache))){
						#create cache ID 
						$cacheid=false;
						if(isset($handler->cache[$act][0])){
							$id=$handler->cache[$act][0];
							if(is_array($id)){
								foreach ($id as $valId) {
									if(isset($data->{$valId}))
										$tid[]=$data->{$valId};
								}
								$cacheid=implode('/', $tid);
							}else
								$cacheid=isset($data->{$id})?$data->{$id}:0;
						}
						$exp=empty($handler->cache[$act][1])?'10 min':$handler->cache[$act][1];
						$cachedData=cache::getHandler(array($handler,$act),(array)$data,$cacheid,$exp);
						#если закеширован сам объект handler, то достаем и его
						if($cachedData->handler)
							$handler=$cachedData->handler;
					}
				}
				$handlerData=isset($cachedData->result)?$cachedData->result:call_user_func_array(array($handler,$act),(array)$data);
				$handlerData=add2obj($handlerData,$input);
			}elseif($act!='install'){
				#выводит ошибку, если метода в контроллере не существует (исключение составляет отсутствие метода install)
				trigger_error("System error: method [{$act}] for [\\{$moduleNames['handler']}] is not exists. [PS. Возможно нужно наследовать родительский Handler]",E_USER_NOTICE);
			}
			
			if(!empty($control) && @$handler->headers)$control->__stop($handler->headers);
		}else $data=(object)array();

		#задаем имя view (шаблона) для вывода, может быть задано непосредственно в методе класса handler
		if(!isset($handler->view)) $handler->view=$act;

		return array($handler,$handlerData);
	}
	#Делаем вывод данных модуля
	static function view($moduleName,$act,$data,$denycustom){
		$template=new subTemplate($moduleName,$denycustom);
		$tpl=new stdClass;
		$filename="tpl/$act.php";
		#Подключаем tpl-$act.php
		if(!file_exists($path=$template->inc("$act.php")))
			if(!file_exists($path=$template->inc("tpl.php")))#Подключаем tpl.php
				$path=false;
		
		if($path!==false){
			ob_start();
			include($path);
			$tpl->body=ob_get_contents();
			ob_end_clean();	
		}
		
		return $tpl;
	}
	/*
		определяет яввляется ли модуль административным
	*/
	static function isAdminModule($moduleName){
		return (preg_replace(array('!^admin/?.*$!','!^.+/admin$!','!^.+/admin/.*$!'), '', $moduleName)=='')?true:false;
	}
	/*
		определяет название классов handler и input по имени модуля
	*/
	static function _buildModuleName($moduleName){
		#post\blog
		#post_blog
		#post/blog
		#post
		$namespace=str_replace(['_','/'],'\\',$moduleName);
		$names=[];
		$names['autoexec_method']=str_replace(['/','\\'],'_',$moduleName);
		#define handler class name
		$names['handler']=$namespace.'\handler';
		if(!class_exists($names['handler']))
			$names['handler']=$names['autoexec_method'].'\handler';
		#define control class name
		$names['control_class']=$namespace.'\input';
		if(!class_exists($names['control_class']))
			$names['control_class']=$names['autoexec_method'];
		$names['base_module_dir']=self::$path.'/'.str_replace('\\','/',$namespace);
		return $names;
	}
}
#Общий клас для обработки входных параметров и передачи их в заголовки
class control{
	public $act='';
	function __stop($headers){#Для установки заголовков. Все переменные для обработки лежат в $this->headers
		if(!empty($headers->cookie))foreach($headers->cookie as $k=>$v){
			@setcookie($k,(string)$v[0], (int)strtotime((string)$v[1]),"/",COOKIE_HOST);
			#print "$k - ".((string)$v[0])." -- ".(string)$v[1]." --- ".COOKIE_HOST."<br>\n";
		}
		if(!empty($headers->location)) location($headers->location);
	}
}
