<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head><?=timer(2);?>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="description" content="<?=@htmlspecialchars(ucfirst($data->desc))?>" />
<title><?=ucfirst(@$data->title)?></title>

<?if(defined('FACEBOOKAPPID')){?><meta property="fb:app_id" content="<?=FACEBOOKAPPID?>" /><?}?>
<?if(defined('FACEBOOKLANG')){?><meta property="og:locale" content="<?=FACEBOOKLANG?>" /><?}?>
<?=@$data->meta?>
<?=@$data->headlink?>
<link href="<?=HREF?>/favicon.ico" rel="shortcut icon"/>
<link href="<?=HREF?>/modules/template/tpl/files/bootstrap/4.0/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?=HREF?>/template/tpl/files/style.css"/>
<link rel="stylesheet" href="<?=HREF?>/modules/template/tpl/files/font-awesome/4.7.0/css/font-awesome.min.css">
<?
/*
//Админская тема с вычетом Основной и темы поста
	<link rel="stylesheet" type="text/css" href="<?=$data->css->admin?>"/>
//Основная тема с вычетом темы поста
	<?if(!empty($data->css->mtheme)){?>
		<link rel="stylesheet" type="text/css" href="<?=$data->css->mtheme?>"/>
	<?}?>
//Тема поста
	<?if(!empty($data->css->theme)){?>
		<link rel="stylesheet" type="text/css" href="<?=$data->css->theme?>"/>
	<?}?>
*/
?>
<script type="application/javascript" src="<?=HREF?>/files/js/jquery-3.2.1.min.js"></script>
<script src="<?=HREF?>/files/js/popper.min.js" type="text/javascript"></script>
<script src="<?=HREF?>/files/bootstrap/4.0/js/bootstrap.min.js" type="text/javascript"></script>
<script type="application/javascript" src="<?=HREF?>/modules/posts/lists/search/tpl/files/js/autocomplite.js"></script>
<?include $template->inc('template/basejs.php');?>
</head>
<body><?=$data->status?>
<?include $template->inc('template/header.php');?>
<div class="body-container mt-3">
	<?=$data->body?>
</div>	
<?include $template->inc('template/footer.php');?>
<?=$data->panel?>
<?include $template->inc('template/stat.php');?>
</body>
</html>
