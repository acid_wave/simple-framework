<nav class="navbar navbar-expand-lg navbar-dark bg-dark justify-content-between">
	<a class="navbar-brand" style="width:210px;" href="<?=HREF?>"><?=NAME?></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarNav">
		<form class="form-inline w-75">
			<input type="hidden" name="module" value="posts/lists/search">
			<input class="form-control w-75 mr-2" type="search" placeholder="Search" aria-label="Search" name="q" id="name"  autocomplete="off">
			<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
		</form>
		<a class="btn btn-info ml-auto mr-1" href="<?=$data->user->id ? url::images_upload() : url::userLogin()?>">Contribute / Upload</a>
		<?=$data->userMenu?>
	</div>
</nav>