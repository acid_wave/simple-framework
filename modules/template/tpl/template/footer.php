<footer class="footer">
	<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<table><tr>
				<td class="logo">
					<?=NAME?>&nbsp;<?=date('Y')?>
				</td>
				<td align="right">
					<a href="<?=url::listTop()?>">Popular</a>
					<a href="<?=url::staticPage('privacy')?>" rel="nofollow">Privacy Policy</a>
					<a href="<?=url::staticPage('terms')?>" rel="nofollow">Terms</a>
					<a href="<?=url::contacts()?>" rel="nofollow">Contact us</a>
					<a href="<?=url::post_rss()?>">RSS</a>
					<a href="<?=url::post_blog_list('blog')?>">Blog</a>
					<a href="<?=url::categoryBrowse()?>">Browse</a>
				</td>
				<td class="soc-mini" align="right">
					<a rel="nofollow" href="#" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u='+document.location);return false;" class="soc1"></a>
					<a rel="nofollow" href="https://twitter.com/share" data-lang="en" target="_blank" class="soc2"></a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
					<a rel="nofollow" href="#" onclick="javascript:window.open('https://plus.google.com/share?url='+document.location,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" class="soc3"></a>
				</td>
			</tr></table>
		</div>
	</div>
	</div>
</footer>
