<?php
namespace plugins\cron;
use module,db,url;

include_once __DIR__.'/func.php';

class handler{
	function __construct(){
		$this->template='template';
		$this->headers=new \stdClass;
		$this->uhandler=module::exec('user',[],true)->handler;
		$this->user=$this->uhandler->user;
	}
	function index(){
		if($this->user->rbac!=1){
			$this->headers->location=HREF; return;
		}
		return [
			'crontab'=>implode("\n",cron::crontablist()),
		];
	}
	function set($time,$cmd){
		if($this->user->rbac!=1){
			$this->headers->location=HREF; return;
		}
		cron::set($time,$cmd);
		$this->headers->location=url::plugins_cron();
		return;
	}
}
