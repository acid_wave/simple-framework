<?php
namespace plugins\cron;
use module,db,url;

class cron{
	/*
		устанавливает cron
	*/
	static function set($time='',$cmd=''){
		$shout=self::remove($cmd);
		$shout[]="{$time} {$cmd}";
		self::_write($shout);
		
		return self::status($cmd);
	}
	static function remove($cmd){
		exec('crontab -l',$shout);
		foreach ($shout as $k=>$str) {
			#стираем задачу, если существует для этого же исполняемого файла
			if(strstr($str, $cmd)){
				unset($shout[$k]);
			}
		}
		self::_write($shout);
		return $shout;
	}
	static function _write($str){
		$shin=implode("\n", $str);
		#create task file
		if(!file_put_contents($taskfile=TMP.'crontask.txt', $shin.PHP_EOL)){
			echo 'error cron setup'; return false;
		}
		exec("crontab {$taskfile}",$mes);
		return $mes;
	}
	/*
		проверяет наличие задачи в cron
	*/
	static function status($cmd){
		exec('crontab -l',$shout);
		if(empty($shout)){
			$status=false;
		}else{
			foreach ($shout as $k=>$str) {
				if(strstr($str, $cmd)){
					$status=true; break;
				}
			}
			if(empty($status)) $status=false;
		}
		return $status;
	}
	/*
		получает все задачи в crontab
	*/
	static function crontablist(){
		exec('crontab -l',$shout);
		foreach ($shout as $key => $v) {
			if(preg_match('!^#!',$v)) unset($shout[$key]);
		}
		return $shout;
	}
}

?>