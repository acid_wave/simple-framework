<div class="row">
	<div class="col-md-12">
		<h1>Установить любой cron</h1>
		<form class="form-inline" method="post" action="<?=url::plugins_cron()?>&act=set">
			<input type="text" name="time" placeholder="* * * * *" class="form-control">
			<input type="text" name="cmd" placeholder="command" class="form-control">
			<input type="submit" name="" value="set" class="btn">
		</form>
		<div class="row" style="margin-top: 2em">
			<div class="col-md-6">
				<h3>Установленные crontab</h3>
				<textarea class="form-control" rows="7" readonly=""><?=$data->crontab?></textarea>
			</div>
		</div>
	</div>
</div>