<?if(empty($data->pages))return;?>
<div class="row justify-content-center my-3">
	<nav aria-label="Page navigation">
		<ul class="pagination">
		<?if($data->page!=1){?>
			<li class="page-item"><a class="page-link" href="<?=$data->nav->first?>" title="First">First</a></li>
			<li class="page-item"><a class="page-link" href="<?=$data->nav->prev?>" rel="prev" title="Previous">Previous</a></li>
		<?}?>
		<?foreach ($data->pages as $i => $p) {
			if($data->page!=$i){?><li class="page-item"><a class="page-link" href="<?=$p?>"><?=$i?></a></li><?}
			else {?><li class="page-item active"><span class="page-link" title="<?=$i?>"><?=$i?><span class="sr-only">(current)</span></span></li><?}
		}?>
		<?if($data->page<$data->allPage){?>
			<li class="page-item"><a class="page-link" href="<?=$data->nav->next?>" rel="next" title="Next">Next</a></li>
			<li class="page-item"><a class="page-link" href="<?=$data->nav->end?>" title="End">End</a></li>
		<?}?>
		</ul>
		<?if($data->showPagen){?>
		<div class="countOnPage text-center">
			Results on page:
			<select name="pagen" onchange="location.href='<?=$data->firstPage.(strpos($data->firstPage,"?")?"&":'?')?>num='+this.value;">
			<?foreach(array(1,5,10,20,50,100,300,500,1000,5000) as $v){?>
				<option value="<?=$v?>"<?=($data->num==$v?' selected="selected"':'')?>><?=$v?></option>
			<?}?>
			</select>
		</div>
		<?}?>
	</nav>
</div>