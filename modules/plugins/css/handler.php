<?php
/*
	Использование файла стилей
		//example.com/style.css - админская тема
		//example.com/style.css?theme=[theme_name] - подключает css для темы [theme_name]
		//example.com/style.css?theme=[theme_name1]&minus[]=[theme_name2] - подключает css для темы [theme_name1] без файлов из темы [theme_name2]
		//example.com/style.css?debug=1 - показывает файл с метками о подключенных файлах и без компрессии
*/
namespace plugins_css;
use module,db,url,cache;

include_once module::$path.'/admin/themes/handler.php';

class handler{
	function __construct(){
		$this->template='';
		$this->headers=new \stdClass;
	}
	function index($theme,$minus,$debug){
		header("Content-Type: text/css");
		
		$css=new css($theme,$minus,$debug);
		return [
			'str'=>$css->style,
		];
	}
}

/* 
	сборщик стилей
	- подключает рядом лежащие css файлы в tpl директориях (например: для example.php подключит example.css)
*/
class css{
	public $included=array();
	public $debug;
	public $mode;
	
	function __construct($theme,$minus=array(),$debug=false){
		//Получаем список файлов для основной темы
		$path=$this->getPathByName($theme);
		$files=$this->getFiles($path);	
		//Собираем массивы css для каждой темы
			foreach($minus as $t){
				$ar=$this->getFiles($this->getPathByName($t));
				#убираем найденые файлы из основной темы
				foreach($ar as $f=>$k){
					unset($files[$f]);
				}
			}
		$this->style=$this->inc($files,$path,$debug);

	}
	/*
	 * Возврашает путь к теме по названию
	*/ 
	function getPathByName($theme){
		return ($theme=='')?PATH."modules":PATH."themes/$theme";
	}
	/*
	 * Создает массив всех файлов с именем .css
		 path - путь в котором искать css файлы
		 checkFiles - стоит ли проверять файлы в выбранной директории
		 files - массив файлов css
	*/ 
	function getFiles($path,$basePath='',&$files=''){
		if(empty($basePath)){
			$basePath=$path;
			$files=array();
		}
		if(strstr($path, 'modules/example')||strstr($path, 'modules/images/files')) return $files;
		$dh=opendir($path);
		while ($f=readdir($dh)) {
			if(substr($f,0,1)=='.') continue;
			$file="$path/$f";
			if(is_dir($file)){
				if($f=='files') continue;
				$this->getFiles($file,$basePath,$files);
			}elseif(substr($f,-4)=='.css'){
				$files[str_replace($basePath,'',$file)]=1;
			}
		}
		closedir($dh);
		return $files;
	}
	/*
		находит tpl директории
	*/
	function themesInc($basedir,$themeinc=false,$findcss=false){
		if($themeinc!==false&&THEME_CURRENT=='') return;
		$dh=opendir($basedir);
		while ($dirName=readdir($dh)) {
			if(substr($dirName,0,1)=='.'||in_array($dirName, array('example','images'))) continue;
			if(is_dir($path="$basedir/$dirName")){
				if($dirName=='tpl') $findcss=true;
				elseif($dirName=='files') continue;
				$this->themesInc($path,$themeinc,$findcss);
			}elseif($findcss)
				$this->collect($path,$themeinc);
		}
		closedir($dh);
	}
	/*
		подключает css файлы
		- либо из темы, либо основной
	*/
	function collect($path,$themeinc){
		$css=\admin_themes\cssExists($path);
		if($css===false) return;
		elseif($css->exists){
			#если находится в файлах темы
			if($themeinc){
				$extpath=str_replace($themeinc, module::$path.'/', $css->path);
				if(isset($this->included[$extpath])){
					unset($this->included[$extpath]);
				}
			}
			$this->included[$css->path]=1;
		}
	}
	/*
		убирает лишние пробелы, переносы строк, комментарии
	*/
	function compress(&$str){
		return preg_replace(
			array('!/\*.*?\*/!si','!\r?\n!','!\t!','!\s{2,}!','!\s*([\}\{\:\;])\s*!'), 
			array('','',' ',' ','$1'),
			$str
		);
	}
	/*
		подключает содержимое css файлов
	*/
	function inc($files,$path,$debug){
		if(empty($files))return;
		ob_start();
			foreach ($files as $f=>$v) {
				$p="$path$f";
				if($debug) echo "\n\n/****************** ".str_replace(PATH,'',$p)." *************************/\n\n";
				include_once $p;
			}
		$str=ob_get_clean();
		return $debug?$str:$this->compress($str);
	}
}
