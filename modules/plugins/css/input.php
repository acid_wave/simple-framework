<?php
class plugins_css extends control{
	function __construct($input=''){
		$this->data=new stdClass;
		if(empty($input->act)) $input->act='index';
		
		if($input->act=='index'){
			$this->data=(object)array(
				'theme'=>isset($input->theme)?$input->theme:false,
				'minus'=>isset($input->minus)?$input->minus:array(),
				'debug'=>isset($input->debug)?$input->debug:false,
			);
		}else
			$this->act=false;
	}
}
