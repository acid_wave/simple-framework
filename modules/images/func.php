<?php
namespace images;
use module,db,url,stdclass;

function downloadHeaders($filename){
	// required for IE, otherwise Content-disposition is ignored
	if(ini_get('zlib.output_compression'))ini_set('zlib.output_compression', 'Off');
	$file_extension=strtolower(substr(strrchr($filename,"."),1));
	header("Pragma: public"); // required
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: private",false); // required for certain browsers 
	// change, added quotes to allow spaces in filenames, by Rajkumar Singh
	header("Content-Disposition: attachment; filename=\"".basename($filename)."\";" );
	header("Content-Transfer-Encoding: binary");
}
/* 
	генерирует preview изображение
	используется imagick
*/
function makeBackground($img,$dir,$text,$otherimgs){
	$image=new \Imagick(realpath($dir.$img));
	$image->stripImage();
	$w=$image->getImageWidth();
	$h=$image->getImageHeight();
	if($w<300||$h<300) return false;
	#вычисляем размер текста
	$text=mb_substr($text, 0, 40);
	$fontSize=(int)($w*0.025);
	$imageTextTop=textWithGradient($text,$fontSize,'right');
	$imageTextBottom=textWithGradient(SITE,$fontSize*1.3,'left');

	#накладываем на основное изображение
	$image->compositeImage($imageTextTop,\Imagick::COMPOSITE_DEFAULT,0,0);
	$image->compositeImage($imageTextBottom,\Imagick::COMPOSITE_DEFAULT,$w-$imageTextBottom->getImageWidth(),$h-$imageTextBottom->getImageHeight());
	#делаем превью других изображений
	if(!empty($otherimgs))
		$imgs=explode(',', $otherimgs);
	if(isset($imgs[0])){
		$rotate[]=17;
		$rotate[]=-10;
		$rotate[]=16;
		$countBox=new \Imagick();
		$countBox->newImage($bw=$w*0.09, $bh=$bw*0.6, 'white');
		$countBox->setImageFormat('png');
		$countBox->borderImage('grey',1,1);
		$draw=new \ImagickDraw();
		$draw->setFillColor('red');
		$draw->setFont("Bookman-DemiItalic");
		$draw->setFontSize($bfs=(int)($w*0.018));
		$draw->setGravity(\Imagick::GRAVITY_NORTH);
		$draw->setStrokeColor('black');
		$draw->setStrokeOpacity(0.5);
		$countBox->annotateImage($draw, 0, $bh-$bh*0.8, 0, count($imgs));
		$draw->setFillColor('#A9A9A9');
		$draw->setFontSize($bfs*0.8);
		$draw->setGravity(\Imagick::GRAVITY_SOUTH);
		$countBox->annotateImage($draw, 0, $bh-$bh*0.8, 0, 'pictures');
		$oiw=$w*0.13;#ширина превью картинки
		$oih=$oiw*0.75;#высота превью картинки
		$ph=$h-$h*0.98;#положение блока превью по оси Y
		$image->compositeImage($countBox,\Imagick::COMPOSITE_DEFAULT,$w-$bw*1.77,$ph+$oih*1.16);
		foreach ($imgs as $i=>$im) {
			if(!$imgpath=realpath($dir.$im)) continue;
			$timg=new \Imagick($imgpath);
			$timg->resizeImage($oiw=$w*0.13,0,\Imagick::FILTER_GAUSSIAN,1);
			$timg->cropThumbnailImage($oiw,$oih);
			$timg->borderImage('white',5,5);
			$timg->borderImage('grey',1,1);
			$timg->setImageFormat('png');
			$timg->rotateImage('none',$rotate[$i]);
			$otherIm[$i]=$timg;
			if($i>=2) break;
		}
		if(isset($otherIm[0]))
			$image->compositeImage($otherIm[0],\Imagick::COMPOSITE_DEFAULT,$w-$oiw*2.25,$ph+$oih*0.7);
		if(isset($otherIm[1]))
			$image->compositeImage($otherIm[1],\Imagick::COMPOSITE_DEFAULT,$w-$oiw*2.1,$ph);
		if(isset($otherIm[2]))
			$image->compositeImage($otherIm[2],\Imagick::COMPOSITE_DEFAULT,$w-$oiw*1.3,$ph+$oih*0.08);
	}
	header('Content-type: image/'.$image->getImageFormat());
	header('Cache-Control: no-store');
	echo $image;
}
function textWithGradient($text,$fontSize,$gradientPos='right'){
	$imageTextBackg=new \Imagick();
	$draw=new \ImagickDraw();
	$draw->setFillColor('white');
	$draw->setFont("Bookman-DemiItalic");
	$draw->setFontSize($fontSize);
	$draw->setGravity(\Imagick::GRAVITY_WEST);
	$gradientW=60;
	$textMetrics=$imageTextBackg->queryFontMetrics($draw,$text);
	#вычисляем размер фона для текста
	$textBackgrndW=$textMetrics['textWidth']+$gradientW*1.2;
	$textBackgrndH=$fontSize*1.8;
	#создаем прозрачный холст
	$imageTextBackg->newImage($textBackgrndW, $textBackgrndH, "none");
	$imageTextBackg->setImageFormat('png');
	#создаем фон для текста
	$background=new \Imagick();
	$background->newImage($textBackgrndW-$gradientW, $textBackgrndH, "black");
	$background->setImageFormat('png');
	#пишем текст
	$background->annotateImage($draw, 10, 0, 0, $text);
	if($gradientPos=='right')
		$x=0;
	elseif($gradientPos=='left'){
		$x=$gradientW;
	}
	$imageTextBackg->compositeImage($background,\Imagick::COMPOSITE_DEFAULT,$x,0);
	#создаем полоску с градиентом
	$gradient=new \Imagick();
	if($gradientPos=='right')
		$g="gradient:black-none";
	elseif($gradientPos=='left'){
		$g="gradient:none-black";
	}
	$gradient->newPseudoImage($textBackgrndH, $gradientW, $g);
	$gradient->setImageFormat('png');
	$gradient->rotateImage(new \ImagickPixel(),270);
	#накладываем все на прозрачный холст
	if($gradientPos=='right')
		$x=$textBackgrndW-$gradientW;
	elseif($gradientPos=='left'){
		$x=0;
	}
	$imageTextBackg->compositeImage($gradient,\Imagick::COMPOSITE_DEFAULT,$x,0);
	return $imageTextBackg;
}
/**
 * Cоздает и сохраняет файл изображения в виде JPG файла с заданными размерами
 * @param string $src - полный путь к исходному файлу
 * @param string|bool $dest - файл назначения (true - автоматически генерирует путь к файлу назначения | string - явно задан полный путь)
 * @param int $xx - требуемая ширина изображения
 * @param int $yy - требуемая высота изображения
 * @return blob
 */
function resize($src,$dest,$xx,$yy){
	error_reporting(0);
	set_time_limit(20);
	if($dest===true){
		if(!is_dir($thPath=thumbnail::$srcpath.((!$xx)?'':$xx).'_'.((!$yy)?'':$yy)))
			@mkdir($thPath);
		$dest=$thPath.DIRECTORY_SEPARATOR.thumbnail::filename($src);
	}
	$isrc = $src;
	if (mime_content_type($src)=='image/webp') $src = imagecreatefromwebp ($isrc);
	else $src = imagecreatefromstring(file_get_contents($isrc));
	if (!$src) {
		$im = new \Imagick($isrc);
		$im->setImageFormat("png");
		$src = imagecreatefromstring($im->getImageBlob());
		$im->clear();
		$im->destroy();
	}
	$x = imagesx($src);
	$y = imagesy($src);
	if($x){
		if($src){
			$dst_x=0;
			$dst_y=0;
			$autoSide=0;# (bool) - crop enable (On/Off)
			if(!$xx) {$xx=floor($x/($y/$yy)); $autoSide=1;}
			if(!$yy) {$yy=floor($y/($x/$xx)); $autoSide=1;}
			$dst=imagecreatetruecolor($xx,$yy);
			$white = imagecolorallocate($dst,  255, 255, 255);
			imagefilledrectangle($dst, 0, 0, $xx, $yy, $white);
			// limit source area to original image area
			$thumbnailCropX = 0;
			$thumbnailCropY = 0;
			$thumbnailCropW = $x;
			$thumbnailCropH = $y;
			if(!$autoSide){
				$scaling_X = $x  / $xx;
				$scaling_Y = $y / $yy;
				if ($scaling_X > $scaling_Y) {
					// some of the width will need to be cropped
					$allowable_width = $x / $scaling_X * $scaling_Y;
					$thumbnailCropW = round($allowable_width);
					$thumbnailCropX = round(($x - $allowable_width) / 2);
				} elseif ($scaling_Y > $scaling_X) {
					// some of the height will need to be cropped
					$allowable_height = $y / $scaling_Y * $scaling_X;
					$thumbnailCropH = round($allowable_height);
					$thumbnailCropY = round(($y - $allowable_height) / 2);

				} else {
					// image fits perfectly, no cropping needed
				}
			}
			$destination_offset_x = 0;
			$destination_offset_y = 0;
			$thumbnail_image_width = $xx;
			$thumbnail_image_height = $yy;
			
			# копирует прямоугольную часть одного изображения на другое изображение
			
			imagecopyresampled(
				$dst,
				$src,
				$destination_offset_x,
				$destination_offset_y,
				$thumbnailCropX,
				$thumbnailCropY,
				$thumbnail_image_width,
				$thumbnail_image_height,
				$thumbnailCropW,
				$thumbnailCropH
			);
			
			imagedestroy($src);
			@ob_end_clean();
			ob_start();
			imagejpeg($dst);
			if($dest!==false){
				$str=ob_get_contents();
				@file_put_contents($dest,$str);
				jpegoptimRun($dest);
				ob_end_clean();
			}else ob_end_flush();
			imagedestroy($dst);
		}
	}
	return $str;
}
/**
 * Cоздает и сохраняет файл изображения в виде JPG файла с исходными размерами
 * @param string $src - полный путь к исходному файлу
 * @param string|bool $dest - файл назначения (true - автоматически генерирует путь к файлу назначения | string - явно задан полный путь или имя файла без полного пути)
 * @param bool $asfile - вернуть путь к файлу или blob (содержимое изображения)
 * @return bool|string|blob
 */
function mkorigin($src,$dest=true,$asfile=true){
	error_reporting(0);
	set_time_limit(20);
	@mkdir($originDir=thumbnail::$srcpath."Origin/",0777);
	if($dest===true) $dest=$originDir.thumbnail::filename($src);
	elseif($dest!==false&&!strstr($dest,DIRECTORY_SEPARATOR)) $dest=$originDir.$dest; #если передано имя файла без полного пути
	if(file_exists($dest))
		return $asfile?$dest:file_get_contents($dest);
	$isrc = $src;
	if (mime_content_type($src)=='image/webp') $src = imagecreatefromwebp ($isrc);
	else $src = imagecreatefromstring(file_get_contents($isrc));
	if (!$src) {
		$im = new \Imagick($isrc);
		$im->setImageFormat("png");
		$src = imagecreatefromstring($im->getImageBlob());
		$im->clear();
		$im->destroy();
	}
	$x = imagesx($src);
	$y = imagesy($src);
	if($x){
		$dst=imagecreatetruecolor($x,$y);
		$white = imagecolorallocate($dst,  255, 255, 255);
		imagefilledrectangle($dst, 0, 0, $x, $y, $white);
		imagecopy($dst,$src,0,0,0,0,$x,$y);
		#получаем картинку из буфера
		@ob_end_clean();
		ob_start();
		imagejpeg($dst);
		$str=ob_get_contents();
		#сохраняем в файл
		file_put_contents($dest,$str);
		ob_end_clean();
		#очищаем занятую память
		imagedestroy($dst);
		imagedestroy($src);
		#запускаем jpegoptim
		jpegoptimRun($dest);
		return $asfile?$dest:$str;
	}
	return false;
}
/*
	выполняет jpegoptim для jpeg`ов
*/
function jpegoptimRun($file){
	static $jpegoptim;
	# проверяем установлен ли jpegoptim
	if($jpegoptim!==false){
		preg_match('!jpegoptim\:\s([^\s]+)\s!',shell_exec('whereis jpegoptim'),$m);
		$jpegoptim=!isset($m[1])?false:trim($m[1]);
	}
	# оптимизируем jpg если установлен jpegoptim
	if(file_exists($file)&&preg_match('!^.+\.jpe?g$!i', $file)&&is_string($jpegoptim))
		shell_exec("{$jpegoptim} --strip-all $file");
}
/*
	методы для работы с thumbnails
*/
class thumbnail{
	static $srcpath=PATH.'modules/images/files/images';
	/**
	 * генерирует имя thumbnail картинки (всегда в формате jpeg)
	 * @param string $filename - полный путь к файлу или только название файла
	 * @return string - название файла, без полного пути
	 */
	static function filename($filename){
		$a=explode(DIRECTORY_SEPARATOR,$filename);
		$basename=end($a);
		return preg_replace('!^(.+?)\.([^\.]+)$!', '$1.jpg', $basename);
	}
	/**
	 * возвращает имя исходного файла картинки
	 * @param string $thumbfile filename.jpg OR PATH.'modules/images/files/images/filename.jpg'
	 * @return string PATH.'modules/images/files/images/filename.jpg'
	 */
	static function srcExists($thumbfile){
		if(!strstr($thumbfile, self::$srcpath))
			$thumbfile=self::$srcpath.'/'.$thumbfile;
		$a=explode('/',$thumbfile);
		if(!strstr(end($a),'.')) $thumbfile.='.';
		$srcPattern=preg_replace('!^(.+?)\.[^\.]*$!', '$1.', $thumbfile);
		foreach(['jpg','jpeg','png','ico','gif','bmp','svg','webp'] as $format){
			if(file_exists($file=$srcPattern.$format)) return $file;
		}
		return false;
	}
	/*
		удаляет все thumbnail для картинки
	*/
	static function remove($name){
		$thumbDirsArr=scandir($imageDirs=self::$srcpath.'/../');
		foreach($thumbDirsArr as $tdir){
			if(!is_dir($imageDirPath=$imageDirs.$tdir)) continue;
			if(!preg_match('!^images(\d|\_|Origin)!',$tdir)) continue;
			$filename=$imageDirPath.'/'.self::filename($name);
			@unlink($filename);
		}
	}
}
?>
