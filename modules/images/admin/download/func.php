<?php
namespace images_admin_download;
use module,db,url;

/*
	получает все ID кейвордов всех постов
	в формате KID_PID
*/
function getAllkidpid(){
	$kidpid=[];
	db::query("SELECT CONCAT(kid,'_',pid) AS kp FROM ".PREFIX_SPEC."keyword2post WHERE tbl='post'");
	while($d=db::fetch()){
		$kidpid[]=$d->kp;
	}
	return $kidpid;
}
/*
	заполняет массивы ID постов и ID кейвордов
*/
function fillKidsPids(&$kp,&$kids,&$pids){
	if(empty($kp)) return;
	if(!is_array($kp)) $kp=explode(',', $kp);

	foreach ($kp as $val) {
		list($kid,$pid)=explode('_', $val);
		$kids[(int)$kid]=1;
		$pids[(int)$pid]=1;
	}
}
/*
	строит параметры "search tools" для парсинга выдачи google images
*/
function googleSearchToolsBuild($imsize){
	$gglparam='';
	$isw=!empty($imsize['ex']['w']);
	$ish=!empty($imsize['ex']['h']);
	if($isw||$ish){
		$gglparam='tbs=isz:ex,';
		if($isw&&!$ish) $gglparam.="iszw:{$imsize['ex']['w']},iszh:{$imsize['ex']['w']}";
		if(!$isw&&$ish) $gglparam.="iszw:{$imsize['ex']['h']},iszh:{$imsize['ex']['h']}";
		if($isw&&$ish) $gglparam.="iszw:{$imsize['ex']['w']},iszh:{$imsize['ex']['h']}";
	}elseif(!empty($imsize['lth'])){
		$gglparam="tbs=isz:lt,islt:{$imsize['lth']}";
	}else{
		$gglparam=($imsize['natsize']=='a')?'':"tbs=isz:{$imsize['natsize']}";
	}
	return $gglparam;
}
/*
	определяет запущен ли процесс по пути к исполняемому файлу
*/
function processRunning($pname){
	$ps=shell_exec("ps aux|grep ".escapeshellarg($pname)."|grep -v ' grep '");
	if(empty($ps))
		return 0;
	else{
		$running=explode("\n", trim($ps));
		return (count($running));
	}
}
/*
	формирует данные для закачки и записывает входной файл для парсера
*/
function prepareData($input,$kids,$pids,$mainkey,$all,$is_new,$count,$word,$seachtools,$skipExists,$uid,$allowgallery,$manimsize){
	$fh=fopen($daemonInputTMP=$input.'.tmp', 'a');
	if($mainkey){
		db::query(
			"SELECT k.id AS kid,k.title,k2p.pid,COUNT(imgs.kid) AS total FROM 
				(SELECT * FROM `keyword` WHERE id IN (".implode(',',array_keys($kids)).")) k 
			JOIN `".PREFIX_SPEC."keyword2post` k2p 
				ON k.id=k2p.kid && k2p.tbl='post'
			INNER JOIN `post` 
				ON post.id=k2p.pid && post.kid=k2p.kid
			LEFT OUTER JOIN ".PREFIX_SPEC."imgs imgs 
				ON imgs.kid=k2p.kid AND imgs.pid=k2p.pid
			WHERE k2p.pid IN (".implode(',',array_keys($pids)).") 
			GROUP BY k.id"
		);
	}elseif($all){
		db::query(
			"SELECT k.id AS kid,k.title,k2p.pid,COUNT(imgs.kid) AS total FROM keyword k
			JOIN `".PREFIX_SPEC."keyword2post` k2p 
				ON k.id=k2p.kid && k2p.tbl='post'
			LEFT OUTER JOIN ".PREFIX_SPEC."imgs imgs 
				ON imgs.kid=k2p.kid AND imgs.pid=k2p.pid
			GROUP BY k.id,k2p.pid
		");
	}else{
		db::query(
			"SELECT k.id AS kid,k.title,k2p.pid,COUNT(imgs.kid) AS total FROM 
				(SELECT * FROM `keyword` WHERE id IN (".implode(',',array_keys($kids)).")) k 
			JOIN `".PREFIX_SPEC."keyword2post` k2p 
				ON k.id=k2p.kid && k2p.tbl='post'
			LEFT OUTER JOIN ".PREFIX_SPEC."imgs imgs 
				ON imgs.kid=k2p.kid AND imgs.pid=k2p.pid
			WHERE k2p.pid IN (".implode(',',array_keys($pids)).") 
			GROUP BY k.id,k2p.pid
		");
	}

	while ($d=db::fetch()) {
		if($is_new){ // докачать ещё count
			$d->need=$count;
		}else{ // докачать до count
			if($count<=$d->total){ // пропускаем, те, у которых достаточно
				continue;
			}
			$d->need=$count-$d->total;
		}
		$d->must=$d->need+$d->total;
		$d->querytitle=!empty($word)?"{$d->title} {$word}":$d->title;
		$d->searchtools=$seachtools;
		$d->uid=$uid;
		$d->skipExists=$skipExists;
		$d->allowgallery=$allowgallery;
		$d->manimsize=$manimsize;
		fwrite($fh, serialize($d)."\n");
	}
	fclose($fh);
	return $daemonInputTMP;
}
?>