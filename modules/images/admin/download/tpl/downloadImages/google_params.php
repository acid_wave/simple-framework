<div id="google-params">
	<strong>Google params</strong>
	<fieldset>
	<legend>Google options</legend>
		<table border="0">
			<tr><td><label for="r_any">Any</label></td><td><input id="r_any" type="radio" name="imsize[natsize]" checked="" value="a"/></td></tr>
			<tr><td><label for="r_large">Large</label></td><td><input id="r_large" type="radio" name="imsize[natsize]" value="l" /></td></tr>
			<tr><td><label for="r_medium">Medium</label></td><td><input id="r_medium" type="radio" name="imsize[natsize]" value="m"/></td></tr>
			<tr><td><label for="r_icon">Icon</label></td><td><input id="r_icon" type="radio" name="imsize[natsize]" value="i"/></td></tr>
			<tr><td><label for="s_largethan">Large than</label></td><td><select id="s_largethan" name="imsize[lth]">
				<option value="">-</option>
				<option value="qsvga">400×300</option>
				<option value="vga">640×480</option>
				<option value="svga">800×600</option>
				<option value="xga">1024×768</option>
				<option value="2mp">2 MP (1600×1200)</option>
				<option value="4mp">4 MP (2272×1704)</option>
				<option value="6mp">6 MP (2816×2112)</option>
				<option value="8mp">8 MP (3264×2448)</option>
				<option value="10mp">10 MP (3648×2736)</option>
				<option value="12mp">12 MP (4096×3072)</option>
				<option value="15mp">15 MP (4480×3360)</option>
				<option value="20mp">20 MP (5120×3840)</option>
				<option value="40mp">40 MP (7216×5412)</option>
				<option value="70mp">70 MP (9600×7200)</option>
				</select></td></tr>
			<tr><td><label for="t_exactly">Exactly</label></td><td><input id="t_exactly" type="text" style="width:50px" name="imsize[ex][w]"/>x<input style="width:50px" type="text" name="imsize[ex][h]" /></td></tr>
		</table>
	</fieldset>
	<fieldset>
		<legend>Manual options</legend>
		<table border="0">
			<tr><td>
				<label><input type="checkbox" name="man[imsize][]" value="1024x768"/> 1024 x 768</label><br/>
				<label><input type="checkbox" name="man[imsize][]" value="1280x960"/> 1280 x 960</label><br/>
				<label><input type="checkbox" name="man[imsize][]" value="1280x1024"/> 1280 x 1024</label><br/>
				<label><input type="checkbox" name="man[imsize][]" value="1280x800"/> 1280 x 800</label><br/>
				<label><input type="checkbox" name="man[imsize][]" value="1600x600"/> 1600 x 600</label><br/>
				<label><input type="checkbox" name="man[imsize][]" value="1600x1200"/> 1600 x 1200</label><br/>
				<div id="m_opt_adddim"><textarea style="width:70%;" placeholder="1024x768"></textarea><button>+</button></div>
			</td><td>
				<label><input type="checkbox" name="man[ratio][]" value="16:10"/> 16:10</label><br/>
				<label><input type="checkbox" name="man[ratio][]" value="16:9"/> 16:9</label><br/>
				<label><input type="checkbox" name="man[ratio][]" value="5:4"/> 5:4</label><br/>
				<label><input type="checkbox" name="man[ratio][]" value="5:3"/> 5:3</label><br/>
				<label><input type="checkbox" name="man[ratio][]" value="4:3"/> 4:3</label><br/>
				<label><input type="checkbox" name="man[ratio][]" value="3:2"/> 3:2</label><br/>
				<div id="m_opt_addratio"><input style="width:70%;" type="text" placeholder="16:9" /><button>+</button></div>
			</td></tr>
		</table>
		<br/><hr/>
		<table border="0">
			<tr>
				<td><label><input type="radio" name="man[lim][type]" value="m"/> More than</label>&nbsp;&nbsp;</td>
				<td><label><input type="radio" name="man[lim][type]" value="lth"/> Less than</label>&nbsp;&nbsp;</td>
				<td><label><input type="radio" name="man[lim][type]" value="e"/> Equal</label>&nbsp;&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3">
					<label><input type="text" style="width:50px" placeholder="width" name="man[lim][w]"/></label>or(and)
					<label><input type="text" style="width:50px" placeholder="height" name="man[lim][h]"/></label>
				</td>
			</tr>
		</table>
	</fieldset>
</div>