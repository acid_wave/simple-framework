<script type="text/javascript">
	function getKidPids(){
		var vals=[];
		$('.download_checkbox_kidpids:checked').each(function(i,e){
			vals.push(e.value);
		});
		return vals.join(',');
	}
	function getPids(){
		var vals=[];
		$('.download_checkbox_pids:checked').each(function(i,e){
			vals.push(e.value);
		});
		return vals.join(',');
	}
	function showKeywords(e){
		var td=$(e).parent();
		td.parent().parent().find('.keywords_tr').css('display','table-row');
		td.prop('rowspan', 1).html('<i>Total</i>');
	}
	$(function(){
		$('.download_checkbox_post').change(function(){
			$(this).parent().parent().parent()
					.find('.download_checkbox_pids, .download_checkbox_kidpids')
					.prop('checked',$(this).is(':checked'));
		});
	});
	//меняет интерфейс в зависимости от источника - google|bing
	function searchToolsControl(select){
		var ston=$(select).find('option:selected').val()+'-params';
		
		var htmlParams=$('#params-container').html('');
		var htmlParamsDisabled=$('#params-disabled #'+ston);

		$('#params-container').html(htmlParamsDisabled.clone());
	}
	$(document).ready(function(){
		$('#m_opt_adddim button').click(function(){
			var text=$('#m_opt_adddim textarea').val();
			var strArr=text.split("\n");
			for(i=0;i<strArr.length;i++){
				var dim=strArr[i].split("x");
				dim[0]=parseInt(dim[0]);
				dim[1]=parseInt(dim[1]);
				if(dim[0]<=0||dim[1]<=0||isNaN(dim[0])||isNaN(dim[1])) {alert("incorrect"); return false;}
				var dimval=dim[0]+'x'+dim[1];
				str=dim[0]+' x '+dim[1];
				$('#m_opt_adddim').before('<label><input type="checkbox" name="man[imsize][]" value="'+dimval+'" checked=""/> '+str+'</label><br/>');
			}
			return false;
		});
		$('#m_opt_addratio button').click(function(){
			var str=$('#m_opt_addratio input').val();
			var rat=str.split(":");
			rat[0]=parseInt(rat[0]);
			rat[1]=parseInt(rat[1]);
			if(rat[0]<=0||rat[1]<=0||isNaN(rat[0])||isNaN(rat[1])) {alert("incorrect"); return false;}
			rat=rat[0]+':'+rat[1];
			$('#m_opt_addratio').before('<label><input type="checkbox" name="man[ratio][]" value="'+rat+'" checked=""/> '+rat+'</label><br/>');
			return false;
		});
		//событие для mainkeyword
		$('input[name="mainkey"]').change(function(){
			var url=document.location.href;
			if($(this).prop('checked'))
				document.location=url+'&mainkey=1';
			else
				document.location=url.replace('&mainkey=1','');
		});
		//событие для смены источника - google|bing
		$('select[name="source"]').change(function(){
			searchToolsControl(this);
		});
		searchToolsControl($('select[name="source"]')[0]);
		//
		anotherRunningAtt();
	});
	function anotherRunningAtt(){
		$.get("<?=HREF?>/?module=images/admin/download",{
			act:'anotherRunning',
		},function answer(data){
			if(data) {
				if(!confirm("HAVE TO CONTINUE?\nAnother daemon runnig\n"+data)){
					$('[name=upload_new_submit]').attr('disabled','');
					$('[name=upload_to_submit]').attr('disabled','');
					$('[name=upload_all_submit]').attr('disabled','');
				}
			}
		});
	}
</script>
<style type="text/css">
	.leftcol table tr * {
		padding: 5px;
	}
	.leftcol table tbody:nth-child(odd) {
		background: #eee;
	}
	.keywords_tr{
		display:none;
	}
</style>
<div class="row mt-4">
	<div class="col-md-9">	
		<?if(!$data->tmpwritable){?><div class="alert alert-danger" role="alert"><h4 class="alert-heading">Set permission</h4><p>chmod 0777 <?=$data->tmp?></p></div><?}?>
		<?if($data->freespace<30){?><div class="alert alert-danger" role="alert">Free space <strong><?=$data->freespace?> Gb</strong></div><?}?>
		<div>
			<a href="<?=url::status()?>">status</a> |
			<small>free space <?=$data->freespace?> Gb</small>
			<?=isset($data->createKeywords)?$data->createKeywords:''?>
		</div>
		<table class="table table-striped mt-4">
			<thead>
				<tr class="text-center">
					<th class="align-middle" width="10"></th>
					<th class="align-middle" width="40">Pid</th>
					<th class="align-middle" width="125">Date<br><a class="btn btn-default btn-sm" href="<?=url::downloadImages().'&sort=datePublish+asc'.($data->mainkey?'&mainkey=1':'')?>"><i class="fa fa-arrow-down"></i></a><a class="btn btn-default btn-sm" href="<?=url::downloadImages().'&sort=datePublish+desc'?>"><i class="fa fa-arrow-up"></i></a></th>
					<th class="align-middle">Title</th>
					<th class="align-middle">Keyword</th>
					<th class="align-middle" width="95">Images Count<br><a class="btn btn-default btn-sm" href="<?=url::downloadImages().'&sort=countPhoto+asc'.($data->mainkey?'&mainkey=1':'')?>"><i class="fa fa-arrow-down"></i></a><a class="btn btn-default btn-sm" href="<?=url::downloadImages().'&sort=countPhoto+desc'?>"><i class="fa fa-arrow-up"></i></a></th>
				</tr>
			</thead>
			<?
			foreach($data->posts as $pid=>$post){
				$i=0;
				echo '<tbody>';
				if(!$post->keywords){
					?>
					<tr>
						<td><input data-id="<?=$post->id?>" class="download_checkbox_post" type="checkbox" checked="checked"/></td>
						<td><?=$post->id?></td>
						<td><?=$post->datePublish?></td>
						<td><a href="<?=url::post($post->url)?>"><?=$post->title?></a></td>
						<td><label><input class="download_checkbox_pids" type="checkbox" value="<?=$post->id?>" checked="checked"/></label></td>
						<td align="right"></td>
					</tr>
					<?
				}else{
					$rowspan='rowspan="'.(count($post->keywords)+1).'"';
					$sum=0;
					foreach($post->keywords as $keyword){
						$sum+=$keyword->countPhoto;
					}
					if(count($post->keywords)==1){
						?>
						<tr>
							<td><input data-id="<?=$post->id?>" class="download_checkbox_post" type="checkbox" checked="checked"/></td>
							<td><?=$post->id?></td>
							<td><?=$post->datePublish?></td>
							<td><a href="<?=url::post($post->url)?>"><?=$post->title?></a></td>
							<td><label><input class="download_checkbox_kidpids" type="checkbox" value="<?=$post->keywords[0]->kidpid?>" checked="checked"/> <?=$post->keywords[0]->title?></label></td>
							<td align="right"><?=$sum?></td>
						</tr>
						<?
					}else{
						?>
						<tr>
							<td <?=$rowspan?>><input data-id="<?=$post->id?>" class="download_checkbox_post" type="checkbox" checked="checked"/></td>
							<td <?=$rowspan?>><?=$post->id?></td>
							<td <?=$rowspan?>><?=$post->datePublish?></td>
							<td <?=$rowspan?>><a href="<?=url::post($post->url)?>"><?=$post->title?></a></td>
							<td <?=$rowspan?>><a href="javascript:void(0);" onclick="showKeywords(this);">show keywords (<?=count($post->keywords)?>)</a></td>
							<td align="right"><?=$sum?></td>
						</tr>
						<?foreach($post->keywords as $keyword){?>
							<tr class="keywords_tr">
								<td><label><input class="download_checkbox_kidpids" type="checkbox" value="<?=$keyword->kidpid?>" checked="checked"/> <?=$keyword->title?></label></td>
								<td align="right"><?=$keyword->countPhoto?></td>
							</tr>
						<?
						}
					}
				}
				echo '</tbody>';
				?>
			<?}?>
		</table>
		<?include $template->inc('downloadImages/delposts.php');?>
		<?=$data->paginator?>
	</div>
	<div class="col-md-3">
		<div class="rightcol">
			<div>
				<form action="<?=url::status()?>" onsubmit="$('#upload_kidpids').val(getKidPids());$('#upload_pids').val(getPids());" method="POST">
					<select name="source">
						<option value="google" selected="selected">google</option>
					</select><br/><br/>
					<fieldset>
						<legend>Download options / Start buttons</legend>
						<label>
							<table cellspacing="0" cellpadding="0" border="0">
								<tr>
									<td style="width: 100px">Upload <small>(max 100)</small></td>
									<td><input name="upload_new" type="text" value="20" size="3"/> images</td>
									<td><input type="submit" name="upload_new_submit" value="Start"<?=!$data->tmpwritable?' disabled=""':''?>></td>
								</tr>
							</table>
						</label><br/>
						<label>
							<table cellspacing="0" cellpadding="0" border="0">
								<tr>
									<td style="width: 100px">Load up to</td>
									<td><input name="upload_to" type="text" value="20" size="3"/> images</td>
									<td><input type="submit" name="upload_to_submit" value="Start"<?=!$data->tmpwritable?' disabled=""':''?>></td>
								</tr>
							</table>					 
						</label><br/>
						<label>
							<table cellspacing="0" cellpadding="0" border="0">
								<tr>
									<td style="width: 100px">Load up to<br>for all</td>
									<td><input name="upload_all" type="text" value="20" size="3"/> images</td>
									<td><input type="submit" name="upload_all_submit" value="Start"<?=!$data->tmpwritable?' disabled=""':''?>></td>
								</tr>
							</table>					 
						</label><br/>
					</fieldset><br/>
					<label>Main keyword only <input type="checkbox" <?=$data->mainkey?'checked="" ':''?>name="mainkey" /></label><br/><br/>
					<label>Allow in gallery block <input type="checkbox" checked="" name="allowgallery" /></label><br/><br/>
					<label>Skip if exists for any post<input type="checkbox" name="skipExists" /></label><br/><br/>
					<label>Add word<input type="text" name="addword"></label>
					<input id="upload_kidpids" name="kidpids" type="hidden"/>
					<input id="upload_pids" name="pids" type="hidden"/>
					<br/>
					<div id="params-container"></div>
				</form>
			</div>
		</div>
	</div>
</div>
<div id="params-disabled" style="display: none;">
	<?include $template->inc('downloadImages/google_params.php');?>
</div>
