<?php
namespace images_admin_download;
use module,db,url,Imagick,user\user;

include __DIR__.'/func.php';
require_once(module::$path.'/user/handler.php');

class handler{
	function __construct(){
		#Определяем в какой шаблон будем вписывать
		$this->template='template';
		$this->headers=(object)array();
		$this->uhandler=user::init();
		$this->user=$this->uhandler->user;
		$this->parserdaemon=[
			'google'=>[
				'path'=>__DIR__."/daemon/gglparse.daemon.php",
				'input'=>__DIR__."/daemon/tmp/gglparse.input.txt",
				'log'=>__DIR__."/daemon/tmp/gglparse.log",
				'cache'=>__DIR__.'/daemon/tmp/gglparse.tmp',
			],
		];
	}
	/*
		Страница скачивания новых картинок для постов
		- настройки
		- выбор постов
	*/
	function downloadImages($page,$num,$sort,$mainkey=false){
		if(!$this->uhandler->rbac(@$this->user->rbac,'downloadImages')){
			$this->headers->location='/';
			return;
		}
		$data=new \StdClass;
		#проверки ресурсов
		@mkdir($data->tmp=__DIR__.'/daemon/tmp/');
		$data->tmpwritable=is_writable($data->tmp);
		$data->freespace=round(disk_free_space(PATH)/1024/1024/1024,0);
		$data->mainkey=$mainkey;

		$sqlWhere=($mainkey)?"&& post.kid!=''":'';
		$offset=($page-1)*$num;
		$res=db::query(
			"SELECT SQL_CALC_FOUND_ROWS 
				post.id, 
				post.url, 
				post.datePublish, 
				post.title,
				COUNT(DISTINCT imgs.id) AS countPhoto 
			FROM post
			LEFT JOIN ".PREFIX_SPEC."imgs imgs 
				ON imgs.tbl='post' && imgs.pid=post.id
			WHERE 1 {$sqlWhere} 
			GROUP BY post.id ORDER BY {$sort} LIMIT {$offset}, {$num}");
		$total=db::qfetch("SELECT FOUND_ROWS() as total")->total;
		$posts=array();
		while($d=db::fetch($res)){
			$d->keywords=array();
			$posts[$d->id]=$d;
		}
		if($posts){
			if($mainkey){
				db::query(
					"SELECT 
						k2p.pid, 
						k.id as kid, 
						k.title, 
						COUNT(DISTINCT imgs.id) AS countPhoto 
					FROM keyword k 
					JOIN ".PREFIX_SPEC."keyword2post k2p 
						ON k2p.kid=k.id && k2p.tbl='post'
					INNER JOIN `post` 
						ON post.id=k2p.pid && post.kid=k2p.kid
					LEFT JOIN ".PREFIX_SPEC."imgs imgs 
						ON imgs.kid=k2p.kid AND imgs.pid=k2p.pid && imgs.tbl='post'
					WHERE k2p.pid IN (".implode(',',array_keys($posts)).")
					GROUP BY k.id,k2p.pid"
				);
			}else{
				db::query(
					"SELECT 
						k2p.pid, 
						k.id as kid, 
						k.title, 
						COUNT(DISTINCT imgs.id) AS countPhoto 
					FROM keyword k 
					JOIN ".PREFIX_SPEC."keyword2post k2p 
						ON k2p.kid=k.id && k2p.tbl='post'
					LEFT JOIN ".PREFIX_SPEC."imgs imgs 
						ON imgs.kid=k2p.kid AND imgs.pid=k2p.pid && imgs.tbl='post'
					WHERE k2p.pid IN (".implode(',',array_keys($posts)).")
					GROUP BY k.id,k2p.pid"
				);
			}
			while($d=db::fetch()){
				$d->kidpid="{$d->kid}_{$d->pid}";
				$posts[$d->pid]->keywords[]=$d;
			}
		}
		$data->paginator=module::exec('plugins/paginator',array(
			'page'=>$page,'num'=>$num,'count'=>$total,
			'uri'=>url::downloadImages().($sort!='datePublish desc'?'&sort='.urlencode($sort):'').($mainkey?'&mainkey=1':'').'&page=%d'
		),1)->str;
		$data->posts=$posts;
		if(!is_dir($dir=module::$path."/images/files/images")){mkdir($dir);}

		$data->access=$this->uhandler->access;

		if(!db::qrow("SELECT pid FROM ".PREFIX_SPEC."keyword2post LIMIT 1"))
			$data->createKeywords=module::execStr('posts/keywords/admin',['act'=>'createFromPosts']);

		return $data;
	}
	/*
	 * Проверяем наличие на диске картинок из таблице zspec_images и пытаемся скачать из оригинала
	 */ 
	function redownloadImages($do){
		if(!$this->uhandler->rbac(@$this->user->rbac,'downloadImages')){
			$this->headers->location='/';
			return;
		}
		#Запуск перезакачки картинок
			if(!empty($do)){
				$script=PATH."modules/images/admin/sh/redownload.php";
				shell_exec("nohup php $script >/dev/null 2> /dev/null & echo $!");
				print "<script>window.close()</script>";
			}
		$cnAll=$cnExists=0;
		#Получаем все изображения с диска
			$files=array_flip(scandir(PATH."/modules/images/files/images/"));
		#Получаем все изображения из базы
			db::query("SELECT url FROM `".PREFIX_SPEC."imgs`");
			while($d=db::fetch()){
				$cnAll++;
				if(isset($files[$d->url]))$cnExists++;
			}
		$log=shell_exec("ps aux | grep php");
		return [
			'cnAll'=>$cnAll,
			'cnExists'=>$cnExists,
			'cnNo'=>$cnAll-$cnExists,
			'log'=>$log,
			'Imagick'=>class_exists('imagick'),
		];
	}
	/*
		Запуск закачки картинок
	*/
	function status($is_new,$count,$kidpids,$allowgallery,$word,$imsize,$manimsize,$skipExists,$mainkey,$all=false,$source){
		if(!$this->uhandler->rbac(@$this->user->rbac,'downloadImages')){
			$this->headers->location=url::dowloadImages();
			return;
		}

		$dImgdownload=__DIR__."/daemon/imgdownload.daemon.php";

		if($all) $kidpids=getAllkidpid();

		$kids=$pids=array();
		fillKidsPids($kidpids,$kids,$pids);

		if(!empty($kids)){
			set_time_limit(60);
			ini_set('memory_limit', '1G');

			$daemon=$this->parserdaemon[$source];

			if($source=='google'){
				$seachtools=googleSearchToolsBuild($imsize);
			}elseif($source=='bing'){
				$seachtools='';
			}
			#формируем данные
			$daemonInputTMP=prepareData(
				$daemon['input'],
				$kids,
				$pids,
				$mainkey,
				$all,
				$is_new,
				$count,
				$word,
				$seachtools,
				$skipExists,
				$this->user->id,
				$allowgallery,
				$manimsize
			);
			
			#rename tmp file after full filling
			rename($daemonInputTMP, $daemon['input']);
			/*
				запуск демонов (если не были запущены ранее)
					1. качает выдачу google, парсит
					2. качает картинки
			*/
			shell_exec("nohup php {$daemon['path']} > {$daemon['log']} 2>&1 &");
			shell_exec("nohup php {$dImgdownload} > ".__DIR__."/daemon/tmp/imgdownload.log 2>&1 &");
		}

		$status=[];
		foreach ($this->parserdaemon as $name => $daemon) {
			$status[$name]=processRunning($daemon['path']);
		}

		return (object)array(
			'statusRunning'=>$status,
			'dImgdownload'=>processRunning($dImgdownload),
		);
	}
	/*
		отвечает на ajax запрос, читает указанные логи
	*/
	function showLog($log,$tail,$start){
		if(!$this->uhandler->rbac(@$this->user->rbac,'downloadImages')){
			$this->headers->location='/';
			return;
		}
		if($log&&file_exists($file=__DIR__.'/daemon/tmp/'.$log)){
			$length=1024*10;
			if($start<1){
				$size=filesize($file);
				$start=$size-$length;
				if($start<0)$start=0;
			}
			$fp=fopen($file,'r');
			fseek($fp,$start);
			$str=fread($fp,$length);
			echo (strlen($str)+$start)."\n$str";
		}
		die;
	}
	/*
		останавливает демоны, стирает логи
	*/
	function stopDaemons(){
		if(!$this->uhandler->rbac(@$this->user->rbac,'downloadImages')){
			$this->headers->location='/';
			return;
		}

		foreach ($this->parserdaemon as $src => $val) {
			$pid=trim(shell_exec($t="ps aux|grep ".escapeshellarg($val['path'])."|grep -v ' grep '|awk '{print $2}'"));	
			if(is_numeric($pid)){
				$shout=shell_exec("kill {$pid}");
				echo "{$val['path']} ".($shout?$shout:'stoped')."\n";
			}
		}

		$dImgdownload=__DIR__."/daemon/imgdownload.daemon.php";
		$pid=trim(shell_exec($t="ps aux|grep ".escapeshellarg($dImgdownload)."|grep -v ' grep '|awk '{print $2}'"));	
		if(is_numeric($pid)){
			$shout=shell_exec("kill {$pid}");
			echo "$dImgdownload ".($shout?$shout:'stoped')."\n";
		}
		#clear logs
		if($dh=opendir($tmpDir=__DIR__."/daemon/tmp/")){
			while ($file=readdir($dh)) {
				if(!preg_match('!\.log$!i', $file)) continue;
				unlink($tmpDir.$file);
			}
		}
		#remove images parse results
		shell_exec('rm -rf '.__DIR__.'/daemon/tmp/gglparseImgs.tmp');
		die;
	}
	/*
		удаляет скаченные html с результатами от google
	*/
	function clearCache(){
		if(!$this->uhandler->rbac(@$this->user->rbac,'downloadImages')){
			$this->headers->location='/';
			return;
		}
		foreach ($this->parserdaemon as $src => $val) {
			shell_exec('rm -rf '.$val['cache']);
		}
		echo 'done';
		die;
	}
	/*
		определяет запущен ли такой же процесс в любом месте на данном сервере
	*/
	function anotherRunning(){
		foreach ($this->parserdaemon as $src => $val) {
			$commonPath=str_replace(__DIR__, '', $val['path']);
			$ps=trim(shell_exec($str="ps aux|grep '".escapeshellarg($commonPath)."'|grep -v ' grep '|awk '{print $12}'"));
			if(!empty($ps)&&$ps!=__DIR__.$commonPath){
				echo $ps."\n";
			}
		}
		die;
	}
}
