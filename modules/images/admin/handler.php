<?php
namespace images_admin;
use module,db,url;

require_once(__DIR__.'/func.php');
#сторонние модули
require_once(module::$path.'/images/handler.php');
require_once(module::$path.'/posts/handler.php');
require_once(module::$path.'/category/handler.php');
require_once(module::$path.'/category/admin/handler.php');
require_once(module::$path.'/user/handler.php');

class handler{
	function __construct(){
		$this->template='';#Определяем в какой шаблон будем вписывать
		$this->headers=(object)array();
		$this->uhandler=module::exec('user',array(),1)->handler;
		$this->user=$this->uhandler->user;
	}
	/*
		Удаление картинки
			- удаляет основную картинку
			- удаляет все thumbnail к картинке
	*/
	function del($imid,$ajax,$withoutRbacCheck=false){
		list($tbl,$pid,$url,$user,$sha1)=db::qrow("SELECT tbl,pid,url,uid,sha1 FROM `".PREFIX_SPEC."imgs` WHERE id='$imid'");
		if($withoutRbacCheck!==true){
			#Проверяем разрешение на удаление картинки
			if(!$rbac=$this->uhandler->rbac('delImage'))
				$rbac=($this->uhandler->rbac('delImageMy')&&$this->user->id==$user);
			if(!$rbac){
				return array('html'=>'permission denied');
			}
		}
		$count_same=db::num_rows(db::query("SELECT `id` FROM `".PREFIX_SPEC."imgs` WHERE `sha1` = '".$sha1."'"));
		if($count_same==1){
			# удаляем основную картинку
			unlink($tFile=module::$path.'/images/files/images/'.$url);
			@unlink(module::$path.'/images/files/googleBack/'.$url);
			if(!file_exists($tFile))
				db::query("DELETE FROM `".PREFIX_SPEC."imgs` WHERE `id`='$imid'");
			else
				return (object)array(
					'html'=>'fail, set permissions'
				);
			# удаляем thumbnails
			\images\thumbnail::remove($url);
		}elseif($count_same>1){
			db::query("DELETE FROM `".PREFIX_SPEC."imgs` WHERE `id`='$imid'");
		}
		# пересчитываем количество картинок
		module::exec('posts/admin',array('act'=>'imgRecount','pid'=>$pid,'tbl'=>$tbl),'data');
		return (object)array(
			'html'=>$ajax?module::exec('images/admin',array('act'=>'listofimages','pid'=>$pid,'tbl'=>$tbl),1)->str:'done'
		);	
	}
	/*
	 * Delete images with same sha1
	 */
	function delImages($imid,$all=true,$withoutRbacCheck=false) {
		error_reporting(0);
		$this->template='';
		$this->view='delRequest';
		if($withoutRbacCheck!==true){
			#Проверяем разрешение на удаление картинки
			if(!$rbac=$this->uhandler->rbac('delImage'))
				$rbac=($this->uhandler->rbac('delImageMy')&&$this->user->id==$user);
			if(!$rbac){
				return (object)array('status'=>'error','reason'=>'permission denied');
			}
		}
		list($sha1)=db::qrow("SELECT `sha1` FROM `".PREFIX_SPEC."imgs` WHERE `id`='$imid'");
		$images = db::qall("SELECT imgs.id as imid, imgs.url as imurl, p.* FROM `".PREFIX_SPEC."imgs` imgs LEFT JOIN `post` p ON imgs.`pid`=p.`id` WHERE imgs.`sha1` = '".$sha1."'".(!$all?" and imgs.`id`='{$imid}'":''));
		foreach ($images as $image) {
			unlink($tFile=module::$path.'/images/files/images/'.$image->imurl);
			@unlink(module::$path.'/images/files/googleBack/'.$image->imurl);
			if(!file_exists($tFile))
				db::query("DELETE FROM `".PREFIX_SPEC."imgs` WHERE `id`='{$image->imid}'");
			else
				return (object)array(
					'status'=>'error','reason'=>'fail, set permissions'
				);
			# удаляем thumbnails
			\images\thumbnail::remove($image->imurl);
		}
		return (object)array(
			'status'=>'ok',
			'images'=>$images,
			'count'=> count($images),
		);
	}
	/*
	 * Request list of images with posts to delete
	 */
	function delRequest($imid,$withoutRbacCheck=false) {
		$this->template='';
		if($withoutRbacCheck!==true){
			#Проверяем разрешение на удаление картинки
			if(!$rbac=$this->uhandler->rbac('delImage'))
				$rbac=($this->uhandler->rbac('delImageMy')&&$this->user->id==$user);
			if(!$rbac){
				return (object)array('status'=>'error','reason'=>'permission denied');
			}
		}
		list($sha1)=db::qrow("SELECT sha1 FROM `".PREFIX_SPEC."imgs` WHERE id='$imid'");
		$images = db::qall("SELECT imgs.id as imid, imgs.url as imurl, p.* FROM `".PREFIX_SPEC."imgs` imgs  LEFT JOIN `post` p ON imgs.pid=p.id WHERE `sha1` = '".$sha1."'");
		return (object)array(
			'status'=>'ok',
			'images'=>$images,
			'count'=> count($images),
		);
	}
	function dellWithoutFile(){
		if(!$this->uhandler->access->downloadImages){
			$this->headers->location=HREF;
			return;
		}
		$this->template='template';
		#Запуск перезакачки картинок
			if(!empty($do)){
				$script=PATH."modules/images/admin/sh/redownload.php";
				shell_exec("nohup php $script >/dev/null 2> /dev/null & echo $!");
			}
		$cnAll=$cnExists=0;
		#Получаем все изображения с диска
			$files=array_flip(scandir(PATH."/modules/images/files/images/"));
		#Получаем все изображения из базы
			$q=db::query("SELECT id,url FROM `".PREFIX_SPEC."imgs`");
			while($d=db::fetch($q)){
				if(!isset($files[$d->url])){
					db::query("DELETE FROM `".PREFIX_SPEC."imgs` WHERE id='$d->id'");
				}
			}
	}
	/*
		удаляет картинки по ID поста
	*/
	function delByPid($pid,$tbl){
		if(empty($pid)||empty($tbl)) return;
		db::query("SELECT `id`,`uid` FROM `".PREFIX_SPEC."imgs` WHERE `pid`='$pid' && `tbl`='{$tbl}'");
		while($d=db::fetch()){
			$this->del($d->id,false,$withOutRbacCheck=true);
		}
	}
	function install(){
		db::query("CREATE TABLE IF NOT EXISTS `".PREFIX_SPEC."imgs` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `kid` int(11) NOT NULL,
			  `gtitle` varchar(255) NOT NULL,
			  `gtbn` varchar(255) NOT NULL,
			  `text` varchar(255) NOT NULL,
			  `tbl` varchar(50) NOT NULL DEFAULT 'posts',
			  `pid` int(11) NOT NULL,
			  `url` varchar(255) NOT NULL,
			  `sha1` varchar(50) NOT NULL,
			  `uid` int(11) NOT NULL,
			  `date` date NOT NULL,
			  `width` int(11) NOT NULL,
			  `height` int(11) NOT NULL,
			  `filesize` int(11) NOT NULL,
			  `source` varchar(255) NOT NULL,
			  `sourcePage` varchar(255) DEFAULT NULL,
			  `exif` TEXT NOT NULL,
			  `type` varchar(50) NOT NULL,
			  `keyword_type` varchar(255) NOT NULL,
			  `priority` tinyint(4) NOT NULL DEFAULT '0',
			  `approve` int(11) NOT NULL DEFAULT '0',
			  `licence` varchar(255) NOT NULL,
			  `statViews` int(11) NOT NULL,
			  `statViewsShort` int(11) NOT NULL,
			  `statShortFlag` date NOT NULL,
			  `statDownload` INT(11) NOT NULL,
			  PRIMARY KEY (`id`),
			  UNIQUE KEY `uniq` (`tbl`,`pid`,`url`),
			  UNIQUE KEY `tbl_2` (`tbl`,`pid`,`sha1`),
			  KEY `sha1` (`sha1`),
			  KEY `pid` (`pid`),
			  KEY `tbl` (`tbl`),
			  KEY `kid` (`kid`),
			  KEY `gtbn` (`gtbn`),
			  KEY `statViews` (`statViews`),
			  KEY `statViewsShort` (`statViewsShort`),
			  KEY `tbl_priority` (`tbl`,`priority`),
			  KEY `tbl_pid_priority` (`tbl`, `pid`, `priority`),
			  KEY `uid` (`uid`),
			  KEY `uid_statDownload` (`uid`,`statDownload`)
			) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;",1);

		return;
	}
	function dellNotApproved(){
		db::query("delete FROM `".PREFIX_SPEC."imgs` WHERE approve=2");
		$this->template='template';
		return [];
	}
	/*
	 * модерация картинок
	 * 1. Доступ для пользователей с атрибутом rbac=3 или 1
	 * 
	 * Логика работы
	 * 		- для каждой картинки проставить поле approve -> админ переносит удаленные картинки в таблицу imgsDel
	 * 		- approve = 1 - оставить,  2 - удалить, 0 - не определено
	 * */
	function bulkDell($status,$limit,$page,$data,$cat,$delCat){
		#Удаляем посты из выбранной категории
		if($delCat && $cat){
			$del=array();
			foreach($data as $k=>$v){
				if($v==1)continue;
				$del[]=$k;
			}
			db::query("SELECT id,url FROM post WHERE id in (".implode(",",$del).")");
			$del=array();
			while($d=db::fetch()){
				$del[]=$d->url;
			}
			if(count($del)){
				db::query("DELETE FROM `".PREFIX_SPEC."category2post` WHERE cid='$cat' and pid in('".implode("','",$del)."')");
			}
		}
		$this->template='template';
		$sql_where='';
		if(!$rbac=$this->uhandler->access->moderImage){
			if($rbac=$this->uhandler->access->moderImageMy)
				$sql_where=" && `uid`='{$this->user->id}'";
		}
		if(!$rbac){
			$this->headers->location=HREF;
			return;
		}
		$start=($page-1)*$limit;
		# запись в базу из POST данных
		if($data){
			$unapproved=$approved=array();
			foreach($data as $id=>$val){
				if($val==2) $unapproved[]=$id;
				else $approved[]=$id;
			}
			# помечаем unapproved
			if(isset($unapproved[0]))
				db::query("UPDATE `".PREFIX_SPEC."imgs` SET `approve`=2 WHERE `id` IN('".implode("','",$unapproved)."')".$sql_where);
			# помечаем approved	
			if(isset($approved[0]))
				db::query("UPDATE `".PREFIX_SPEC."imgs` SET `approve`=1 WHERE `id` IN('".implode("','",$approved)."')".$sql_where);
		}
		
		$cn=array();
		$imgsCurrent=array();
		$sql_where_status=($status==-1?' ':" && `approve`='$status' ");
		if($cat){
			#Получаем все дочерние категории.
			$cats=\category\getLeafCats($cat);
			$cats[]=$cat;
			#получаем количество картинок
				db::query("SELECT count(distinct img.id) as cn,approve
					FROM `".PREFIX_SPEC."imgs` as img
					LEFT JOIN post as p
						ON p.id=img.pid
					LEFT JOIN `".PREFIX_SPEC."category2post` as c2p
						ON p.url=c2p.pid
					where 
						img.tbl='post'
						and c2p.cid in('".implode("','",$cats)."')
					GROUP BY approve
				");
				$cn=[0,0,0];
				while($d=db::fetch()){
					$cn[$d->approve]=$d->cn;
				}
				$cn['all']=array_sum($cn);
			$statusSql=$status!=-1?"and img.approve=$status ":'';
			db::query("SELECT img.*,p.title as keyword
				FROM `".PREFIX_SPEC."imgs` as img
				LEFT JOIN post as p
					ON p.id=img.pid
				LEFT JOIN `".PREFIX_SPEC."category2post` as c2p
					ON p.url=c2p.pid
				where 
					img.tbl='post'
					and c2p.cid in('".implode("','",$cats)."')
					$statusSql
					group by img.id
					LIMIT $start,$limit
			");
		}else{
			#получаем количество картинок
			$cn=[
				# common count
				'all'=>db::qrow("SELECT COUNT(`id`) FROM `".PREFIX_SPEC."imgs` WHERE 1".$sql_where.$sql_where_status)[0],
				# not defined
				db::qrow("SELECT COUNT(`id`) FROM `".PREFIX_SPEC."imgs` WHERE `approve`='0'".$sql_where)[0],
				# approved
				db::qrow("SELECT COUNT(`id`) FROM `".PREFIX_SPEC."imgs` WHERE `approve`='1'".$sql_where)[0],
				# not approved
				db::qrow("SELECT COUNT(`id`) FROM `".PREFIX_SPEC."imgs` WHERE `approve`='2'".$sql_where)[0],
			];
			# получаем картинки с указанным статусом
			
			db::query(
				"SELECT img.*,post.title AS keyword FROM `".PREFIX_SPEC."imgs` img
					LEFT OUTER JOIN `post` 
					ON post.id=img.pid
				WHERE 1"
				.$sql_where.$sql_where_status
				."LIMIT $start,$limit");
		}
		while($d=db::fetch()){
			$imgsCurrent[]=$d;
		}
		return (object)array(
			'count'=>$cn,
			'limit'=>$limit,
			'imgs'=>$imgsCurrent,
			'currentStatus'=>$status,
			'paginator'=>module::execStr("plugins/paginator",[
					'page'=>$page,
					'num'=>$limit,
					'count'=>$cn[$status==-1?'all':$status],
					'uri'=>url::imagesBulkDell($status,$cat).'&page=%d',
					'showPagen'=>false,
			]),
		);
	}
	function moderateByCat($cat,$bindCat,$page,$num,$submit,$ids,$newCat){
		if(!empty($submit[0])){
			$curl=db::qrow("SELECT url FROM `category` WHERE url='$newCat'")[0];
			if(!empty($ids))foreach($ids as $k=>$v){
				if($v=='')unset($ids[$k]);
			}
			if(count($ids)){
				db::query("UPDATE category SET parentId='$curl' WHERE id in (".implode(",",array_keys($ids)).")");
			}
		}elseif(!empty($submit[1])){#Удаляем категории
			$q=db::query("SELECT url FROM category WHERE id in(".implode(",",array_keys($ids)).")");
			while($d=db::fetch($q)){
				db::query("DELETE FROM `".PREFIX_SPEC."category2post` where cid='$d->url'");
			}
			db::qrow("DELETE FROM category WHERE id in(".implode(",",array_keys($ids)).")");
			
		}
		$this->template='template';
		#Получаем категории
			$cats=array();
			#Получаем Все категории первого уровня
				$start=($page-1)*$num;
				$sqlLimit=($cat=='tags')?"and countAdmin>20":'';
				$q="SELECT * FROM `category` where parentId='$cat' $sqlLimit";
				db::query("$q ORDER BY title LIMIT $start,$num");
				while($d=db::fetch()){
					$cats[$d->url]=$d;
				}
				$count=db::qrow(preg_replace("!SELECT.*?FROM!","SELECT COUNT(*) FROM",$q))[0];
			#Считаем кол-во категорий второго уровня
				db::query("SELECT 
						parentId,count(*) as cn 
					FROM `category` 
					where parentId in ('".implode("','",array_keys($cats))."')
					GROUP BY parentId
				");
				while($d=db::fetch()){
					$cats[$d->parentId]->subCount=$d->cn;
				}
		return [
			'cats'=>$cats,
			'cat'=>$cat,
			'bindCat'=>$bindCat,
			'paginator'=>module::execStr("plugins/paginator",['page'=>$page,'num'=>$num,'count'=>$count,'uri'=>url::imagesModerateByCat($cat,$bindCat)."&page=%d"]),
		];
	}
	/*
	 * Полная модерация картинки
	*/ 
	function moderate($id='',$title,$cat,$tag,$type,$quality,$submit,$updateId){
		if($submit=="save" && $updateId){
			#Получаем пост id
				$pid=db::qrow("SELECT pid FROM `".PREFIX_SPEC."imgs` WHERE id='$updateId'")[0];
				$purl=db::qrow("SELECT url FROM `post` WHERE id='$pid'")[0];
			#Получаем теги
				$tag=array_flip($tag);
				foreach($tag as $k=>$v){
					unset($tag[$k]);
					$k=trim(strtolower($k));
					if($k=='')continue;
					$tag[$k]=$v;
				}
				db::query("SELECT * FROM category WHERE parentId='tags' && LOWER(title) in ('".implode("','",array_keys($tag))."')");
				while($d=db::fetch()){
					$tag[strtolower($d->title)]=[$d->id,$d->url];
				}
				#Записываем теги которых нет
				foreach($tag as $k=>$v){
					if(is_array($v))continue;
					$tag[$k]=array();
					$tag[$k][1]=\category_admin\getUrl('category',$k);
					db::query("INSERT INTO category SET title='".db::escape($k)."',url='{$tag[$k][1]}',parentId='tags'");
					$tag[$k][0]=db::insert();
				}
			#Обновляем title и ставим статус промодерированно
				db::query("UPDATE `".PREFIX_SPEC."imgs` SET gtitle='".db::escape($title)."',approve=1 WHERE id='$updateId'");
				db::query("UPDATE `post` SET title='".db::escape($title)."' WHERE id='$pid'");
			#Записываем привязку поста к категориям
				db::query("DELETE FROM `".PREFIX_SPEC."category2post` WHERE pid='$purl'");
				$sql=array("('$cat','$purl'),('$type','$purl'),('$quality','$purl')");
				foreach($tag as $k=>$v){
					$sql[]="('$v[1]','$purl')";
				}
				db::query("INSERT INTO `".PREFIX_SPEC."category2post` (cid,pid) VALUES ".implode(",",$sql));
		}elseif($submit=="delete"){
			db::query("UPDATE `".PREFIX_SPEC."imgs` SET approve=2 WHERE id='$updateId'");
		}
		$this->template='template';
		#Получаем картинку по id
			$sql=$id?"id='$id'":'approve=0';
			$img=db::qfetch("SELECT * FROM `".PREFIX_SPEC."imgs` WHERE $sql limit 1");
		#Получаем список типов качества
			$qualitys=array();
			db::query("SELECT * FROM `category` WHERE parentId='quality'");
			while($d=db::fetch()){
				$qualitys[]=$d;
			}
			if(count($qualitys)==0){
				$qualitys=[
					(object)['url'=>'perfect','title'=>'Perfect'],
					(object)['url'=>'normal','title'=>'Normal'],
				];
				db::query("INSERT INTO category SET title='quality',url='quality',parentId='',`view`=0 ON DUPLICATE KEY update parentId=''");
				foreach($qualitys as $v){
					db::query("INSERT INTO category SET title='$v->title',url='$v->url',parentId='quality' ON DUPLICATE KEY update parentId='quality'");
				}
			}
		#Получаем список типов изображений
			$types=array();
			db::query("SELECT * FROM `category` WHERE parentId='types'");
			while($d=db::fetch()){
				$types[]=$d;
			}
			if(count($types)==0){
				$types=[
					(object)['url'=>'illustration','title'=>'Illustration'],
					(object)['url'=>'background','title'=>'Background'],
					(object)['url'=>'pattern','title'=>'Pattern'],
					(object)['url'=>'silhouette','title'=>'Silhouette'],
					(object)['url'=>'black-white','title'=>'Black & White'],
					(object)['url'=>'symbol','title'=>'Symbol'],
					(object)['url'=>'clip-art','title'=>'Clip Art'],
				];
				db::query("INSERT INTO category SET title='types',url='types',parentId='',`view`=0 ON DUPLICATE KEY update parentId=''");
				foreach($types as $k=>$v){
					db::query("INSERT INTO category SET title='$v->title',url='$v->url',parentId='types' ON DUPLICATE KEY update parentId='types'");
				}
			}
		if(!empty($img)){
			#Получаем пост
				$post=db::qfetch("SELECT * FROM `$img->tbl` WHERE id='$img->pid'");
			#Получаем категории
				db::query("SELECT category.*,c2p.* FROM `".PREFIX_SPEC."category2post` as c2p
						left join category on category.url=c2p.cid
					WHERE c2p.pid='$post->url'");
				$cat=array();
				while($d=db::fetch()){
					if($d->cid=='tags')continue;
					$catTitles[$d->cid]=$d->title;
					$cat[$d->parentId][]=$d->cid;
				}
			#Получаем следующую картинку для модерации
				$next=db::qfetch("SELECT * FROM `".PREFIX_SPEC."imgs` WHERE approve=0 && id>'$img->id' limit 1");
			#Получаем все категории верхнего уровня кроме служебных
				$mainCats=array();
				db::query("SELECT * FROM `category` WHERE parentId='' and view=1");
				while($d=db::fetch()){
					$mainCats[]=$d;
				}
				usort($mainCats,'\images_admin\byTitle');
		}
		return [
			'img'=>$img,
			'post'=>@$post,
			'cat'=>@$cat,
			'mainCats'=>@$mainCats,
			'next'=>@$next,
			'catTitles'=>@$catTitles,
			'types'=>$types,
			'qualitys'=>$qualitys,
		];
	}
	/*
		вывод
			1. формы загрузки картинок
			2. список загруженных картинок listofimages()
	*/
	function edit($pid,$tbl,$key){
		return (object)array(
			'images'=>module::exec('images/admin',array('act'=>'listofimages','pid'=>$pid,'tbl'=>$tbl),1)->str,
			'pid'=>$pid,
			'tbl'=>$tbl,
			'key'=>$key,
			'limitFileUpload'=>ini_get('upload_max_filesize'),
			'user'=>$this->user->id,
		);
	}
	/*
		список загруженных картинок
			вход: tbl, pid
		запрашивается в edit и для обновления списка, например: после загрузки картинок
	*/
	function listofimages($pid,$tbl,$image_sort){
		if(!empty($image_sort)) {
			foreach($image_sort as $priority=>$image_id) {
				db::query("UPDATE `".PREFIX_SPEC."imgs` SET `priority`='".$priority."'  WHERE `id`='$image_id' && `tbl`='$tbl' && `pid`='$pid'");	
			}
		}
		$images=array();
		# получаем картинки
		db::query(
			"SELECT imgs.*,imgs.gtitle as title, keyword.title as keyword FROM `".PREFIX_SPEC."imgs` imgs 
				LEFT JOIN keyword ON keyword.id=imgs.kid 
			WHERE imgs.`tbl`='$tbl' && imgs.`pid`='$pid' 
			ORDER BY imgs.`priority` DESC");
		while($d=db::fetch()){
			$d->gif=@mime_content_type(module::$path."/images/files/images/".$d->url)=="image/gif";
			$images[]=$d;
		}
		return (object)array('images'=>$images,'pid'=>$pid,'tbl'=>$tbl);
	}
	/*
		Сохранение картинок
	*/
	function save($pid,$title,$tbl,$images,$type){
		$kids=array();
		$error='';
		@mkdir(\images\thumbnail::$srcpath);
		if($type=='save'){
			if(isset($images['title_updade'])||isset($images['text_update']))
				textAttributesUpdate($images,$pid,$tbl);
			/*
				Запуск пакетной загрузки файлов
			*/
			# собираем список файлов загружаемых по ссылке
			if(is_array(@$images['urls'])){
				$user=empty($this->user->id)?0:$this->user->id;
				foreach($images['urls'] as $k=>$url){
					if(!preg_match('!^(?:http|ftp|sftp)!i',$url))
						unset($images['urls'][$k]);
				}
				
				$quantity=count($images['urls']);
				list($startPriority)=db::qrow("SELECT MAX(`priority`) FROM `".PREFIX_SPEC."imgs` WHERE `tbl`='{$tbl}' && `pid`='{$pid}'");
				if(empty($startPriority)) $startPriority=0;

				$i=0;
				foreach($images['urls'] as $k=>$url){
					$url=trim($url);
					$keyword=empty($images['keyword'][$k])?'':$images['keyword'][$k];
					# определение title
					$title_key=stripslashes(empty($keyword)?$title:$keyword);
					
					$kid=createKeyword($title_key,$pid, $tbl);
					$kids[]=$kid;
					
					$imageName=getImageName($title_key);
					# основные параметры для сохранения картинок
					$args=(object)array(
						'pid'=>$pid,
						'uid'=>$user,
						'tbl'=>$tbl,
						'url'=>$imageName,#имя файла картинки
						'link'=>$url,#ссылка на оригинал
						'kid'=>$kid,
						'gtitle'=>empty($images['gtitle'][$k])?'':$images['gtitle'][$k],
						'text'=>empty($images['description'][$k])?'':$images['description'][$k],
						'gtbn'=>empty($images['tbn'][$k])?'':$images['tbn'][$k],
						'sourcePage'=>empty($images['href'][$k])?'':$images['href'][$k],
						'priority'=>$startPriority+$quantity-$i,#устанавливаем сортировку каринки
					);
					$i++;

					$shellparams=base64_encode(serialize($args));
					# запускаем скрипт закачки
					#echo "nohup php ".module::$path."/images/admin/sh/uploadImages.php 'status_{$pid}_{$user}_{$tbl}' '{$shellparams}' >/dev/null 2> /dev/null & echo $!";
					shell_exec("nohup php ".module::$path."/images/admin/sh/uploadImages.php 'status_{$pid}_{$user}_{$tbl}' '{$shellparams}' >/dev/null 2> /dev/null & echo $!");
				}
			}
		}elseif($type=='saveFile'){
			/*
				сохранение файла загруженного с локали
			*/
			$tImg=$images['files'];
			$user=empty($this->user->id)?0:$this->user->id;
			$keyword=empty($images['keyword'])?'':$images['keyword'];
			$title_key=db::escape(stripslashes(empty($keyword)?$title:$keyword));
			
			$kid=createKeyword($title_key,$pid,$tbl);
			$kids[]=$kid;
			if(imageExist(sha1_file($tImg),$tbl,$pid)){
				@unlink($tImg);
				$error='imageExist';
			}else{
				//Если картинки с таким хэшем нет в бд - грузим
				if(!storeDBOnly($tImg,$pid,$tbl,$user,$kid)){
					$imageName=getImageName($title_key);
					if(move_uploaded_file($tImg,$imgFile=\images\thumbnail::$srcpath.'/'.$imageName)){
						if(@$imgData=getimagesizeExt($imgFile)){
							$imgFile=formatRename($imgFile,$imgData);
							$_patharr=explode(DIRECTORY_SEPARATOR, $imgFile);
							$imageName=array_pop($_patharr);
							if($imgData[2]==IMAGETYPE_JPEG)
								\images\jpegoptimRun($imgFile);
							$hash=sha1_file($imgFile);
							db::query("INSERT INTO `".PREFIX_SPEC."imgs` SET
								`tbl`='{$tbl}',
								`pid`='{$pid}',
								`kid`={$kid},
								`url`='".db::escape($imageName)."',
								`date`=NOW(),
								`gtitle`='".(!empty($images['gtitle'])?db::escape($images['gtitle']):'')."',
								`uid`='{$user}',
								`sha1`='{$hash}',
								`width`='{$imgData[0]}',
								`height`='{$imgData[1]}',
								`filesize`='".filesize($imgFile)."',
								`approve`=1,
								`source`=''");
							# resize
							foreach(\images\Handler::$resolution as $r){
								\images\resize($imgFile,true,$r[0],$r[1]);
							}
							# пересчитываем количество картинок
							module::exec('posts/admin',array('act'=>'imgRecount','pid'=>$pid,'tbl'=>$tbl),'data');
						}else{
							@unlink($imgFile);
							$error='notAnImage';
						}
					}
				}
			}
		}
		# Check achievements change
		checkAchievement($user);
		return (object)array('kids'=>$kids,'error'=>$error);
	}
	/*
		Обновляет кейворд у загруженных картинок
	*/
	function updateKeyword($pid,$keywords,$tbl){
		$newkids=array();
		#главный кейворд
		list($mkid)=db::qrow("SELECT `kid` FROM `{$tbl}` WHERE `id`='{$pid}' LIMIT 1");
		$keyword_str=array();
		foreach($keywords as $title){
			if($title=='') continue;
			$keyword_str[db::escape($title)]=1;
		}
		$kids=array();
		db::query("SELECT id,title FROM keyword WHERE title IN ('".implode("','",array_keys($keyword_str))."')");
		while($d=db::fetch()){
			$kids[$d->title]=$d->id;
		}
		#вместо пустого кейворда назначается главный кейворд
		$kids['']=$mkid;
		foreach($keywords as $id=>$title){
			if(isset($kids[$title])){
				$kid=$kids[$title];
			}else{
				db::query("INSERT INTO keyword (title) VALUES ('".db::escape($title)."')");
				$kid=(int)db::insert();
				$newkids[]=$kid;
			}
			if(!$kid){
				continue;
			}
			db::query("UPDATE `".PREFIX_SPEC."imgs` SET `kid`={$kid}
				WHERE `id`='{$id}' && `tbl`='{$tbl}' && `pid`='{$pid}' LIMIT 1",2);
		}
		return (object)array('newkids'=>$newkids);
	}
	/*
		Количество работающих потоков пакетной закачки картинок
	*/
	function ImageLoadStatus($tbl,$pid,$uid){
		if(empty($tbl)||empty($pid)||empty($uid)) die;
		$out=shell_exec("ps aux|grep \".*uploadImages.php status_{$pid}_{$uid}_{$tbl}\"");
		return (object)array('count'=>count(explode("\n", trim($out)))-2);
	}
	/*
		Парсинг картинок с гугла
	*/
	function searchRequest($keyword, $nocheck=false){
		if(!$keyword) return;
		
		$keyword = trim($keyword);
		if (!$nocheck) {
			list($c)=db::qrow("SELECT COUNT(*) FROM `".PREFIX_SPEC."imgs` imgs WHERE kid = (SELECT id FROM keyword WHERE title = '".db::escape($keyword)."')");
			if (@$c>0) {
				echo '"exists"';
				exit;
			}
		}

		$count=30;
		$options=array('http' => array('user_agent' => 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/29.0.1547.65 Chrome/29.0.1547.65 Safari/537.36'));
		$context=stream_context_create($options);
		$urls = array();
		foreach (explode("\n", $keyword) as $line) {
			$kw=trim($line);
			if(!$kw) continue;
			$r=file_get_contents('http://www.google.com/search?q='.urlencode(preg_replace('/[\s]+/', '+', $kw)).'&hl=en&tbm=isch', false, $context);
			#preg_match_all('!/imgres\?imgurl=([^\&]+)[^\s]*imgrefurl=([^\&]+).+?<div class="rg_meta">(.+?)</div>!is', $r, $m);
			preg_match_all('!class="rg_meta[^\>]+>(.+?)</div>!is', $r, $m);
			if(empty($m[1])) continue;

			$i=0;
			foreach($m[1] as $k=>$jsonEnc) {
				$obj = json_decode($jsonEnc);
				if(is_object($obj)){
					#получаем google thumbnail ID
					$tu=preg_replace('!^.+?tbn\:(.+?)$!si','$1',$obj->tu);
				}else{
					continue;
				}
				#if($i++>=$count) break;
				
				#исключаем wikipedia
				if(!preg_match('/wikipedia\.[a-z]{2,3}/i',$obj->ru)) 
					$urls[]=array($obj->ru, $kw, $obj->s, $obj->ou, $tu, $obj->pt);
			}
		}
		return (object)array('json'=>json_encode($urls));
	}
	/*
		обрезает изображение
	*/
	function crop($url,$playicon,$scale,$x1,$y1,$width,$height){
		if(strpos($url,'/')!==false||strpos($playicon,'/')!==false){
			return false;
		}
		$status=false;
		$path=module::$path.'/images/files/images/'.$url;
		if($width){
			list(,,$t,)=getimagesize($path);
			if($t==IMAGETYPE_GIF)
				$src=imagecreatefromgif($path);
			elseif($t==IMAGETYPE_JPEG)
				$src=imagecreatefromjpeg($path);
			elseif($t==IMAGETYPE_PNG)
				$src=imagecreatefrompng($path);
			$width/=$scale;
			$height/=$scale;
			$dest=imagecreatetruecolor($width,$height);
			imagecopyresampled($dest,$src,0,0,$x1/$scale,$y1/$scale,$width,$height,$width,$height);
			if($t==IMAGETYPE_GIF)
				$status=imagegif($dest,$path);
			elseif($t==IMAGETYPE_JPEG)
				$status=imagejpeg($dest,$path,80);
			elseif($t==IMAGETYPE_PNG)
				$status=imagepng($dest,$path,80);
		}
		if(!empty($playicon)){
			$playicon_path=module::$path.'/images/files/images/'.$playicon;
			if(file_exists($playicon_path)){
				$status=rename($playicon_path,$path);
			}else{
				$playicon=null;
			}
		}
		if($status){
			shell_exec('rm '.module::$path.'/images/files/images*_*/'.$url);
			db::query("UPDATE `".PREFIX_SPEC."imgs` SET `sha1`='".sha1_file($path)."' WHERE `url`='".db::escape($url)."' LIMIT 1");
		}
		return;
	}
	/*
		накладывание play icon на изображение
	*/
	function playicon($url,$scale,$x1,$y1,$width,$height,$drop){
		if(strpos($url,'/')!==false){
			return false;
		}
		$path=module::$path.'/images/files/images/'.$url;
		if($drop){
			@unlink($path);
		}else{
			$playiconfile='playicon_'.$url;
			$temp=microtime(true).'_'.$url;
			copy($path, $pathtmp=module::$path.'/images/files/images/'.$temp);
			module::exec('images/admin',array('act'=>'crop','url'=>$temp,'playicon'=>'','scale'=>$scale,'x1'=>$x1,'y1'=>$y1,'width'=>$width,'height'=>$height),1);
			list(,,$t,)=getimagesize($pathtmp);
			if($t==IMAGETYPE_GIF)
				$src=imagecreatefromgif($pathtmp);
			elseif($t==IMAGETYPE_JPEG)
				$src=imagecreatefromjpeg($pathtmp);
			elseif($t==IMAGETYPE_PNG)
				$src=imagecreatefrompng($pathtmp);
			$dest=imagecreatetruecolor(470,246);
			imagecopyresampled($dest,$src,0,0,0,0,470,246,imagesx($src),imagesy($src));
			$play=imagecreatefrompng(module::$path.'/images/tpl/files/icons/play.png');
			imagecopy($dest,$play,199,87,0,0,72,72);
			imagejpeg($dest,module::$path.'/images/files/images/'.$playiconfile,80);
			unlink($pathtmp);
			echo $playiconfile;
		}
		exit;
	}
	/*
	 * Инструмент для создания thumbnail картинок для все базы
	*/ 
	function bulkThumbnail($size){
		#Получаем список запущенных скриптов
			$log=shell_exec("ps aux | grep thumbnail.php");
			$log=explode("\n",$log);
			foreach($log as $k=>$v){
				$v=trim($v);
				if($v=='' or strpos($v," grep ")!==false)unset($log[$k]);
			}
			$log=implode($log);
		if($size){
			$cn=db::qrow("SELECT COUNT(distinct url) FROM `".PREFIX_SPEC."imgs`")[0];
			$found=shell_exec("ls ".PATH."/modules/images/files/$size | wc -l");
			if(trim($log)==""){
				$script="php ".module::$path.'/images/admin/sh/thumbnail.php "'.$size.'"';
				#print $script;exit;
				shell_exec("nohup $script >/dev/null 2>/dev/null & echo $!");
			}
		}
		$this->template='template';
		#Получаем возможные размеры изображений
			$size=scandir(PATH."/modules/images/files/");
			foreach($size as $k=>$v){
				if(!preg_match("!^images.!",$v))unset($size[$k]);
			}
		return[
			'size'=>$size,
			'count'=>@$cn,
			'found'=>@$found,
			'log'=>@$log,
		];
	}
	/*
	 * Заполняем размер файла у изображений
	 */
	function fillFilesize($all=false){
		$this->template='template';
		$i=$j=0;
		$imgs = db::query("SELECT * FROM `".PREFIX_SPEC."imgs`".(!$all?' WHERE `filesize` = 0':''));
		while ($img = db::fetch($imgs)){
			$j++;
			if (file_exists(PATH.'modules/images/files/images/'.$img->url)){
				$i++;
				$size = filesize(PATH.'modules/images/files/images/'.$img->url);
				db::query("UPDATE `".PREFIX_SPEC."imgs` SET `filesize` = '{$size}' WHERE `id`='{$img->id}'");
			}
		}
		return [
			'count' => $j,
			'filled'=>$i,
		];
	}
}
