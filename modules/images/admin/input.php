<?php
/*
* Должен возвращать
* $this->data - объект обработанных входных переменных
* $this->act - какую функцию обработки используем
*/
class images_admin extends control{
	function __construct($input=''){ # $input - объект входных переменных от других модулей
		set_time_limit(0);
		$this->data=(object)array();
		if(!empty($input->images)&&$input->act=='edit'){
			$input->act='save';
		}
		$this->act=$input->act;
		switch ($input->act) {
			case 'install':
				break;
			case 'fillFilesize':
				$this->data = (object) array(
					'all' => !empty($input->all) ? true : false,
				);
				break;
			case 'del':
				if (isset($input->ajax)) {
					$imid = @(int) $_POST['imid'];
					$ajax = true;
				} else {
					$imid = @(int) $input->imid;
					$ajax = false;
				}
				$this->data = (object) array(
					'imid' => !empty($imid) ? $imid : 0,
					'ajax' => $ajax,
				);
				break;
			case 'delByPid':
				if ($input->easy)
					$this->data = array('pid' => (int) $input->pid, 'tbl' => @$input->tbl);
				break;
			case 'delRequest':
				$this->data = (object) array(
					'imid' => !empty($input->imid) ? @(int) $input->imid : 0,
				);
				break;
			case 'delImages':
				$this->data = (object) array(
					'imid' => !empty($input->imid) ? @(int) $input->imid : 0,
					'all' => empty($input->one) ? true : false,
				);
				break;
			case 'moderate':
				$this->data = [
					'id' => @(int) $input->id,
					'title' => @$input->title,
					'cat' => @db::escape($input->cat),
					'tag' => @$input->tag,
					@$input->type,
					@$input->quality,
					'submit' => @$input->submit,
					'updateId' => @(int) $input->updateId,
				];
				break;
			case 'moderateByCat':
				$this->data = [
					db::escape(@$input->cat),
					@$input->bindCat,
					empty($input->page) ? 1 : $input->page,
					(int) cookiePage($input, 'num', 20),
					@$input->submit,
					@$input->ids,
					@$input->category,
				];
				break;
			case 'bulkDell':
				$this->data = [
					(!isset($input->status) or $input->status === '') ? -1 : $input->status,
					140,
					empty($_GET['page']) ? 1 : $_GET['page'],
					empty($_POST['approve']) ? false : $_POST['approve'],
					@$input->cat,
					@$input->delCat,
				];
				break;
			case 'edit':
				# show form
				$this->data = (object) array(
					'pid' => isset($input->pid) ? (int) $input->pid : 0,
					'tbl' => @$input->tbl,
					'key' => @$input->key,
				);
				break;
			case 'save':
				$this->data = (object) array(
					'pid' => (int) $input->pid,
					'title' => empty($input->title) ? '' : $input->title,
					'tbl' => $input->tbl,
					'images' => $input->images,
					'type' => (isset($input->images['files']) && @$input->images['files'] !== false) ? 'saveFile' : 'save',
				);
				break;
			case 'updateKeyword':
				$this->data = (object) array(
					'pid' => (int) $input->pid,
					'keywords' => $input->keywords,
					'tbl' => $input->tbl,
				);
				break;
			case 'listofimages':
				if (!$input->easy && @!$_POST['easy'])
					die;
				$this->data = (object) array(
					'pid' => isset($input->pid) ? (int) $input->pid : 0,
					'tbl' => @$input->tbl,
					'image_sort' => @$_POST['image_sort'],
				);
				break;
			case 'checkStatus':
				$this->act = 'ImageLoadStatus';
				$this->data = (object) array(
					'tbl' => @db::escape($_POST['tbl']),
					'pid' => @(int) $_POST['pid'],
					'uid' => @(int) $_POST['uid'],
				);
				break;
			case 'searchRequest':
				$this->data = (object) array(
					'keyword' => empty($_POST['keywords']) ? false : $_POST['keywords'],
					'nocheck' => !empty($_POST['nocheck']) && $_POST['nocheck'] == 1,
				);
				break;
			case 'crop':
				$this->data = (object) array(
					'url' => $input->url,
					'playicon' => $input->playicon,
					'scale' => (float) $input->scale,
					'x1' => (int) $input->x1,
					'y1' => (int) $input->y1,
					'width' => (int) $input->width,
					'height' => (int) $input->height,
				);
				break;
			case 'playicon':
				$this->data = (object) array(
					'url' => $_POST['url'],
					'scale' => !empty($_POST['scale']) ? (float) $_POST['scale'] : false,
					'x1' => !empty($_POST['x1']) ? (int) $_POST['x1'] : 0,
					'y1' => !empty($_POST['y1']) ? (int) $_POST['y1'] : 0,
					'width' => !empty($_POST['width']) ? (int) $_POST['width'] : 0,
					'height' => !empty($_POST['height']) ? (int) $_POST['height'] : 0,
					'drop' => (int) $_POST['drop'],
				);
				break;
			case 'bulkThumbnail':
				$this->data = [
					'size' => @preg_replace("![^0-9a-z_]!", '', $input->size),
				];
				break;
			default :
				$this->act = '';
				break;
		}
	}
}
