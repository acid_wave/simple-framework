<?php
namespace images_admin;
use module,db,url,stdclass;


/*
	определяет доступное имя картинки без расширения файла
*/
function getImageName($title){
	static $lastTitle;
	static $i;
	if(!isset($i)||@$lastTitle!=$title) $i=0;
	$lastTitle=$title;
	$title=preg_replace("![^\w\d\s]!u",'',mb_strtolower($title,'UTF8'));
	$title=preg_replace("!\s+!",'-',$title);
	$go=true;
	while($go){
		$go=false;
		$file=($i==0?$title:"$title-$i").'.';
		$i++;
		if(\images\thumbnail::srcExists($file)!==false) $go=true;
	}
	return $file;
}
/*
	переименовывает скачанный файл в соотвествии с форматом mime type
*/
function formatRename($file,$imgdata){
	preg_match('!image/(\w+)!i', $imgdata['mime'], $m);
	$ext=($m[1]=='jpeg')?str_replace('e', '', $m[1]):$m[1];;
	rename($file,$name=$file.$ext);
	return $name;
}
/*
	Проверка существования картинки в базе по хэшу
*/
function imageExist($hash,$tbl,$pid){
	list($id)=db::qrow("SELECT `id` FROM `".PREFIX_SPEC."imgs` WHERE `tbl`='{$tbl}' && `pid`='{$pid}' && `sha1`='{$hash}'");
	return (bool)$id;
}
/*
 *  Проверка на существование картинки в бд, если есть - не грузим заново, а делаем запись с имеющейся уже
 */
function storeDBOnly($url,$pid,$tbl,$user,$kid,$skip=false){
	$isset = db::qfetch("SELECT * FROM `".PREFIX_SPEC."imgs` WHERE `sha1`='".sha1_file($url)."'");
	if(!empty($isset->id)){
		if(!$skip){
			unset($isset->id);
			$isset->pid=$pid;
			$isset->tbl=$tbl;
			$isset->uid=$user;
			$isset->date=date("Y-m-d");
			$isset->kid=$kid;
			foreach($isset as $k=>$v) {
				$fields[]="`".$k."`='".$v."'";
			}
			db::query("INSERT INTO `".PREFIX_SPEC."imgs` SET ".implode(", ", $fields));
		}
		return true;
	}else
		return false;
}
/*
	проверяет права на запись в каталоги для картинок
*/
function dirWritable($dir){
	if(!is_writable($dir)){
		return false;
	}else{
		foreach(scandir($dir) as $file){
			if($file{0}=='.'||!is_dir($dir.$file)){
				continue;
			}
			if(!is_writable($dir.$file)){
				return false;
			}
		}
	}
	return true;
}
/*
	создает новый кейворд
*/
function createKeyword($title,$pid,$tbl='post'){
	$kid=(int)@db::qfetch("SELECT id FROM keyword WHERE TRIM('  ' FROM title) = '".$title."'")->id;
	if(!$kid){
		db::query("INSERT INTO keyword (title) VALUES ('".trim($title)."')");
		$kid=(int)db::insert();
                db::query("INSERT IGNORE INTO `".PREFIX_SPEC."keyword2post` SET `pid`='{$pid}',`tbl`='{$tbl}',`kid`='{$kid}'");
	}
	return $kid;
}
/*
	обновляет поля gtitle, text
*/
function textAttributesUpdate($images,$pid=false,$tbl=false){
	if(!$pid||!$tbl) return;
	if(is_array($images['title_updade']))
		foreach ($images['title_updade'] as $id => $txt) {
			db::query("
				UPDATE `".PREFIX_SPEC."imgs` SET `gtitle`='".db::escape($txt)."'
				WHERE `id`='".db::escape($id)."' && `tbl`='".db::escape($tbl)."' && `pid`='".db::escape($pid)."'
				LIMIT 1");
		}
	if(is_array($images['text_update']))
		foreach ($images['text_update'] as $id => $txt) {
			db::query("
				UPDATE `".PREFIX_SPEC."imgs` SET `text`='".db::escape($txt)."'
				WHERE `id`='".db::escape($id)."' && `tbl`='".db::escape($tbl)."' && `pid`='".db::escape($pid)."'
				LIMIT 1");
		}
}
function byTitle($a,$b){
	return strcasecmp($a->title,$b->title);
}
/**
 * getimagesize + SVG format + webp format
 * @return array
 */
function getimagesizeExt($path){
	$mime=mime_content_type($path);
	if($mime=='image/svg+xml'){
		if(($xmlo=simplexml_load_file($path))===false) return false;
		$at=$xmlo->attributes();
		$d=[
			(string)$at->width,
			(string)$at->height,
			'mime'=>$mime,
		];
	}elseif($mime=='image/webp'){
		$res=imagecreatefromwebp($path);
		$w=imagesx($res);
		$h=imagesy($res);
		imagedestroy($res);
		$d=[
			$w,
			$h,
			'mime'=>$mime,
		];
	}else $d=getimagesize($path);
	return $d;
}
/*
 * Check achievment for upload images
 */
function checkAchievement($uid) {
	if (!$uid) return false;
	$ua = new \user\achievement($uid);
	if ($ua->getAchievement('upload')) return $ua->getAchievement('upload');
	db::query("SELECT * from `".PREFIX_SPEC."imgs` WHERE `uid` = {$uid}");
	if (db::affected()>=10) return $ua->setAchievement ('upload');
}