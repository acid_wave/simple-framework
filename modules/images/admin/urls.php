<?php
#url модуля
$urls->imagesAdminDelRequest=HREF.'/?module=images/admin&act=delRequest&imid=$imid';
$urls->imagesAdminDel=HREF.'/?module=images/admin&act=del&imid=$imid';
$urls->imagesBulkDell=HREF.'/?module=images/admin&act=bulkDell&status=$status&cat=$cat';
$urls->imagesModerate=HREF.'/?module=images/admin&act=moderate&id=$id';
$urls->images_dellWithoutFile=HREF.'/?module=images/admin&act=dellWithoutFile';
$urls->imagesDellNotApproved=HREF.'/?module=images/admin&act=dellNotApproved';
$urls->imagesBulkThumbnail=HREF.'?module=images/admin&act=bulkThumbnail';
$urls->imagesFillFilesize=HREF.'?module=images/admin&act=fillFilesize';
$urls->imagesModerateByCat=HREF.'?module=images/admin&act=moderateByCat&cat=$curl&bindCat=$bindCat';
$urls->imagesModerateTags=HREF.'?module=images/admin&act=moderateTags&cat=$curl';
