<script type="application/javascript" src="<?=HREF?>/files/images/admin/js/search.js"></script>
<script type="application/javascript" src="<?=HREF?>/files/images/admin/js/crop.js"></script>
<script type="application/javascript" src="<?=HREF?>/files/images/admin/js/jquery-ui.js"></script>
<script type="application/javascript" src="<?=HREF?>/files/images/admin/js/jquery.imgareaselect.min.js"></script>
<script type="application/javascript">
	function insertImageToEditor(obj,type){
		var parent=$(obj).parents('.imageBox');
		var imgObj=parent.find('img');
		var size=parent.find('select option:selected').val();
		if(imgObj.data("type")=="gif"||size=='original'){
			var imgSrc=window.location.basepath+"images/"+imgObj.data("url");
		}else{
			var imgSrc=window.location.basepath+"images600_/"+imgObj.data("url");
		}

		var imgUrl=imgObj.attr('data-url');
		var galleryUrl=imgObj.attr('data-gurl');
		var title=imgObj.attr('alt');
		if(!title){
			//try get the title from input
			title=$('input[name="title"]').val();
		}

		var html='<img alt="'+title+'" src="'+imgSrc+'"/>';

		if(type=='link'){
			html='<a href="'+galleryUrl+'" data-tbl="'+imgObj.attr('data-tbl')+'" data-pid="'+imgObj.attr('data-pid')+'" data-url="'+imgUrl+'" name="'+imgUrl+'" title="'+title+'">'+html+'</a>';
		}

		if(tinyMCE!=undefined){
			tinyMCE.execCommand(
				'mceInsertContent',
				false,
				html	
			);
		}
	}
</script>
<style type="text/css">
	/* search result */
	@import '<?=HREF?>/files/images/admin/css/jquery-ui.css';
	/* crop */
	@import '<?=HREF?>/files/images/admin/css/imgareaselect/imgareaselect-default.css';
	.search_result img {
		max-width: 250px;
		max-height: 200px;
		cursor: pointer;
	}
	.search_result .search_img {
		width: 250px;
		height: 250px;
		border: 1px solid #ccc;
		margin: 5px 0 0 5px;
		text-align: center;
		vertical-align: middle;
		display: inline-block;
		position: relative;
	}
	.search_result_image {
		text-align: center;
	}
	.search_result_image img {
		max-width: 95%;
		max-height: 95%;
	}
	.search_result .search_img div {
		position: absolute;
		bottom: 0;
		height: 40px;
		padding: 5px;
	}
	.crop_image {
		text-align: center;
	}
	.crop_image img {
		max-width: 100%;
		max-height: 100%;
	}
</style>
<div id="status"></div>
<?include $template->inc('edit/listimages.php');?>
<div style="clear:both;"></div>
<?include $template->inc('edit/uploadimages.php');?>
