<?
switch ($data->status) {
	case 'error':
	{?>
		<div class="alert alert-danger"><?=$data->reason?></div>
	<?}
		break;
	case 'ok':
	{?>
	<div class="alert alert-info"><?=$data->act=='delRequest'?'Found':'Deleted'?> <?=$data->count?> image(s)</div>
	<table class="table table-striped table-sm">
		<thead>
			<tr>
				<td>Image ID</td>
				<td>Image Url</td>
				<td>Post title</td>
			</tr>
		</thead>
		<tbody>
			<?foreach ($data->images as $image){?>
			<tr>
				<td><?=$image->imid?></td>
				<td><?=$image->imurl?></td>
				<td><a href="<?=url::post($image->url)?>" target="_blank"><?=$image->title?></a></td>
			</tr>
			<?}?>
		</tbody>
	</table>
	<?}
		break;
}
?>
