<?
$img=$data->img;
if(empty($img)){print "<h1>Nothing to moderate</h1>";return;}
$post=$data->post;
$title=$img->gtitle?$img->gtitle:$post->title;
?>
<style>
.width400{width:400px;}
.width240{width:240px;}
.link{cursor: pointer;}
.tabePadding td{padding-left:10px;}
</style>
<form action="<?=url::imagesModerate()?>&id=<?=$data->next->id?>" method="post" id="myForm">
<table>
	<tr>
		<td width=200 valign="top">
			<b>Category:</b><br>
				<?foreach($data->mainCats as $v){?>
					<span><input name="cat" type="radio" value="<?=$v->url?>"> <span class="link" onclick="javascript:selectInp(this)"><?=$v->title?></span></span><br>
				<?}?>
		<td width=200 valign="top">
			<b>Tags:</b><br>
				<input class="form-control" id="itagPick" type="text" placeholder="add tag" onchange="newTag(this.value)">
				<span id="tags"></span>
				<a href="" onclick="return dellAllTags();">dell all</a><br>
		</td>
		<td width=10>&nbsp;</td>
		<td valign="top">
			<b>Title:</b><br>
			<input class="form-control" class="width400" id="title" name="title" type="text" placeholder="title" value="<?=$title?>"><br>
			<b>Quality:</b><br>
				<?foreach($data->qualitys as $k=>$v){?>
				<span><input name="quality" type="radio" value="<?=$v->url?>"> <span class="link" onclick="javascript:selectInp(this)"><?=$v->title?></span></span><br>
				<?}?>
			<b>Type</b><br>
				<table class="tabePadding"><tr><td>
				<?
				$types="Illustration,Background,Pattern,Silhouette,Black & White";
				foreach($data->types as $k=>$v){
					if($k%2==0)print "<td>";
					?>
					<span><input type="radio" name="type" value="<?=$v->url?>"> <span class="link" onclick="javascript:selectInp(this)"><?=$v->title?></span></span><br>
				<?}?>
				</td></tr></table>
			<br>
			<input class="width240" type="submit" name="submit" value="delete">
			<input class="width240" type="submit" name="submit" value="save">
			<input type="hidden" name="updateId" value="<?=$img->id?>">
			
			<br><br><img src="<?=url::imgThumb("450_",$img->url)?>" width=500><br>
		</td>
	</tr>
</table>
</form>
<script>
function selectInp(el){
	if($(el).parent().children("input").prop('checked'))
		$(el).parent().children("input").prop('checked', false);
	else
		$(el).parent().children("input").prop('checked', true);
}
var tagStr="<input type=\"text\" value=\"%title%\" name=\"tag[]\"> <a href=\"\" onclick=\"return dellTag('%title%')\">dell</a><br>";
function dellTag(title){
	title=title.replace("+","\\+").replace("&","\\&amp;")
	var patern=tagStr.split("%title%").join(title);
	console.log(title);
	var ar={"(":"\\(",")":"\\)","]":"\\]","[":"\\["};
	for(var i in ar){patern=patern.replace(i,ar[i]);}
	var re = new RegExp(patern,"g");
	$("#tags").html($("#tags").html().replace(re,""));
	return false;
}
function newTag(title){
	$("#tags").append(tagStr.split("%title%").join(title));
	$("#itagPick").val('');
	return false;
}
<?if(!empty(@$data->cat['tags']))foreach($data->cat['tags'] as $v){
	$title=str_replace("\n",' ',$data->catTitles[$v]);
	print "newTag('$title');";
}?>
function dellAllTags(){
	$("#tags").html('');
	return false;
}
$(document).on("click", ":submit", function(e){
	if($(this).val()=='delete')return true;
	tagExists=0;
	$('#myForm :input').each(function() {
		if(this.name=="tag[]"){
			if($(this).val()!='')tagExists=tagExists+1;
		}
    });
    if($("#title").val()==""){
		alert("Please type title!");
		return false;
	}else if(!radioChacked('cat')){
		alert("Please choose category!");
		return false;
	}else if(!radioChacked('type')){
		alert("Please choose type!");
		return false;
	}else if(!radioChacked('quality')){
		alert("Please choose quality!");
		return false;
	}else if(tagExists<1){
		alert("Please add minimum 1 tags!");
		return false;
	}
	return true;
});
function radioChacked(name){
	var chacked=false;
	$('#myForm :input').each(function() {
		if(this.name==name){
			if($(this).prop('checked'))chacked=true;
		}
	});
	return chacked;
}

</script>
