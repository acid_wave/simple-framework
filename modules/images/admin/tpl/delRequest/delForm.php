<style>
	.modal{color: #212529;}
	.modal a{color:#007bff;}
	.modal a:hover{color: #0056b3;}
</style>
<div class="modal fade" id="deleteDialog" tabindex="-1" role="dialog" aria-labelledby="deleteDialogTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="deleteDialogTitle">Delete images</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="deleteDialogContent">

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning" id="deleteDialogDeleteOne">Delete this one</button>
				<button type="button" class="btn btn-danger" id="deleteDialogDeleteAll">Delete all images</button>
				<button type="button" class="btn btn-success" id="deleteDialogButton" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var imid=0;
	$('#deleteDialog').on('show.bs.modal', function (e) {
		imid = $(e.relatedTarget).data('imid');
		$.get('<?=HREF?>', {module: 'images/admin', act: 'delRequest', imid: imid}, function (data) {
			$("#deleteDialogTitle").html('Delete images');
			$("#deleteDialogContent").html(data);
			$('#deleteDialog').modal('handleUpdate');
		});
	});
	$('#deleteDialogDeleteAll').on('click', function (e) {
		$.get('<?=HREF?>', {module: 'images/admin', act: 'delImages', imid: imid}, function (data) {
			$("#deleteDialogTitle").html('Images deleted');
			$("#deleteDialogContent").html(data);
			$("#deleteDialogDeleteOne, #deleteDialogDeleteAll").addClass('d-none');
			$("#deleteDialogButton").html('Close');
			$('#deleteDialog').modal('handleUpdate');
		});
	});
	$('#deleteDialogDeleteOne').on('click', function (e) {
		$.get('<?=HREF?>', {module: 'images/admin', act: 'delImages', imid: imid, one: true}, function (data) {
			$("#deleteDialogTitle").html('Images deleted');
			$("#deleteDialogContent").html(data);
			$("#deleteDialogDeleteOne, #deleteDialogDeleteAll").addClass('d-none');
			$("#deleteDialogButton").html('Close');
			$('#deleteDialog').modal('handleUpdate');
		});
	});
</script>