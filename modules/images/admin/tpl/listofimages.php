<?
if(!empty($data->images)){
	foreach($data->images as $img){?>
		<div class="imageBox">
			<div style="margin-bottom: 2px;">
				<select style="width: 84px;height: 25px;">
					<option value="original">original</option>
					<option value="600">600px</option>
				</select>
				<a href="#insert-link" title="paste as link" onclick="insertImageToEditor(this,'link')">link</a>
				<a href="#insert" title="paste" onclick="insertImageToEditor(this)">paste</a>
			</div>
			<img title="click to insert" alt="<?=$img->title?>" src="<?=url::imgThumb('250_',$img->url)?>" 
				data-gurl="<?=url::img($data->tbl,$data->pid,$img->url)?>" 
				data-url="<?=$img->url?>"
				data-tbl="<?=$data->tbl?>"
				data-pid="<?=$data->pid?>"
				data-id="<?=$img->id?>"
				<?=$img->gif?'data-type="gif"':''?>
				onclick="insertImageToEditor(this)" />
			<br/><input type="text" name="image_title[<?=$img->id?>]" value="<?=$img->title?>" size="10"/>
			<br/><input type="text" name="image_description[<?=$img->id?>]" value="<?=$img->text?>" size="10"/>
			<a href="#del" data-id="<?=$img->id?>" onclick="ajaxDelImage(this);return false;">del</a>
			<a style="float:right;" href="#crop" data-id="<?=$img->id?>" data-name="<?=$img->url?>" onclick="cropImageUI(this);return false;">crop</a>
		</div>
	<?}
}