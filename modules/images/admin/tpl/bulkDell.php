<script>
	function setApprove(objId){
		var input = $('#input'+objId);
		var visual = $('#visual'+objId);
		if(input.attr('value')!=2){
			input.attr('value','2');
			visual.attr('class','visual active');
		}else{
			input.attr('value','1');
			visual.attr('class','visual passive');
		}
	}
</script>
<?if(@$data->cat){print "<h1>category:<a href='".url::imagesModerateByCat($data->cat,$data->status)."'>$data->cat</a></h1>";}?>
<div class="bar">
	<table  class="moderPanel">
		<tr>
			<td><a <?=($data->currentStatus=='-1')?'class="current" ':''?> href="<?=url::imagesBulkDell(-1)?>">All</a></td>
			<td><a <?=($data->currentStatus=='1')?'class="current" ':''?> href="<?=url::imagesBulkDell(1,$data->cat)?>">Approved (<?=$data->count[1]?>)</a></td>
			<td><a <?=($data->currentStatus=='2')?'class="current" ':''?>href="<?=url::imagesBulkDell(2,$data->cat)?>">Not approved (<?=$data->count[2]?>)</a></td>
			<td><a <?=($data->currentStatus=='0')?'class="current" ':''?>href="<?=url::imagesBulkDell(0,$data->cat)?>">Not defined (<?=$data->count[0]?>)</a></td>
		</tr>
	</table>
</div>
<div class="moderatebox">
	<form method="post" action="">
	<input type="button" value="submit" onclick="javascript:form.submit();" />
	<div class="line"></div>
	<?
	$i=-1;
	foreach($data->imgs as $img){
		if(!(++$i%7)&&$i) echo '<div class="line"></div>';?>
		<div id="visual<?=$img->id?>" class="visual <?=$img->approve!=2?'passive':'active'?>">
			<input id="input<?=$img->id?>" type="hidden" name="approve[<?=$img->id?>]" value="<?=$img->approve?>" />
			<span>&#10005;</span>
			<div>
				<img id="img<?=$img->id?>" src="<?=url::imgThumb('450_',$img->url)?>" class="" onclick="javascript:setApprove(<?=$img->id?>);"/>
				<p><a target="_blank" href="<?=url::imagesModerate($img->pid)?>"><?=$img->keyword?></a></p>
			</div>
			<script>
				$('#img<?=$img->id?>').dblclick(function(){
						$('#visual<?=$img->id?>').append('<img id="imgFull<?=$img->id?>" class="imgpopup" src="<?=url::image($img->url)?>" />');
						var popupObj = $('#imgFull<?=$img->id?>');
						popupObj.fadeIn();
						popupObj.click(function(){
								$(this).fadeOut();
								$(this).remove();
							});
					});
			</script>
		</div>
	<?}?>
	<div class="line"></div>
	<input type="button" value="Save" onclick="javascript:form.submit();" />
	<?if($data->cat){?>
		<input type="hidden" name='cat' value="<?=$data->cat?>">
		<input type="submit" name="delCat" value="Delete Selected from category" onclick="javascript:form.submit();" />
	<?}?>
	</form>
	<?=$data->paginator?>
</div>
