<?
define ('SITEOFF',1);
include __DIR__.'/../../../../index.php';
set_time_limit(0);
define('IRDL_LOG',TMP.'/images/admin/download/redownload.log');
@mkdir(PATH."tmp/images/admin/download/",0777,true);
@mkdir(PATH."modules/images/files/images/",0777);
$start=intval(@$argv[1]);
$limit=@$argv[2];
#Получаем все изображения с диска
	$files=array_flip(scandir(PATH."/modules/images/files/images/"));
#Получаем все изображения из базы
	$q=db::query("SELECT id,url,source FROM `".PREFIX_SPEC."imgs` WHERE id>=$start ORDER BY id ASC");
	while($d=db::fetch($q)){
		if(!isset($files[$d->url])){
			@$cn++;if($cn>$limit)exit;
			echo "download {$d->source} -> {$d->url}";
			$imgInfo=download($d->source,PATH."modules/images/files/images/$d->url");
			if($imgInfo){
				$nurl=nurl($d->url,$imgInfo->type);
				$status=mupdateSql($nurl,$d->id,$imgInfo);
				rename(PATH."modules/images/files/images/$d->url",PATH."modules/images/files/images/$nurl");
				echo " - done\n";
			}else{
				echo " - fail\n";
			}
		}else{
			echo "skip {$d->url} - exists\n";
		}
	}

#Функция закачки картинки
	function download($link,$dest){
		$ch=curl_init($link);
		$urlData=parse_url($link);
		curl_setopt_array($ch,array(
				CURLOPT_REFERER=>$urlData['scheme'].'://'.$urlData['host'].'/',
				CURLOPT_FOLLOWLOCATION=>1,
				CURLOPT_RETURNTRANSFER=>1,
				CURLOPT_HEADER=>0,
				CURLOPT_USERAGENT=>'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1944.0 Safari/537.36',
			)
		);
		$data=curl_exec($ch);
		if($data===false){
			file_put_contents(IRDL_LOG,date("Y-m-d H:i:s")." {$link} download error [CURL: ".curl_error($ch)."]\n",FILE_APPEND);
			$imgInfo=false;
		}else{
			file_put_contents($dest,$data);
			$imgInfo=imageinfo($data,$dest);
			if(!$imgInfo) {
				file_put_contents(IRDL_LOG,date("Y-m-d H:i:s")." {$link} error format\n",FILE_APPEND);
				unlink($dest);
			}
		}
		curl_close($ch);
		return $imgInfo;
	}
#Узнаем размер и тип изображения
	function imageinfo($data,$url){
		$size=getimagesize($url);
		if($size){
			$imageTypeArray = array(
				0=>'UNKNOWN',
				1=>'gif',
				2=>'jpg',
				3=>'png',
				4=>'swf',
				5=>'psd',
				6=>'bmp',
				7=>'TIFF_II',
				8=>'TIFF_MM',
				9=>'JPC',
				10=>'JP2',
				11=>'JPX',
				12=>'JB2',
				13=>'SWC',
				14=>'IFF',
				15=>'WBMP',
				16=>'XBM',
				17=>'ICO',
				18=>'COUNT'  
			);
			$obj=(object)[
				'type'=>$imageTypeArray[$size[2]],
				'width'=>$size[0],
				'height'=>$size[1],
			];
		}elseif(($mime=mime_content_type($url)=='image/svg+xml')){
			$obj=(object)[
				'type'=>'svg',
				'width'=>0,
				'height'=>0,
			];
		}else{
			return false;
		}
		$obj->filesize=filesize($url);
		$obj->sha1=sha1_file($url);
		$exif=@exif_read_data($url);
		$obj->exif=$exif?json_encode($exif):'';
		return $obj;
	}
#Обновляем базу
	function mupdateSql($url,$id,$imgInfo){
		if(!$imgInfo->type)return false;
		db::query("UPDATE `".PREFIX_SPEC."imgs` SET 
			url='$url',
			sha1='$imgInfo->sha1',
			date=NOW(),
			width='$imgInfo->width',
			height='$imgInfo->height',
			filesize='$imgInfo->filesize',
			type='$imgInfo->type',
			exif='".db::escape($imgInfo->exif)."'
			WHERE
				id='$id' LIMIT 1
		");
		return true;
	}
#Получить новый урл с учетом типа картинки
	function nurl($url,$type){
		$e=explode(".",$url);
		unset($e[count($e)-1]);
		return implode(".",$e).".".$type;
	}
