<?php
#url модуля
$urls->image=HREF.'/images/$url';
$urls->imageDownload=HREF.'?module=images&act=download&file=$url&x=$width&y=$height';
$urls->imagePreDownload=HREF.'?module=images&act=preDownload&file=$url';
$urls->imgThumb='';#см. url_imgThumb()
$urls->image404=HREF.'/modules/images/files/404.png';

# examples size: 600_, 200_150, Origin
function url_imgThumb($size,$url){
	return HREF."/images{$size}/".preg_replace('!^(.+?)\.[^\.]+$!', '$1.jpg', $url);
}
