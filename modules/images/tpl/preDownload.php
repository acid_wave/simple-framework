<div class="container">
  <div class="modal fade" id="dModal" role="dialog" style="z-index: 99999;">
	<div class="modal-dialog">
	  <div class="modal-content">
		<div class="modal-header">
			<h4 class="modal-title">Download is beginning</h4>
			<button type="button" class="close" data-dismiss="modal">&times;</button>
		</div>
		<div class="modal-body">
			<p class="alert alert-success">Thank you. Please put the attribution for this file.</p>
			<input type="text" onclick="this.select();" class="form-control" value="<a href='<?=url::post($data->post->url)?>?ref=<?=$data->user->id?>'>Image credit</a>">
			<?if(defined('PAYPAL_ID')){?>
			<br><button class="btn btn-primary"><i class="fa fa-coffee"></i> Donate to author</button>
			<?}?>
		</div>
		<div class="modal-footer">
		  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>
	  </div>
	</div>
  </div>
</div>
<script>
$(document).ready(function(){
	$("#dModal").modal();
	var i=setInterval(function(){
		window.location="<?=url::imageDownload($data->file,(!empty($data->size)?$data->size:''))?>";
		clearInterval(i);
	},3000);
});
</script>