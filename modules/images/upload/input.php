<?php
namespace images\upload;
use control;

class input extends control{
	function __construct($input){
		if(empty($input->act)) $input->act='index';
		
		if($input->act=='index'){
			$this->data=[];
		}elseif($input->act=='upload'){
			$this->data=[
				'file'=>!empty($_FILES['file']['tmp_name'])?$_FILES['file']['tmp_name']:false,
				'title'=>!empty($_POST['title'])?$_POST['title']:'',
				'tags'=>!empty($_POST['tags'])?$_POST['tags']:'',
			];
		}elseif($input->act=='getTips'){
			$this->data=[
				'tagstr'=>!empty($input->tagstr)?$input->tagstr:'',
			];
		}else
			$this->act=false;
	}
}
