(function(){
	function genNewForm(file){
		var inner=$('.img-preview').parent();
		var newcloned=$('.img-preview').first().clone();
		var form=newcloned.find('form');
		var img=newcloned.find('img');
		var index=imageUpload.files.length;
		imageUpload.files[index]=file;
		//store file index in related array imageUpload.files
		form.attr('data-fileindex',index);
		//insert new cloned block
		inner.append(newcloned);

		//set preview
		var reader = new FileReader();
		reader.onload=function(instanceFileReader){
			img.attr('src',instanceFileReader.target.result);
			newcloned.css({'display':'block'});
			//set save button to visible
			$('#save-all').css({'display':'inline-block'});
		};
		reader.readAsDataURL(file);
	}

	function checkFormart(file){
		$('#message').empty();
		var match = ["image/jpeg", "image/png", "image/jpg", "image/gif", "image/svg+xml"];
		var valid=false;
		for (var i = match.length - 1; i >= 0; i--) {
			if(file.type==match[i]) {valid=true; break;}
		}
		if (!valid){
			$('#message').html('<div class="alert alert-warning" role="alert">Unvalid image format. Allowed formats: JPG, JPEG, PNG, GIF, SVG</div>');
			return false;
		}
		return true;
	}

	function validateForm(form){
		var title=$(form.title);
		var tags=$(form.tags);
		if(!title.val()){
			title.parent('.form-group').addClass('has-error');
			return false;
		}
		if(!tags.val()){
			tags.parent('.form-group').addClass('has-error');
			return false;
		}
		return true;
	}

	function save(form){
		if(!validateForm(form)) return;
		var formData=new FormData(form);
		formData.append('act','upload');
		formData.append('file',imageUpload.files[$(form).attr('data-fileindex')]);

		var formInner=$(form).parent();
		var progressbar=formInner.find('.progress');
		$(form).fadeOut(300,function(){
			progressbar.css({'display':'block'});
		});

		var xhr = new XMLHttpRequest();
		xhr.onload = xhr.onerror = function() {
			if(this.status==200){
				if(xhr.responseText) var res=JSON.parse(xhr.responseText);
				if(res.error){
					console.log(res.error);
					formInner.html('<div class="alert alert-danger">Upload error</div>');
				}else{
					formInner.html('<div class="alert alert-success">Done</div>');
				}
			}else{
				console.log("upload error "+this.status);
			}
		};
		xhr.upload.onprogress = function(event) {
			progressbar.children('div').css('width',event.loaded/event.total*100+'%');
		}
		xhr.open("POST", form.action, true);
		xhr.send(formData);
		
		return false;
	}

	function saveAll(){
		$('.img-preview form').each(function(){
			if($(this).attr('data-fileindex')==undefined) return;
			save(this);
		});
	}

	function getTips(e){
		var str=$(e).val();
		var form=$(e).parents('form');
		var action=form.attr('action');
		var tipsInner=form.find('.tips');
		var tagsTextarea=form.find('[name=tags]');
		var tags=str.split(',');
		tags=tags.map(function(a){ return a.trim(a); });
		if(tags.length>=3){
			let data={'act':'getTips','tagstr':str};
			$.post(action,data,function(response){
				if(!response) return;
				e.oninput=null; //disable event to request tips
				var tips=JSON.parse(response);
				if(tips.length){
					for (var i = tips.length - 1; i >= 0; i--) {
						tipsInner.append('<span class="btn btn-primary btn-xs" style="margin-right:0.1em;">'+tips[i]+'</span>');
					}
					tipsInner.find('span').click(function(){
						tags=tags.filter(function(a){
							return a!==''&&a!==' ';
						});
						tags.push($(this).html());
						//console.log(tags);
						tagsTextarea.val(tags.join(', '));
					});
				}
			});
		}
	}

	imageUpload={
		delete:function(item){
			if(!confirm('Are you sure?')) return;
			var block=$(item).parents('.img-preview');
			block.detach();
		},
		save:save,
		saveAll:saveAll,
		files:[],
		getTips:getTips,
	};

	$(document).ready(function (e) {
		$('#file').change(function() {
			for (var i = this.files.length - 1; i >= 0; i--) {
				if(!checkFormart(this.files[i])) continue;
				genNewForm(this.files[i]);
			}
		});
	});
})();
