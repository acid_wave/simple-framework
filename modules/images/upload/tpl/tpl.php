<?php
$tpl->title="Upload images";
?>
<script src="<?=HREF?>/modules/images/upload/tpl/files/js/upload-image.js"></script>
<div class="container" style="margin: 5em 0;">
	<div class="row">
		<div class="col-md-3">
			<h3>Upload images</h3>
			<p>JPG, PNG, GIF and SVG images up to <b><?=$data->config->maxsize?>B</b> with at least <b><?=$data->config->minpixels?></b> pixels along one side.</p>
			<form class="form-inline">
				<p>For <b>multiple</b> selections, press <b>Ctrl</b></p>
				<span class="btn btn-primary btn-file btn-lg">
					Select files<input type="file" name="file" id="file" required accept='image/*' multiple>
				</span>
				<button class="btn btn-lg btn-success" type="button" style="display: none;" id="save-all" onclick="imageUpload.saveAll();return false;">Save all</button>
			</form>
		</div>
		<div class="col-md-9">
			<h3>Type a title and tags</h3>
			<p>The title is the most important description of the image. It should contain the most accurate description of the image. The title should not contain extra words, but it should not consist of one word.</p>
			<p>Tags are keywords that describe your image. Each tag is as important as the image itself because without accurate tags, the image can not be found.</p>
			<div id="message"></div>
			<div class="row">
				<div class="col-12">
					<div class="img-preview" style="display: none;margin-bottom: 1em;">
						<div class="row">
							<div class="col-3">
								<img src="noimage" class="img-fluid">
							</div>
							<div class="col-9">
								<form method="post" enctype="multipart/form-data" action="<?=url::images_upload()?>" onsubmit="imageUpload.save(this);return false;">
									<div class="content">
										<div class="form-group">
											<input type="text" class="form-control" placeholder="Title..." required name="title">
										</div>
										<div class="form-group">
											<textarea class="form-control" rows="4" placeholder="tag 1, tag2, ... (Tags separated by comma)" required name="tags" oninput="imageUpload.getTips(this);""></textarea>
											<div class="tips" style="margin-top: 0.2em;"></div>
										</div>
										<button class="btn btn-default" onclick="imageUpload.delete(this)">Delete</button>
										<button class="btn btn-success" type="submit">Save</button>
									</div>	
								</form>
								<div class="progress" style="display: none;">
									<div class="progress-bar" role="progressbar" style="width: 0%;"></div>
								</div>
							</div>
						</div>
						<hr>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
