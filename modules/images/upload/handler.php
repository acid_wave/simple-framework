<?php
namespace images\upload;
use module,db,url,posts\tables,category\tags\tags,posts_admin\Sv;

include_once module::$path.'/posts/handler.php';
include_once module::$path.'/posts/admin/func.php';
include_once module::$path.'/category/tags/func.php';

class handler{
	function __construct(){
		$this->template='template';
		$this->headers=new \stdClass;
		$this->uhandler=module::exec('user',[],true)->handler;
		$this->user=$this->uhandler->user;
		if(!$this->user->id) $this->headers->location=url::userLogin();
	}
	function index(){

		return [
			'config'=>(object)[
				'maxsize'=>ini_get('upload_max_filesize'),
				'minpixels'=>100
			],
		];
		
	}
	function upload($file,$title,$tags){
		if(!$file) return ['message'=>['error'=>'upload_error']];
		$this->template='';
		$tbls=tables::init();

		$tags=array_map(function($a){
			return db::escape(strtolower(trim($a)));
		}, explode(',',$tags));

		$cats=[];
		$newTags=array_flip($tags);
		foreach (tags::get($tags) as $t) {
			$cats[]=$t->url;
			#определяем не существующие
			if(isset($newTags[$t->title])) unset($newTags[$t->title]);
		}		
		
		$cats=$cats+tags::insert($newTags);
		#записываем пост
		$postSaver=new Sv();
		$pid=$postSaver->save($tbls,false,$title,'',false,$cats,'','','',$this->user->id,0);
		#записываем картинки
		if($pid){
			$moduleImagesResponce=module::execData('images/admin',
				[
					'act'=>'save',
					'pid'=>$pid,
					'title'=>$title,
					'tbl'=>$tbls->post,
					'images'=>[
						'files'=>$file,
						'gtitle'=>$title,
					],
				]
			);
			$err=$moduleImagesResponce->error;
		}else
			$err='createPostFail';

		return [
			'message'=>[
				'error'=>$err,
			],
		];
	}
	function getTips($tagstr){
		$this->template='';
		if(empty($tagstr)) return;
		$tbls=tables::init();

		$tags=explode(",",$tagstr);
		$tags=array_map(function($a){
			return strtolower(trim($a));
		},$tags);

		foreach(tags::get($tags) as $t){
			$tagurls[]=$t->url;
		};

		if(!isset($tagurls)) return;
		#получаем идентификаторы постов у которых встречаются все указанные тэги одновременно
		db::query("
			SELECT cp.pid, COUNT(cp.pid) AS c FROM `{$tbls->category2post}` cp 
			WHERE cp.cid IN('".implode("','",$tagurls)."') 
			GROUP BY cp.pid HAVING c>1 LIMIT 3");
		while ($d=db::fetch()) {
			$pids[]=$d->pid;
		}
		if(!isset($pids)) return;
		#получаем идентификаторы тэгов для списка постов, исключая те что заданы на входе в функцию
		$tips=[];
		db::query("
			SELECT c.title FROM `{$tbls->category2post}` cp 
			INNER JOIN `{$tbls->category}` c ON c.url=cp.cid && c.parentId='tags'
			WHERE cp.pid IN('".implode("','",$pids)."')
			GROUP BY c.title");
		while ($d=db::fetch()) {
			$d->title=strtolower(trim($d->title));
			if(in_array($d->title, $tags)) continue;
			$tips[]=$d->title;
		}

		return ['tips'=>$tips];
	}
}

