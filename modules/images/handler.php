<?php
namespace images;
use module,db,url,stdclass,user\user;

require_once(__DIR__.'/func.php');
require_once module::$path.'/user/handler.php';

/*
 * Должен возвращать:
 * $this->data - объект переменных для вывода в шаблоне
 * $this->headers - объект для изменения заголовков при отдаче
 * $this->act - если есть несколько вариантов ответов
 */
class handler{
	static $resolution=[
		[250,0],
	];
	function __construct(){
		$this->template='';#Определяем в какой шаблон будем вписывать
		$this->headers=(object)array();
	}
	/*
		Генерация превью для картинки в гугле
	*/
	function genImg($img,$dir){
		if(!is_dir($storeDir=PATH."modules/images/files/googleBack")){mkdir($storeDir);chmod($storeDir,0777);}
		$filename="$storeDir/$img";
		set_time_limit(10);
		ini_set('memory_limit','1024M');
		list($pid,$tbl)=db::qrow("SELECT pid,tbl FROM `".PREFIX_SPEC."imgs` WHERE `url`='{$img}' LIMIT 1");
		list($title)=db::qrow("SELECT `title` FROM `{$tbl}` WHERE id='{$pid}'");
		list($otherimgs)=db::qrow("SELECT GROUP_CONCAT(url) FROM `".PREFIX_SPEC."imgs` WHERE tbl='{$tbl}' && pid='{$pid}' && url!='{$img}'");
		ob_start();
		if(makeBackground($img,$dir,$title,$otherimgs)!==false){
			$str=ob_get_contents();
			file_put_contents($filename,$str); @chmod($filename,0666);
		}
		ob_end_flush();
		die;
	}
	function hotlink($img){
		$this->template='';
		list($tbl,$pid)=db::qrow("SELECT `tbl`,`pid` FROM `".PREFIX_SPEC."imgs` WHERE `url`='$img' LIMIT 1");
		$this->headers->location=url::img($tbl,$pid,$img);
		return;
	}
	function mkThumb($img,$size,$easy){
		if(!$size) return;		
			
		if($src=thumbnail::srcExists($img)){
			if($easy) ob_flush();
			if($size=='Origin'){
				$image=mkorigin($src,true,false);
			}else{
				list($sizeW,$sizeH)=explode('_',$size);
				if(!$sizeW&&!$sizeH) return;
				$image=resize($src,$dest=true,$sizeW,$sizeH);
			}
			
			if(!$easy){
				header("Content-type: image/jpeg");
				print $image;
				die;	
			}
		}else{
			header("Content-type: image/png");
			#очищаем предыдущий вывод, на всякий случай
			ob_end_clean();
			die(file_get_contents(PATH.'modules/images/files/404.png'));
		}
		return;
	}
	function download($file,$x,$y){
		$file=thumbnail::$srcpath.'/'.basename($file);
		if($file=="" or !file_exists($file)){location(HREF);};
		if(empty($x)&&empty($y)){
			downloadHeaders($file);
			header("Content-type: ".mime_content_type($file));
			header("Content-Length: ".filesize($file));
			readfile($file);
		}else{
			downloadHeaders(preg_replace('!(.+\.)([^\.]+)$!','$1jpg',$file));
			header("Content-type: image/jpeg");
			resize($file,false,$x,$y);
		}
		die;
	}
	function preDownload($file,$pid,$tbl,$size){
		if(!$file) return;
		if(empty($tbl)) $tbl='post';
		$img=db::qfetch("SELECT i.* FROM `".PREFIX_SPEC."imgs` i 
			WHERE url='{$file}'".(!empty($pid)?" && pid='{$pid}' && tbl='{$tbl}'":'')." LIMIT 1");
		if(!$img) return;
		$post=db::qfetch("SELECT id,url FROM `{$img->tbl}` WHERE id='{$img->pid}'");
		list($c)=db::qrow("UPDATE ".PREFIX_SPEC."imgs SET `statDownload`= ".($img->statDownload+1)." WHERE id = {$img->id}");
		$user = user::init()->user;
		db::query("INSERT INTO `".PREFIX_SPEC."userDownloads` (`uid`, `imid`) VALUES ({$user->id},{$img->id}) ON DUPLICATE KEY UPDATE data = '". date('Y-m-d H:i:s',time())."'");
		return ['file'=>$file,'img'=>$img,'post'=>@$post,'size'=>$size,'user'=>$user];
	}
}
