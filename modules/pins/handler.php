<?php
namespace pins;
use module,db,url,cache,\user\user;

#функции
require_once(module::$path.'/pins/func.php');
#сторонние модули
require_once(module::$path.'/user/handler.php');
require_once module::$path."/posts/lists/func.php";
require_once module::$path."/posts/handler.php";

class handler{
	function __construct(){
		$this->template='template';#Определяем какой модуль использовать как основной шаблон вывода
		$this->headers=new \stdClass;
		$this->userControl= user::init();
		$this->user=$this->userControl->user;
	}
	function showFolders($uid, $page, $num){
		if (!$uid) return;
		$start = ($page-1)*$num;
		list($count) = db::qrow("SELECT count(*) FROM `".PREFIX_SPEC."likeFolders` WHERE uid = {$uid}");
		db::query("SELECT * FROM `".PREFIX_SPEC."likeFolders` WHERE uid = {$uid} LIMIT $start,$num");
		$folders=[];
		while ($folder=db::fetch()){
			$folders[$folder->id]=$folder;
		}
		getImages2folders($folders);
		#Получаем данные по пользователю
		$userinfo=\user\getUser($uid);
		return [
			'folders' => $folders,
			'user' => $this->user,
			'userinfo'=>$userinfo,
			'paginator'=>module::execStr('plugins/paginator',
				[
					'page'=>$page,
					'num'=>$num,
					'count'=>$count,
					'uri'=>url::userFolders($uid).'?page=%d',
				]
			),
		];
	}
	function showPins($uid, $furl, $page, $num){
		$tbl=\posts\tables::init();
		db::query("SELECT * FROM `".PREFIX_SPEC."likeFolders` WHERE uid = {$uid} && url != '{$furl}' ORDER BY count DESC LIMIT 4");
		$folders=[];
		while ($folder=db::fetch()){
			$folders[$folder->id]=$folder;
		}
		getImages2folders($folders);
		$folder = db::qfetch("SELECT * FROM `".PREFIX_SPEC."likeFolders` WHERE uid={$uid} && url = '{$furl}'");
		$start=($page-1)*$num;
		#Получаем список id картинок которые лайкнул пользователь
		$ids=[];
		db::query("SELECT imid FROM `".PREFIX_SPEC."like2img` WHERE uid={$uid} && fid={$folder->id} LIMIT $start,$num");
		while ($d=db::fetch()){
			$ids[]=$d->imid;
		}
		#Получаем данные по картинкам
		$imgs=[];
		if(isset($ids[0])){
			db::query(
				"SELECT imgs.*,li.date as datepin FROM `".PREFIX_SPEC."imgs` imgs
				INNER JOIN `".PREFIX_SPEC."like2img` li ON li.imid=imgs.id
				WHERE imgs.id IN (".implode(",",$ids).") && imgs.tbl='{$tbl->post}' GROUP BY imgs.id");
			while ($d=db::fetch()){
				$imgs[$d->pid][]=$d;
			}
		}
		#получаем посты
		$d= \posts_lists\listPostsByPid(array_keys($imgs),$this->userControl);
		#присоединяем полученные ранее картинки
		foreach ($d->posts as $k=>&$v) {
			$v->imgs=$imgs[$k];
		}
		#Получаем категории для рекомендуемых папок пинов
		$pinSudgests = [];
		if (!empty($d->posts) && $uid!=$this->user->id){
			foreach ($d->posts as $post) {
				$pinSudgests[] = $post->cid;
			}
		}
		#Получаем данные по пользователю
		$userinfo=\user\getUser($uid);
		return [
			'owner' => $uid==$this->user->id,
			'folder'=>$folder,
			'posts'=>$d->posts,
			'user'=>$this->user,
			'userinfo'=>$userinfo,
			'page'=>$page,
			'num'=>$num,
			'paginator'=>module::execStr('plugins/paginator',
				[
					'page'=>$page,
					'num'=>$num,
					'count'=>$d->count,
					'uri'=>url::showFolder($uid,$folder->url).'?page=%d',
				]
			),
			'count'=>$d->count,
			'prfxtbl'=>$tbl->prfx,
			'pinForm'=>module::execStr('pins',['act'=>'pinForm','sudgests'=>$pinSudgests]),
			'folders' => $folders,
		];
	}
	function addPin($imid, $fid) {
		$this->template='';
		$this->view='addFolder';
		if(!$imid||!$this->user->id) return;
		list($count) = db::qrow("SELECT count FROM `".PREFIX_SPEC."likeFolders` WHERE id = ".db::escape($fid));
		db::query("INSERT IGNORE INTO `".PREFIX_SPEC."like2img` values('{$this->user->id}',".db::escape($imid).",".db::escape($fid).", NOW())");
		list($ncount) = db::qrow("SELECT count(*) FROM `".PREFIX_SPEC."like2img` WHERE uid = '{$this->user->id}' && fid = '".db::escape($fid)."'");
		if ($count!=$ncount) db::query("UPDATE `".PREFIX_SPEC."likeFolders` SET count = {$ncount} WHERE id = ".db::escape($fid));
		return [
			'id' => $ncount,
		];
	}
	function removePin($imid, $fid) {
		if(!$imid||!$fid||!$this->user->id) return;
		db::query("DELETE FROM `".PREFIX_SPEC."like2img` WHERE uid = '{$this->user->id}' && imid = ".db::escape($imid)." && fid = ".db::escape($fid));
		list($count) = db::qrow("SELECT count(*) FROM `".PREFIX_SPEC."like2img` WHERE uid = '{$this->user->id}' && fid = '".db::escape($fid)."'");
		db::query("UPDATE `".PREFIX_SPEC."likeFolders` SET count = {$count} WHERE id = ".db::escape($fid));
	}
	function addFolder($fname) {
		$this->template='';
		if(!$fname||!$this->user->id) return;
		db::query("INSERT IGNORE INTO `".PREFIX_SPEC."likeFolders` (`url`, `title`, `uid`) values('".db::escape(makeAlias($fname))."','".db::escape($fname)."','{$this->user->id}')");
		return [
			'id' => db::insert(),
		];
	}
	function removeFolder($fid) {
		if(!$fid||!$this->user->id) return;
		db::query("DELETE FROM `".PREFIX_SPEC."like2img` WHERE uid = '{$this->user->id}' && fid = ".db::escape($fid));
		db::query("DELETE FROM `".PREFIX_SPEC."likeFolders` WHERE id = ".db::escape($fid));
	}
	function pinForm($sudgests) {
		if (!$this->user->id) return;
		$this->template='';
		$tbls=\posts\tables::init();
		if(empty($sudgests)) $sudgests = [];
		if(!is_array($sudgests)) $sudgests= explode(',', $sudgests);
		$folders=module::execData('pins',[
			'act' => 'showFolders',
			'user' => $this->user->id,
		]);
		db::query("SELECT * FROM {$tbls->category} WHERE url IN ('".implode("','", $sudgests)."') && view=1 ORDER BY `parentId`,`count` DESC LIMIT 5");
		$cats=[];
		while ($cat=db::fetch()){
			$cats[]=$cat;
		}
		//$post->cats=$cats;
		return [
			'sudgests' => $cats,
			'folders' => $folders->folders,
		];
	}
}
