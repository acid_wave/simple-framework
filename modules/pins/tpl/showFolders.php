<?
$tpl->title='Folders for pinned posts by '.$data->userinfo->altName;
$tpl->desc='The user '.$data->userinfo->altName.' posts collection';
$tpl->meta="<meta name='robots' content='noindex, follow'/>";
?>
<div class="container">
	<?include $template->inc('../../user/tpl/settings/userinfo.php');?>
	<div class="row mb-3">
		<div class="col-12 text-center">
			<?include $template->inc("../../posts/tpl/post/share.php");?>
		</div>
	</div>
	<div class="row">
		<?if(!empty($data->folders)){?>
		<?foreach($data->folders as $folder){?>
		<div class="col-md-3">
			<?include $template->inc('showFolders/card.php');?>
		</div>
		<?}?>
		<?}?>
	</div>
</div>
<script type="text/javascript">
	function removeFolder(e){
		var data = {act: 'removeFolder', fid: $(e).data("folder")};
		$.post(window.location.basepath + '?module=pins', data, function () {
			window.location.reload();
		});
	}
</script>