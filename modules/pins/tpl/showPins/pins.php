<?if(empty($data->posts))return;?>
<?foreach($data->posts as $p){?>
<div class="col-md-3 mb-3">
	<div class="postList card">
		<?=$p->funcPanel?>
		<a href="<?=url::post($p->url, $data->prfxtbl)?>">
			<div class="photo-item-img">
				<img class="img-fluid p-1" src="<?=@url::imgThumb('450_', $p->imgs[0]->url)?>" alt="<?=$p->title?>"/>
			</div>
		</a>
		<div class="photo-item-title">
			<div class="row">
				<div class="col-12">
					<p class="text-center"><b><?=$p->title?></b></p>
				</div>
				<div class="col-sm-6">
					<?if(!empty($p->authorName)){?>
					<i class="postList-avatar rounded-circle" style="background-image: url('<?=$p->authorAvatar?>');"></i> <?=$p->authorName?>
					<?}?>
					<i class="fa fa-eye" title="All view:<?=$p->statViews?>. Views for 7 days: <?=$p->statViewsShort?>"></i> <?=$p->statViews?>
				</div>
				<div class="col-sm-6 text-right">
					<?if(isset($p->imgs[0]->url)){?>
					<?if($data->user->id){
					$downloadHref='href="#" onclick="preDownload(\''.$p->id.'\',\''.@$p->imgs[0]->url.'\'); return false"';
					$likeHref='onclick="return false;" data-toggle="modal" data-target="#pinForm" data-imid="'.$p->imgs[0]->id.'" data-url="'.url::imgThumb('450_', $p->imgs[0]->url).'" data-title="'.$p->title.'"';
					}else{
					$downloadHref='href="'.url::userLogin(true).'" target="_blank"';
					$likeHref='href="'.url::userLogin(true).'" target="_blank"';
					}?>
					<a <?=$downloadHref?> rel="nofollow"><i class="fa fa-download fa-2x"></i></a>
					<a <?=$likeHref?> rel="nofollow"><i class="fa fa-heart-o fa-2x"></i></a>
					<?}?>
				</div>
				<?if($data->owner){?>
				<div class="col-4 text-center mt-2">
					<a href onclick="copyPin(this); return false;" rel="nofollow" data-image="<?=$p->imgs[0]->id?>" title="Copy to another folder"><i class="fa fa-copy fa-2x"></i></a>
				</div>
				<div class="col-4 text-center mt-2">
					<a href onclick="movePin(this); return false;" rel="nofollow" data-image="<?=$p->imgs[0]->id?>" title="Move to another folder"><i class="fa fa-clipboard fa-2x"></i></a>
				</div>
				<div class="col-4 text-center mt-2">
					<a href onclick="deletePin(this);" rel="nofollow" data-image="<?=$p->imgs[0]->id?>" title="Delete from folder"><i class="fa fa-recycle fa-2x"></i></a>
				</div>
				<?}?>
			</div>
		</div>
	</div>
</div>
<?}?>

<script type="text/javascript">
	var folder = <?=$data->folder->id?>;
	function preDownload(pid, file) {
		var data = {act: 'preDownload', file: file, pid: pid};
		$.post(window.location.basepath + '?module=images', data, function (response) {
			$('#download-modal').detach();
			$('body').append('<div id="download-modal">' + response + '</div>');
		});
	}
	function deletePin(e){
		var data = {act: 'removePin', imid: $(e).data('image'), fid: folder};
		$.post(window.location.basepath + '?module=pins', data, function () {
			window.location.reload();
		});
	}
	function copyPin(e){
		$('[data-id=<?=$data->folder->id?>]').addClass('d-none');
		$('#pinFormLabel').html('Copy pin');
		del = false;
		$('[data-imid='+$(e).data('image')+']').click();
	}
	function movePin(e){
		$('[data-id=<?=$data->folder->id?>]').addClass('d-none');
		$('#pinFormLabel').html('Move pin');
		del = true;
		$('[data-imid='+$(e).data('image')+']').click();
	}
</script>
