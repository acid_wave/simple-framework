<div class="card mb-3">
	<div class="card-img-top">
		<a href="<?=url::showFolder($data->userinfo->id, $folder->url)?>">
			<div class="row no-gutters">
				<?foreach($folder->imgs as $img){?>
				<div class="col-4 border">
					<img class="img-fluid" src="<?=@url::imgThumb('150_150', $img->url)?>" alt="<?=$img->gtitle?>">
				</div>
				<?}?>
				<?for ($i=count($folder->imgs); $i<6; $i++){?>
				<div class="col-4 border">
					<img class="img-fluid" src="/modules/pins/files/empty.png" alt="no image"/>
				</div>
				<?}?>
			</div>
		</a>
	</div>
	<div class="card-body">
		<a href="<?=url::showFolder($data->userinfo->id, $folder->url)?>"><?=$folder->title?> <span class="badge badge-info"><?=$folder->count?></span></a>
		<?if (!empty($data->mail)) if ($data->mail==$data->userinfo->mail){?>
		<a href onclick="removeFolder(this); return false;" data-folder="<?=$folder->id?>" class="btn btn-danger btn-sm float-right"><i class="fa fa-recycle"></i></a>
		<?}?>
	</div>
</div>