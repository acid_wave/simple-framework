<?
$tpl->title=$data->folder->title.' folder content of pinned posts by '.$data->userinfo->altName;
$tpl->desc='The user '.$data->userinfo->altName.' posts collection';
$tpl->meta="<meta name='robots' content='noindex, follow'/>";
?>
<div class="container">
	<?include $template->inc('../../user/tpl/settings/userinfo.php');?>
	<div class="row mb-3">
		<div class="col-12 text-center">
			<?include $template->inc("../../posts/tpl/post/share.php");?>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<div class="row">
				<div class="col-12 text-center">
					<h1><?=ucfirst($data->userinfo->altName)?>`s <?=$data->folder->title?></h1>
				</div>
				<?include $template->inc("showPins/pins.php");?>
				<div class="col-12 text-center">
					<?=$data->paginator?>
				</div>
				<?if(@$data->count){?>
				<div class="col-12">
					<small>items:&nbsp<i><?=@$data->count?></i></small>
				</div>
				<?}?>
			</div>
		</div>
		<div class="col-12">
			<h4 class="text-center">Other <?=$data->userinfo->altName?>'s folders</h4>
			<?if(!empty($data->folders)){?>
			<div class="row">
				<?foreach($data->folders as $folder){?>
				<div class="col-12 col-md-3">
				<?include $template->inc('showFolders/card.php');?>
				</div>
				<?}?>
			</div>
			<?}?>
			<a class="btn btn-outline-dark btn-block" href="<?=url::userFolders($data->userinfo->id)?>">All folders</a>
		</div>
	</div>
</div>
<?=$data->pinForm?>