<div class="modal fade" id="pinForm" tabindex="-1" role="dialog" aria-labelledby="pinFormLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="pinFormLabel">Pin image</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body p-0">
				<div class="container-fluid">
					<div class="row no-gutters">
						<div class="col-md-6 text-center my-auto pr-3">
							<img src="url" alt="title" id="pinImg" class="img-thumbnail">
							<h6 class="img-title text-left mt-3" id="pinTitle">title</h6>
						</div>
						<div class="col-md-6 border-left py-3 mh-70">
							<div id="selectFolderForm" class="pl-3">
								<?if(!empty($data->folders)){?>
								<h5>Choose folder</h5>
								<div class="list-group" id="selectFolder">
									<?foreach($data->folders as $folder){?>
									<a class="list-group-item list-group-item-action" href data-id="<?=$folder->id?>" data-title="<?=$folder->title?>" data-url="<?=$folder->url?>"><?=$folder->title?></a>
									<?}?>
								</div>
								<hr>
								<?}?>
								<a class="list-group-item list-group-item-action h4 text-danger" href id="createFolder"><i class="fa fa-plus-circle"></i> Create folder</a>
								<?if(!empty($data->sudgests)){?>
								<h6>Suggested folder names</h6>
								<?}?>
								<div class="list-group" id="suggestFolders">
									<?foreach($data->sudgests as $cat){?>
									<a class="list-group-item list-group-item-action" href data-title="<?=ucfirst($cat->title)?>"><i class="fa fa-plus"></i> <?=ucfirst($cat->title)?></a>
									<?}?>
								</div>
							</div>
							<div id="createFolderForm" class="pl-3 h-100 d-none">
								<h4>Create folder</h4>
								<form class="needs-validation h-100" novalidate id="cFolder">
									<div class="form-group h-75">
										<label for="folderName">Name</label>
										<input class="form-control" type="text" name="folder" id="folderName" required placeholder="Like &quot;Places to Go&quot; or &quot;Recipes to Make&quot;">
										<div class="invalid-feedback">
											Please choose a folder name.
										</div>
									</div>
									<hr>
									<button id="fCancel" class="btn btn-secondary">Cancel</button>
									<button id="fSubmit" type="submit" class="btn btn-success float-right">Create</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var del = false;
	$(document).ready(function () {
		var fname = '';
		var fid = '';
		var imid = '';
		$('#suggestFolders > a').each(function () {
			$(this).click(function (event) {
				event.preventDefault();
				fname = $(this).data('title');
				$("#pinForm").modal('hide');
				$.post(window.location.basepath + '?module=pins',
						{
							act: 'addFolder',
							folder: fname,
						},
						function (response) {
							if ($.trim(response)) {
								$('#selectFolder').append('<a class="list-group-item list-group-item-action" href data-id="'+$.trim(response)+'">'+fname+'</a>');
								$.post(window.location.basepath + '?module=pins',
										{
											act: 'addPin',
											fid: $.trim(response),
											imid: imid,
										},
										function (response) {
											if ($.trim(response)) {
												if (del) {
													var data = {act: 'removePin', imid: imid, fid: folder};
													$.post(window.location.basepath + '?module=pins', data, function () {
														window.location.reload();
													});
												}
											}
										}
								);
							}
						},
						);
			});
		});
		$('#selectFolder > a').each(function () {
			$(this).click(function (event) {
				event.preventDefault();
				fid = $(this).data('id');
				fname = $(this).data('title');
				$("#pinForm").modal('hide');
				$.post(window.location.basepath + '?module=pins',
						{
							act: 'addPin',
							fid: fid,
							imid: imid,
						},
						function (response) {
							if ($.trim(response)) {
								if (del) {
									var data = {act: 'removePin', imid: imid, fid: folder};
									$.post(window.location.basepath + '?module=pins', data, function () {
										window.location.reload();
									});
								}
							}
						}
				);
			});
		});
		$('#createFolder').click(function (event) {
			event.preventDefault();
			$('#selectFolderForm').addClass('d-none');
			$('#createFolderForm').removeClass('d-none');
		});
		$('#fCancel').click(function (event) {
			event.preventDefault();
			$('#selectFolderForm').removeClass('d-none');
			$('#createFolderForm').addClass('d-none');
			$('#folderName').val('');
		});
		(function () {
			'use strict';
			window.addEventListener('load', function () {
				// Fetch all the forms we want to apply custom Bootstrap validation styles to
				var forms = document.getElementsByClassName('needs-validation');
				// Loop over them and prevent submission
				var validation = Array.prototype.filter.call(forms, function (form) {
					form.addEventListener('submit', function (event) {
						if (form.checkValidity() === false) {
							event.preventDefault();
							event.stopPropagation();
							form.classList.add('was-validated');
							return;
						}
						event.preventDefault();
						fname = $('#folderName').val();
						$('#pinForm').modal('hide');
						$('#selectFolderForm').removeClass('d-none');
						$('#createFolderForm').addClass('d-none');
						$('#folderName').val('');
						$('#cFolder').removeClass('was-validated');
						$.post(window.location.basepath + '?module=pins',
								{
									act: 'addFolder',
									folder: fname,
								},
								function (response) {
									if ($.trim(response)) {
										$('#selectFolder').append('<a class="list-group-item list-group-item-action" href data-id="'+$.trim(response)+'">'+fname+'</a>');
										$.post(window.location.basepath + '?module=pins',
												{
													act: 'addPin',
													fid: $.trim(response),
													imid: imid,
												},
												function (response) {
													if ($.trim(response)) {
														if (del) {
															var data = {act: 'removePin', imid: imid, fid: folder};
															$.post(window.location.basepath + '?module=pins', data, function () {
																window.location.reload();
															});
														}
													}
												}
										);
									}
								},
								);
					}, false);
				});
			}, false);
		})();
		$('#pinForm').on('hidden.bs.modal', function () {
			$('#selectFolderForm').removeClass('d-none');
			$('#createFolderForm').addClass('d-none');
			$('#folderName').val('');
			$('#cFolder').removeClass('was-validated');
			$('#pinFormLabel').html('Pin image');
		});
		$('#pinForm').on('show.bs.modal', function (e) {
			imid = $(e.relatedTarget).data('imid');
			$('#pinImg').attr("src", $(e.relatedTarget).data('url'));
			$('#pinTitle').html($(e.relatedTarget).data('title'));
		});
	});
</script>