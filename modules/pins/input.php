<?php
namespace pins;
use control;

class input extends control {
	function __construct($input) {# $input - объект входных переменных от других модулей
		if (empty($input->act))
			$input->act = 'showFolders';

		switch ($input->act) {
			case 'showFolders':
				$this->data = [
					'uid' => empty($input->user) ? false : (int) $input->user,
					'page' => empty($input->page) ? 1 : (int) $input->page,
					'num' => empty($input->num) ? 12 : (int) $input->num,
				];
				break;
			case 'showPins':
				$this->data = [
					'uid' => empty($input->user) ? false : (int) $input->user,
					'furl' => empty($input->folder) ? '' : $input->folder,
					'page' => empty($input->page) ? 1 : (int) $input->page,
					'num' => empty($input->num) ? 20 : (int) $input->num,
				];
				break;
			case 'addPin':
				$this->data = [
					'imid' => empty($input->imid) ? false : (int) $input->imid,
					'fid' =>  empty($input->fid) ? 0 : (int) $input->fid,
				];
				break;
			case 'removePin':
				$this->data = [
					'imid' => empty($input->imid) ? false : (int) $input->imid,
					'fid' =>  empty($input->fid) ? false : (int) $input->fid,
				];
				break;
			case 'addFolder':
				$this->data = [
					'fname' => empty($input->folder) ? false : $input->folder,
				];
				break;
			case 'removeFolder':
				$this->data = [
					'fid' => empty($input->fid) ? false : (int) $input->fid,
				];
				break;
			case 'pinForm':
				$this->data = [
					'sudgests' => empty($input->sudgests) ? [] : $input->sudgests,
				];
				break;
			default:
				$this->act = false;
				break;
		}
	}

}
