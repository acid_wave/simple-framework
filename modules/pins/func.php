<?php
namespace pins;
use module,db,url;

/*
 *  Create url from title
 */
function makeAlias($title) {
	$restrictcharspattern = '/[\0\x0B\t\n\r\f\a&=+%#<>"~`@\?\[\]\{\}\|\^\'\\\\]/';
	$delimiter = '-';
	$charset = 'UTF-8';
	$trimchars = '/.' . $delimiter;
	/* strip html */
	$alias = strip_tags($title);
	/* replace &nbsp; with the specified word delimiter */
	$alias = str_replace('&nbsp;', $delimiter, $alias);
	/* decode named entities to the appropriate character for the character set */
	$alias = html_entity_decode($alias, ENT_QUOTES, $charset);
	/* restrict title to legal URL characters only */
	$alias = preg_replace($restrictcharspattern, '', $alias);
	/* replace one or more space characters with word delimiter */
	$alias = preg_replace('/\s+/u', $delimiter, $alias);
	/* change case to lowercase */
	$alias = mb_convert_case($alias, MB_CASE_LOWER, $charset);
	/* trim specified chars from both ends of the segment */
	$talias=$alias = trim($alias, $trimchars);
	$i=0;
	do{
		list($tid)=db::qrow("SELECT id FROM `" . PREFIX_SPEC . "likeFolders` WHERE url='$talias' LIMIT 1");
		if($tid){
			$i++;
			$talias=$alias.'-'.$i;
		}
	}while($tid);
	return $talias;
}
/*
 *  Get images for selectet folders
 */
function getImages2folders(&$folders, $limit=6) {
	if (empty($folders)) return;
	foreach ($folders as $id => $folder) {
		db::query("SELECT i.* FROM `" . PREFIX_SPEC . "like2img` l2i 
					INNER JOIN `" . PREFIX_SPEC . "imgs` i ON l2i.imid=i.id 
					WHERE l2i.fid = {$id} 
					ORDER BY l2i.date
					LIMIT {$limit}");
		while ($d=db::fetch()) {
			$img=new \posts\classImg;
			$folder->imgs[]=add2obj($img,$d);
		}
	}
}
