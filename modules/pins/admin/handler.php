<?php
namespace pins\admin;
use db;

class handler {
	function install() {
		db::query("CREATE TABLE IF NOT EXISTS `" . PREFIX_SPEC . "like2img` (
			`uid` int(11) NOT NULL,
			`imid` int(11) NOT NULL,
			`fid` int(11) NOT NULL,
			`date` date NOT NULL,
			UNIQUE KEY `uid` (`uid`, `imid`, `fid`),
			KEY `date` (`date`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8
		", 1);
		db::query("CREATE TABLE IF NOT EXISTS `" . PREFIX_SPEC . "likeFolders` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`url` varchar(100) NOT NULL,
			`title` varchar(100) NOT NULL,
			`uid` int(11) NOT NULL,
			`count` int(11) NOT NULL,
			PRIMARY KEY (`id`),
			UNIQUE KEY `url` (`url`,`uid`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8
		", 1);
		return;
	}

}
