<?php
/*
 * Входные двнные находятся в stdClass $data
 * */
$cat = $data->cat;
$posts = $data->posts;
$tpl->title = NAME . " main.";
$tpl->desc = "Discover our website, interesting stories and beautiful media content.";
?>
<div class="container">
	<div class="row no-gutters">
		<div class="col-md-9">
			<?if(isset($data->access->editNews)){?><span><a href="<?=url::post_adminAdd($cat->url)?>">new post</a></span><?}?>
			<div class="row no-gutters index-models">
				<?include $template->inc('index/listposts.php');?>
			</div>
			<div class="row justify-content-center my-3">
				<?include $template->inc('../../posts/lists/tpl/mainList/delposts.php');?>
			</div>
			<?=$data->paginator?><small>items:&nbsp<i><?=@$data->count?></i></small>
		</div>
		<div class="col-md-3 pl-2">
			<?=$data->subCats?>
			<?=$data->topLevelCats?>
		</div>
	</div>
	<?include $template->inc('index/seealso.php');?>
</div>