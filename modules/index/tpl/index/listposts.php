<?if(empty($posts))return;
foreach($posts as $p){?>
	<div class="col-md-3 card model p-1">
		<?=$p->funcPanel?>
		<a href="<?=url::post($p->url)?>" title="<?=$p->title?>" class="model-link">
			<div class="model-image" style="background-image:url(<?=!empty($p->imgs[0])?url::imgThumb('450_',$p->imgs[0]):'/images/404.png'?>);">
				<img class="img-fluid" src="<?=!empty($p->imgs[0])?url::imgThumb('450_',$p->imgs[0]):'/images/404.png'?>" title="<?=$p->title?>" alt="<?=$p->title?>"/>
			</div>
			<div class="model-title"><?=$p->title?></div>
		</a>
		<div class="model-text">
			<small><?=date('Y-m-d', strtotime($p->datePublish?$p->datePublish:$p->date))?> 
				<?if(!empty($p->authorName)){?>
				<br><i class="fa fa-user-circle"></i> <?=$p->authorName?>
				<?}?>
			<i class="fa fa-eye" aria-hidden="true" title="All view:<?=$p->statViews?>. Views for 7 days: <?=$p->statViewsShort?>"></i> <?=$p->statViews?>
			<i class="fa fa-file-image-o" title="count photos"></i> <?=$p->countPhoto?>
			</small>
		</div>
	</div>
<?}?>
<div class="clearfix"></div>
