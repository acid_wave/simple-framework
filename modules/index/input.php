 <?php
/*
* Должен возвращать
* $this->data - объект обработанных входных переменных
*/
class index extends control{
	function __construct($input=''){ #$input - объект входных переменных от других модулей
		$this->data=new stdClass;
		if(empty($input->act)) $input->act='index';

		if($input->act=='index'){
			$this->data=[
				'page'=>empty($input->page)?1:$input->page,
				'num'=>cookiePage($input,'num',20),
			];
		}else
			$this->act=false;

		
	}
}
