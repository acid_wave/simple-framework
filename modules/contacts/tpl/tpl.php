<?php
list($status, $mail, $text) = array($data->status, $data->mail, $data->txt);
$txt = array(
	'sent' => 'Mail sent!',
	'capcha' => 'CAPTCHA Failed',
	'mail' => 'Wrong mail'
);
$tpl->title = "Contacts.";
$tpl->meta = '<meta name="robots" content="noindex,follow" />';
$tpl->container = "container-fluid";
?>
<div class="row justify-content-center mb-5">
	<div class="col-12 col-md-6">
		<h1>Contact Us</h1>
		<?foreach($status as $v){?>
		<div class="alert alert-<?=($v == 'sent') ? 'success' : 'danger'?>">
			<strong><?=($v == 'sent') ? 'Success' : 'Error'?>!</strong> <?=$txt[$v]?>
		</div>
		<?}?>
		<form name="contact" method="post" action="">
			<div class="form-group">
				<label for="mail">Email address</label>
				<input type="email" class="form-control<?=in_array('mail',$status)?' is-invalid':''?>" id="mail" aria-describedby="emailHelp" placeholder="Enter email" name="mail" value="<?=$mail?>">
				<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
			</div>
			<div class="form-group">
				<label for="text">Your message</label>
				<textarea class="form-control" id="text" name="text" rows="7"><?=$text?></textarea>
			</div>
			<div class="form-group text-center">
				<label for="capcha"><img src="<?=url::contactsCapcha()?>"></label>
				<input class="form-control" id="capcha" name="secret" type="text"/>
			</div>
			<input type="submit" name='send' class="btn btn-outline-secondary btn-block" value="Submit">
		</form>
	</div>
</div>
