<?php
/*
  * Должен возвращать
  * $this->data - объект обработанных входных переменных
  * $this->act - какую функцию обработки используем
*/
class contacts extends control{
	function __construct($input=''){ # $input - объект входных переменных от других модулей
		@session_start();
		$this->data=(object)array(
			'save'=>isset($input->send)?(bool)$input->send:false,
			'ssecret'=>@$_SESSION['secret'],
			'secret'=>isset($input->secret)?$input->secret:'',
			'mail'=>isset($input->mail)?$input->mail:'',
			'txt'=>isset($input->text)?$input->text:'',
			'host'=>$_SERVER['HTTP_HOST'],
			'subj'=>isset($input->subj)?$input->subj:'',
		);
		if(@$input->act=='')$input->act='index';
		if(in_array($input->act,array('index','save','capcha'))){
			$this->act=$input->act;
		}
	}
}
