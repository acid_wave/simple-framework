<?php
/*
* Должен возвращать
* $this->data - объект обработанных входных переменных
*/
class posts extends control{
	function __construct($input=''){ # $input - объект входных переменных от других модулей
		if(empty($input->act)) $input->act='post';
		if(@$_COOKIE['mobile']==1&&!$input->easy){
			#смена шаблона вывода
			$input->act='mobile';
		}

		$this->act=$input->act;

		if($input->act=='post'){
			$this->data=(object)array(
				'url'=>db::escape(urldecode($input->url)),
				'imgfromcookie'=>empty($_COOKIE['imagefile'])?false:$_COOKIE['imagefile'],
				'prfxtbl'=>!empty($input->prfxtbl)?$input->prfxtbl:'',
			);
		}elseif($input->act=='mobile'){
			$this->act=$input->act;
			$this->data=(object)array(
				'url'=>db::escape(urldecode($input->url)),
				'imgfromcookie'=>empty($_COOKIE['imagefile'])?false:$_COOKIE['imagefile'],
				'prfxtbl'=>!empty($input->prfxtbl)?$input->prfxtbl:'',
			);
		}elseif(@$input->act=='editPanel'){
			$this->data=(object)array(
				'post'=>empty($input->post)?0:$input->post,
			);
		}elseif($input->act=='rss'){
			$this->data=(object)array();
		}
	}
}
