<?if(empty($post->cats))return;?>
<div class="row">
	<div class="col-12">
		<h2>Filed under:</h2>
		<div>
		<?foreach ($post->cats as $val) {
		if ($val->view){?>
			<a class="btn btn-info btn-sm" style="margin-bottom: 5px;" href="<?=url::category($val->url,$data->prfxtbl)?>"><?=$val->title?></a>
		<?}}?>
		</div>
	</div>
</div>