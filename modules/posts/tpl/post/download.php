<div class="container-download">
	<div class="row">
		<div class="col-sm-6 text-left">
			<small>
				<i class="fa fa-exclamation-circle" aria-hidden="true"></i> 
				<a href="<?=url::contacts()?>?text=<?=urlencode('DMCA Report - '.url::post($post->url,$data->prfxtbl))?>" rel="nofollow">Copyright complaint</a>
			</small>
		</div>
		<div class="col-sm-6 text-right">
			<small><i class="fa fa-bug" aria-hidden="true"></i> <a href="<?=url::contacts()?>" rel="nofollow">Problems</a></small>
		</div>
	</div>
</div>
<div class="container-download mt-5">
	<div class="row">
		<div class="col-12 text-center mt-2">
			<div class="btn-group">
				<?if($data->user->id){
					$downloadOnclick="preDownload('".$post->id."','".@$img->url."'); return false";
				}else{
					$downloadOnclick="window.location='".url::userLogin(url::post($post->url)."#download_{$post->id}_{$img->url}_0")."'";
				}?>
				<button type="button" class="btn btn-success" onclick="<?=$downloadOnclick?>">Download</button>
				<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<?
					$koef=$img->height/$img->width;
					$ar=array('Original'=>$img->width,'Large'=>1920,'Medium'=>1280,'Small'=>640);
					foreach($ar as $k=>$v){
						if($v>$img->width) continue;
						if($data->user->id){
							$downloadOnclick="preDownload('".$post->id."','".@$img->url."','".($v==$img->width?0:$v)."'); return false";
						}else{
							$downloadOnclick="window.location='".url::userLogin(url::post($post->url)."#download_{$post->id}_{$img->url}_{$img->width}")."'";
						}
						?>
						<li><a onclick="<?=$downloadOnclick?>" href="#"><?=$k?> (<?=$v?> x <?=intval($v*$koef)?>)</a></li>
					<?}?>
				</ul>
			</div>
		</div>
		<div class="col-12 mt-2">
			<div class="row">
				<div class="col-md-4 text-center"><i class="fa fa-download"></i> <?=$img->statDownload?></div>
				<div class="col-md-4 text-center">
					<i class="fa fa-eye" aria-hidden="true" title="All view:<?=$post->statViews?>. Views for 7 days: <?=$post->statViewsShort?>"></i>
					<?=$post->statViews?>
				</div>
				<div class="col-md-4 text-center">
					<?if($data->user->id){
						$likeHref='data-toggle="modal" data-target="#pinForm" data-imid="'.$img->id.'" data-url="'.url::imgThumb('450_', $img->url).'" data-title="'.$post->title.'"';
					}else{
						$likeHref='href="'.url::userLogin(true).'" target="_blank"';
					}?>
					<a <?=$likeHref?> rel="nofollow"><i class="fa fa-heart-o"></i></a> <?=intval(@$img->pinCount)?>
				</div>
			</div>
		</div>
		<div class="col-12 text-center mt-2">
			Licence: <?=ucfirst($img->licence)?>
		</div>
	</div>
</div>
<?=$data->pinForm?>
