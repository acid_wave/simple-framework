<?
if(!$data->accessEdit&&!$data->accessPub) return;
$post=$data->post;
if(empty($GLOBALS['post_panel'])){
	?>
	<style>
	.post_panel{
		box-shadow: 1px 1px 2px 0px;
		padding: 3px 4px 0 0;
		background-color: white !important;
		position:absolute;
		right:0;
		z-index: 9998;
	}
	.post_panel .icons{display: inline-block;float:left;width: 20px;height: 20px;background-repeat: no-repeat;background-image:url('<?=HREF?>/modules/posts/tpl/files/icons/iset.png');background-color: transparent;padding: 0;margin: 0;}
	.post_panel .icons-del{background-position: -18px -258px;}
	.post_panel .icons-history{background-position: -295px -206px;}
	.post_panel .icons-edit{background-position: -340px -24px;}
	.post_panel .icons-internet{background-position: -156px -232px;}
	.post_panel .icons-inactive{background-position: -18px -388px;}
	.post_panel .icons-active{background-position: -64px -388px;}
	.post_panel .icons-diff{background-position: -157px -414px;}
	.post_panel .icons-imgfix{background-position: -432px -24px;}
	</style>
	<script type="application/javascript" src="<?=HREF?>/files/posts/admin/js/published.js"></script>
	<script type="application/javascript" src="<?=HREF?>/modules/posts/tpl/files/js/gallery.js"></script>
<?}$GLOBALS['post_panel']=1;?>
<div style="position:relative">
	<div class="post_panel">
		<?if($data->accessPub){
			$status=$post->published==1?'yes':'no';?>
			<button 
				name="published" 
				data-id="<?=$post->id?>"
				data-prfxtbl="<?=$data->prfxtbl?>"
				class="published-<?=$status?> icons icons-<?=$status=='yes'?'active':'inactive'?>" 
				value="<?=$status?>"
				style="cursor:pointer;border: 0;"
				title="<?=$post->published==1?'published':'unpublished'?>"
			></button>
		<?}?>
		<?if($data->accessEdit){?>
		<a href="<?=url::post_adminEdit($post->id,$data->prfxtbl)?>" title="edit" class="icons icons-edit"></a>
		<?}?>
		<?if($data->accessDel){?>
			<a href="<?=url::post_adminDel($post->id,$data->prfxtbl)?>" onclick="return confirm('delete?');" title="delete" class="icons icons-del"></a>
			<?if(!empty($post->keyword)){?>
				<a href="http://www.google.com/search?q=<?=urldecode($post->keyword)?>&tbm=isch&sout=1&hl=en&gl=us" target="_blank" class="icons icons-internet" title="in google"></a>
			<?}?>
			<input type="checkbox" name="post[]" value="<?=$post->id?>"/>
		<?}?>
		<?if($data->accessHistory){
			if(!empty($post->has_diff)){?>
			<a href="<?=url::post_diff($post->id)?>" onclick="show_diff(this);return false;" class="icons icons-diff" title="diff"></a>
			<?}?>
			<a href="<?=url::post_archive($post->id)?>" class="icons icons-history" title="history"></a>
		<?}?>
		<?if(isset($post->imgs[0])){?>
			<a href="<?=url::imgOverlay($post->tbl,$post->id,$post->imgs[0]->url)?>" class="icons icons-imgfix" title="test overlay"></a>
		<?}?>
	</div>
</div>
