<?
$post=$data->post;
$img=@$data->post->imgs[0];
$tpl->title="{$post->title} - ".NAME;
@$tpl->desc="{$post->title} by ".NAME;
$tpl->meta="
	<script type=\"application/javascript\" src=\"".HREF."/files/js/mobiledetect.js\"></script>
	
	<meta name='robots' content='index, follow'/>
	<meta name='revisit-after' content='1 days'/>

	<meta property='og:locale' content='en_US'/>
	<meta property='og:type' content='article'/>
	<meta property='og:title' content='{$post->title} - ".NAME."'/>
	<meta property=\"og:description\" content=\"".htmlspecialchars(@$post->title)." by ".NAME."\"/>
	<meta property='og:url' content='".url::post($post->url)."'/>
	<meta property='og:site_name' content='".NAME."'/>
	<meta property='og:image' content='".url::image(@$post->imgs[0]->url)."'/>
";
?>
<div class="container">
	<div class="row">
		<div class="col-md-9 text-center container-photo">
			<?=$post->funcPanel?>
			<h1><?=$post->title?></h1>
		<?if(isset($img->url)){
			$title=$img->gtitle!=''?$img->gtitle:$post->title;?>
			<img src="<?=url::imgThumb('Origin',$img->url)?>" alt="<?=$title?>">
		<?}?>
		</div>
		<div class="col-md-3">
		<?if(isset($img->url)){?>
			<?include $template->inc('post/download.php');?>
			<?if($post->user->id){?>
			<div class="container-userinfo mt-4">
				<div class="row">
					<div class="col-12 text-center">
						<a href="<?=url::author($post->user->id)?>">
							<img class="rounded-circle" style="background-image: url('<?=$post->user->avatar?>'); background-size: cover; width: 70px; height:70px; box-shadow: 0px 0px 12px #999;">
						</a>
					</div>
					<div class="col-12 text-center mt-2">
						<a href="<?=url::author($post->user->id)?>">
							<h5><?=$post->authorName?></h5>
							<i class="text-muted">View all <?=$post->user->stat->postsCount?> images</i>
						</a>
					</div>
					<div class="col-12">
						<div class="row justify-content-center">
							<?foreach ($post->user->achievements as $achievement){?>
							<i class="achievement <?=$achievement->name?>" title="Received <?=$achievement->date?>"></i>
							<?}?>
						</div>
						<div class="row no-gutters text-center border-bt mb-2">
							<div class="col-4">
								<i class="fa fa-eye" title="Views received"></i>
								<div><?=$post->user->stat->postsViews?></div>
							</div>
							<div class="col-4 border-left">
								<i class="fa fa-heart-o" title="Likes received"></i>
								<div><?=$post->user->stat->imgsPin?></div>
							</div>
							<div class="col-4 border-left">
								<i class="fa fa-download" title="Downloads received"></i>
								<div><?=$post->user->stat->imgsDownload?></div>
							</div>
						</div>
					</div>
					<?include $template->inc('post/donate.php');?>
				</div>
			</div>
			<?}?>
		<?}?>
		</div>
	</div>
	<?include $template->inc('post/tags.php');?>
	<?include $template->inc('post/related.php');?>
</div>
<script type="text/javascript">
	function preDownload(pid,file,size=''){
		var data={act:'preDownload',file:file,pid:pid,size:size};
		$.post(window.location.basepath+'?module=images',data,function(response){
			$('#download-modal').detach();
			$('body').append('<div id="download-modal">'+response+'</div>');
		});
	}
	$(document).ready(function(){
		var params;
		if(params=window.location.hash.match(/#download_(\d+)_(.+)_(\d+)$/)){
			preDownload(params[1],params[2],params[3]);
		}	
	});
</script>
