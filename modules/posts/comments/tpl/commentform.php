<div class="comments-form">
	<form action="" method="post">
		<div id="formContent">
			<?if(!$data->user->id){?>
			<div class="form-group">
				<label for="author">Email (required):</label>
				<input class="form-control" type="text" name="mail" value="" size="22" tabindex="1" aria-required="true">
			</div>
			<div class="form-group">
				<b>or</b><a href="<?=url::userLogin()?>"> Sign in</a>
			</div>
			<?}?>
		</div>
		<div class="form-group">
			<label for="comment">Type your comment here:</label>
			<textarea class="commentText form-control" name="commentText"></textarea><br>
			<input name="" type="submit" id="submit" value="Send" class="btn btn-primary pull-right" onclick="commentSubmit();return false;">
		</div>
		<div style="clear:both;"></div>
	</form>
</div>
<script type="text/javascript">
	tinyMCE.EditorManager.init({
		mode : "specific_textareas",
		theme : "modern",
		menubar: false,
		statusbar: false,
		toolbar: 'undo redo | bold italic | bullist numlist',
		plugins : "",
		image_advtab: true,
		editor_selector : "commentText", 
		skin : "lightgray",
		plugin_preview_width : "800",
		plugin_preview_height : "600",
		template_replace_values : {
			username : "Some User"
		}
	});
</script>
