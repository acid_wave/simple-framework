<?php
namespace posts\admin\viewer;
use module,db,url,cache,\category\category;

require_once __DIR__.'/../../handler.php';

class handler{
	function __construct(){
		$this->template='template';
		$this->headers=new \stdClass;
		$this->uhandler=module::exec('user',array(),1)->handler;
		$this->user=$this->uhandler->user;
	}
	function listPosts($cat,$prfxtbl,$page,$num){
		$tbls=\posts\tables::init($prfxtbl);
		$data=module::execData('posts/lists',[
			'act'=>'mainList',
			'cat'=>$cat,
			'prfxtbl'=>$prfxtbl,
			'page'=>$page,
			'num'=>$num,
		]);
		$data->paginator=module::execStr(
			'plugins/paginator',
				[
					'page'=>$page,
					'num'=>$num,
					'count'=>$data->count,
					'uri'=>url::postList_admin_viewer($cat,$prfxtbl)."&page=%d",
					true,
				]
			);
		$baseCat=new category($cat,$this->uhandler->access->showAllCat);
		$data->subCats=module::execData('category',['act'=>'subList','url'=>$cat]);
		$data->topLevelCats=module::execData('category',['act'=>'mlist','category'=>$cat,'parentId'=>$baseCat->parentId]);
		return $data;
	}
}
