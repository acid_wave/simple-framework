<?php
class posts_admin_viewer extends control{
	function __construct($input){# $input - объект входных переменных от других модулей
		$this->data=new stdClass;
		if(empty($input->act)) $input->act='index';
		
		if($input->act=='post'){
			$this->data=(object)array(
				'url'=>db::escape(urldecode($input->url)),
				'prfxtbl'=>!empty($input->prfxtbl)?$input->prfxtbl:'',
			);
		}elseif($input->act=='listPosts'){
			$this->data=(object)array(
				'cat'=>empty($input->cat)?'':db::escape(urldecode($input->cat)),
				'prfxtbl'=>empty($input->prfxtbl)?'':$input->prfxtbl,
				'page'=>empty($input->page)?1:$input->page,
				'num'=>cookiePage($input,'num',10),
			);
		}else
			$this->act=false;
	}
}
