<?if(!empty($posts)){
foreach($posts as $p){?>
<div class="row model">
	<div class="col-md-3">
		<a><img style="height:100px;" src="<?=empty($p->imgs) ? HREF . '/files/images/404.png' : url::image($p->imgs[0])?>" alt="<?=$p->title?>"/></a>
	</div>
	<div class="col-md-9 model-text">
		<?=@$p->funcPanel?>
		<div class="title"><?=$p->title?></div>
		<small><?=date('Y-m-d', strtotime($p->datePublish ? $p->datePublish : $p->date))?> 
			<?if(!empty($p->authorName)){?>
			<i><?=$p->authorName?></i>
			<?}?>
		</small>
		<small style="float:right;clear:both;">
			<?=$p->statViews?> / <?=$p->statViewsShort?>, 
			pic: <?=$p->countPhoto?>
		</small>
		<p><?=strip_tags($p->txt)?></p>
	</div>
	<div class="clearfix"></div>
</div>
<?}
}?>
