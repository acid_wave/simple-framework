<?
$tlData=$data->topLevelCats;
if($tlData->samelevel&&$tlData->samelevel->cats){?>
<div class="toplevel">
	<h4>
		<?=$tlData->samelevel->baseCat->title?> 
		<?if($tlData->edit){?><a href="<?=url::category_edit(0,'',$tlData->prfxtbl)?>">add</a><?}?>
	</h4>
	<ul class="nav flex-column">
	<?foreach($tlData->samelevel->cats as $val){?>
		<li class="nav-item">
			<?if($tlData->samelevel->current==$val->url){?>
				<?=$val->title?>&nbsp;<small title="count of posts">(<?=$val->count?>)</small>&nbsp;
			<?}else{?>
				<a href="<?=url::postList_admin_viewer($val->url,$tlData->prfxtbl)?>"><?=$val->title?></a>&nbsp;<small title="count of posts">(<?=$val->count?>)</small>&nbsp;
			<?}?>
			<?if(!empty($val->funcPanel)){?><?=$val->funcPanel?><?}?>
		</li>
	<?}?>
	</ul>
</div> 
<?}?>
<div class="toplevel">
<?if(!empty($tlData->cats)){?>
	<h4>
		Categories 
		<?if($tlData->edit){?><a href="<?=url::category_edit(0,'',$tlData->prfxtbl)?>"><img width="15" src="<?=HREF?>/files/template/icons/add.png" /></a><?}?>
	</h4>
	<ul class="nav flex-column">
	<?foreach($tlData->cats as $val){?>
		<li class="nav-item">
			<?if(!empty($val->funcPanel)){?><?=$val->funcPanel?><?}?>
			<a href="<?=url::postList_admin_viewer($val->url,$tlData->prfxtbl)?>"><?=$val->title?></a>&nbsp;<small title="count of posts">(<?=$val->count?>)</small>&nbsp;
		</li>
	<?}?>
	</ul>
<?}?>
</div> 
