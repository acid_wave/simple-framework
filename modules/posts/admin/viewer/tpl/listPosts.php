<?php
$cat=$data->cat;
$posts=$data->posts;
$tpl->title=(!empty($cat->title)?$cat->title:'').(@$data->page>1?" - Page {$data->page}":'');
$tpl->desc='';
?>
<div class="container">
	<div class="row">
		<div class="col-md-8 top-level">
			<?include $template->inc('listPosts/pinposts.php');?>
			<div class="top-level-models">
				<?if(isset($data->access->editNews) && !empty($cat->url)){?><span><a href="<?=url::post_adminAdd($cat->url,$data->prfxtbl)?>">new post</a></span><?}?>
				<?include $template->inc('listPosts/listposts.php');?>
				<?=@$data->paginator?>
				<?if(@$data->count){?>
					<small>items:&nbsp<i><?=@$data->count?></i></small>
				<?}?>
			</div>
		</div>
		<div class="col-md4">
			<?include $template->inc('listPosts/subList.php');?>
			<?include $template->inc('listPosts/mlist.php');?>
		</div>
	</div>
</div>