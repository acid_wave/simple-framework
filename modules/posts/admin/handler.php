<?php
namespace posts_admin;
use module,db,url,plugins\cron\cron,user\user;
#подключаем функции
require_once(module::$path.'/posts/admin/func.php');
#Используем сторонние модули
require_once(module::$path.'/category/handler.php');
require_once(module::$path.'/posts/handler.php');
require_once(module::$path.'/user/handler.php');
require_once(module::$path.'/admin/themes/handler.php');
require_once(module::$path.'/plugins/cron/handler.php');
require_once(module::$path.'/user/handler.php');

/*
 * Должен возвращать:
 * $this->data - объект переменных для вывода в шаблоне
 * $this->headers - объект для изменения заголовков при отдаче
 * $this->act - если есть несколько вариантов ответов
 */
class handler{
	function __construct(){
		$this->template='template';#Определяем в какой шаблон будем вписывать
		$this->headers=(object)array();
		$this->uhandler=user::init();
		$this->user=$this->uhandler->user;
		$this->autopostingCron='php '.PATH.'modules/posts/admin/autoposting.cron.php';
	}
	/*
		1. данные для вывода формы редактирования поста
	*/
	function edit($id,$prfxtbl,$cat){
		if($rbac=$this->uhandler->rbac("lightEditor"))
			$this->view='editLight';

		$tbls=\posts\tables::init($prfxtbl);
		$post=new \StdClass;
		#получаем данные поста, если есть ID
		if($id){
			$post=db::qfetch(
				"SELECT post.*,GROUP_CONCAT(rel.cid) AS catConcat
				FROM `{$tbls->post}` post
					LEFT OUTER JOIN `{$tbls->category2post}` rel ON rel.`pid`=post.url
				WHERE post.`id`='$id' GROUP BY post.`id`");
			if(!empty($post->catConcat))
				$cat=explode(',', $post->catConcat);
			#Проверяем разрешение на редактирование новости
			if(!$rbac=$this->uhandler->rbac('editNews')){
				if($this->user->id==$post->user) $rbac=$this->uhandler->rbac('editNewsMy');
			}
			# получаем кейворды
			$post->keywords=getKeywords($post);
			$post->keyword=isset($post->keywords[$post->kid])?$post->keywords[$post->kid]:false;
			# преобразование в мнемоники для совместимости с tinyMCE
			$post->txt=htmlspecialchars($post->txt);
		}else{
			#Проверяем разрешение на cоздание новости
			$rbac=$this->uhandler->rbac("addNews");
		}
		if(@!$rbac){
			$this->headers->location=url::userLogin(); return;
		}
		# получаем данные по категории
			$category=array();
			if(!empty($cat)){
				foreach ($cat as $val) {
					if(empty($cat)) continue;
					$sqlCats[]=db::escape($val);
				}
				if(!empty($sqlCats)){
					db::query("SELECT * FROM `{$tbls->category}` WHERE `url` IN('".implode("','",$sqlCats)."')");
					while ($d=db::fetch()) {
						$category[$d->url]=$d;
					}
				}
			}
		# получаем список используемых сайтов
		$siteList=array();
		db::query("SELECT `site` FROM `{$tbls->post}` WHERE site!='' GROUP BY `site`");
		while ($d=db::fetch()) {
			$siteList[]=$d->site;
		}

		paramsPrettyPrint($post->data);

		return (object)array(
			'post'=>$post,
			'tbl'=>$tbls->post,
			'prfxtbl'=>$tbls->prfx,
			'category'=>$category,
			'siteList'=>$siteList,
			'themes'=>$this->uhandler->rbac('themesSet')?\admin_themes\themes():array(),
			'accessPostAdd'=>$this->uhandler->rbac('addNews'),
			'accessSaveNoText'=>$this->uhandler->rbac('saveNoTextPost'),
			'accessPublish'=>$this->uhandler->rbac('publishPost'),
			'accessSetUser'=>$this->uhandler->rbac('userSetPost'),
			'userList'=>getAuthors($this->uhandler->rbacByUT('addNews')),
			'imagesForm'=>module::exec('images/admin',array('act'=>'edit','pid'=>$id,'tbl'=>$tbls->post,'key'=>@$post->keyword->title),1)->str,
			'access'=>$this->uhandler->access,
			'accessPinpost'=>$this->uhandler->rbac('pinpost'),
			'accessEditKeyword'=>$this->uhandler->rbac('editKeyword'),
		);
	}
	/*
		1. сохраняет пост
		2. поключает форму редактирования поста
	*/
	function save($id,$prfxtbl,$title,$text,$params,$cats,$pincid,$sources,$foruser,$images,$site,$formAction,$keyword,$new_keywords,$theme,$editorlinks){
		$tbls=\posts\tables::init($prfxtbl);
		$post=new \StdClass;
		$error=$message='';
		$published=null;
		$access=$this->uhandler->access;
		# Получаем ID всех родительских категорий
		$cats=getCatsParent($cats);
		if($id){# post update
			$post=db::qfetch("SELECT * FROM `{$tbls->post}` WHERE id='{$id}' LIMIT 1");
			#Проверяем разрешение на редактирование
			$rbac=false;
			$published=$post->published;
			$rbac=\posts\checkAccess($this->uhandler,$post->user);
			if(!$rbac) return array('error'=>'forbidden');
			# refresh cats count
			$catsForRefresh=getCatsByPost($post->url);
			#Обновляем счетчик категории Uncategorized 
				if(!empty($cats) and empty($catsForRefresh)){
					db::query("INSERT INTO `{$tbls->category}` 
						(title,url) 
						VALUES ('Uncategorized','no-category'
					");
					db::query("UPDATE `{$tbls->category}` 
						SET countAdmin=countAdmin+1 where url='no-category'
					");
				}elseif(empty($cats) and !empty($catsForRefresh)){
					db::query("INSERT INTO `{$tbls->category}` 
						(title,url) 
						VALUES ('Uncategorized','no-category'
					");
					db::query("UPDATE `{$tbls->category}` 
						SET countAdmin=countAdmin-1 where url='no-category'
					");
				}
			if(!$foruser) $foruser=$post->user;
		}else{# new post
			# refresh cats count
			$catsForRefresh=$cats;
			#Если не заданы категории то увеличиваем счетчик категории Uncategorized
			if(empty($catsForRefresh)){
				db::query("INSERT INTO `{$tbls->category}` 
					(title,url) 
					VALUES ('Uncategorized','no-category'
				");
				db::query("UPDATE `{$tbls->category}` 
					SET countAdmin=countAdmin+1 where url='no-category'
				");
			}
			if(!$foruser) $foruser=$this->user->id;
		}
		if($status=calcStatus($foruser,$this->uhandler,$published,$formAction))
			$published=$status;
		# проверяем права
		if(!@$access->pinpost){
			$pincid=-1;
		}
		# права для назначения темы поста
		if(!@$access->themesSet)
			$theme='';

		$sv=new Sv();
		$sv->stripText($title,$text,$editorlinks,$access);
		$sv->checkParams($params);
		if($params===false)
			$error='ParseParamsFail';

		$sv->requireFields=array('title');
		# определяем site поста
		if(is_array($site)){
			$site=($site[0]==''&&!empty($site[1]))?$site[1]:$site[0];
			if($site!=''&&!preg_match('!^https?://!is', $site)){
				$site='http://'.$site;
			}
		}
		# сохраняем
		$result=$sv->save(
			$tbls,
			$post,
			$title,
			$text,
			$params,
			$cats,
			$pincid,
			$sources,
			$site,
			$foruser,
			$published,
			$keyword,
			prepareKeywordsFromText($new_keywords),
			$theme
		);
		if(!$result)
			$error='UnknownError';
		else{
			if(!$id) $id=$result;
			$message='saved';
			# обновляем количество постов в затронутых категориях
				module::exec('category',array('act'=>'updateCount','cats'=>$catsForRefresh),'data');
		}
		# сохраняем изменения в историю
		saveInHistory($id, $this->uhandler->user->id);

		# записываем картинки
		if($id&&!empty($images)){
			$moduleImagesData=module::execData('images/admin',[
				'act'=>'save',
				'pid'=>$id,
				'title'=>$title,
				'tbl'=>$tbls->post,
				'images'=>$images
			]);
			if(!empty($moduleImagesData->error)) $error=$moduleImagesData->error;
		}
		return (object)array(
			'html'=>module::exec('posts/admin',array('act'=>'edit','pid'=>$id),1)->str,
			'message'=>$message,
			'error'=>$error,
			'id'=>$id,
		);
	}
	/*
		Загрузка картинок к посту с локали
		- запрос приходит из JS
	*/
	function saveImagesMultiUpload($id,$prfxtbl,$title,$images){
		if(!$id||empty($images)) return;
		$tbls=\posts\tables::init($prfxtbl);
		$moduleImagesData=module::exec('images/admin',array('act'=>'save','pid'=>$id,'title'=>$title,'tbl'=>$tbls->post,'images'=>$images),'data')->data;
		# сохраняем связь с кейвордами от картинок
		saveRelKeyword($moduleImagesData->kids,$id);
		if(!empty($moduleImagesData->error)) 
			echo $moduleImagesData->error;
		else echo 'done';
		die;
	}
	/*
		1. удаляет пост
		2. подключает вывод списка постов
	*/
	function del($pid,$prfxtbl){
		$tbls=\posts\tables::init($prfxtbl);
		# Проверяем наличие прав delNews
		if(!$this->uhandler->rbac('delNews')){
			if($this->uhandler->rbac('delMyUnpublishedNews')){
				#Проверяем разрешение на удаление для данного пользователя
				$user=false;
				list($user)=db::qrow("SELECT `user` FROM `{$tbls->post}` WHERE id='$pid' && `published`!='published'");
				$rbac=($this->user->id===$user);
			}else $rbac=0;
		}else
			$rbac=1;
		
		if(!$rbac){
			return array('error'=>1);
		}
		
		# refresh cats count
		list($url)=db::qrow("SELECT `url` FROM `{$tbls->post}` WHERE `id`='$pid' LIMIT 1");
		$cats=getCatsByPost($url);
		#Обновляем счетчик категории Uncategorized 
			if(empty($cats)){
				db::query("UPDATE `{$tbls->category}` 
					SET countAdmin=countAdmin-1 where url='no-category'
				");
			}

		#Удаляем пост
		db::query("DELETE FROM `{$tbls->post}` WHERE id='$pid'");
		if(db::affected()){
			$message=(object)array('type'=>'success','txt'=>'done');
		}
		# удаляем предыдущие записи о категориях
		db::query("DELETE FROM `{$tbls->category2post}` WHERE `pid`='{$url}'");

		# обновляем количество постов в затронутых категориях
		module::exec('category',array('act'=>'updateCount','cats'=>$cats,'tbl'=>$tbls->post),'data');

		# удаляем картинки
		module::exec('images/admin',array('act'=>'delByPid','pid'=>$pid,'tbl'=>$tbls->post),1);

		# удаляем связь с кейвордами
		db::query("DELETE FROM `".PREFIX_SPEC."keyword2post` WHERE `pid`='{$pid}' && `tbl`='{$tbls->post}'");
		
		return (object)array(
			'message'=>@$message,
		);
	}
	function postNoImageDell($prfxtbl){
		if(!$this->uhandler->rbac('delNews')){
			echo 'forbidden'; return;
		}
		$tbls=\posts\tables::init($prfxtbl);
		$q=db::query("SELECT post.id from `{$tbls->post}` as post 
			LEFT JOIN `".PREFIX_SPEC."imgs` as img
				ON img.pid=post.id and img.tbl='$tbls->post'
			WHERE img.id is NULL"
		);
		while($d=db::fetch($q)){
			db::query("DELETE FROM `{$tbls->post}` WHERE id='$d->id' limit 1");
		}
		return [];
	}
	/*
		массовое удаление постов
	*/
	function delPosts($ids,$prfxtbl){
		$this->template='';
		# Проверяем наличие прав delNews
		if(!$this->uhandler->rbac('delNews')){
			echo 'forbidden'; return;
		}
		if(empty($ids)) return;
		$tbls=\posts\tables::init($prfxtbl);
		$this->template='';
		foreach($ids as $i=>$id){
			$ids[$i]=(int)$id;
			if($ids[$i]<1) unset($ids[$i]);
		}
		
		# refresh cats count
		list($url)=db::qrow("SELECT `url` FROM `{$tbls->post}` WHERE `id` in ('".implode("','",$ids)."') LIMIT 1");
		$cats=getCatsByPost($url);

		#Удаляем пост
		db::query("DELETE FROM `{$tbls->post}` WHERE `id` IN ('".implode("','",$ids)."')");
		if(db::affected()){
			echo 'done';
		}
		# удаляем предыдущие записи о категориях
		db::query("DELETE FROM `{$tbls->category2post}` WHERE `pid` in ('".implode("','",$ids)."')");

		# обновляем количество постов в затронутых категориях
		module::exec('category',array('act'=>'updateCount','cats'=>$cats,'tbl'=>$tbls->post),'data');

		# удаляем картинки
		foreach($ids as $pid){
			module::exec('images/admin',array('act'=>'delByPid','pid'=>$pid,'tbl'=>$tbls->post),1);
		}
		# удаляем связь с кейвордами
		db::query("DELETE FROM `".PREFIX_SPEC."keyword2post` WHERE `pid` IN ('".implode("','",$ids)."') && `tbl`='{$tbls->post}'");
	}
	function install(){
		$tbl=\posts\tables::init();
		db::query("CREATE TABLE IF NOT EXISTS `{$tbl->post}` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`date` datetime NOT NULL,
			`datePublish` datetime DEFAULT NULL,
			`url` varchar(125) NOT NULL,
			`title` varchar(255) NOT NULL,
			`txt` MEDIUMTEXT NOT NULL,
			`data` TEXT NOT NULL,
			`sources` text NOT NULL,
			`user` int(11) NOT NULL,
			`published` int(11) NOT NULL,
			`site` varchar(255) NOT NULL,
			`statViews` int(11) NOT NULL,
			`statViewsShort` int(11) NOT NULL,
			`statShortFlag` date NOT NULL,
			`countPhoto` int(11) NOT NULL,
			`FBpublished` enum('ban','done','proc') NOT NULL DEFAULT 'proc',
			`like` int(11) NOT NULL DEFAULT '0',
			`dislike` int(11) NOT NULL DEFAULT '0',
			`pincid` VARCHAR(255) NOT NULL DEFAULT '',
			`kid` int(11) NOT NULL,
			`theme` VARCHAR(255) NOT NULL DEFAULT '',
			PRIMARY KEY (`id`),
			UNIQUE KEY `url` (`url`),
			KEY `published` (`published`),
			KEY `pincid` (`pincid`),
			KEY `statViews` (`statViews`),
			KEY `statViewsShort` (`statViewsShort`),
			KEY `user` (`user`),
			FULLTEXT KEY `title_txt` (`title`,`txt`),
			FULLTEXT KEY `txt` (`txt`),
			KEY `kid` (`kid`),
			KEY `user_statViews` (`user`,`statViews`),
			KEY `datePublish` (`datePublish`)
		) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;",1);

		db::query("CREATE TABLE IF NOT EXISTS `".PREFIX_SPEC."{$tbl->postHistory}` (
			`indexId` int(11) NOT NULL AUTO_INCREMENT,
			`id` int(11) NOT NULL,
			`date` datetime NOT NULL,
			`url` varchar(125) NOT NULL,
			`title` varchar(255) NOT NULL,
			`txt` MEDIUMTEXT NOT NULL,
			`sources` text NOT NULL,
			`user` int(11) NOT NULL,
			`published` int(11) NOT NULL,
			`site` varchar(255) NOT NULL,
			`statViews` int(11) NOT NULL,
			`statViewsShort` int(11) NOT NULL,
			`statShortFlag` date NOT NULL,
			PRIMARY KEY (`indexId`),
			FULLTEXT KEY `txt` (`txt`)
		) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;",1);

		db::query("CREATE TABLE IF NOT EXISTS `".PREFIX_SPEC."user2vote` (
			`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`userID` int(10) unsigned NOT NULL,
			`postID` int(10) unsigned NOT NULL,
			PRIMARY KEY (`id`),
			UNIQUE KEY `vote` (`userID`,`postID`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8;",1);

		db::query("CREATE TABLE IF NOT EXISTS `".PREFIX_SPEC."keyword2post` (
			`kid` int(11) unsigned NOT NULL,
			`pid` int(11) unsigned NOT NULL,
			`tbl` varchar(255) NOT NULL, 
			UNIQUE KEY `kid2pid` (`kid`,`pid`,`tbl`),
			KEY `pid` (`pid`,`tbl`)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8
		",1);

		db::query("CREATE TABLE IF NOT EXISTS `keyword` (
			`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			`title` varchar(255) NOT NULL,
			PRIMARY KEY (`id`),
			UNIQUE KEY `title` (`title`)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8
		",1);

		module::execData('posts/admin',['act'=>'autoPostingCron']);
		return array('instaled');
	}
	/*
		1. список статей по пользователю
	*/
	function listByUser($page,$num,$user,$type){
		#Редиректим если нет прав на просмотр этой страницы
		if(!$this->uhandler->rbac(array('searcherList','authorList','editorList'))){
			$this->headers->location=HREF; return;
		}
		$tbls=new \posts\tables;
		#Данные пользователя
		$userData=db::qfetch("SELECT * FROM `".PREFIX_SPEC."users` WHERE `id`='{$user}' LIMIT 1");
		if(!empty($userData))
			$userData->longName=$userData->mail.($userData->name!=''?"&nbsp;({$userData->name})":'');

		$authorList=$this->uhandler->rbac('authorList');
		$searcherList=$this->uhandler->rbac('searcherList');
		# для админа и редактора
		if($editorList=$this->uhandler->rbac('editorList')){
			$sqlWhere='';
			if(!$type) $type='text';
			if($type=='text')
				$sqlWhere=" && `txt`!=''";
			elseif($type=='notext')
				$sqlWhere=" && `txt`=''";
			elseif($type=='pub_no_without_user')
				$sqlWhere=" && published=0 && user = '0'";	
			elseif($type=='unpublished')
				$sqlWhere=" && published=0";
			elseif($type=='autoposting')
				$sqlWhere=" && published=-1";
			elseif($type=='published')
				$sqlWhere=" && published=1";
			elseif($type=='free_posts')
				$sqlWhere=" && `published` = 1 && site = ''";
			$sqlWhere.=$sqlWhereCount=$user!==false?" && `user`='{$user}'":'';
		}
		
		$q="SELECT * FROM `{$tbls->post}` post WHERE 1 ".$sqlWhere;
		
		$history = PREFIX_SPEC."postHistory";
		// если тип - история, то выборка из другой таблицы
		if ($type=='history') {
			$q="SELECT * FROM `{$history}` WHERE 1 ".$sqlWhereCount;	
			list($count)=db::qrow(str_replace(" * "," COUNT(*) ",$q));
			$start=($page-1)*$num;
			db::query("
				SELECT post.*, user.name AS authorName, user.mail AS authorMail, IF(COUNT(post.id)>1,1,0) AS has_diff
				FROM ({$q}) post
				/* присоединяем данные пользователя */
				LEFT JOIN `".PREFIX_SPEC."users` user ON user.id=post.user 
				/* присоединяем таблицу постов */
				LEFT JOIN `{$tbls->post}` p ON post.id=p.id 
				/* уникальность по id и user */
				GROUP BY post.id
				ORDER BY post.`datePublish` DESC,post.`date` DESC LIMIT $start,$num
			");
		}else{
			list($count)=db::qrow(str_replace(" * "," COUNT(*) ",$q));
			$start=($page-1)*$num;
			db::query("
				SELECT post.*, user.name AS authorName, user.mail AS authorMail
				FROM ({$q}) post
				/* присоединяем данные пользователя */
				LEFT JOIN `".PREFIX_SPEC."users` user ON user.id=post.user
				GROUP BY post.id
				ORDER BY post.`datePublish` DESC,post.`date` DESC LIMIT $start,$num
			");
		}
		
		$posts=array();
		while($d=db::fetch()){
			$qNewsIds[]=$d->id;
			$d->txt=\posts\cutText(strip_tags($d->txt));
			$d->funcPanel=module::exec('posts',array('act'=>'editPanel','post'=>$d),1)->str;
			$d->authorName=\posts\setAuthorName($d);
			$posts[$d->id]=$d;
		}

		return array(
			'posts'=>$posts,
			'page'=>$page,
			'num'=>$num,
			'count'=>$count,
			'userData'=>@$userData,
			'paginator'=>module::exec('plugins/paginator',
				array(
					'page'=>$page,
					'num'=>$num,
					'count'=>$count,
					'uri'=>url::post_byUser(@$userData->id,$type,'%d'),
					true,
				)
			,1)->str,
			'usersList'=>getAuthors($this->uhandler->rbacByUT('addNews')),
			'authors'=>getAuthors(array(2)),
			'postsCounter'=>countPosts($sqlWhereCount,$tbls->post,$this->uhandler),
			'type'=>$type,
			'accessPublished'=>$this->uhandler->rbac('publishPost'),
			'accessHistory'=>$this->uhandler->rbac('viewHistory'),
			'accessAutoPosting'=>$this->uhandler->rbac('autoPosting'),
			'editorList'=>$editorList,
			'authorList'=>$authorList,
			'searcherList'=>$searcherList,
		);
	}
	/*
		Изменение статуса публикации поста
	*/
	function published_update($pid,$prfxtbl,$published){
		$tbls=\posts\tables::init($prfxtbl);
		$this->template='';
		if(!$pid) return;
		$types=[
			'published'=>1,
			'unpublished'=>0,
		];
		if(in_array($published, array_flip($types))){
			db::query("
				UPDATE `{$tbls->post}` 
				SET published='".$types[$published]."'".($types[$published]==1?',datePublish=NOW()':'')." 
				WHERE id = '".$pid."' LIMIT 1");
			# refresh cats count
			$catsForRefresh=getCatsByWhere("id='{$pid}'");
			# обновляем количество постов в затронутых категориях
			module::exec('category',array('act'=>'updateCount','cats'=>$catsForRefresh,'tbl'=>'{$tbls->post}'),'data');
		}
	}
	/*
		1. статистика по добавленным постам
	*/
	function stat($user,$start,$stop){
		if(!$this->uhandler->rbac('statPost')){
			$this->headers->location=HREF; 
			return;
		}
		if($user)
			$userData=db::qfetch("SELECT * FROM `".PREFIX_SPEC."users` WHERE `id`='{$user}' LIMIT 1");

		$sqlWhere=$user?" && `user`='{$user}'":'';
		$join=$user?"JOIN ".PREFIX_SPEC."users ON ".PREFIX_SPEC."users.id = post.user":'';
		#Получаем данные о постах по датам старта написания поста
		db::query("SELECT 
			COUNT(*) cn, 
			DATE_FORMAT(`date`, '%Y-%m-%d') AS `reformdate`,
			COUNT(IF(published IN('researchchecked','researchdone','remakesearch'), 1, NULL)) as search,
			COUNT(IF(published = 'waitcopywrite' OR published = 'remakecontent', 1, NULL)) as copywriting,
			COUNT(IF(published = 'readytopublish', 1, NULL)) as ready,
			COUNT(IF(published = 'published', 1, NULL)) as published
		FROM
			`post`
		JOIN ".PREFIX_SPEC."users ON ".PREFIX_SPEC."users.id = post.user
		WHERE
			`date` != '0000-00-00' {$sqlWhere}
				&& `date` >= '{$start}'
				&& `date` < '".date("Y-m-d",strtotime($stop)+24*3600)."'
				&& `txt` != ''
		GROUP BY `reformdate`
		ORDER BY `reformdate` DESC");
		$data=array();
		$total=(object)array('search'=>0,'copywriting'=>0,'ready'=>0,'published'=>0,'sums'=>0);
		while($d=db::fetch()){
			$d->sum = $d->search+$d->copywriting+$d->ready+$d->published;	
			$d->pubReal=0;
			$total->search+=$d->search;
			$total->copywriting+=$d->copywriting;
			$total->ready+=$d->ready;
			$total->published+=$d->published;
			$total->sums+=$d->sum;
			$data[$d->reformdate]=$d;
		}
		#Получаем Данные о дне реальной публикации
		db::query("SELECT 
			COUNT(*) cn, 
			DATE_FORMAT(`datePublish`, '%Y-%m-%d') AS `rdate`
		FROM `post` {$join}
		WHERE
			`datePublish` != '0000-00-00' {$sqlWhere}
				&& `datePublish` >= '{$start}'
				&& `datePublish` < '".date("Y-m-d",strtotime($stop)+24*3600)."'
		GROUP BY `rdate` ORDER BY `rdate` DESC");
		$fields=array('published','ready','copywriting','search','reformdate','sum');
		while($d=db::fetch()){
			if(empty($data[$d->rdate])){
				foreach($fields as $v){
					@$data[$d->rdate]->$v=0;
				}
				$data[$d->rdate]->reformdate=$d->rdate;
			}
			@$data[$d->rdate]->pubReal=$d->cn;
			$total->pubReal+=$d->cn;
		}
		$data=array_values($data);
		return array(
			'userData'=>@$userData,
			'stat'=>$data,
			'total'=>$total,
			'usersList'=>getAuthors($this->uhandler->rbacByUT('addNews')),
			'dateForm'=>module::exec('plugins/dateForm',
				array(
					'start'=>$start,
					'stop'=>$stop,
					'params'=>array(
						'module'=>'posts/admin',
						'act'=>'stat',
						'user'=>$user,
					)
				)
			,1)->str,
		);
	}
	
	/*
	 * Просмотр изменений в постах
	 */
	function diff($id, $from = 0, $to = 0) {
		if(!$this->uhandler->rbac('viewHistory')){
			$this->headers->location=HREF; 
			return;
		}
		
		$this->template = '';	
		$history = PREFIX_SPEC."postHistory";
		
		if($from && $to && $from != $to){
			$sqlWhere="h.`indexId` IN ({$from}, {$to})";
		}else{
			$sqlWhere="h.`id`='{$id}'";
		}
		db::query(
			"SELECT h.date, h.title, h.txt, u.name AS authorName, u.mail AS authorMail FROM `{$history}` h
				LEFT JOIN ".PREFIX_SPEC."users u ON u.id = h.user
			WHERE {$sqlWhere} ORDER BY h.indexId DESC LIMIT 2");	
				
		$h = array();
		while($row = db::fetch()) {
			$row->name=\posts\setAuthorName($row);
			$h[] = $row;
		}
		
		if(!$h) exit;
		
		# Библиотека PHP-FineDiff
		require_once(PATH.'modules/posts/admin/finediff.php');

		$opcodes=\FineDiff::getDiffOpcodes($h[1]->txt, $h[0]->txt);
		$diff=\FineDiff::renderDiffToHTMLFromOpcodes($h[1]->txt, $opcodes);
		$diff=html_entity_decode($diff);
		
		return array(
			'history'=>$h,
			'diff'=>$diff,
		);
	}
	
	/*
	 * Просмотр истории
	 */
	function archive($id) {
		if(!$this->uhandler->rbac('viewHistory')){
			$this->headers->location=HREF; 
			return;
		}
		
		$history = PREFIX_SPEC."postHistory";
		db::query(
			"SELECT indexId, h.date, h.title, h.txt, u.name AS authorName, u.mail AS authorMail FROM `{$history}` h
				LEFT JOIN ".PREFIX_SPEC."users u ON u.id = h.user
			WHERE h.`id`='{$id}' ORDER BY h.indexId DESC");
		
		$h = array();
		while($row = db::fetch()) {
			$row->name=\posts\setAuthorName($row);
			$h[] = $row;
		}
		
		return array(
			'history' => $h,
		);
	}
	
	/*
	 * Установка авторов для выбранных статей
	 */
	function setAuthors($ids, $author_id) {
		if (!empty($ids)) {
			db::query('UPDATE post SET user = '.$author_id.' WHERE id IN ('.implode(',', $ids).')');
		}
		$this->headers->location=url::post_byUser();
		return;
	}
	function imgRecount($pid,$tbl){
		if(!empty($tbl)){
			db::query("UPDATE `{$tbl}` SET `countPhoto`=(SELECT COUNT(*) FROM `".PREFIX_SPEC."imgs` 
			WHERE `pid`=`{$tbl}`.`id` AND `tbl`='{$tbl}') WHERE id='{$pid}' LIMIT 1");
		}
		return;
	}
	/*
		пересчитывает количество картинок у всех постов
	*/
	function imgRecountAll($tbl){
		if($this->user->rbac!=1) return;
		db::query("UPDATE `post` SET `countPhoto`=(SELECT COUNT(*) FROM `".PREFIX_SPEC."imgs` 
			WHERE `pid`=`post`.`id` AND `tbl`='post')");
		foreach (getPrefixList() as $prfx) {
			$tbl="{$prfx}_post";
			db::query("UPDATE `{$tbl}` SET `countPhoto`=(SELECT COUNT(*) FROM `".PREFIX_SPEC."imgs` 
				WHERE `pid`=`{$tbl}`.`id` AND `tbl`='{$tbl}')");
		}
		print "recounting images Done";
		die;
	}
	
	/*
		Like/Dislike
	*/
	function updateLike($pid,$isLike){
		if(!$pid) return;
		if(!$this->uhandler->rbac('voteAccess')){
			return;
		}
		$count=(int)db::qfetch("SELECT COUNT(*) as cnt FROM `".self::tbl."` WHERE id={$pid} AND user={$this->user->id}")->cnt;
		if($count){
			return;
		}
		db::query("INSERT IGNORE ".PREFIX_SPEC."user2vote (userID,postID) VALUES ({$this->user->id},{$pid})");
		if($isLike){
			db::query("UPDATE `".self::tbl."` SET `like`=`like`+1 WHERE id={$pid}");
		}else{
			db::query("UPDATE `".self::tbl."` SET `dislike`=`dislike`+1 WHERE id={$pid}");
		}
		exit;
	}
	function updateFBpublish($pid,$isDone){
		if(!$pid) return;
		if(!$this->uhandler->rbac('FBpublish')){
			return;
		}
		if($isDone){
			db::query("UPDATE `".self::tbl."` SET `FBpublished`='done' WHERE id={$pid}");
		}else{
			db::query("UPDATE `".self::tbl."` SET `FBpublished`='ban' WHERE id={$pid}");
		}
		db::query("DELETE FROM ".PREFIX_SPEC."user2vote WHERE postID='{$pid}'");
		exit;
	}
	function updateDates(){
		if(!$this->uhandler->rbac('editNews')) die('forbidden');
		updateDates();
		exit;
	}
	/*
		возвращает форму редактирования кейвордов
		вызов происходит из редактирования поста с помощью JS
	*/
	function editKeywords($pid,$prfxtbl){
		$tbls=\posts\tables::init($prfxtbl);
		$this->template='';
		$post=db::qfetch("SELECT id,kid FROM `{$tbls->post}` WHERE `id`='{$pid}' LIMIT 1");
		$post->keywords=array();
		#Проверяем разрешение на редактирование кейвордов
		if($accessEditKeyword=$this->uhandler->rbac('editKeyword')){
			# получаем кейворды
			$post->keywords=getKeywords($post);
			unset($post->keywords[$post->kid]);
		}
		return (object)array('post'=>$post,'tbl'=>$tbls->post,);
	}
	/*
	 *	Добавляем новые кейворды к посту
	*/
	function addKeys($pid,$tbl,$keys){
		if(!isset($this->uhandler->access->editKeyword)) return;
		$this->template='';
		$tbls=\posts\tables::initByTbl($tbl);
		$keys=prepareKeywordsFromText($keys);
		addnNewKeywords($pid,$keys);
		echo "added: ".join(', ',$keys);
	}
	/*
	 *	отвязываем кейворд от поста
	*/
	function dellKeys($pid,$tbl,$key){
		if(!isset($this->uhandler->access->editKeyword)) return;
		$this->template='';
		$tbls=\posts\tables::initByTbl($tbl);
		db::query("DELETE FROM `".PREFIX_SPEC."keyword2{$tbls->post}` WHERE kid='$key' && pid='$pid' && tbl='{$tbls->post}'");
		removeKeyword([$key]);
		echo "removed";
	}
	/*
		Функция обновления сайта. Выполняет вывод новых постов из автопостинга
	*/
	function autoPostingCheck(){
		$numaffected=0;
		static $checked=false;
		if(!$checked){
			db::query("UPDATE post SET published=1 WHERE datePublish<='".($date=date('Y-m-d H:i:s'))."' && published=-1");
			db::query("UPDATE ".PREFIX_SPEC."category2post SET published=1 WHERE datePublish<='$date' && published=-1");
			if($numaffected=db::affected()){
				db::query("SELECT url FROM post WHERE `published`='published'");
				while ($d=db::fetch()) {
					$ids[]=$d->url;
				}
				#обновляем количество постов в категориях
				if(!empty($ids))
					module::exec('category',array('act'=>'updateCount','cats'=>getCatsByPost($ids),'tbl'=>'post'),'data');
			}
			$checked=true;
		}
		return ['affected'=>$numaffected];
	}
	/*
		Страница изменения количесва постов добавляющихся на сайт по времени
	*/
	function autoPosting($submit=false, $per_day=5, $at_once=10){
		set_time_limit(0);
		if(!$this->uhandler->access->autoPosting) die('forbidden');
		if($submit){
			db::query("REPLACE INTO ".PREFIX_SPEC."config (`key`, `value`) VALUES ('autopost_per_day', {$per_day})");

			$posts = db::qall("SELECT id,url,datePublish FROM post WHERE published = -1 ORDER BY datePublish ASC");
			if(count($posts)>0){
				if($at_once>0){
					foreach($posts as $k=>$post) {
						if($at_once-- <= 0){break;}
						if(strtotime($post->datePublish)>time()){
							$time=strtotime("today");
							$time+=mt_rand(0,time()-$time);
							$date=date("Y-m-d H:i:s",$time);
						}else{
							$date=$post->datePublish;
						}
						db::query("UPDATE post SET published=1,datePublish='$date' WHERE id = ".intval($post->id));
						db::query("UPDATE ".PREFIX_SPEC."category2post SET published=1,datePublish='$date' WHERE pid = '{$post->url}'");
						unset($posts[$k]);
					}
				}
				foreach($posts as &$post){
					$datepday=getDatePublish($per_day);
					db::query("UPDATE post SET datePublish = '$datepday' WHERE id = ".intval($post->id));
					db::query("UPDATE ".PREFIX_SPEC."category2post SET published=1,datePublish='$datepday' WHERE pid = '{$post->url}'");
				}
			}
			#обновляем количество постов в категориях
			module::exec('category',array('act'=>'updateCount','cats'=>getCatsByWhere("published=1"),'tbl'=>'post'),'data');
		}else{
			list($per_day)=db::qrow("SELECT `value` FROM ".PREFIX_SPEC."config WHERE `key`='autopost_per_day'");
			if(is_null($per_day))$per_day =5;
		}
		# количество доступных постов
		list($count)=db::qrow("SELECT COUNT(*) FROM post WHERE published=-1");

		return (object)array(
			'per_day'=>$per_day,
			'count'=>(int)$count,
			'cron'=>cron::status($this->autopostingCron),
		);
	}
	function autoPostingCron($easy){
		if(cron::status($this->autopostingCron)===true)
			cron::remove($this->autopostingCron);
		else
			cron::set('10 0 * * *',$this->autopostingCron);
		if(!$easy) $this->headers->location=url::autoPosting();
	}
	function createTblPosts($save,$prefix){
		if($save){
			$tbl=\posts\tables::init($prefix);
			module::execData('posts/admin',array('act'=>'install'),1);
			module::execData('category/admin',array('act'=>'install'),1);
		}
		return [];
	}
}
