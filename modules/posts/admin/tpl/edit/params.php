<div <?if(!@$data->access->editCode){?>style="display:none"<?}?>>
	<label for="post-params"><span>Params for template</span></label>
	<small>(JSON format or short equivalent, <a style="cursor:pointer" id="post-params-example">example</a>)
		<pre style="display:none;">
a: 1
b: {
        b1: value
        b2: value
    }
	</pre>
	</small>
	<script type="text/javascript">
		$('#post-params-example').click(function(){
			var parent=$(this).parent('small');
			parent.children('pre').toggle(200);
		});
	</script>
	<textarea name="post-params" placeholder="example: {&quot;a&quot;: 1}"><?=$post->data?></textarea><br />
	<script>
	$("textarea[name='post-params']").on('keydown', function(ev) {
	    var keyCode = ev.keyCode || ev.which;

	    if (keyCode == 9) {
		ev.preventDefault();
		var start = this.selectionStart;
		var end = this.selectionEnd;
		var val = this.value;
		var selected = val.substring(start, end);
		var re, count;

		if(ev.shiftKey) {
		    re = /^\t/gm;
		    count = -selected.match(re).length;
		    this.value = val.substring(0, start) + selected.replace(re, '') + val.substring(end);
		    // todo: add support for shift-tabbing without a selection
		} else {
		    re = /^/gm;
		    count = selected.match(re).length;
		    this.value = val.substring(0, start) + selected.replace(re, '\t') + val.substring(end);
		}

		if(start === end) {
		    this.selectionStart = end + count;
		} else {
		    this.selectionStart = start;
		}

		this.selectionEnd = end + count;
	    }
	});
	</script>
</div>
