<fieldset class="keywords-edit">
	<legend>Edit keywords</legend>
	<?foreach ($data->post->keywords as $k) {?>
	<div>
		<?=$k->title?>
		<a href="#" data-value="<?=$k->id?>" onclick="dellKeys($(this).attr('data-value'));">X</a>
	</div>
	<?}?>
	<script>
	function addKeys(){
		$.get(
				window.location.basepath+'?module=posts/admin',
				{act:'addKeys',pid:'<?=$data->post->id?>',tbl:'<?=$data->tbl?>',keys:$("textarea[name='new_keywords']").val()},
				function(html){
					$('#keysAddStatus').html(html);
				}
		);
	}
	function dellKeys(val){
		$.get(
				window.location.basepath+'?module=posts/admin',
				{act:'dellKeys',pid:'<?=$data->post->id?>',tbl:'<?=$data->tbl?>',key:val},
				function(html){
					$('#keysAddStatus').html(html);
				}
		);
		return false;
	}
	</script>
	<h1>Add new</h1>
	<div id="keysAddStatus"></div>
	<textarea name="new_keywords" rows=10 style="width:100%"></textarea><br>
	<input type="button" onclick="addKeys()" value="save">
</fieldset>
