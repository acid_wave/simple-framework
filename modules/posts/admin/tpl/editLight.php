<?php
$tpl->title="Add/Edit post. ".NAME;
$tpl->desc="";
$firstParentCat=current($data->category);
$post=$data->post;
?>
<script type="text/javascript" src="<?=HREF?>/files/template/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?=HREF?>/files/posts/admin/js/textForm.js"></script>
<script type="text/javascript" src="<?=HREF?>/files/posts/admin/js/addCategories.js"></script>
<?if($data->accessPublish){?> 
	<script type="application/javascript" src="<?=HREF?>/files/posts/admin/js/published.js"></script>
		<style>
	.post_panel{
		box-shadow: 1px 1px 2px 0px;
		float: right;
		padding: 3px 4px 0 0;
		background-color: white !important;
	}
	.post_panel .icons{display: inline-block;width: 20px;height: 20px;background-repeat: no-repeat;background-image:url('<?=HREF?>/modules/posts/tpl/files/icons/iset.png');background-color: transparent;padding: 0;margin: 0;}
	.post_panel .icons-inactive{background-position: -18px -388px;}
	.post_panel .icons-active{background-position: -64px -388px;}
	</style>
<?}?>
<div class="breadcrumbs">
	<?if(!empty($post->url)){?><a href="<?=url::post($post->url,$data->prfxtbl)?>"><?=$post->title?></a><?}?>
</div>
<form action="<?=url::post_adminAdd()?>" method="post" enctype="multipart/form-data" class="form-post-edit">
<div class="cols">
	<div class="left-col">
		<div>
			<?if(!empty($post->id)){?>
				
				<small style="float:right" >status:
				<span class="post_panel">
				<?if($data->access->publishPost){
					$status=$post->published=='published'?'yes':'no';?>
					<button 
						name="published" 
						data-id="<?=$post->id?>"
						data-prfxtbl="<?=$data->prfxtbl?>"
						class="published-<?=$status?> icons icons-<?=$status=='yes'?'active':'inactive'?>" 
						value="<?=$status?>"
						style="cursor:pointer;border: 0;"
						title="<?=$post->published?>"
					></button>
				<?}else {print $post->published;}?>
				</span>
				</small>
			<?}?>
			<label>
				<span>Title *</span>
				<input type="text" name="title" value="<?=!empty($post->title)?$post->title:''?>" size="255" />
			</label>
			<?include $template->inc('editLight/editor.php');?>
			<input type="hidden" name="pid" value="<?=!empty($post->id)?$post->id:''?>"/>
			<input type="hidden" name="tbl" value="<?=!empty($data->tbl)?$data->tbl:''?>"/>
			<input type="hidden" name="prfxtbl" value="<?=!empty($data->prfxtbl)?$data->prfxtbl:''?>"/>
			<?if(!empty($post->date)){?><br/><span><small>date:&nbsp;<?=$post->date?></small></span><?}?>
			<?include $template->inc('editLight/buttons.php');?>
		</div>
	</div>
	<div class="right-col" style="margin-top: 0;">
		<?include $template->inc('edit/category.php');?>
		<div id="type-selector">
			<?=$data->imagesForm?>
		</div>
	</div>
</div>
</form>
