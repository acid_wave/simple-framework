<?php
namespace posts\rating;
use control,db;

class input extends control{
	function __construct($input){
		if($input->act=='set'){
			$this->data=[
				'pid'=>@$input->pid?db::escape($input->pid):false,
				'rate'=>@$input->rate?db::escape($input->rate):false,
			];
		}else
			$this->act=false;
	}
}
