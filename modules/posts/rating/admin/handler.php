<?php
namespace posts\rating\admin;
use module,db,url,cache;

class handler{
	function __construct(){
		$this->template='template';
	}
	function install(){
		db::query("
			CREATE TABLE IF NOT EXISTS `".PREFIX_SPEC."postRating` (
			  `pid` int(11) NOT NULL,
			  `uid` int(11) NOT NULL,
			  `rating` int(11) NOT NULL COMMENT 'rating (1-5)',
			   UNIQUE KEY `pid_uid` (`pid`,`uid`),
			   KEY `pid` (`pid`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		",1);
		return [];
	}
}
