<?php
namespace posts_lists_rating;
use module,db,url;

/*
	получает список пользователей сделавших pin указанных картинок
*/
function getUserList(&$imgs,$limit=5){
	db::query("
		SELECT DISTINCT(imid),uid FROM `".PREFIX_SPEC."like2img` li 
			WHERE imid IN (".implode(",",array_keys($imgs)).") 
			ORDER BY `date` DESC LIMIT {$limit}");
	while($d=db::fetch()){
		$imgs[$d->imid]->users[]=&$users[$d->uid];
	}
	if(!empty($users)){
		include_once module::$path.'/user/func.php';
		\user\getUser(array_keys($users),$users);
	}
	return $imgs;
}
?>