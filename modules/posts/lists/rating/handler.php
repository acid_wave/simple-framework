<?php
namespace posts_lists_rating;
use module,db,url,user\user;
require_once(module::$path."/posts/handler.php");
require_once(module::$path."/user/handler.php");

class handler{
	function __construct(){
		$this->userControl=user::init();
		$this->user=$this->userControl->user;
	}
	function topicSelect($page,$num,$image){
		$this->template='';
		if($image){
			$this->pin($image);
		}
		#Получаем кол-во оцененых картинок
			list($count)=db::qrow("SELECT count(imid) FROM `".PREFIX_SPEC."like2img` WHERE uid='{$this->user->id}'");
		$top=array();
		$start=($page-1)*$num;
		$sql="SELECT * FROM post ORDER BY statViewsShort DESC limit $start,$num";
		db::query($sql);
		while ($d=db::fetch()) {
			$d->txt=\posts\cutText(strip_tags($d->txt),20);
			$d->funcPanel=module::exec('posts',array('act'=>'editPanel','post'=>$d),1)->str;
			$top[$d->id]=$d;
		}
		\posts\getImg2list($top);
		return array(
			'access'=>$this->userControl->access,
			'posts'=>$top,
			'prfxtbl'=>'post',
			'page'=>$page,
			'count'=>$count,
		);
	}
	function pin($imid){
		if(!$imid||!$this->user->id) return;
		db::query("INSERT IGNORE INTO `".PREFIX_SPEC."like2img` values('{$this->user->id}',".db::escape($imid).", NOW())");
	}
}
