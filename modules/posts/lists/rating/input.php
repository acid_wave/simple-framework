<?php
/*
* Должен возвращать
* $this->data - объект обработанных входных переменных
* $this->act - какую функцию обработки используем
*/
class posts_lists_rating extends control{
	function __construct($input=''){ # $input - объект входных переменных от других модулей
		if(@$input->act=='')$input->act='index';
		$this->act=$input->act;
		if($input->act=='topicSelect'){
			$this->data=(object)array(
				'page'=>empty($input->page)?1:$input->page,
				'num'=>3,
				'image'=>empty($input->image)?false:(int)$input->image,
			);
		}elseif($input->act=='pin'){
			$this->data=(object)array(
				'imid'=>empty($input->imid)?false:(int)$input->imid,
			);
		}else{
			$this->act=false;
		}
	}
}
