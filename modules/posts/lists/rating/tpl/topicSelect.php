<link rel="stylesheet" href="<?=HREF?>/modules/posts/lists/tpl/files/topicSelect.css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('input.quiz-button').parents('.quiz-button').click(function(){
			var button=$(this).find('input.quiz-button');
			document.location="<?=url::topicSelect()?>&page=<?=$data->page+1?>&image="+button.attr('data-result');
			return false;
		});
		$('input.quiz-button').click(function(){
			var button=$(this);
			document.location="<?=url::topicSelect()?>?module=posts/lists&act=topicSelect&page=<?=$data->page+1?>&image="+button.attr('data-result');
			return false;
		});
		
	});
</script>
<p style="text-align:center">Help us identify best match content for you.</p>
<h1 style="text-align:center">Please select minimum 10 images you like best!</h1></h1>
<p style="text-align:center">Already done:<?=$data->count?></p>
<p></p>
<div class="row">
    <?
    foreach($data->posts as $k=>$post){
		$img=$post->imgs[0];
		?>
		<div class="4u 12u(medium)">
			<section class="box feature hover-border quiz-button" style="text-align: center;">
				<div class="inner">
					<header>
						<h2><?=ucfirst($post->title)?></h2>
						<p style="color: #1a1a1a;"></p>
						<center>
						<img style="height:150px;" src="<?=url::image($img)?>" alt="<?=$post->title?>" />
						</center>
					</header>
					<input class="quiz-button color-red" data-result="<?=$img->id?>" type="button" value="Choose">
				</div>
			</section>
		</div>
	<?}?>
</div>
<br>
