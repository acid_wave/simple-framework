<?php
/*
* Должен возвращать
* $this->data - объект обработанных входных переменных
* $this->act - какую функцию обработки используем
*/
class posts_lists extends control{
	function __construct($input=''){ # $input - объект входных переменных от других модулей
		$this->data=(object)array();
		#default
		if(empty($input->act)) $input->act='mainList';
		
		if($input->act=='mainList'){
			$this->data=(object)array(
				'category'=>empty($input->cat)?'':db::escape(urldecode($input->cat)),
				'prfxtbl'=>empty($input->prfxtbl)?'':$input->prfxtbl,
				'uid'=>@(int)$input->uid,
				'page'=>empty($input->page)?1:$input->page,
				'num'=>cookiePage($input,'num',12),
				'filter'=>empty($input->filter)?'':$input->filter,
			);
		}elseif($input->act=='postByCat'){
			$this->data=(object)array(
				'curl'=>empty($input->curl)?'':db::escape(urldecode($input->curl)),
				'prfxtbl'=>empty($input->prfxtbl)?'':$input->prfxtbl,
				'uid'=>@(int)$input->uid,
				'page'=>empty($input->page)?1:$input->page,
				'num'=>cookiePage($input,'num',10),
				'filter'=>empty($input->filter)?'':$input->filter,
			);
		}elseif($input->act=='suggests'){
			$this->data=(object)array(
				'category'=>empty($input->cat)?'':db::escape(urldecode($input->cat)),
				'prfxtbl'=>empty($input->prfxtbl)?'':$input->prfxtbl,
				'page'=>empty($input->page)?1:$input->page,
				'num'=>cookiePage($input,'num',12),
			);
		}elseif($input->act=='subCatList'){
			$this->data=(object)array(
				'category'=>empty($input->cat)?'':db::escape(urldecode($input->cat)),
				'prfxtbl'=>empty($input->prfxtbl)?'':$input->prfxtbl,
				'num'=>empty($input->num)?5:$input->num,
			);
		}elseif($input->act=='listVote'){
			$this->data=(object)array(
				'page'=>empty($input->page)?1:$input->page,
				'num'=>cookiePage($input,'num',10),
			);
		}elseif($input->act=='listMyVote'){
			$this->data=(object)array(
				'page'=>empty($input->page)?1:$input->page,
				'num'=>cookiePage($input,'num',10),
			);
		}elseif($input->act=='listByUser'||$input->act=='listDownloadByUser'){
			$this->data=(object)array(
				'page'=>empty($input->page)?1:$input->page,
				'num'=>cookiePage($input,'num',12),
				'uid'=>@(int)$input->uid,
				'prfxtbl'=>empty($input->prfxtbl)?'':$input->prfxtbl,
			);
		}elseif($input->act=='random'){
			$this->data=(object)array(
				'num'=>empty($input->num)?20:$input->num,
			);
		}elseif($input->act=='top'){
			$this->data=(object)array(
				'page'=>empty($input->page)?1:$input->page,
				'num'=>empty($input->num)?20:$input->num,
			);
		}elseif($input->act=='topicSelect'){
			$this->data=(object)array(
				'page'=>empty($input->page)?1:$input->page,
				'num'=>empty($input->num)?3:(int)$input->num,
				'image'=>empty($input->image)?false:(int)$input->image,
			);
		}elseif($input->act=='userPhotos'){
			$this->data=(object)array(
				'uid'=>empty($input->user)?false:(int)$input->user,
				'page'=>empty($input->page)?1:(int)$input->page,
				'num'=>empty($input->num)?20:(int)$input->num,
			);
		}else{
			$this->act=false;
		}
	}
}
