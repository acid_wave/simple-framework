<?php
route::set('^/user/([0-9]+)(?:/([0-9]+))?/?',array('module'=>'posts/lists','act'=>'listByUser','uid','page'),2);
route::set('^/downloads/([0-9]+)(?:/([0-9]+))?/?',array('module'=>'posts/lists','act'=>'listDownloadByUser','uid','page'),2);
route::set('^/c/([^/]+?)/([0-9]+)?/?',array('module'=>'posts/lists','act'=>'mainList','cat','page'));
route::set('^/random/',array('module'=>'posts/lists','act'=>'random'),2);
route::set('^/top/',array('module'=>'posts/lists','act'=>'top'),2);
route::set('^/details/([^/]+?)/',array('module'=>'posts/lists','act'=>'subCatList','cat'),2);
route::set('^/list/([^/]+?)/',array('module'=>'posts/lists','prfxtbl'),2);

route::set('/blog/?',array('module'=>'posts/blog/lists'),10);
route::set('/blog/page-(\d+)/?',array('module'=>'posts/blog/lists','page'),12);
route::set('/blog/([^\?][^/]+)/?',array('module'=>'posts/blog/lists','cat'),11);
route::set('/blog/([^/]+)/page-(\d+)/?',array('module'=>'posts/blog/lists','cat','page'),13);
route::set('/blog/([^/]+)\.html',array('module'=>'posts/blog','url'),14);

$urls->author=HREF.'/user/$id/';
$urls->downloads=HREF.'/downloads/$id/';
$urls->userPhotos=HREF.'/pins/$id/';
$urls->listRandom=HREF.'/random/';
$urls->listTop=HREF.'/top/';
$urls->listPrfx=HREF.'/list/$prfxtbl/';

$urls->listVote=HREF.'/?module=posts/lists&act=listVote';
$urls->listMyVote=HREF.'/?module=posts/lists&act=listMyVote';

$urls->post_blog_list=HREF.'/blog/';
$urls->post_blog_category=HREF.'/blog/$category/';

/*
 	определяет вид url для категории
 	url::category()
*/
function url_category($url,$prfxtbl=''){
	if(empty($prfxtbl)){
		$default=$url==''?'':'/c/%s%s/';
		$defaultDetails='/details/%s%s/';	
	}else{
		if($prfxtbl===true) $prfxtbl='';
		$default='/%s/%s/';
		$defaultDetails='/?module=posts/lists&act=subCatList&cat=%s&prfxtbl=%s';
	}
	if(is_object($url)){
		if(!empty($url->subCatList))
			$str=sprintf($defaultDetails,$prfxtbl,$url->url);
		else
			$str=sprintf($default,$prfxtbl,$url->url);
	}else
		$str=sprintf($default,$prfxtbl,$url);
	return HREF.$str;
}
