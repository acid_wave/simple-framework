<?php
namespace posts_lists_search;
use module,db,url,user\user;
require_once(module::$path."/user/handler.php");
require_once(module::$path."/posts/handler.php");
require_once(module::$path."/posts/lists/handler.php");

class handler{
	function __construct(){
		$this->template='template';#Определяем в какой шаблон будем вписывать
		$this->headers=(object)array();
		$this->uhandler=user::init();
		$this->user=$this->uhandler->user;
	}
	/*
		поиск по таблице post
	*/
	function index($q,$prfxtbl,$page,$num){
		$query=db::escape(substr(trim($q),0,255));
		list($count,$tbls,$posts)=searchPosts($query,$prfxtbl,$page,$num);
		return (object)array(
			'cat'=>(object)array('title'=>ucfirst($query)),
			'query'=>ucfirst($query),
			'posts'=>$posts,
			'count'=>$count,
			'prfxtbl'=>$tbls->prfx,
			'paginator'=>module::exec("plugins/paginator",array(
				'page'=>$page,
				'count'=>$count,
				'uri'=>url::searchQueryPage($q,'%d'),
				'num'=>$num,
			),1)->str,
			'page'=>$page,
			'topLevelCats'=>module::exec('category',array('act'=>'mlist','tbl'=>$tbls->post,'category'=>'','parentId'=>''),1)->str,
			'noindex'=>1,
			'user'=>$this->user,
		);
	}
	function autocomplite($query){
		if (!$query) die;
		$this->template='';
		$query = db::escape($query);
		db::query("SELECT distinct title FROM `category` 
			WHERE title LIKE '%{$query}%' and `count`>10 and view=1 ORDER BY title LIMIT 10");
		$data = array();
		while($d=db::fetch()){
			$data[] = $d->title;
		}

		return array(
			'posts'=> $data,
		);
	}
}

function searchPosts($query,$prfxtbl='',$page=1,$num=10,$uselike=1){
		$tbls=\posts\tables::init($prfxtbl);
		$limit="LIMIT ".(($page-1)*$num).",$num";
		$fsql=array();
		#Проверяем наличие категорий для выбранного запроса
			$cat=db::qfetch("SELECT * FROM `category` 
			WHERE title LIKE '$query'");
		if($cat){
			location(url::category($cat->url));
			exit;
		}
		#Создаем запрос, выбираем like или match
			$sqlWhere="&& published=1 && post.pincid=''";
			$qq=preg_split("! +!",$query);
			$good=1;
			foreach($qq as $v){
				if(mb_strlen($v)<=3 && !in_array($v,array('оn','in','at','to','out','оf','off','by','for','and','but','or','if','so','as'))){$good=0;}
			}
			if($good or $uselike==0){
				$sql="SELECT post.*,MATCH(`title`,`txt`) AGAINST('{$query}' IN BOOLEAN MODE) AS score, u.id AS authorId, u.name AS authorName, u.mail AS authorMail FROM `{$tbls->post}` post "
				. "/* присоединяем данные пользователя */
				LEFT JOIN `".PREFIX_SPEC."users` u ON u.id=post.user "
				. "WHERE 1 {$sqlWhere} HAVING `score`!=0 ORDER BY `score` DESC";
				list($count)=db::qrow("SELECT COUNT(id) FROM `{$tbls->post}` post WHERE 1 {$sqlWhere} && MATCH(`title`,`txt`) AGAINST('{$query}'  IN BOOLEAN MODE)");
			}else{
				$sql="SELECT post.*, u.id AS authorId, u.name AS authorName, u.mail AS authorMail FROM `{$tbls->post}` post "
				. "/* присоединяем данные пользователя */
				LEFT JOIN `".PREFIX_SPEC."users` u ON u.id=post.user "
				. "WHERE (`title` LIKE '%{$query}%' OR `txt` LIKE '%{$query}%') {$sqlWhere}";
				list($count)=db::qrow("SELECT COUNT(id) FROM `{$tbls->post}` post WHERE (`title` LIKE '%{$query}%' OR `txt` LIKE '%{$query}%') {$sqlWhere}");
			}
		#Получаем найденные посты
			db::query("$sql $limit");
			$posts=array();
			$userHandler=user::init();
			while($d=db::fetch()){
				$d->imgs=[];
				$d->cats=[];
				$postsUrls[$d->url]=$d->id;
				$posts[$d->id]=\posts_lists\createPost($d,$userHandler->user->id);
			}
		if(!empty($posts)){
			# получаем картинки
				\posts\getImg2list($posts);
			# получаем категории
				db::query("SELECT title,url,cat.pid FROM `{$tbls->category2post}` cat 
					JOIN category ON category.url=cat.cid 
					WHERE cat.pid IN('".implode("','", array_keys($postsUrls))."')");
				while ($d=db::fetch()) {
					$o=clone $d;
					unset($o->pid);
					$posts[$postsUrls[$d->pid]]->cats[$d->url]=$o;
				}
		}
		return [
			$count,
			$tbls,
			$posts
		];
}
