<?php
namespace posts_lists;
use module,db,url,cache,\category\category,user\user;

#функции
require_once module::$path."/posts/lists/func.php";
#сторонние модули
require_once module::$path."/posts/handler.php";
require_once module::$path."/category/handler.php";
require_once module::$path."/user/handler.php";

class handler{
	const tbl='post';
	function __construct(){
		$this->headers=(object)array();
		$this->template='template';#Определяем в какой шаблон будем вписывать
		$this->userControl=user::init();
		$this->user=$this->userControl->user;
	}
	/*
		1. список постов по категории
		2. подключает данные категории (текущей и список подкатегорий)
	*/ 
	function mainList($category,$prfxtbl,$uid,$page,$num,$filter){
		$tbls=\posts\tables::init($prfxtbl);
		$baseCat=new category($category,$this->userControl->access->showAllCat);
		if(!$this->user->id&&$baseCat->id&&(!$baseCat->disableLimit&&$baseCat->count<3)) {
			$this->headers->location=HREF; return;
		}
		#Получить посты категории
			$postsData=module::execData('posts/lists',[
				'act'=>'postByCat',
				'curl'=>$baseCat->url,
				'prfxtbl'=>$prfxtbl,
				'page'=>$page,
				'num'=>$num,
				'filter'=>$filter,
			]);
		if(empty($postsData->posts)&&$category&&!$this->user->id){
			$this->headers->location=HREF; return;
		}
		$sugests=getSugests($baseCat,array_keys($postsData->posts),15);
		#Получаем категории для рекомендуемых папок пинов
		$pinSudgests = [$category];
		if (!empty($sugests)){
			$pinSudgests[] = current($sugests)->url;
			for ($i = 2; $i <= min(4, count($sugests)); $i++)
				$pinSudgests[] = next($sugests)->url;
		}
		#получаем pin посты
			$pins=getPins($category,$this->userControl);			
			\posts\getImg2list($pins,10);
		#Выводим снипеты картинок если у постов нет текстов
			foreach($postsData->posts as $k=>&$pp){
				if($pp->txt==''&&isset($pp->imgs[0]))
					$pp->txt=$pp->imgs[0]->text;
			}		
		#собираем title постов (3 шт) для description страницы
			$forDesc=array();
			if(!empty($postsData->posts)){
				reset($postsData->posts);
				while (@++$i<=3) {
					$p=current($postsData->posts);
					if(!empty($p)){
						$forDesc[]=$p->title; next($postsData->posts);
					}
				}
				reset($postsData->posts);
			}
			$urlfilter=$filter?urldecode(http_build_query(['filter'=>$filter])):'';
			$filter=$filter?$filter:array('datePublish'=>'desc');
		return array(
			'posts'=>$postsData->posts,
			'prfxtbl'=>$tbls->prfx,
			'pins'=>$pins,
			#получаем данные категорий
			'cat'=>$baseCat,
			'breadCrumbs'=>\posts\getCrumbs($baseCat),
			'subCats'=>module::exec('category',array('act'=>'subList','url'=>$category),1)->str,
			'topLevelCats'=>module::exec('category',array('act'=>'mlist','category'=>$category,'parentId'=>@$baseCat->parentId),1)->str,
			'page'=>$page,
			'num'=>$num,
			'paginator'=>module::exec('plugins/paginator',
				array(
					'act'=>'index',
					'page'=>$page,
					'num'=>$num,
					'count'=>$postsData->count,
					'uri'=>url::category($category?$category:'main',$tbls->prfx)."%d/".($urlfilter!=''?'?'.$urlfilter:''),
					true,
				)
			)->str,
			'count'=>$postsData->count,
			'access'=>$this->userControl->access,
			'forDesc'=>$forDesc,
			'suggests'=>!empty($sugests)?$sugests:[],
			'user'=>$this->user,
			'pinForm'=>module::execStr('pins',['act'=>'pinForm','sudgests'=>$pinSudgests]),
			'filter'=>$filter,
		);
	}
	/*
	 * список постов по категории без всего лишнего
	*/ 
	function postByCat($curl,$prfxtbl,$uid,$page,$num,$filter=''){
		//$filter = is_object($filter)?$filter:json_decode($filter);
		$this->template='';
		$tbls=\posts\tables::init($prfxtbl);
		$start=($page-1)*$num;
		#Получаем посты
		if(!$filter)
			$data=listPosts($curl,$this->userControl,$start,$num);
		else
			$data= listPostsFilteredImg($filter,$curl,$this->userControl,$start,$num);
		#Добавляем изображения
		\posts\getImg2list($data->posts);
		return [
			'posts'=>$data->posts,
			'count'=>$data->count,
			'prfxtbl'=>$tbls->prfx,
			'user'=>$this->user,
			'filter'=>$filter,
		];
	}
	/*
	 * Получаем подсказки по категориям
	*/
	function suggests($category,$prfxtbl,$page,$num){
		$this->template='';
		$tbls=\posts\tables::init($prfxtbl);
		$baseCat=new category($category,$this->userControl->access->showAllCat);
		#Получить посты категории
		$start=($page-1)*$num;
		$data=listPosts($baseCat->url,$this->userControl,$start,$num);
		$posts=$data->posts;
		#определяем сколько нужно дополнительных постов
		$num-=count($data->posts);
		if($num){
			$start=($page-2)*$num-$data->count;
			if($start<0) $start=0;
			#Получить подсказки расширяющие категорию, если постов не хватает
			$sugests=getSugests($baseCat,array_keys($posts),15);
			foreach ($sugests as $v) {
				$s[$v->url]=$v;
			}
			if(!empty($s)) {
				$dataS=listPosts(array_keys($s),$this->userControl,$start,$num);
				$posts+=$dataS->posts;
			}
		}
		\posts\getImg2list($posts);
		
		return [
			'posts'=>$posts,
			'prfxtbl'=>$tbls->prfx,
			'user'=>$this->user,
		];
	}
	/*
		особый вывод списка
			- все подкатегории
			- и заданное количество постов в подкатегориях
	*/
	function subCatList($category,$prfxtbl,$num){
		$tbls=\posts\tables::init($prfxtbl);
		$baseCat=new category($category, $this->userControl->access->showAllCat);
		if(!$this->user->id&&$baseCat->id&&$baseCat->count<3) {
			$this->headers->location=HREF; return;
		}
		$accessEditNewsMy=$this->userControl->rbac('editNewsMy');
		$accessPublish=$this->userControl->rbac('publishPost');
		$showRights=showRights($accessPublish,$accessEditNewsMy);
		
		#получаем список подкатегорий
		db::query(
			"SELECT cat.*,SUBSTRING_INDEX(GROUP_CONCAT(post.id SEPARATOR ' '),' ',$num) posts
			FROM `{$tbls->category2post}` cp
			INNER JOIN `{$tbls->category}` cat ON cat.url=cp.cid && cat.parentID='{$category}'
			INNER JOIN `{$tbls->post}` ON post.url=cp.pid{$showRights}
			GROUP BY cp.cid");
		$subCats=array();
		$pids=array();
		$posts=array();
		while ($d=db::fetch()) {
			$d->posts=explode(' ', $d->posts);
			foreach ($d->posts as $val) {
				$pids[]=$val;
			}
			$subCats[]=$d;
		}
		#получаем посты для подкатегорий
		$concatPids='';
		if(count($pids)){
			$concatPids=implode(",", $pids);
			$keywords=array();
			db::query("SELECT * FROM `{$tbls->post}` WHERE id IN({$concatPids})");
			while ($d=db::fetch()) {
				$d->stxt=pageBreak($d->txt);
				$d->txt=\posts\cutText($d->txt);
				$posts[$d->id]=$d;
				$posts[$d->id]->keyword=&$keywords[$d->kid];
			}
		}
		#Получаем панель админа и основной кей
		if($this->user->id&&!empty($keywords)){
			db::query("SELECT * FROM `keyword` WHERE id IN (".implode(",",array_keys($keywords)).")");
			while($d=db::fetch()){
				$keywords[$d->id]=$d->title;
			}
			foreach($posts as $id=>$d){
				$posts[$id]->funcPanel=module::exec('posts',array('act'=>'editPanel','post'=>$d),1)->str;
			}
		}
		#получаем картинки
		$posts=getPostsImages($concatPids,$posts);

		#получаем pin посты
		$pins=getPins($category,$this->userControl);

		return array(
			'posts'=>$posts,
			'prfxtbl'=>$tbls->prfx,
			'subCatsPosts'=>$subCats,
			#получаем данные категорий
			'cat'=>$baseCat,
			'subCats'=>module::exec('category',array('act'=>'subList','url'=>$category,'tbl'=>$tbls->post),1)->str,
			'pins'=>$pins,
			'breadCrumbs'=>\posts\getCrumbs($baseCat),
			'topLevelCats'=>module::exec('category',array('act'=>'mlist','tbl'=>$tbls->post,'category'=>$category,'parentId'=>$baseCat->parentId),1)->str,
			'access'=>$this->userControl->access,
		);
	}
	/*
		список постов по автору
		получает ID автора
	*/
	function listByUser($page,$num,$uid,$prfxtbl){
		if(empty($uid)){
			$this->headers->location=HREF; return;
		}
		$tbls=\posts\tables::init($prfxtbl);
		#данные пользователя
		$userData=\user\getUser($uid);
		#получаем посты
		$start=($page-1)*$num;
		$data=listPostsByUser($uid,$this->userControl,$start,$num);
		#Получаем категории для рекомендуемых папок пинов
		$pinSudgests = [];
		if (!empty($data->posts)){
			foreach ($data->posts as $post) {
				$pinSudgests[] = $post->cid;
			}
		}
		#Добавляем изображения
		\posts\getImg2list($data->posts);

		return array(
			'user'=>$this->user,
			'userinfo'=>$userData,
			'posts'=>$data->posts,
			'page'=>$page,
			'num'=>$num,
			'count'=>$data->count,
			'paginator'=>module::execStr('plugins/paginator',
				[
					'page'=>$page,
					'num'=>$num,
					'count'=>$data->count,
					'uri'=>url::author($uid).'%d/',
					true,
				]
			),
			'prfxtbl'=>$tbls->prfx,
			'pinForm'=>module::execStr('pins',['act'=>'pinForm','sudgests'=>$pinSudgests]),
		);
	}
	/*
		список скачанных постов-изображений пользователем
	*/
	function listDownloadByUser($page,$num,$uid,$prfxtbl){
		if(empty($uid)){
			$this->headers->location=HREF; return;
		}
		$tbls=\posts\tables::init($prfxtbl);
		#данные пользователя
		$userData=\user\getUser($uid);
		#получаем посты
		$start=($page-1)*$num;
		$data=listDownloadsByUser($uid,$this->userControl,$start,$num);
		#Получаем категории для рекомендуемых папок пинов
		$pinSudgests = [];
		if (!empty($data->posts)){
			foreach ($data->posts as $post) {
				$pinSudgests[] = $post->cid;
			}
		}
		#Добавляем изображения
		\posts\getImg2list($data->posts);

		return array(
			'user'=>$this->user,
			'userinfo'=>$userData,
			'posts'=>$data->posts,
			'page'=>$page,
			'num'=>$num,
			'count'=>$data->count,
			'paginator'=>module::execStr('plugins/paginator',
				[
					'page'=>$page,
					'num'=>$num,
					'count'=>$data->count,
					'uri'=>url::author($uid).'%d/',
					true,
				]
			),
			'prfxtbl'=>$tbls->prfx,
			'pinForm'=>module::execStr('pins',['act'=>'pinForm','sudgests'=>$pinSudgests]),
		);
	}
	function random($num){
		$cat=(object)array('title'=>'random','url'=>'random');
		return array(
			'cat'=>$cat,
			'access'=>$this->userControl->access,
			'posts'=>\posts\randomPosts($num),
			'breadCrumbs'=>array($cat),
		);
	}
	function top($page,$num){
		$tbls=\posts\tables::init();
		$top=array();
		$start=($page-1)*$num;
		$sql="SELECT * FROM {$tbls->post} ORDER BY statViewsShort DESC limit $start,$num";
		db::query($sql);
		while ($d=db::fetch()) {
			$d->txt=\posts\cutText(strip_tags($d->txt),20);
			$d->funcPanel=module::exec('posts',array('act'=>'editPanel','post'=>$d),1)->str;
			$top[$d->id]=$d;
		}
		\posts\getImg2list($top);
		$cat=(object)array('title'=>'Top','url'=>'top');
		return array(
			'cat'=>$cat,
			'access'=>$this->userControl->access,
			'posts'=>$top,
			'breadCrumbs'=>array($cat),
			'prfxtbl'=>$tbls->prfx,
			'user'=>$this->user,
		);
	}
	/**
	 * Список постов, за которые не голосовали
	 */
	function listVote($page,$num){
		#Проверяем права
		if(!$this->userControl->rbac('voteAccess')){
			$this->headers->location=HREF;
			return;
		}
		$tbls=new \posts\tables;
		$start=($page-1)*$num;
		db::query(
			"
				SELECT SQL_CALC_FOUND_ROWS post.*, user.name AS authorName, user.mail AS authorMail, (post.like-post.dislike) AS likeSum
				FROM `{$tbls->post}` post
				LEFT JOIN `".PREFIX_SPEC."user2vote` vote ON vote.postID=post.id AND vote.userID={$this->user->id}
				LEFT JOIN `".PREFIX_SPEC."users` user ON user.id=post.user 
				WHERE `user`!='{$this->user->id}' && vote.userID IS NULL && post.published=1 && post.FBpublished='proc'
				ORDER BY post.datePublish DESC
				LIMIT $start,$num
			"
		);
		$posts = array();
		while($d=db::fetch()){
			$d->txt=\posts\cutText($d->txt);
			$d->authorName=\posts\setAuthorName($d);
			$d->canVote=$d->user!=$this->user->id;
			$d->funcPanel=module::exec('posts',array('act'=>'editPanel','post'=>$d),1)->str;
			$posts[$d->id]=$d;
		}
		list($count)=db::qrow('SELECT FOUND_ROWS()');

		if($page>1&&empty($posts)){
			$this->headers->location=HREF;
			return;
		}
		return array(
			'posts'=>$posts,
			#получаем категории
			'page'=>$page,
			'num'=>$num,
			'paginator'=>module::exec('plugins/paginator',
				array(
					'page'=>$page,
					'num'=>$num,
					'count'=>$count,
					'uri'=>'/?module=posts&act=listVote&page=%d',
					true,
				)
			,1)->str,
			'count'=>$count,
			'FBpublishAcceess'=>$this->userControl->rbac('FBpublish'),
		);
	}

	/**
	 * Список постов, за которые голосовали либо авторские
	 */
	function listMyVote($page,$num){
		#Проверяем права
		if(!$this->userControl->rbac('myVoteList')){
			$this->headers->location=HREF;
			return;
		}
		$tbls=new \posts\tables;
		$start=($page-1)*$num;
		db::query(
			"
				SELECT SQL_CALC_FOUND_ROWS post.*, user.name AS authorName, user.mail AS authorMail, (post.like-post.dislike) AS likeSum, FBpublished
				FROM `{$tbls->post}` post
				LEFT JOIN `".PREFIX_SPEC."user2vote` vote ON vote.postID=post.id AND vote.userID={$this->user->id}
				LEFT JOIN `".PREFIX_SPEC."users` user ON user.id=post.user 
				WHERE (`user`='{$this->user->id}' || vote.userID IS NOT NULL) && post.published=1 && post.FBpublished='proc'
				ORDER BY likeSum DESC
				LIMIT $start,$num
			"
		);
		$posts = array();
		while($d=db::fetch()){
			$d->txt=\posts\cutText($d->txt);
			$d->authorName=\posts\setAuthorName($d);
			$d->funcPanel=module::exec('posts',array('act'=>'editPanel','post'=>$d),1)->str;
			$posts[$d->id]=$d;
		}
		list($count)=db::qrow('SELECT FOUND_ROWS()');

		if($page>1&&empty($posts)){
			$this->headers->location=HREF;
			return;
		}
		return array(
			'posts'=>$posts,
			#получаем категории
			'page'=>$page,
			'num'=>$num,
			'paginator'=>module::exec('plugins/paginator',
				array(
					'page'=>$page,
					'num'=>$num,
					'count'=>$count,
					'uri'=>'/?module=posts&act=listVote&page=%d',
					true,
				)
			,1)->str,
			'count'=>$count,
			'FBpublishAcceess'=>$this->userControl->rbac('FBpublish'),
		);
	}
	/*
		Список pins пользователя
	*/
	function userPhotos($uid,$page,$num){
		$tbl=\posts\tables::init();
		$start=($page-1)*$num;
		#Получаем список id картинок которые лайкнул пользователь
		$ids=[];
		db::query("SELECT imid FROM `".PREFIX_SPEC."like2img` WHERE uid='$uid' LIMIT $start,$num");
		while ($d=db::fetch()){
			$ids[]=$d->imid;
		}
		#Получаем данные по картинкам
		$imgs=[];
		if(isset($ids[0])){
			db::query(
				"SELECT imgs.*,li.date as datepin FROM `".PREFIX_SPEC."imgs` imgs
				INNER JOIN `".PREFIX_SPEC."like2img` li ON li.imid=imgs.id
				WHERE imgs.id IN (".implode(",",$ids).") && imgs.tbl='{$tbl->post}' GROUP BY imgs.id");
			while ($d=db::fetch()){
				$imgs[$d->pid][]=$d;
			}
		}
		#получаем посты
		$d=listPostsByPid(array_keys($imgs),$this->userControl);
		#присоединяем полученные ранее картинки
		foreach ($d->posts as $k=>&$v) {
			$v->imgs=$imgs[$k];
		}
		#Получаем данные по пользователю
		$userinfo=\user\getUser($uid);

		return array(
			'posts'=>$d->posts,
			'user'=>$this->user,
			'userinfo'=>$userinfo,
			'page'=>$page,
			'num'=>$num,
			'paginator'=>module::execStr('plugins/paginator',
				[
					'page'=>$page,
					'num'=>$num,
					'count'=>$d->count,
					'uri'=>url::userPhotos($uid).'?page=%d',
				]
			),
			'count'=>$d->count,
			'prfxtbl'=>$tbl->prfx,
		);
	}
}
