<?if(empty($data->suggests))return;?>
<div class="row mb-3">
	<div class="col-12">
	<?foreach ($data->suggests as $cat) {?>
		<a class="btn btn-info btn-sm mb-2" href="<?=url::category($cat->url)?>"><?=$cat->title?></a>
	<?}?>
	</div>
</div>
