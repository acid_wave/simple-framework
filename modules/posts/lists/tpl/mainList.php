<?php
/*
 * Входные данные находятся в stdClass $data
 * */
$cat=$data->cat;
$posts=$data->posts;
$tpl->title=(!empty($cat->title)?$cat->title:"Fun pics & images").(@$data->page>1?" - Page {$data->page}":'');
$tpl->desc=!empty($cat->title)?"Records for category \"{$cat->title}\".":'';
if(count($posts)<3 or @$data->page>1 or @$data->noindex)$tpl->meta='<meta name="robots" content="noindex,follow" />';
if(!count($data->posts)){echo "Nothing Found!";return;}
?>
<script type="text/javascript" src="<?=HREF?>/modules/posts/lists/tpl/files/scrollappend.js"></script>
<div class="container">
	<h1><?=ucfirst($cat->title)?> clip arts</h1>
	<?include $template->inc('mainList/suggests.php');?>
	<div class="d-flex justify-content-end my-3">
		<div class="btn-group">
			<a class="btn btn-outline-secondary btn-sm<?=isset($data->filter['statViews'])?' active':''?>" rel="noindex,follow" href='<?=url::category($data->cat->url)?>?filter[statViews]=desc'>Most popular</a>
			<a class="btn btn-outline-secondary btn-sm<?=isset($data->filter['statViewsShort'])?' active':''?>" rel="noindex,follow" href='<?=url::category($data->cat->url)?>?filter[statViewsShort]=desc'>Most popular 7 days</a>
			<a class="btn btn-outline-secondary btn-sm<?=isset($data->filter['datePublish'])?' active':''?>" rel="noindex,follow" href='<?=url::category($data->cat->url)?>?filter[datePublish]=desc'>Newest first</a>
			<a class="btn btn-outline-secondary btn-sm<?=isset($data->filter['statDownload'])?' active':''?>" rel="noindex,follow" href='<?=url::category($data->cat->url)?>?filter[statDownload]=desc'>Most downloaded</a>
		</div>
	</div>
	<div class="row no-gutters" id="infinityScroll" data-page="<?=$data->page?>" data-num="<?=$data->num?>" data-cat="<?=@$data->cat->url?>">
		<?include $template->inc('postByCat.php');?>
	</div>
</div>
<?=$data->pinForm?>
