<?php
$tpl->title="{$data->userinfo->altName}".($data->page>1?" - Page {$data->page}":'');
$tpl->desc="{$data->count} Uploaded by {$data->userinfo->altName}";
$posts=$data->posts;
?>
<div class="container">
	<?include $template->inc('../../../user/tpl/settings/userinfo.php');?>
	<div class="row mb-3">
		<div class="col-12 text-center">
			<?include $template->inc("../../tpl/post/share.php");?>
		</div>
	</div>
	<div class="row no-gutters">
		<?include $template->inc("../tpl/postByCat.php");?>
		<div class="col-12 text-center">
			<?=$data->paginator?>
		</div>
	</div>
</div>
<?=$data->pinForm?>
