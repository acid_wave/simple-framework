<?
$tpl->title='Pinned posts by '.$data->userinfo->altName;
$tpl->desc='The user '.$data->userinfo->altName.' likes these posts';
$tpl->meta="<meta name='robots' content='noindex, follow'/>";
?>
<?include $template->inc('../../../user/tpl/settings/userinfo.php');?>
<div class="row container-wrapper">
	<div class="col-12 text-center">
		<h1><?=ucfirst($data->userinfo->altName)?>`s favorite posts</h1>
	</div>
	<?include $template->inc("../tpl/postByCat.php");?>
	<div class="col-12 text-center">
		<?=$data->paginator?>
	</div>
	<?if(@$data->count){?>
	<div class="col-12">
		<small>items:&nbsp<i><?=@$data->count?></i></small>
	</div>
	<?}?>
</div>