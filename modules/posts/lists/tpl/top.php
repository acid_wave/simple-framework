<?php
/*
 * Входные данные находятся в stdClass $data
 * */
$cat=$data->cat;
$posts=$data->posts;
$tpl->title=(!empty($cat->title)?$cat->title:"Fun pics & images").(@$data->page>1?" - Page {$data->page}":'');
$tpl->desc=!empty($cat->title)?"Records for category \"{$cat->title}\".":'';
if(count($posts)<3 or @$data->page>1 or @$data->noindex)$tpl->meta='<meta name="robots" content="noindex,follow" />';
if(!count($data->posts)){echo "Nothing Found!";return;}
?>
<h1><?=$cat->title?> clip arts</h1>
<div class="row">
	<div id="infinityScroll" data-page="<?=$data->page?>" data-num="<?=$data->num?>" data-cat="<?=@$data->cat->url?>">
		<?include $template->inc('postByCat.php');?>
	</div>
</div>