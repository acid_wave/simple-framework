$(document).ready(function(){
	var data=$('#infinityScroll');
	var page=parseInt(data.attr("data-page"));
	var num=parseInt(data.attr("data-num"));
	var cat=data.attr("data-cat");

	//first run request
	if(data.parent('div').height()<$(window).height()){
		page=page+1;
		request(data,cat,page,num);
	}
	var appendFunc=function() {
		if($(window).scrollTop() >= data.parent('div').height()-$(window).height()) {
			page=page+1;
			console.log("page:"+page);
			request(data,cat,page,num);
			//unbind event
			$(window).unbind('scroll',appendFunc);
		}
	}
	//set event
	$(window).scroll(appendFunc);

	function request(data,cat,page,num){
		$.post(window.location.basepath+'?module=posts/lists',
			{
				act:'suggests',
				cat:cat,
				page:page,
				num:num,
			},
			function(response){
				if($.trim(response)) {
					data.append(response);
					//reset event
					$(window).scroll(appendFunc);
					console.log(data.parent('div').height()+' '+$(window).height());
					//recursive run request
					if(response.trim().match('/<[^>]+>/')&&data.parent('div').height()<$(window).height()){
						page=page+1;
						request(data,cat,page,num);
					}
				}
			}
		);
	}
});
