<?
namespace posts_lists;
use module,db,url,cache;
/*
	получает список постов по категории и их количество
*/
function listPosts($cat,$userHandler,$start,$num){
	$posts=[];
	$tbls=\posts\tables::init();
	if($cat){
		if(!is_array($cat)) $cat=[$cat];
		if($userHandler->user->id){
			list($c)=db::qrow("SELECT COUNT(pid) FROM {$tbls->category2post} WHERE cid IN('".implode("','",$cat)."')".genAccessParamsC2P($userHandler));
		}else{
			list($c)=db::qrow("SELECT SUM(`count`) FROM {$tbls->category} WHERE url IN('".implode("','",$cat)."')");
		}
		
		db::query("
			SELECT p.*,c2p.cid,c.title AS catTitle, u.id AS authorId, u.name AS authorName, u.mail AS authorMail
			FROM (
				/* выбираем ID постов */
				SELECT pid,cid FROM {$tbls->category2post} rel 
				WHERE cid IN('".implode("','",$cat)."')".genAccessParamsC2P($userHandler)." ORDER BY datePublish DESC LIMIT $start,$num
			) c2p
			/* присоединяем данные постов */
			INNER JOIN {$tbls->post} p ON c2p.pid=p.url
			/* присоединяем данные категории */
			INNER JOIN {$tbls->category} c ON c.url=c2p.cid
			/* присоединяем данные пользователя */
			LEFT JOIN `".PREFIX_SPEC."users` u ON u.id=p.user
			ORDER BY datePublish DESC
		");
	}else{
		#Получаем список по всем категориям
		list($c)=db::qrow("SELECT COUNT(id) FROM {$tbls->post} WHERE 1".genAccessParamsP($userHandler));
                db::query("
			SELECT p.*,c2p.cid,c.title AS catTitle, u.id AS authorId, u.name AS authorName, u.mail AS authorMail
			FROM (
				/* выбираем ID постов */
				SELECT * FROM {$tbls->post} WHERE 1".genAccessParamsP($userHandler)."
				ORDER BY datePublish DESC LIMIT $start,$num
			) p
			/* присоединяем данные связи с категориями */
			LEFT JOIN {$tbls->category2post} c2p ON c2p.pid=p.url
			/* присоединяем данные категории */
			LEFT JOIN {$tbls->category} c ON c.url=c2p.cid
			/* присоединяем данные пользователя */
			LEFT JOIN `".PREFIX_SPEC."users` u ON u.id=p.user
			GROUP BY p.id
			ORDER BY datePublish DESC
		");
	}
	while($d=db::fetch()){
		$posts[$d->id]=createPost($d,$userHandler->user->id);
	}

	return (object)['posts'=>$posts,'count'=>$c];
}
/*
	получает список постов по фильтру
*/
function listPostsFilteredImg($filter,$cat,$userHandler,$start,$num){
	$posts=[];
	$tbls=\posts\tables::init();
	$condition=db::filter($filter);
	if($cat){
		list($c)=db::qrow("
			SELECT COUNT(post.id) FROM {$tbls->post} post
			/* присоединяем данные картинки */
			INNER JOIN ".PREFIX_SPEC."imgs i ON i.tbl='{$tbls->post}' && i.pid=post.id
			/* присоединяем данные связи с категориями */
			INNER JOIN {$tbls->category2post} c2p ON c2p.pid=post.url && c2p.cid='$cat'
			WHERE 1".genAccessParamsP($userHandler,'post.').' '.$condition->where);
		db::query("
			SELECT p.*, c.url AS cid, c.title AS catTitle, u.id AS authorId, u.name AS authorName, u.mail AS authorMail
			FROM (
				SELECT post.*, c2p.cid, i.width, i.height, i.licence, i.statDownload FROM {$tbls->post} post
				/* присоединяем данные картинки */
				INNER JOIN ".PREFIX_SPEC."imgs i ON i.tbl='{$tbls->post}' && i.pid=post.id
				/* присоединяем данные связи с категориями */
				INNER JOIN {$tbls->category2post} c2p ON c2p.pid=post.url && c2p.cid='$cat'
				WHERE 1 ".$condition->where."
				GROUP BY post.id
				".$condition->order."
				LIMIT $start,$num
			) p
			/* присоединяем данные категории */
			INNER JOIN {$tbls->category} c ON c.url=p.cid
			/* присоединяем данные пользователя */
			LEFT JOIN `".PREFIX_SPEC."users` u ON u.id=p.user
			GROUP BY p.id
			".$condition->order."
		");
	}else{
		list($c)=db::qrow("
			SELECT COUNT(post.id) FROM {$tbls->post} post
			INNER JOIN ".PREFIX_SPEC."imgs i ON i.tbl='{$tbls->post}' && i.pid=post.id
			WHERE 1".genAccessParamsP($userHandler).' '.$condition->where);
		db::query("
			SELECT p.*,c2p.cid,c.title AS catTitle, u.id AS authorId, u.name AS authorName, u.mail AS authorMail
			FROM (
				/* выбираем ID постов */
				SELECT post.*, i.width, i.height, i.licence,i.statDownload FROM {$tbls->post} post
				/* присоединяем данные картинки */
				INNER JOIN ".PREFIX_SPEC."imgs i ON i.tbl='{$tbls->post}' && i.pid=post.id
				WHERE 1".genAccessParamsP($userHandler)."
				".$condition->where."
				GROUP BY post.id
				".$condition->order."				
				LIMIT $start,$num
			) p
			/* присоединяем данные связи с категориями */
			LEFT JOIN {$tbls->category2post} c2p ON c2p.pid=p.url
			/* присоединяем данные категории */
			LEFT JOIN {$tbls->category} c ON c.url=c2p.cid
			/* присоединяем данные пользователя */
			LEFT JOIN `".PREFIX_SPEC."users` u ON u.id=p.user
			GROUP BY p.id
			".$condition->order."
		");
	}

	while($d=db::fetch()){
		$posts[$d->id]=createPost($d,$userHandler->user->id);
	}

	return (object)['posts'=>$posts,'count'=>$c];
}
/*
	получает список постов пользователя и их количество
*/
function listPostsByUser($uid,$userHandler,$start,$num){
	if(empty($uid)) return false;
	$posts=[];
	$tbls=\posts\tables::init();
	
	list($c)=db::qrow("SELECT COUNT(id) FROM {$tbls->post} WHERE `user`='{$uid}'".genAccessParamsP($userHandler));
	db::query("
		SELECT p.*,c2p.cid,c.title AS catTitle, u.id AS authorId, u.name AS authorName, u.mail AS authorMail
		FROM (
			/* выбираем ID постов */
			SELECT * FROM {$tbls->post} WHERE `user`='{$uid}'".genAccessParamsP($userHandler)."
			ORDER BY datePublish DESC LIMIT $start,$num
		) p
		/* присоединяем данные связи с категориями */
		LEFT JOIN {$tbls->category2post} c2p ON c2p.pid=p.url
		/* присоединяем данные категории */
		LEFT JOIN {$tbls->category} c ON c.url=c2p.cid
		/* присоединяем данные пользователя */
		LEFT JOIN `".PREFIX_SPEC."users` u ON u.id=p.user
		GROUP BY p.id
	");

	while($d=db::fetch()){
		$posts[$d->id]=createPost($d,$userHandler->user->id);
	}

	return (object)['posts'=>$posts,'count'=>$c];
}
/*
	получает список скачанных постов пользователя и их количество
*/
function listDownloadsByUser($uid,$userHandler,$start,$num){
	if(empty($uid)) return false;
	$posts=[];
	$tbls=\posts\tables::init();
	
	list($c)=db::qrow("SELECT COUNT(imid) FROM `".PREFIX_SPEC."userDownloads` WHERE `uid`='{$uid}'");
	db::query("
		SELECT p.*,c2p.cid,c.title AS catTitle, u.id AS authorId, u.name AS authorName, u.mail AS authorMail
		FROM (
			/* выбираем ID постов */
			SELECT p.*, ud.data as d_date FROM `".PREFIX_SPEC."userDownloads` ud
			LEFT JOIN `".PREFIX_SPEC."imgs` i ON ud.imid=i.id
			LEFT JOIN `{$tbls->post}` p ON i.pid=p.id
			WHERE ud.uid='{$uid}'".genAccessParamsP($userHandler)."
			ORDER BY ud.data DESC LIMIT $start,$num
		) p
		/* присоединяем данные связи с категориями */
		LEFT JOIN {$tbls->category2post} c2p ON c2p.pid=p.url
		/* присоединяем данные категории */
		LEFT JOIN {$tbls->category} c ON c.url=c2p.cid
		/* присоединяем данные пользователя */
		LEFT JOIN `".PREFIX_SPEC."users` u ON u.id=p.user
		GROUP BY p.id
		ORDER BY p.d_date DESC
	");

	while($d=db::fetch()){
		$posts[$d->id]=createPost($d,$userHandler->user->id);
	}

	return (object)['posts'=>$posts,'count'=>$c];
}
/*
	получает список постов по указанным ID и их количество
*/
function listPostsByPid($pids,$userHandler){
	if(!is_array($pids)) $pids[]=$pids;
	if(empty($pids)) (object)['posts'=>[],'count'=>0];
	$posts=[];
	$tbls=\posts\tables::init();
	
	list($c)=db::qrow("SELECT COUNT(id) FROM {$tbls->post} WHERE id IN(".implode(',',$pids).")".genAccessParamsP($userHandler));
	db::query("
		SELECT p.*,c2p.cid,c.title AS catTitle, u.id AS authorId, u.name AS authorName, u.mail AS authorMail
		FROM (
			/* выбираем ID постов */
			SELECT * FROM {$tbls->post} WHERE id IN(".implode(',',$pids).")".genAccessParamsP($userHandler)."
		) p
		/* присоединяем данные связи с категориями */
		LEFT JOIN {$tbls->category2post} c2p ON c2p.pid=p.url
		/* присоединяем данные категории */
		LEFT JOIN {$tbls->category} c ON c.url=c2p.cid
		/* присоединяем данные пользователя */
		LEFT JOIN `".PREFIX_SPEC."users` u ON u.id=p.user
		GROUP BY p.id
	");

	while($d=db::fetch()){
		$posts[$d->id]=createPost($d,$userHandler->user->id);
	}

	return (object)['posts'=>$posts,'count'=>$c];
}
/*
	обертка для объекта картинок на списках
*/
class classImg{
	public function __toString(){
		return $this->url;
	}
}

/**
 * Генерирует параметры видимости постов для запроса к category2post
 * в зависимости от:
 * 	- admin - показывать все посты (без учета значения published)
 *  - пользователь - только published и посты пользователя без учета значения published
 *  - не авторизированый пользователь - только published=1
 * @param user_object $userHandler 
 * @return string - часть SQL запроса
 */
function genAccessParamsC2P($userHandler,$prfx=''){
	if($userHandler->access->editNews) $sqlPublish='';
	elseif($userHandler->access->editNewsMy)
		$sqlPublish=" && ({$prfx}published=1 OR ({$prfx}published!=1 && {$prfx}uid='{$userHandler->user->id}'))";
	else $sqlPublish=" && {$prfx}published=1";
	return $sqlPublish;
}
/**
 * Генерирует параметры видимости постов для запроса к post
 * в зависимости от:
 * 	- admin - показывать все посты (без учета значения published)
 *  - пользователь - только published и посты пользователя без учета значения published
 *  - не авторизированый пользователь - только published=1
 * @param user_object $userHandler 
 * @return string - часть SQL запроса
 */
function genAccessParamsP($userHandler,$prfx=''){
	if($userHandler->access->editNews) $sqlPublish='';
	elseif($userHandler->access->editNewsMy)
		$sqlPublish=" && ({$prfx}published=1 OR ({$prfx}published!=1 && {$prfx}user='{$userHandler->user->id}'))";
	else $sqlPublish=" && {$prfx}published=1";
	return $sqlPublish;
}
/*
	получает прикрепленные посты для категории
*/
function getPins($category,$userHandler){
	$tbls=\posts\tables::init();
	#получаем pin посты
	$pins=array();
	if($category&&$category!='no-category'){
		db::query("SELECT post.*,u.mail AS authorMail, u.name AS authorName FROM `{$tbls->post}` post 
			LEFT JOIN `".PREFIX_SPEC."users` u ON u.id=post.user
			WHERE `pincid`='{$category}'".genAccessParamsP($userHandler,'post.')." ORDER BY `datePublish`");
		while ($d=db::fetch()) {
			$d->funcPanel=module::exec('posts',array('act'=>'editPanel','post'=>$d),1)->str;
			$d->authorName=\posts\setAuthorName($d);
			$pins[$d->id]=$d;
		}
	}
	return $pins;
}
/*
	получает картинки для списка постов
*/
function getPostsImages($pids,&$posts){
	$tbls=\posts\tables::init();
	if(is_array($pids)) $concatPids=implode(',', $pids);
	else $concatPids=$pids;
	db::query(
	"SELECT tmp.* FROM (
		SELECT img.tbl,img.pid,img.kid,img.url,img.width,img.height,img.priority,img.text,keyword.title 
		FROM `".PREFIX_SPEC."imgs` img 
		LEFT JOIN `keyword` ON img.kid=keyword.id  
		WHERE img.pid IN({$concatPids}) && img.tbl='{$tbls->post}'
	) tmp ORDER BY tmp.priority DESC");
	while ($d=db::fetch()) {
		$img=new classImg;
		if(isset($posts[$d->pid])) $posts[$d->pid]->imgs[]=add2obj($img,$d);
	}
	return $posts;
}
/*
	получает данные из текста поста, до pagebreak
*/
function pageBreak($str){
	$str=preg_replace('!<[^\>]+>[^\<]*(\<\!\-\- pagebreak \-\-\>)[^\<]*</[^\>]+>!s', '$1', $str);
	$pos=strpos($str,'<!-- pagebreak -->');
	if($pos===false){
		$str=\posts\cutText($str,40,'...','<img><iframe>');		
	}else{
		$str=substr($str,0,$pos);
	}
	return $str;
}

/*
	получить несколько пользователей у которых есть pins 
*/
function pinsUsersRand($lim=4,$exclude=[]){
	if(!is_array($exclude)&&$exclude) $exclude=[$exclude];
	$sqlExclude=!empty($exclude)?" && uid NOT IN(".implode(',', $exclude).")":'';
	list($count)=db::qrow("
		SELECT COUNT(t.uid) FROM (
			SELECT uid FROM ".PREFIX_SPEC."like2img li WHERE 1{$sqlExclude} GROUP BY `uid`
		) t");
	$offset=rand(0,$count-$lim);
	if($offset<0) $offset=0;
	db::query("
		SELECT u.id,u.name,u.visit,u.rating FROM ".PREFIX_SPEC."like2img li 
		INNER JOIN ".PREFIX_SPEC."users u ON u.id=li.uid
		WHERE 1{$sqlExclude}
		GROUP BY `uid` LIMIT {$lim} OFFSET {$offset}");
	while ($d=db::fetch()) {
		$d->avatar=\user\avatar::exists($d->id,true);
		$users[]=$d;
	}

	return isset($users)?$users:[];
}
/*
 * Получает список подсказок для категории
 * - Использует полнотекстовый поиск
*/
function getSugests($cat,$pids=array(),$limit=25){
	$tbls=\posts\tables::init();
	$suggests=[];
	
	$found[]=$cat->url;
	if(!empty($cat->title)){
		#пытается найти точное соответствие фразе
		$title=db::escape($cat->title);
		db::query("
			SELECT distinct c.title, c.* FROM `{$tbls->category}` c
			WHERE `count`>=3 && MATCH (`title`) AGAINST('+\"$title\"' IN BOOLEAN MODE)
			ORDER BY `count` DESC
			LIMIT $limit
		");
		while($d=db::fetch()){
			#убираем главное слово
			$d->title=preg_replace('!(^|[^\w]+)'.$cat->title.'($|[^\w]+)!i','',$d->title);
			#убираем незначимые слова
			$d->title=filterSignWords($d->title);
			if($d->title==''||isset($suggests[$k=strtolower($d->title)])) continue;
			$suggests[$k]=$d;
			$foundKeys[]=$d->url;
		}
		#если найдено меньше $limit, то пытается найти соответствие частям фразы
		$foundCount=count($suggests);
		if($foundCount<$limit){
			$limit-=$foundCount;
			$str=filterSignWords($cat->title);
			db::query("
				SELECT distinct c.title, c.* FROM `{$tbls->category}` c
				WHERE `count`>=3 && MATCH (`title`) AGAINST('$str' IN NATURAL LANGUAGE MODE)
					".(!empty($foundKeys)?"&& `url` NOT IN('".implode("','",$foundKeys)."')":'')."
				ORDER BY `count` DESC
				LIMIT $limit
			");
			while($d=db::fetch()){
				#убираем главное слово
				$d->title=preg_replace('!(^|[^\w]+)'.$cat->title.'($|[^\w]+)!i','',$d->title);
				#убираем незначимые слова
				$d->title=filterSignWords($d->title);
				if($d->title==''||isset($suggests[$k=strtolower($d->title)])) continue;
				$suggests[$k]=$d;
			}
		}
	}
	
	return $suggests;
}
/*
	Удаляет незначимые слова из строки (EN)
*/
function filterSignWords($str){
	$str=preg_replace(['![^\w\d]!','!\s+!'],' ',trim($str));
	$e=explode(" ",$str);
	$skip=["a","about","above","after","again","against","all","am","an","and","any","are","aren't","as","at","be","because","been","before","being","below","between","both","but","by","can't","cannot","could","couldn't","did","didn't","do","does","doesn't","doing","don't","down","during","each","few","for","from","further","had","hadn't","has","hasn't","have","haven't","having","he","he'd","he'll","he's","her","here","here's","hers","herself","him","himself","his","how","how's","i","i'd","i'll","i'm","i've","if","in","into","is","isn't","it","it's","its","itself","let's","me","more","most","mustn't","my","myself","no","nor","not","of","off","on","once","only","or","other","ought","our","ours","ourselves","out","over","own","same","shan't","she","she'd","she'll","she's","should","shouldn't","so","some","such","than","that","that's","the","their","theirs","them","themselves","then","there","there's","these","they","they'd","they'll","they're","they've","this","those","through","to","too","under","until","up","very","was","wasn't","we","we'd","we'll","we're","we've","were","weren't","what","what's","when","when's","where","where's","which","while","who","who's","whom","why","why's","with","won't","would","wouldn't","you","you'd","you'll","you're","you've","your","yours","yourself","yourselves","clipart","clip art","cliparts","clip arts"];
	foreach ($e as $v) {
		if(in_array($v,$skip)){
			$eq=preg_quote($v);
			$str=preg_replace('!(^|\s)'.$eq.'($|\s)!i','',$str);
		}
	}
	return $str;
}
/*
 * Создаем Данные поста категории на основе данных из бд
*/ 
function createPost($d,$uid=0,$maxLen=70){
	//Получаем список параметров для шаблона из текста
		$d->data=$d->data!=''?json_decode($d->data):'';
		$d->stxt=pageBreak($d->txt,$maxLen);
		$d->txt=\posts\cutText($d->txt);
	#set author's name
		$d->authorName=\posts\setAuthorName($d);
		$d->authorAvatar=\user\avatar::exists(@$d->authorId,true);
	#Добавляем панель редактирования поста
		$d->funcPanel=$uid?module::exec('posts',array('act'=>'editPanel','post'=>$d),1)->str:'';
	return $d;
}
/*
 * Добавляем пользователей к постам
*/ 
function addUser(&$posts){
	foreach($posts as $i=>$v){
		$posts[$i]->user=&$user[$v->user];
	}
	db::query("SELECT id,name,mail FROM `".PREFIX_SPEC."users` WHERE id in(".implode(",",array_keys($user)).")");
	while($d=db::fetch()){
		if(empty($d->name)){$d->name=!empty($d->mail)?preg_replace('!@.+?$!i', '',$d->mail):'Anonym';}
		$user[$d->id]=$d;
	}
}
