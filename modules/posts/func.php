<?
namespace posts;
use module,db,url,cache;

#Получаем пост по урлу
function &getPost($url,$access,$imglastviewed=''){
	$tbls=tables::init();
	$post=db::qfetch("SELECT 
			post.*,keyword.title as keyword
		FROM 
			(SELECT 
					DISTINCT post.*,GROUP_CONCAT(rel.cid) cids 
				FROM `{$tbls->post}` post
				LEFT JOIN `{$tbls->category2post}` rel 
				ON rel.pid=post.url
					WHERE `url`='$url' GROUP BY `post`.id 
				LIMIT 1
			) post
		LEFT JOIN `keyword`
			ON keyword.id=post.kid
	");
	if(empty($post))return false;
	$post->tbl=$tbls->post;
	#получаем данные по автору
	$post->user=\user\getUser($post->user);
	$post->authorName=setAuthorName($post);
	//Получаем список параметров для шаблона из текста
	$post->data=$post->data!=''?json_decode($post->data):'';
	# Обрабаываем данные в посте
	$post->shortTxt=strip_tags(cutText($post->txt));
	#получаем rating поста
	$post->rating=db::qfetch("SELECT COUNT(uid) AS votesNum, SUM(rating)/COUNT(uid) AS averageRating FROM `".PREFIX_SPEC."postRating` WHERE pid='$post->id'");
	#получаем категории
	$post->cats=!empty($post->cids)?\category\getCategoryData(explode(',', $post->cids),@$access->showAllCat):array();
	$post->cats=sortCats($post->cats);
	#получаем картинки
	list($post->imglastviewed,$post->imgs)=getImages($tbls->post,$post->id,$imglastviewed);
	#Получаем кол-во пинов для каждой картинки
		$imIds=array();foreach($post->imgs as $k=>$v){$imIds[$v->id]=$k;}
		if(count($imIds)){
			db::query("SELECT COUNT(*) as cn,imid 
				FROM `".PREFIX_SPEC."like2img` 
				WHERE imid in(".implode(",",array_keys($imIds)).") 
				group by imid
			");
			while($d=db::fetch()){
				$post->imgs[$imIds[$d->imid]]->pinCount=$d->cn;
			}
		}
	#Получаем панель редактирования поста
	$post->funcPanel=module::execStr('posts',['act'=>'editPanel','post'=>$post]);
	#получаем prev|next посты в рамках категории поста
	$post->prevnext=getPrevNext($post,isset($post->cats[0])?$post->cats[0]:'');
	# Записываем статистику просмостров поста
	statViews($post,$tbls->post);
	return $post;
}
function getPrevNext($post,$cat){
	$tbls=tables::init();
	$cid=is_object($cat)?$cat->url:$cat;
	$res=new \stdClass;
	if(!empty($cid)){
		$res->prev=db::qfetch("SELECT post.id, post.title, post.url FROM `{$tbls->post}` post 
			WHERE url=(
				SELECT pid FROM `{$tbls->category2post}`
				WHERE cid='{$cid}' AND pid<'{$post->url}' AND published=1 LIMIT 1
			)");
		if(!$res->prev){
			$res->prev=db::qfetch("SELECT post.id, post.title, post.url FROM `{$tbls->post}` post 
				WHERE url=(
					SELECT pid FROM `{$tbls->category2post}`
					WHERE cid='{$cid}' AND pid!='{$post->url}' AND published=1 LIMIT 1
				)");
		}
		$res->next=db::qfetch("SELECT post.id, post.title, post.url FROM `{$tbls->post}` post 
			WHERE url=(
				SELECT pid FROM `{$tbls->category2post}`
				WHERE cid='{$cid}' AND pid>'{$post->url}' AND published=1 ".(isset($res->prev->url)?"AND pid!='{$res->prev->url}'":'')."
				LIMIT 1
			)");
		if(!$res->next){
			$res->next=db::qfetch("SELECT post.id, post.title, post.url FROM `{$tbls->post}` post 
				WHERE url=(
					SELECT pid FROM `{$tbls->category2post}`
					WHERE cid='{$cid}' AND published=1 AND pid NOT IN('{$post->url}'".(isset($res->prev->url)?",'{$res->prev->url}'":'').") 
					LIMIT 1
				)");
		}
	}
	return $res;
}
#Проверяем права на просмотр и редактирование поста
function checkAccess($uh,$user){
	return (
		//Может редактировать все новости
		$uh->access->editNews or
		//Может редактировать только свою новость
		($uh->access->editNewsMy 
			&& $uh->user->id==$user
		)
	)?true:false;
}
#Получаем картинки для поста
function getImages($tbl,$pid,$imglastviewed=false){
	db::query("SELECT 
			imgs.*,keyword.title
		FROM `".PREFIX_SPEC."imgs` imgs 
		LEFT JOIN keyword ON keyword.id=imgs.kid 
			WHERE `tbl`='{$tbl}' && `pid`='{$pid}' && type!=1 
		ORDER BY `priority` DESC");
	$imgs=array(); $lv=false;
	while($d=db::fetch()){
		#Получаем картинку которую видел человек в google images
		if($imglastviewed&&$lv===false)
			if($imglastviewed==$d->url)
				$lv=$d;
		$imgs[$d->id]=$d;
	}

	include_once module::$path.'/posts/lists/rating/func.php';
	\posts_lists_rating\getUserList($imgs);

	return array($lv,array_values($imgs));
}
/*
	Обрезает текст до конца предложения
		- сохраняет вставки img и iframe
	params
		text, (int) - количество слов, симв. окончания
*/
function cutText($str,$cword=40,$e='...',$savetag=''){
	
	$str=trim(strip_tags($str,$savetag));

	$textArr=array();
	do{
		preg_match('!^(.*?)(<(?:img|iframe)[^>]*>(?:\s*</iframe>)?)(.*?)$!si', $str,$m);
		if(isset($m[1])){
			foreach (explode(' ', $m[1]) as $val) {
				array_push($textArr, $val);
			}
			if(count($textArr)>=$cword) {
				$textArr=array_slice($textArr,0,$cword);
				break;
			}
			if(!empty($m[2]))
				$textArr[]=$m[2];
		}else{
			foreach (explode(' ', $str) as $val) {
				array_push($textArr, $val);
			}
			$textArr=array_slice($textArr,0,$cword);
			break;
		}
		if(empty($m[3])) break;
		$str=$m[3];
	}while(1);
	if(isset($textArr[0])){
		$str=implode(' ',$textArr);
		if(!empty($str))$str.=$e;
	}

	return trim($str);
}
/*
	Записывает количество просмотров для поста
*/
function statViews(&$post,$tbl){
	if(date('Y-m-d',strtotime($post->statShortFlag))<date('Y-m-d',time()-604800)){
		$statViewsShort=", `statViewsShort`='1', `statShortFlag`=NOW()";
	}else{
		$statViewsShort=", `statViewsShort`=`statViewsShort`+1";
	}
	db::query("UPDATE `{$tbl}` SET `statViews`=`statViews`+1{$statViewsShort} WHERE `id`='{$post->id}'");
}
/*
	Получает список постов по категории
*/
function relatedPosts($cid,$post,$limit=5){
	$rel=array();
	$equal=is_array($cid)?" IN('".implode("','",$cid)."')":"='{$cid}'";
	$q="SELECT DISTINCT post.*,rel.cid FROM `".PREFIX_SPEC."category2{$post->tbl}` rel
				INNER JOIN `{$post->tbl}` ON post.url=rel.pid && cid{$equal} 
			WHERE post.published=1 && post.pincid='' && %s";
	db::query(sprintf($q,"post.id>'{$post->id}' LIMIT {$limit}"));
	while ($d=db::fetch()) {
		$d->txt=cutText(strip_tags($d->txt),20);
		$rel[$d->id]=$d;
	}
	$c=count($rel);
	if($c<$limit){
		$limit=$limit-$c;
		$sqlIn=empty($rel)?'':",'".implode("','",array_keys($rel))."'";
		db::query(sprintf($q,"post.id>0 && post.id NOT IN('{$post->id}'{$sqlIn}) LIMIT {$limit}"));
		while ($d=db::fetch()) {
			$d->txt=cutText(strip_tags($d->txt),20);
			$rel[$d->id]=$d;
		}	
	}
	getImg2list($rel);
	return $rel;
}
/*
	Случайная категория у которой постов >= 5
*/
function getRandCat($exclude=false){
	$cat=array();
	db::query("SELECT title,url,count AS cn FROM `category` WHERE `parentId`='' && `view`=1 && count>=5".($exclude?" && `url`!='{$exclude->url}'":''));
	while ($d=db::fetch()) {
		$cat[]=$d;
	}
	shuffle($cat);
	return !empty($cat)?$cat[0]:false;
}
/*
	Получает список популярных постов в категории
*/
function popularPostsInCategory($cid, $limit=4){
	$tbls=tables::init();
	$relTbl=PREFIX_SPEC.$tbls->category2post;
	$limit = (int) $limit;
	if (!$cid){return;}
	$cid=db::escape($cid);
	$subsql=
		"SELECT %s FROM (
			SELECT pid,cid FROM `{$relTbl}` cat WHERE cid='{$cid}'
		) cat 
		INNER JOIN {$tbls->post} ON post.url=cat.pid WHERE post.published = 'published' && post.pincid='' %s";
	
	$c=db::qfetch(sprintf($subsql, "COUNT(post.id) as cnt", ""));
	$offset=0;
	$from=(int)$c->cnt - $limit;
	if($from>0){$offset=mt_rand(0, $from);}
	db::query(sprintf($subsql,"post.id, post.title, post.url, post.txt","ORDER BY post.statViewsShort DESC LIMIT {$limit} OFFSET {$offset}"));
	while ($d=db::fetch()){
		$d->txt=cutText(strip_tags($d->txt), 20);
		$posts[$d->id]=$d;
	}
	if(empty($posts)) return;
	getImg2list($posts);
	return $posts;
}

/*
	Получает список популярных постов
*/
function popularPosts($post=null, $limit=4){
	$tbl=tables::init()->post;
	$limit=(int)$limit;
	$sqlWhere=$post?
		(is_array($post)?
			" && p.id NOT IN('".implode("','", $post)."')":
			" && p.id!='{$post->id}'"
		):
	'';
	$sql="
	SELECT p.*,u.name AS authorName,u.mail AS authorMail, u.id AS authorId FROM `{$tbl}` p
		LEFT JOIN `".PREFIX_SPEC."users` u ON u.id=p.user
	WHERE 1 {$sqlWhere} && p.published=1 && p.pincid='' 
	ORDER BY p.statViewsShort DESC LIMIT 100";
	$posts = array();
	db::query($sql);
	while ($d=db::fetch()) {
		$d->txt=cutText(strip_tags($d->txt), 20);
		$posts[$d->id]=$d;
	}
	shuffle($posts);
	$p=[];
	foreach (array_slice($posts, 0, $limit) as $v) {
		setAuthorName($v);
		$v->authorAvatar=\user\avatar::exists(@$v->authorId,true);
		$p[$v->id]=$v;
	}
	# получаем картинки
	getImg2list($p);

	return $p;
}

/*
	Получает список постов автора
*/
function authorPosts($post, $limit=4){
	$tbl=tables::init()->post;
	$uid=is_object($post->user)?$post->user->id:$post->user;
	$limit=(int)$limit;
	$sql="
		SELECT * FROM `{$tbl}` 
		WHERE user = {$uid} && %s && published = '1' && pincid=''
		ORDER BY statViewsShort DESC 
		LIMIT %d";
	$posts=array();
	db::query(sprintf($sql, "post.id > {$post->id}", $limit));
	while($p = db::fetch()) {
		$p->txt = cutText(strip_tags($p->txt), 40);
		$posts[$p->id] = $p;
	}
	$c = count($posts);
	if($c<$limit){$limit -= $c;
		db::query(sprintf($sql, "post.id < {$post->id}", $limit));
		while ($p = db::fetch()) {
			$p->txt = cutText(strip_tags($p->txt), 40);
			$posts[$p->id] = $p;
		}	
	}
	getImg2list($posts);
	return $posts;
}

/*
	Получает список случайных фото
*/
function featuredPhoto($cid=null, $limit=9) {
	$tbls=tables::init();
	$cid=db::escape($cid);
	$sql="SELECT %s FROM `{$tbls->post}` post ".(
		$cid?
			"JOIN `{$tbls->category2post}` cat
			ON post.url=cat.pid AND cid = '{$cid}'"
			:"").
		" WHERE post.pincid='' %s";
	list($max)=db::qrow(sprintf($sql,"MAX(post.id)",''));
	$offset=($max>0)?mt_rand(0,$max):0;
	# получаем посты у которых есть картинки
	$photos = array();
	db::query(sprintf($sql,"post.id,post.url","&& post.id>'{$offset}' && post.id<='".($offset+300)."' && published=1 LIMIT {$limit}"));
	while ($d=db::fetch()) {
		$photos[$d->id]=new \stdClass;
		$photos[$d->id]->imgs=array();
		$photos[$d->id]->post_url=$d->url;
	}
	#добираем
	$c=count($photos);
	if($c<$limit){
		$limit=$limit-$c;
		db::query(sprintf($sql,"post.id,post.url","&& post.id>0 && published=1 LIMIT {$limit}"));
		while ($d=db::fetch()) {
			$photos[$d->id]=new \stdClass;
			$photos[$d->id]->imgs=array();
			$photos[$d->id]->post_url=$d->url;
		}	
	}
	# получаем картинки для выбранных постов
	db::query("SELECT pid,url,keyword.title FROM `".PREFIX_SPEC."imgs` img 
		LEFT OUTER JOIN keyword ON keyword.id=img.kid
		WHERE `tbl`='{$tbls->post}' && `pid` IN(".implode(',', array_keys($photos)).")");
	while ($d=db::fetch()) {
		$photos[$d->pid]->imgs[]=$d;
	}
	# рандомизируем
	$result=array();
	foreach ($photos as $k => $v) {
		if(empty($v->imgs))continue;
		shuffle($v->imgs);
		$d=$v->imgs[0];
		$d->post_url=$v->post_url;
		$result[]=$d;
	}

	return $result;
}
/*
	преобразование имени пользователя 
	в зависимости от полей mail и name
*/
function setAuthorName(&$post){
	if(!empty($post->user->mail)) $post->authorMail=$post->user->mail;
	if(!empty($post->user->name)) $post->authorName=$post->user->name;
	if(empty($post->authorName)){
		$post->authorName=!empty($post->authorMail)?preg_replace('!@.+?$!i', '', $post->authorMail):'Anonym';
	}
	return $post->authorName;
}
/*
	Получаем ID всех родительских категорий c view=0, для хлебных крошек
*/
function getCrumbs($cat){
	$tbls=tables::init();
	static $Res;
	if(!isset($Res)) $Res[]=$cat;
	$catsEscape=array();
	$last=end($Res);
	if(!empty($last->parentId)){
		$d=db::qfetch("SELECT * FROM `{$tbls->category}` WHERE `url`='".db::escape($last->parentId)."' LIMIT 1");
		$Res[]=$d;
		getCrumbs($d);
	}else{
		# убираем категории запрещенные к показу
		foreach ($Res as $key => $v) {
			if(isset($v->view))
				if($v->view==0) unset($Res[$key]);
		}
		rsort($Res);
	}
	return $Res;
}
/*
	получает список постов в одной из категорий текущего поста
*/
function relatedByCat($post,$limit=4){
	$tbls=tables::init();
	$related=array();
	if(!empty($post->cats)){
		$cats=array();
		foreach ($post->cats as $cat) {
			if($cat->view==0)continue;
			$cats[$cat->url]=clone $cat;
		}
		#выбираем оптимальную категорию
		db::query("SELECT `parentID` FROM `{$tbls->category}` WHERE parentID IN ('".implode(',',array_keys($cats))."') GROUP BY parentID");	
		while ($d=db::fetch()) {
			unset($cats[$d->parentID]);
		}
		#уменьшаем количество категорий в которых надо искать до 2х
		$cats=array_slice($cats,0,2);
		if(!empty($cats)){
			db::query("
				SELECT DISTINCT pid FROM `{$tbls->category2post}` 
				WHERE cid IN ('".implode("','",array_keys($cats))."') && published=1 && pid>'{$post->url}'
				LIMIT {$limit}");
			while ($d=db::fetch()) {
				$pids[]=$d->pid;
				$limit--;
			}
			if($limit){
				db::query("
					SELECT DISTINCT pid FROM `{$tbls->category2post}` 
					WHERE cid IN ('".implode("','",array_keys($cats))."') && published=1 && pid<'{$post->url}'
					LIMIT {$limit}");
				while ($d=db::fetch()) {
					$pids[]=$d->pid;
					$limit--;
				}
			}
			if(!empty($pids)){
				db::query("
					SELECT post.*,u.name AS authorName,u.mail AS authorMail FROM `{$tbls->post}` post
					LEFT JOIN `".PREFIX_SPEC."users` u ON u.id=post.user
					WHERE post.url IN('".implode("','",$pids)."')");
				while ($d=db::fetch()) {
					$d->txt=cutText(strip_tags($d->txt),20);
					setAuthorName($d);
					$related[$d->id]=$d;
					$limit--;
				}
			}
			getImg2list($related);
		}
	}
	return $related;
}
/*
	получает картинки для выбранных постов
	! медленная для большого количества картинок на пост
*/
function getImages2list(&$posts){
	$tbl=tables::init()->post;
	if(empty($posts))return;
	db::query(
		"SELECT img.id,pid,url,kid,keyword.title,filesize,width,height,gtitle,text,source FROM `".PREFIX_SPEC."imgs` img 
		LEFT OUTER JOIN keyword ON keyword.id=img.kid
		WHERE `tbl`='{$tbl}' && `pid` IN(".implode(',', array_keys($posts)).") ORDER BY `priority` DESC"
	);
	while ($d=db::fetch()) {
		$img=new classImg;
		$posts[$d->pid]->imgs[]=add2obj($img,$d);
	}
}
/**
 * получает заданное количество картинок с наивысшим приоритетом для выбранных постов
 * ! с большим количеством постов лучше не использовать
 * @param array &$posts 
 * @param int $limit (optional)
 * @return void
 */
function getImg2list(&$posts,$limit=1){
	$tbl=tables::init()->post;
	if(empty($posts)) return;
	$buildSQLQuery=[];
	foreach (array_keys($posts) as $pid) {
		$buildSQLQuery[]="
			(SELECT id,pid,url,kid,filesize,width,height,gtitle,text,source
			FROM `".PREFIX_SPEC."imgs` img WHERE `tbl`='{$tbl}' && `pid`={$pid}
			ORDER BY priority DESC LIMIT {$limit})";
	}
	$buildSQLQueryFinal="
		SELECT unionimg.*,kw.title FROM (
		".implode('UNION', $buildSQLQuery)."
		) unionimg LEFT OUTER JOIN keyword AS kw ON kw.id=unionimg.kid
	";

	db::query($buildSQLQueryFinal);
	while ($d=db::fetch()) {
		$img=new classImg;
		$posts[$d->pid]->imgs[]=add2obj($img,$d);
		#делаем одну картинку главной, для совместимости со старыми функциями
		if(!isset($posts[$d->pid]->imgUrl)){
			$posts[$d->pid]->imgUrl=$d->url;
			$posts[$d->pid]->imgTitle=$d->title;
		}
	}
}
/*
	получает случайные посты
*/
function randomPosts($limit=4,$exclude=array()){
	$tbl=tables::init()->post;
	$l=$limit*3;
	$random=array();
	list($max)=db::qrow("SELECT MAX(id) FROM `{$tbl}`");
	$rnd=mt_rand(0,$max);
	$SQLexclude=!empty($exclude)?" && id NOT IN(".implode(',', array_keys($exclude)).")":'';
	$sql="SELECT * FROM `{$tbl}` WHERE pincid='' && published=1{$SQLexclude} && %s";

	db::query(sprintf($sql,"id>='".($rnd-300)."' && id<'".($rnd+300)."' LIMIT {$l}"));
	while ($d=db::fetch()) {
		$d->txt=cutText(strip_tags($d->txt),20);
		$random[$d->id]=$d;
		$l--;
		if(!--$limit) break;
	}
	#добираем
	if($l&&$limit){
		db::query(sprintf($sql,"id>0 LIMIT {$l}"));
		while ($d=db::fetch()) {
			$d->txt=cutText(strip_tags($d->txt),20);
			$random[$d->id]=$d;
			if(!--$limit) break;
		}
	}
	$t_random=$random;
	$random=[];
	shuffle($t_random);
	foreach ($t_random as $v) {
		$random[$v->id]=$v;
	}
	getImg2list($random);
	return $random;
}
/*
	получает несколько постов с самой крайней датой обновления
*/
function recentUpdatePosts($limit=4){
	$recent=[];
	$tbl=tables::init()->post;
	$sql="SELECT * FROM `{$tbl}` WHERE pincid='' && published=1 ORDER BY `datePublish` DESC LIMIT {$limit}";
	db::query($sql);
	while ($d=db::fetch()) {
		$d->txt=cutText(strip_tags($d->txt),20);
		$recent[$d->id]=$d;
	}
	getImg2list($recent);
	return $recent;
}
/*
	сортирует категории относителдьно их родительских категорий
*/
function sortCats($cats){
	if(empty($cats)) return;
	foreach ($cats as $i=>$v) {
		if(!isset($v->parentId)) continue;
		$parents[$v->url]=new \stdclass;
		$parents[$v->url]=$v->parentId;
		$cats2url[$v->url]=$i;
	}
	if(!isset($parents)) return;
	#сортируем
	asort($parents);
	#возвращаем остальные данные объекта
	foreach ($parents as $url => $v) {
		$parents[$url]=$cats[$cats2url[$url]];
	}
	#помещаем подкатегории в их родителей
	foreach ($parents as $url => $v) {
		if(isset($parents[$v->parentId])){
			$parents[$v->parentId]->child[$url]=$parents[$url];
			unset($parents[$url]);
		}
	}
	#помещаем подкатегории следом за их родителем
	return recSortedCats($parents);
}
/*
	рекурсивный проход по дочерним категориям
*/
function recSortedCats($cats){
	static $sorted;
	if(empty($sorted)) $sorted=array();
	foreach ($cats as $url => $v) {
		$sorted[]=$v;
		if(isset($v->child)){
			recSortedCats($v->child);
			unset($v->child);
		}
	}
	return $sorted;
}
/*
 * Создает содержание текста из тиегов h1-h6
*/ 
function makeContents($fromHtml){
	require_once 'simple_html_dom.php';
	$html = str_get_html($fromHtml);
	if(!$html)return array('',$fromHtml);
	$toc = '';
	$last_level = 0;
	foreach($html->find('h1,h2,h3,h4,h5,h6') as $h){
		$innerTEXT = strip_tags(trim($h->innertext));
		$id = strtr($innerTEXT,array("'"=>''));
		$h->id= $id; // add id attribute so we can jump to this element
		$level = intval($h->tag[1]);
		if($level > $last_level)
			$toc .= "<ol>";
		else{
			$toc .= str_repeat('</li></ol>', $last_level - $level);
			$toc .= '</li>';
		}
		$toc .= "<li><a href=\"#{$id}\">{$innerTEXT}</a>";
		$last_level = $level;
	}
	$toc .= str_repeat('</li></ol>', $last_level);
	return array($toc,$html->save());
}
/*
	Добаляет cid и название 1-й категории к списку постов
*/
function joinCat(&$posts){
	$tbl=tables::init();
	db::query("
		SELECT p.id,cp.cid,c.title AS catTitle FROM (
			SELECT * FROM `{$tbl->post}` WHERE id IN(".implode(',',array_keys($posts)).")
		) p 
		LEFT JOIN {$tbl->category2post} cp ON p.url=cp.pid
		LEFT JOIN {$tbl->category} c ON c.url=cp.cid
		GROUP BY p.id");
	while ($d=db::fetch()) {
		$posts[$d->id]->cid=$d->cid;
		$posts[$d->id]->catTitle=$d->catTitle;
	}
	return $posts;
}
/*
	обертка для объекта картинок на списках
*/
class classImg{
	public function __toString(){
		return $this->url;
	}
}
/*
	класс для работы с таблицами постов
*/
class tables{
	public $tbls;
	static $instance;
	function __construct(){
		$this->tbls['post']='post';
		$this->tbls['category']='category';
		$this->tbls['category2post']=PREFIX_SPEC.'category2post';
		$this->tbls['postHistory']='postHistory';
		$this->prfx='';
	}
	/*
		возвращает существующий объект класса tables
	*/
	static function init($prfx=''){
		if(self::$instance===null)
			self::$instance=new tables;
		self::$instance->change($prfx);
		return self::$instance;
	}
	static function switchTable($prfx=''){
		self::$instance=new tables;
		self::$instance->change($prfx);
	}
	static function initByTbl($tbl){
		$tbl=str_replace(PREFIX_SPEC, '', $tbl);
		preg_match('!^([^_]+)_!', $tbl, $m);
		$prfx=isset($m[1])?$m[1]:'';
		if(self::$instance===null)
			self::$instance=new tables;
		self::$instance->change($prfx);
		return self::$instance;
	}
	function __get($k){
		if(!isset($this->tbls[$k])){
			trigger_error('Class [table]: using table "'.$k.'" is not set');
			return '';
		}
		return $this->tbls[$k];
	}
	/*
		задает префиксы для таблиц поста
	*/
	function change($prfx){
		if(empty($prfx)||$this->prfx==$prfx) return;
		$this->prfx=$prfx;
		foreach ($this->tbls as &$v) {
			if(strstr($v, PREFIX_SPEC))
				$tname=PREFIX_SPEC.$prfx.'_'.str_replace(PREFIX_SPEC, '', $v);
			else
				$tname=$prfx.'_'.$v;
			$v=db::escape($tname);
		}
	}
}
