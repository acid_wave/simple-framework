<?if($data->topLevelCats->samelevel&&$data->topLevelCats->samelevel->cats){?>
<div class="toplevel">
	<h4>
		<?=$data->topLevelCats->samelevel->baseCat->title?> 
		<?if($data->topLevelCats->edit){?><a href="<?=url::category_edit(0,'',$data->topLevelCats->prfxtbl)?>">add</a><?}?>
	</h4>
	<ul>
	<?foreach($data->topLevelCats->samelevel->cats as $val){?>
		<li>
			<?if($data->topLevelCats->samelevel->current==$val->url){?>
				<?=$val->title?>&nbsp;<small title="count of posts">(<?=$val->count?>)</small>&nbsp;
			<?}else{?>
				<a href="<?=url::category($val->url,'blog')?>"><?=$val->title?></a>&nbsp;<small title="count of posts">(<?=$val->count?>)</small>&nbsp;
			<?}?>
			<?if(!empty($val->funcPanel)){?><?=$val->funcPanel?><?}?>
		</li>
	<?}?>
	</ul>
</div> 
<?}?>
<div class="toplevel">
<?if(!empty($data->topLevelCats->cats)){?>
	<h4>
		Categories 
		<?if($data->topLevelCats->edit){?><a href="<?=url::category_edit(0,'',$data->topLevelCats->prfxtbl)?>"><img width="15" src="<?=HREF?>/files/template/icons/add.png" /></a><?}?>
	</h4>
	<ul>
	<?foreach($data->topLevelCats->cats as $val){?>
		<li>
			<?if(!empty($val->funcPanel)){?><?=$val->funcPanel?><?}?>
			<a href="<?=url::category($val->url,'blog')?>"><?=$val->title?></a>&nbsp;<small title="count of posts">(<?=$val->count?>)</small>&nbsp;
		</li>
	<?}?>
	</ul>
<?}?>
</div> 
