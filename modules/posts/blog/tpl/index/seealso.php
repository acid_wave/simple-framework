<?if(empty($data->otherPosts)) return;?>
<div class="row">
	<div class="col-md-12">
		<h4>See also</h4>
	</div>
</div>
<?foreach($data->otherPosts as $rel){?>
<div class="row blog-widget">
	<div class="col-md-5">
		<div class="thumb_sm thumbnail">
			<a href="<?=url::post($rel->url,'blog')?>">
				<div class="photo-item-img">
					<img src="<?=url::imgThumb('450_',@$rel->imgs[0])?>" alt="<?=$rel->title?>"/>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-7 pl-0">
		<a href="<?=url::post($rel->url,'blog')?>" style="color: #000000;text-decoration: none;">
			<h5 class="font-weight-bold mt-0"><?=$rel->title?></h5>
			<i class="fa fa-calendar"></i> <?=$rel->date?>
			<p><?=$rel->txt?></p>
		</a>
	</div>
</div>
<?}?>
