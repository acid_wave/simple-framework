<?
if(empty($data->related)) return;?>
<div class="row">
	<div class="col-12"><h4>Related posts</h4></div>
	<?foreach($data->related as $p){?>
	<div class="col-md-3">
		<div class="postList thumbnail">
			<a href="<?=url::post($p->url,'blog')?>">
				<div class="photo-item-img">
					<img src="<?=url::imgThumb('450_',@$p->imgs[0])?>" alt="<?=$p->title?>"/>
				</div>
			</a>
			<div class="photo-item-title">
				<div class="row">
					<div class="col-12">
						<p class="text-center"><b><?=$p->title?></b></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?}?>
</div>
