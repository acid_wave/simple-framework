<?if(empty($data->post->imgs))return?>
<?
if(!function_exists('editTextImg')){
	function editTextImg($text){
		$text=trim($text,".");
		$text=preg_replace("! +!",' ',$text);
		$text=str_replace("%20",' ',$text);
		$text=str_replace("-",' ',$text);
		$e=explode(" ",$text);
		$text=trim(implode(" ",array_slice($e,0,5)));
		$text=trim($text,".,");
		return ucfirst($text);
	}
}
?>
<style>
	.imageFrame{
		width:205px;
		height:150px;
		display:inline-block;
		padding:10px;
	}
	.imageFrame .image{
		width:100%;
		height:85%;
		background-position: center center;
		background-repeat: no-repeat;
		background-size: cover;
		position: relative;
		margin-bottom:5px;
	}
	.imageFrame .image img{
		opacity:0;
		width:100%;
		height:100%
	}
	.imageFrame .textOverImage{
		background: rgba(0,0,0,0.5);
		color: white;
		cursor: pointer;
		display: table;
		height: 100%;
		width:100%;
		left: 0;
		position: absolute;
		top: 0;
		opacity: 0;
	}
	.imageFrame .textOverImage span {
		display: table-cell;
		text-align: center;
		vertical-align: middle;
	}
	.imageFrame .image a:hover .textOverImage{
		opacity: 1;
	}
	.avatar{
		vertical-align:middle;
		width:25px;
		height:25px;
		display:inline-block;
		border-radius:50%;
	}
</style>
<?
$page=1; $num=0; $cols=4;
foreach($data->post->imgs as $img){
	if($num&&!($num%$cols)) $page++;
	$num++;
	$text=editTextImg($img->text);
	$ptitle[]=strtolower($text);
	$title="$img->title #$num";?>
	<div class="imageFrame">
		<div class="image" style="background-image:url('<?=url::imgThumb('250_',$img->url)?>');">
			<a title="<?=$title?>" href="<?=url::img('post',$img->pid,$img->url)?>" title="<?=$img->text?>"
				data-tbl="<?=$post->tbl?>" data-pid="<?=$post->id?>" data-url="<?=$img->url?>">
				<img src="<?=url::image($img->url)?>" alt="<?=$title?>"/>
				<span class="textOverImage"><span><?=$img->gtitle?> <i class="fa fa-heart-o"></i> <?=intval(@$img->pinCount)?> <a href="<?=url::pinImage($img->id)?>" target="_blank">Like</a><br></span></span>
			</a>
		</div>
		<div style="float:right">
		<?if(!empty($img->users[0])){
			$user=current($img->users);?>
			<a href="<?=url::userPhotos($user->id)?>">
				<?=$user->name?>
				<img src="<?=$user->avatar?>" class="avatar" style="width: 20px;height: 20px;">
			</a>
		<?}?>
		</div>
	</div>
<?}?>
<div style="clear:both;"></div>

<?
$tpl->title.=" | ".ucfirst(implode(", ",array_slice($ptitle,0,3))).".";
$tpl->desc.=". ".ucfirst(implode(", ",array_slice($ptitle,3,6))).".";
