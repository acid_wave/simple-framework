<?php
$post=$data->post;
$tpl->title="{$post->title} - ".NAME;
@$tpl->desc="{$post->imgs[0]->gtitle} by ".NAME;
$tpl->meta="
	<script type=\"application/javascript\" src=\"".HREF."/files/js/mobiledetect.js\"></script>
	
	<meta name='robots' content='index, follow'/>
	<meta name='revisit-after' content='1 days'/>

	<meta property='og:locale' content='en_US'/>
	<meta property='og:type' content='article'/>
	<meta property='og:title' content='{$post->title} - ".NAME."'/>
	<meta property=\"og:description\" content=\"".htmlspecialchars(@$post->imgs[0]->gtitle)." by ".NAME."\"/>
	<meta property='og:url' content='".url::post($post->url)."'/>
	<meta property='og:site_name' content='".NAME."'/>
	<meta property='og:image' content='".url::image(@$post->imgs[0]->url)."'/>
";
?>
<script type="application/javascript" src="<?=HREF?>/files/posts/js/gallery.js"></script>
<script type="application/javascript" src="<?=HREF?>/files/posts/js/social-load.js"></script>
<!--[if lt IE 9]>
<script type="text/javascript" src="<?=HREF?>/files/index/js/ba-hashchange.js"></script>
<![endif]-->
<div class="row">
	<div class="col-md-8">
		<?=$post->funcPanel?>
		<div class="title"><h1><?=ucfirst($post->title)?></h1></div>
		<?if(!empty($post->imgcookie)){?>
			<div class="images">
				<a class="overlay-enable" href="<?=url::img('post',$post->imgcookie->pid,$post->imgcookie->url)?>" data-tbl="<?=$post->tbl?>" data-pid="<?=$post->id?>" data-url="<?=$post->imgcookie->url?>">
					<img src="<?=url::imgThumb('600_',$post->imgcookie->url)?>" alt="<?=$post->imgcookie->title?>" title="<?=$post->imgcookie->title?>"/>
				</a>
			</div>
		<?}?>
		<?include $template->inc('index/sliderGroup.php');?>
		<?include $template->inc('index/content.php');?><br>
		<?include $template->inc('index/slider.php');?>
		<center><?=@$data->ads->top?></center>
		<?include $template->inc('index/subscribe.php');?>

		<div class="text">
			<?include $template->inc('index/postbody.php');?>
		</div>
		<?if(!empty($post->prevnext->next)&&!empty($post->prevnext->prev)){?>
		<div class="navv">
			<a href="<?=url::post($post->prevnext->prev->url,$data->prfxtbl)?>" class="previous">
				<strong>Previous</strong>
				<span><?=$post->prevnext->prev->title?></span>
			</a>
			<a href="<?=url::post($post->prevnext->next->url,$data->prfxtbl)?>" class="next">
				<strong>Next</strong>
				<span><?=$post->prevnext->next->title?></span>
			</a>
			<div style="clear:both;"></div>
		</div>
		<?}?>
		<div style="clear:both;"></div>
		<?include $template->inc('index/soc-buttons.php');?>
		<div style="clear:both;"></div>
		<?include $template->inc('index/breadcrumbs.php');?>
		<?if(defined('FACEBOOKPAGE')){?>
		<div class="follow">
			<h4>Funny news, interesting pictures</h4><span>Enjoy with us!</span>
			<div id="fb-root"></div>
			<div align="center" id="fb-follow-bottom" class="fb-like" data-href="https://www.facebook.com/<?=FACEBOOKPAGE?>" data-layout="standard" data-action="like" data-ref="art-bottom" data-show-faces="false" data-share="false" data-width="300"></div>
		</div>
		<?}?>
		<?=$data->comments?>
	</div>
	<div class="col-md-4">
		<?=@$data->ads->right?>
		<?include $template->inc('../category/tpl/subList.php');?>
		<?include $template->inc('../category/tpl/mlist.php');?>
		<?include $template->inc('index/seealso.php');?>
	</div>
</div>
<?include $template->inc('index/related.php');?>
<?include $template->inc('index/fb-popup.php');?>
