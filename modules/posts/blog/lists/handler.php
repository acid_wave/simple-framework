<?php
namespace posts\blog\lists;
use module,db,url,\category\category;

include_once module::$path.'/category/handler.php';

class handler{
	function __construct(){
		$this->template='template';
		$this->headers=new \stdClass;
		$this->uhandler=module::exec('user',array(),1)->handler;
		$this->user=$this->uhandler->user;
	}
	function index($category,$page,$num){
		$baseCat=new category($category,$this->uhandler->access->showAllCat,'blog');

		$data=module::execData('posts/lists',[
			'act'=>'postByCat',
			'curl'=>$baseCat->url,
			'prfxtbl'=>'blog',
			'page'=>$page,
			'num'=>$num,
		]);
		if(empty($data->posts)&&$category&&!$this->user->id){
			$this->headers->location=HREF; return;
		}
		if(!$category) \posts\joinCat($data->posts);
		$data->paginator=module::execStr('plugins/paginator',
			[
				'act'=>'typesmall',
				'page'=>$page,
				'num'=>$num,
				'count'=>$data->count,
				'uri'=>($category?url::post_blog_category($category):url::post_blog_list()).'page-%d/',
			]
		);
		$data->cat=$baseCat;
		$data->breadCrumbs=\posts\getCrumbs($baseCat);
		$data->subCats=module::execData('category',['act'=>'subList','url'=>$baseCat->url]);
		$data->topLevelCats=module::execData('category',['act'=>'mlist','category'=>$baseCat->url,'parentId'=>@$baseCat->parentId]);
		
		return $data;
	}
}
