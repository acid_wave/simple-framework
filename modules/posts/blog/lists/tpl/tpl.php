<?php
$cat=$data->cat;
$posts=$data->posts;
$tpl->title=(!empty($cat->title)?$cat->title:"Blog").(@$data->page>1?" - Page {$data->page}":'');
$tpl->desc=!empty($cat->title)?"Records for category \"{$cat->title}\".":'';
if(count($posts)<3 or @$data->page>1 or @$data->noindex)$tpl->meta='<meta name="robots" content="noindex,follow" />';
if(!count($data->posts)){echo "Nothing Found!";return;}
?>
<div class="row">
	<div class="col-md-8">
		<?include $template->inc('index/breadCrumbs.php');?>
		<?if(!empty($data->pins)){?>
		<div class="top-level-models">
			<?include $template->inc('index/pinposts.php');?>
		</div>
		<?}?>
		<div class="top-level-models">
			<?include $template->inc('index/listposts.php');?>
			<?include $template->inc('index/delposts.php');?>
			<?=@$data->paginator?>
			<?if(@$data->count){?>
				<small>items:&nbsp<i><?=@$data->count?></i></small>
			<?}?>
		</div>
	</div>
	<div class="col-md-4">
		<?include $template->inc('../../category/tpl/subList.php');?>
		<?include $template->inc('../../category/tpl/mlist.php');?>
	</div>
</div>
