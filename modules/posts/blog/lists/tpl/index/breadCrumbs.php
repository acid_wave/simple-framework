<?if(empty($data->breadCrumbs))return;?>
<div class="breadcrumbs">
	<a href="<?=HREF?>">HOME</a> >
	<?foreach($data->breadCrumbs as $val){
		if($val->url==$cat->url){
			print "$val->title";
			continue;
		}?>
		<a href="<?=url::category($val->url)?>" title="<?=$val->title?>"><?=$val->title?></a>
	<?}?>
</div>
