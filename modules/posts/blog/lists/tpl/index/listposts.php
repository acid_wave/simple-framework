<?if(!empty($posts)){
	foreach($posts as $p){?>
	<?=@$p->funcPanel?>
	<div class="model">
		<a href="<?=url::post($p->url,$data->prfxtbl)?>">
			<img style="height:100px;" src="<?=empty($p->imgs)?HREF.'/files/images/404.png':url::image($p->imgs[0])?>"
					alt="<?=$p->title?>"
			/>
		</a>
		<div class="model-text">
			<div class="title"><a href="<?=url::post($p->url,$data->prfxtbl)?>" title="<?=$p->title?>"><?=$p->title?></a></div>
			<small>
				<i class="fa fa-calendar"></i> <?=date('Y-m-d', strtotime($p->datePublish?$p->datePublish:$p->date))?> 
				<?if(!empty($p->authorName)){?>
					<i class="fa fa-user"></i> <?=$p->authorName?>
				<?}?>
			</small>
			<small class="float-right clearfix">
				<i class="fa fa-eye"></i> <?=$p->statViews?> (<span title="for 7 days"><?=$p->statViewsShort?></span>)
				<i class="fa fa-picture-o"></i> <?=$p->countPhoto?>
			</small>
			<p><?=strip_tags($p->txt)?></p>
		</div>
		<div class="clearfix"></div>
	</div>
	<?}
}?>
