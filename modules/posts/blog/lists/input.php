<?php
namespace posts\blog\lists;
use control,db;

class input extends control{
	function __construct($input){
		if(empty($input->act)) $input->act='index';
		if($input->act=='index'){
			$this->data=[
				'category'=>empty($input->cat)?'':db::escape(urldecode($input->cat)),
				'page'=>empty($input->page)?1:$input->page,
				'num'=>cookiePage($input,'num',10),
			];
		}else
			$this->act=false;
	}
}
