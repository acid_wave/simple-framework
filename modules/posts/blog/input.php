<?php
namespace posts\blog;
use control,stdClass;

class input extends control{
	function __construct($input){
		$this->data=new stdClass;
		if(empty($input->act)) $input->act='index';
		
		if($input->act=='index'){
			$this->data=(object)array(
				'url'=>!empty($input->url)?$input->url:false,
			);
		}else
			$this->act=false;
	}
}