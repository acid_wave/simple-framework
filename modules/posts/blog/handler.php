<?php
namespace posts\blog;
use module,db,url,\posts\tables,user\user;

include_once __DIR__.'/../handler.php';
require_once(module::$path.'/user/handler.php');

class handler{
	function __construct(){
		$this->template='template';
		$this->headers=new \stdClass;
		$this->uhandler=module::exec('user',array(),1)->handler;
		$this->user=$this->uhandler->user;
	}
	/*
		страница поста блога
	*/
	function index($url){
		$tbls=tables::init('blog');
		$post=\posts\getPost($url,$this->uhandler->access);
		
		$accessEdit=\posts\checkAccess($this->uhandler,@$post->user->id);
		if(empty($post) or ($post->published!=1 && !$accessEdit)){
			$this->headers->location=HREF;
			return;
		}
		
		$related=\posts\relatedByCat($post,4);
		$ex=$related;
		$ex[$post->id]=$post;
		return [
			'post'=>$post,
			'user'=>$this->user,
			# получаем данные категорий
			'subCats'=>module::execData('category',['act'=>'subList','url'=>isset($post->cats[0])?$post->cats[0]->url:false,'tbl'=>$tbls->post]),
			'topLevelCats'=>module::execData('category',['act'=>'mlist','tbl'=>$tbls->post]),
			# Коментарии
			'comments' => module::execStr('posts/comments',['pid'=>$post->id]),
			# Доступы
			'access'=>(object)[
				'publish'=>$this->uhandler->access->publishPost,
				'edit'=>$accessEdit,
			],
			# Перелинковка на другие посты
			'related'=>$related,
			'otherPosts'=>\posts\randomPosts(30,$ex),
		];
	}
}
