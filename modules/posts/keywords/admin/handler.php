<?php
namespace posts\keywords\admin;
use module,db,url,cache;

class handler{
	function __construct(){
		$this->template='template';
		$this->headers=new \stdClass;
	}
	/**
	 * Записывает в таблицу keywords кейворды полученные из title поста
	 * @return type
	 */
	function createFromPosts($submit=false){
		$this->template='';
		if($submit){
			db::query("INSERT IGNORE INTO `keyword` (`title`) (
				SELECT `title` FROM `post` GROUP BY `title`
			)");
			$res=db::query("INSERT IGNORE INTO ".PREFIX_SPEC."keyword2post (kid, pid, tbl) (
				SELECT k.id, p.id, 'post' FROM `post` p INNER JOIN `keyword` k ON p.title=k.title
			)");
			echo db::affected($res).' new keywords written';
			die;
		}
		return [];
	}
}
