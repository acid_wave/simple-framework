<?php
class posts_keywords extends control{
	function __construct($input){
		$this->data=new stdClass;
		if(empty($input->act)) $input->act='index';
		
		if($input->act=='index'){
			$this->data=[
				'url'=>!empty($input->url)?$input->url:'',
				'prfxtbl'=>!empty($input->prfxtbl)?$input->prfxtbl:'',
			];
		}else
			$this->act=false;
	}
}