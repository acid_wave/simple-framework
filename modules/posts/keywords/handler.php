<?php
namespace posts_keywords;
use module,db,url,cache,posts\tables;

#include posts functions
require_once __DIR__.'/../func.php';

class handler{
	function __construct(){
		$this->template='template';
		$this->headers=new \stdClass;
		$this->userControl=module::exec('user',array(),1)->handler;
		$this->user=$this->userControl->user;
	}
	function index($url,$prfxtbl){
		$tbls=tables::init($prfxtbl);
		$keyword=urldecode($url);
		db::query("
			SELECT i.*, k.title as `title`, post.id as pid, post.url as postUrl FROM `".PREFIX_SPEC."imgs` i 
			INNER JOIN `keyword` k ON k.id=i.kid && k.title='".db::escape($keyword)."'
			INNER JOIN `{$tbls->post}` post ON post.id=i.pid && i.tbl='{$tbls->post}'
		");
		$imgs=[];
		while ($d=db::fetch()) {
			$imgs[]=$d;
		}
		if(empty($imgs)) $this->headers->location=HREF;
		return [
			'prfxtbl'=>$prfxtbl,
			'keyword'=>$keyword,
			'imgs'=>$imgs,
		];
	}
}
