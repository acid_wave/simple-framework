<?php
namespace posts;
use module,db,url,cache,user\user;
#Используем сторонние модули
require_once(module::$path.'/category/handler.php');
require_once(module::$path.'/user/handler.php');

require_once(module::$path."/posts/func.php");
/*
 * Должен возвращать:
 * $this->data - объект переменных для вывода в шаблоне
 * $this->headers - объект для изменения заголовков при отдаче
 * $this->act - если есть несколько вариантов ответов
 */

class handler{
	function __construct(){
		$this->headers=(object)array();
		$this->template='template';#Определяем в какой шаблон будем вписывать
		$this->userControl=user::init();
		$this->user=$this->userControl->user;
	}
	/*
		1. данные текущего поста
		2. подключает данные категории (текущей и список подкатегорий)
	*/
	function post($url,$imglastviewed,$prfxtbl){
		#переключаем таблицы постов
			$tbls=tables::init($prfxtbl);
		#Получаем пост
			$post=getPost($url,$this->userControl->access,$imglastviewed);
		#Проверяем права на просмотр и редактирование поста
			$accessEdit=checkAccess($this->userControl,$post->user->id);
			if(empty($post) or ($post->published!=1 && !$accessEdit)){
				$this->headers->location=HREF;
				return;
			}
		#Получаем посты из той же категории
			$related=relatedByCat($post,4);
			$ex=$related;
			$ex[$post->id]=$post;
		return array(
			'post'=>$post,
			'prfxtbl'=>$tbls->prfx,
			'user'=>$this->user,
			'keyword'=>$post->keyword,
			# получаем данные категорий
			'subCats'=>module::execStr('category',array('act'=>'subList','url'=>isset($post->cats[0])?$post->cats[0]->url:false,'tbl'=>$tbls->post)),
			'topLevelCats'=>module::execStr('category',array('act'=>'mlist','tbl'=>$tbls->post)),
			# Коментарии
			'comments'=>module::execStr('posts/comments', ['pid'=>$post->id]),
			# Доступы
			'access'=>(object)array(
				'publish'=>$this->userControl->rbac('publishPost'),
				'edit'=>$accessEdit,
			),
			# Перелинковка на другие посты
			'related'=>$related,
			'otherPosts'=>randomPosts(30,$ex),
			'pinForm'=>module::execStr('pins',['act'=>'pinForm','sudgests'=>$post->cids]),
		);
	}
	/*
		мобильный шаблон для поста
	*/
	function mobile($url,$imgfromcookie,$prfxtbl){
		//$this->template='';
		return module::exec('posts',array('act'=>'post','url'=>$url,'imgfromcookie'=>'','prfxtbl'=>$prfxtbl),'data')->data;
	}
	/*
		выводит панель удаления/редактирования поста
	*/
	function editPanel($post){
		$tbls=tables::init();
		$rbac=false;
		if(!$rbac=$this->userControl->rbac('editNews')){
			$uid=is_object($post->user)?$post->user->id:$post->user;
			if($this->user->id==$uid && $post->published!=1)
				$rbac=$this->userControl->rbac('editNewsMy');
		}
		$accessEdit=$rbac;
		$accessPub=$this->userControl->rbac('publishPost');
		$accessDel=($rbacDel=$this->userControl->rbac('delNews'))?$rbacDel:($post->published!=1&&$this->userControl->rbac('delMyUnpublishedNews'));
		$accessHistory=$this->userControl->rbac('viewHistory');
		return array(
			'post'=>$post,
			'accessEdit'=>$accessEdit,
			'accessPub'=>$accessPub,
			'accessHistory'=>$accessHistory,
			'accessDel'=>$accessDel,
			'prfxtbl'=>$tbls->prfx,
		);
	}
	/*
		обработка rss шаблона
	*/
	function rss(){
		$this->template='';
		db::query("SELECT * FROM `post` WHERE `published`='1' && `pincid`='' ORDER BY `datePublish` DESC LIMIT 50");
		$posts = array();
		while($d=db::fetch()){
			$d->title=htmlentities(html_entity_decode($d->title),ENT_XML1);
			$d->txt=htmlentities(html_entity_decode(cutText(strip_tags($d->txt))),ENT_XML1);
			$posts[$d->id]=$d;
		}
		
		#получаем картинки
		db::query("SELECT pid,GROUP_CONCAT(img.url ORDER BY img.priority DESC) AS img FROM `".PREFIX_SPEC."imgs` img
			WHERE `tbl`='post' && `pid` IN(".implode(',',array_keys($posts)).") GROUP BY pid");
		while ($d=db::fetch()) {
			$posts[$d->pid]->imgs=explode(',',$d->img);
		}
		
		return array(
			'posts'=>$posts,
		);
	}
}
