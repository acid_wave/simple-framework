<?php
namespace category_admin;
use module,db,url,StdClass;

require_once(__DIR__.'/func.php');
require_once(module::$path.'/admin/themes/handler.php');
require_once(module::$path.'/posts/handler.php');

/*
 * Должен возвращать:
 * $this->data - объект переменных для вывода в шаблоне
 * $this->headers - объект для изменения заголовков при отдаче
 * $this->act - если есть несколько вариантов ответов
 */
class handler{
	function __construct(){
		$this->template='template';#Определяем в какой шаблон будем вписывать
		$this->headers=(object)array();
		$this->userControl=module::exec('user',array(),1)->handler;
	}
	/*
		1. данные для вывода формы редактирования категории
	*/
	function edit($parentID,$itemID,$prfxtbl){
		if(!$this->userControl->rbac('editCat')) die('forbidden');
		$tbls=\posts\tables::init($prfxtbl);
		# получаем данные самого элемента
		if($itemID){
			$item=db::qfetch("SELECT * FROM `{$tbls->category}` WHERE `id`='{$itemID}' LIMIT 1");
			$parentID=$item->parentId;
		}else{
			$item = new StdClass;
			$item->id=$itemID;
		}
		# получаем данные родителя
		if($parentID){
			$parent=db::qfetch("SELECT * FROM `{$tbls->category}` WHERE `url`='{$parentID}' LIMIT 1");
		}else{
			$parent = new StdClass;
			$parent->url='';
			$parent->title='Index';
		}
		return (object)array(
			'parent'=>$parent,
			'item'=>$item,
			'themes'=>$this->userControl->rbac('themesSet')?\admin_themes\themes():array(),
			'prfxtbl'=>$prfxtbl,
		);
	}
	/*
		1. сохранение категории
		2. подключает вывод формы редактирования
	*/
	function save($parentID,$itemID,$itemUrl,$prfxtbl,$merge,$name,$view,$subCatList,$theme,$disableLimit){
		if(!$this->userControl->rbac('editCat')) die('forbidden');
		$tbls=\posts\tables::init($prfxtbl);
		$mesSuccess=$mesWarning='';
		$urlExists=false;
		if($itemID){
			list($oldUrl)=db::qrow("SELECT url FROM `{$tbls->category}` WHERE id='{$itemID}' LIMIT 1");
			#edit
			if($parentID!=''){
				list($parentExists)=db::qrow("SELECT id FROM `{$tbls->category}` WHERE `url`='{$parentID}' LIMIT 1");
				if(empty($parentExists)){
					$mesWarning='parentNotExists';
				}
			}
			if($itemUrl){
				if($itemUrl=='')
					$mesWarning='urlEmpty';
				else{
					list($urlExists)=db::qrow("SELECT id FROM `{$tbls->category}` WHERE `url`='{$itemUrl}' LIMIT 1");
					if(!empty($urlExists)&&!$merge){
						$mesWarning='urlExists';
					}elseif($merge){
						if(mergeCats($oldUrl,$itemUrl)){
							$itemID=$urlExists;
							$mesWarning='mergeComplete';
							$mesSuccess='usuccess';
						}else
							$mesWarning='mergeFail';
						
					}
				}
			}
			if(empty($mesWarning)){
				db::query(
				"UPDATE `{$tbls->category}` SET 
					`title`='{$name}',
					`view`='{$view}',
					`parentID`='{$parentID}',
					".($itemUrl?"`url`='{$itemUrl}',":'')."
					`subCatList`='{$subCatList}',
					`theme`='{$theme}',
					`disableLimit`='{$disableLimit}'
					WHERE `id`='{$itemID}' LIMIT 1");
				if(!db::error()){
					$mesSuccess='usuccess';
					if($urlExists!=$itemID&&$itemUrl)
						updateCategoryUrlRel($oldUrl,$itemUrl);
				}
				else
					$mesWarning='ufail';
			}
		}else{
			#add
			list($exists)=db::qrow("SELECT `id` FROM `{$tbls->category}` WHERE `title`='{$name}' && `parentId`='{$parentID}' LIMIT 1");
			if(!$exists){
				db::query("INSERT INTO `{$tbls->category}` (`url`,`title`,`parentId`,`view`,`theme`,`subCatList`) 
					VALUES('".getUrl($tbls->category,$name)."','{$name}','{$parentID}','{$view}','{$theme}','{$subCatList}')");
				if($itemID=db::insert())
					$mesSuccess='isuccess';
				else
					$mesWarning='ifail';
			}else{
				$mesWarning='nameExists';
			}
		}
		return (object)array(
			'success'=>$mesSuccess,
			'warning'=>$mesWarning,
			'html'=>module::exec('category/admin',
				array('act'=>'edit','itemID'=>$itemID,'prfxtbl'=>$prfxtbl),1)->str,
		);
	}
	/*
		1. удаление категории
	*/
	function del($itemID,$prfxtbl){
		if(!isset($this->userControl->access->delCat)) die('forbidden');
		$tbls=\posts\tables::init($prfxtbl);
		if($itemID){
			list($url,$parentID)=db::qrow("SELECT `url`,`parentId` FROM `{$tbls->category}` WHERE `id`='{$itemID}' LIMIT 1");
			db::query("DELETE FROM `{$tbls->category}` WHERE `id`='{$itemID}' LIMIT 1");
			# удаление связи с постами
				db::query("DELETE FROM `{$tbls->category2post}` WHERE `cid`='{$url}'");
			#Обновляем счетчик категории Uncategorized 
				list($count)=db::qrow("SELECT COUNT(*) FROM `{$tbls->post}` post 
					LEFT OUTER JOIN `{$tbls->category2post}` rel ON rel.pid=post.url
				WHERE rel.cid IS NULL");
				db::query("INSERT INTO `{$tbls->category}` 
					(title,url,countAdmin) 
					VALUES ('Uncategorized','no-category','$count')
					ON DUPLICATE KEY UPDATE countAdmin='$count'
				");
		}
		$this->headers->location=!empty($parentID)?url::category($parentID):HREF;
		return (object)array();
	}
	/*
		вывод панели действй (удалить/редактировать и тд.)
			- включает проверку прав на эти действия
	*/
	function funcPanel($itemID,$itemUrl){
		$accessEdit=$this->userControl->rbac('editCat');
		$accessDel=$this->userControl->rbac('delCat');
		$tbls=\posts\tables::init();
		return (object)array(
			'itemID'=>$itemID,
			'itemUrl'=>$itemUrl,
			'accessEdit'=>$accessEdit,
			'accessDel'=>$accessDel,
			'prfxtbl'=>$tbls->prfx,
		);
	}
	/*
	 * Создаем подкатегории на основе большей категории и слов которые там встречаются
	*/ 
	function createAutoSubCats(){
		set_time_limit(0);
		#Плдучаем список категорий для кого будем пытытьася создать подкатегории
		db::query("SELECT title,cid FROM `".PREFIX_SPEC."category2post` as c2p
			LEFT JOIN `category` AS c
				ON c2p.cid=c.url
			where c.parentId='tags'
			group by cid HAVING count(*)>25");
		while($d=db::fetch()){
			$cids[$d->cid]=$d->title;
		}
		#Получаем список кейвордов для каждой темы из которых будем  пытаться сформировать новые категории
		foreach($cids as $cid=>$ctitle){
			$t2k=new t2k;
			#Получаем Заголовки постов
				db::query("SELECT url,title from post 
					LEFT JOIN `".PREFIX_SPEC."category2post` AS c2p
					ON c2p.pid=post.url
					WHERE c2p.cid='$cid'");
				$pids=array();
				while($d=db::fetch()){
					$pids[]=$d->url;
					$t2k->getKeys($d->title,$d->url,$cid);
				}
			#Получаем теги постов
				db::query("SELECT c2p.pid,title from category as c
					LEFT JOIN `".PREFIX_SPEC."category2post` AS c2p
					ON c2p.cid=c.url
					WHERE c2p.pid in('".implode("','",$pids)."') and view=1");
				while($d=db::fetch()){
					$t2k->getKeys($d->title,$d->pid,$cid);
				}	
			#Вставляем в базу новые подкатегории
			$keys=$t2k->filter();
			foreach($keys as $k=>$v){
				$url=key2url("$ctitle $k");
				db::query("INSERT INTO category SET title='$ctitle $k',url='$url',parentId='$cid'");
				if(db::insert()){
					$sql=array();
					foreach($v as $purl=>$p){
						$sql[]="('$purl','$url')";
					}
					db::query("INSERT INTO `".PREFIX_SPEC."category2post` (pid,cid) VALUES ".implode(",",$sql));
				}
			}
		}
		print "DONE";
		EXIT;
	}
	function install(){
		$tbl=\posts\tables::init();
		db::query("CREATE TABLE IF NOT EXISTS `{$tbl->category}` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `url` varchar(125) NOT NULL,
		  `title` varchar(255) NOT NULL,
		  `parentId` varchar(255) NOT NULL,
		  `count` int(11) NOT NULL COMMENT 'cache count',
		  `countAdmin` int(11) NOT NULL COMMENT 'cache admin count',
		  `view` int(11) NOT NULL DEFAULT '1',
		  `theme` VARCHAR(255) NOT NULL DEFAULT '',
		  `subCatList` int(11) NOT NULL,
		  `disableLimit` int(11) NOT NULL,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `url` (`url`),
		  KEY `parentId` (`parentId`),
		  KEY `title` (`title`),
		  FULLTEXT KEY `ft_title` (`title`)
		) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;",1);
		db::query("INSERT INTO `{$tbl->category}` 
			(title,url,`view`) 
			VALUES ('Uncategorized','no-category',0)
		");
		db::query("CREATE TABLE IF NOT EXISTS `{$tbl->category2post}` (
		  `cid` varchar(125) NOT NULL COMMENT 'category ID',
		  `pid` varchar(125) NOT NULL COMMENT 'post ID',
		  `datePublish` datetime DEFAULT NULL,
		  `published` int(11) NOT NULL,
		  `uid` int(11) NOT NULL,
		  UNIQUE KEY `cid_pid` (`cid`,`pid`),
		  KEY `cid` (`cid`),
		  KEY `pid` (`pid`),
		  KEY `cid_datePublish` (`cid`,`datePublish`),
		  KEY `datePublish` (`datePublish`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8;",1);

		return array('instaled');
	}
}
