<?php
namespace category_admin;
use module,db,url;
/*
	1. преобразование title в URL
*/
function key2url($title){#получить урл по названию
	$url=preg_replace(array('!(\s+|\/)!','![^\w\d\-\_\.]!iu'),array('-',''),strtolower(trim($title)));
	return preg_replace("!-+!",'-',$url);
}
/*
	Получает уникальный URL для таблицы
*/
function getUrl($tbl,$title,$id=''){
	$turl=$url=key2url($title);
	do{
		list($tid)=db::qrow("SELECT id FROM `$tbl` WHERE url='$turl' && id!='$id' LIMIT 1");
		if($tid){
			@$i++;
			$turl="$url-$i";
		}
	}while($tid);
	return $turl;
}
/*
	обновление URL категории 
	- обновляет связи с постами
	- связи с pin поставми
	- связи с дочерними категорями
*/
function updateCategoryUrlRel($old,$url){
	$tbls=\posts\tables::init();
	db::query("UPDATE `{$tbls->category2post}` SET cid='{$url}' WHERE cid='{$old}'");
	db::query("UPDATE `{$tbls->post}` SET pincid='{$url}' WHERE pincid='{$old}'");
	db::query("UPDATE `{$tbls->category}` SET parentId='{$url}' WHERE parentId='{$old}'");
}
/*
	объединяет категории
	a->b
*/
function mergeCats($a,$b){
	$tbls=\posts\tables::init();
	updateCategoryUrlRel($a,$b);
	db::query("DELETE FROM `{$tbls->category}` WHERE `url`='{$a}' LIMIT 1");
	if(!db::error()){
		module::exec('category',array('act'=>'updateCount','cats'=>array($b),'tbl'=>$tbls->post),'data');
		return true;
	}else
		return false;
}
/*
 * Создаем новые теги из заголовков
*/ 
class t2k{
	function __construct(){
		static $w,$skip;
		if(empty($w))$w=array_flip(array_map('trim',file(__DIR__."/words.base")));
		if(empty($skip))$skip=array_flip(array("a","about","above","after","again","against","all","am","an","and","any","are","aren't","as","at","be","because","been","before","being","below","between","both","but","by","can't","cannot","could","couldn't","did","didn't","do","does","doesn't","doing","don't","down","during","each","few","for","from","further","had","hadn't","has","hasn't","have","haven't","having","he","he'd","he'll","he's","her","here","here's","hers","herself","him","himself","his","how","how's","i","i'd","i'll","i'm","i've","if","in","into","is","isn't","it","it's","its","itself","let's","me","more","most","mustn't","my","myself","no","nor","not","of","off","on","once","only","or","other","ought","our","ours","ourselves","out","over","own","same","shan't","she","she'd","she'll","she's","should","shouldn't","so","some","such","than","that","that's","the","their","theirs","them","themselves","then","there","there's","these","they","they'd","they'll","they're","they've","this","those","through","to","too","under","until","up","very","was","wasn't","we","we'd","we'll","we're","we've","were","weren't","what","what's","when","when's","where","where's","which","while","who","who's","whom","why","why's","with","won't","would","wouldn't","you","you'd","you'll","you're","you've","your","yours","yourself","yourselves"));
		$this->skip=$skip;
		$this->words=$w;
		$this->keys=array();
	}
	function getKeys($title,$url,$exclude=''){
		$e=preg_split("![ .,\-\(\)]!",strtolower($title));
		foreach($e as $v){
			if($v=='' or strlen($v)==1 or isset($this->skip[$v]) or $v==$exclude)continue;
			@$this->keys[$v][$url]=1;
			if(!empty($prev))@$this->keys["$prev $v"][$url]=1;
			$prev=$v;
		}
	}
	function filter(){
		$b=0;
		foreach($this->keys as $k=>$v){
			$v=count($v);
			if($v<5 or $b>10 or !isset($this->words[$k]))unset($this->keys[$k]);
			else{
				$b++;
			}
		}
		return $this->keys;
	}
}
