<?if(!$data->accessEdit) return;
if(empty($GLOBALS['category_panel'])){
	?>
	<style>
	.category_panel{
		background-color: white !important;
		display:inline-block;
	}
	.category_panel .icons{display: inline-block;width: 20px;height: 20px;background-repeat: no-repeat;background-image:url('<?=HREF?>/modules/posts/tpl/files/icons/iset.png');background-color: transparent;padding: 0;margin: 0;}
	.category_panel .icons-del{background-position: -18px -258px;}
	.category_panel .icons-edit{background-position: -340px -24px;}
	.category_panel .icons-add{background-position: -110px -258px;}
	</style>
<?}$GLOBALS['category_panel']=1;?>
<div class="category_panel">
	<?if($data->itemID){?>
		<?if($data->accessDel){?><a class="icons icons-del" onclick="return confirm('Are you sure?')" href="<?=url::category_del($data->itemID,$data->prfxtbl)?>" title="delete"></a><?}?>
		<a class="icons icons-edit" href="<?=url::category_edit($data->itemID,'',$data->prfxtbl)?>" title="edit"></a>
	<?}?>
	<a class="icons icons-add" href="<?=url::category_edit(0,$data->itemUrl,$data->prfxtbl)?>" title="new sub item"></a>
</div>