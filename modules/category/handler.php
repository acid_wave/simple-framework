<?php
namespace category;
use module,db,url,\user\user;

require_once(module::$path.'/category/func.php');
require_once(module::$path.'/posts/handler.php');
require_once(module::$path.'/user/handler.php');

class handler{
	function __construct(){
		$this->template='template';#Определяем в какой шаблон будем вписывать
		$this->headers=(object)array();
		$this->userControl=user::init();
	}
	function browse($epcat=''){
		#получаем 1-й уровень категорий
		db::query("SELECT * FROM `category` WHERE view=1 && parentId='".db::escape($epcat)."'");
		$cl2=$cl3=[];
		while ($d=db::fetch()) {
			if(!isVisible($d,$this->userControl->access->showAllCat)) continue;
			$cats[$d->url]=new \stdClass;
			$d->count=getSubCount($d,$this->userControl->access);
			$d->funcPanel=module::execStr('category/admin',['act'=>'funcPanel','itemID'=>$d->id,'itemUrl'=>$d->url]);
			$cats[$d->url]->name=$d;
			$cats[$d->url]->sub=&$cl2[$d->url];
		}
		if(empty($cats)) {$this->headers->location=HREF; return;}
		#получаем 2-й уровень категорий
		db::query("SELECT * FROM `category` WHERE parentId IN('".implode("','",array_keys($cl2))."')");
		while ($d=db::fetch()) {
			$d->funcPanel=module::execStr('category/admin',['act'=>'funcPanel','itemID'=>$d->id,'itemUrl'=>$d->url]);
			$cl2[$d->parentId][$d->url]=$d;
			$cl2[$d->parentId][$d->url]->sub=&$cl3[$d->url];
		}
		#получаем 3-й уровень категорий
		if(!empty($cl3)){
			db::query("SELECT * FROM `category` WHERE parentId IN('".implode("','",array_keys($cl3))."')");
			while ($d=db::fetch()) {
				$d->funcPanel=module::execStr('category/admin',['act'=>'funcPanel','itemID'=>$d->id,'itemUrl'=>$d->url]);
				$cl3[$d->parentId][$d->url]=$d;
			}
		}
		
		return ['cats'=>$cats];
	}
	/*
	* Вывод всех верхних категорий
	*/ 
	function mlist($category,$parentId){
		$tbls=\posts\tables::init();
		# получаем самые верхние категории
		$toplevel=array();
		$accessShowAll=$this->userControl->rbac('showAllCat');
		$res=db::query("SELECT * FROM `{$tbls->category}` WHERE `parentId`=''".(!$accessShowAll?" && `view`='1'":'')." ORDER BY `title` ASC");
		while ($d=db::fetch($res)) {
			$d->count=getSubCount($d,$this->userControl->access);
			if(!isVisible($d,$this->userControl->access->showAllCat)) continue; # выводим только категории в которых больше 2х постов.
			$d->title=ucfirst($d->title);
			if(!$accessShowAll&&!$d->count) continue;
			$d->funcPanel=module::exec('category/admin',array('act'=>'funcPanel','itemID'=>$d->id,'itemUrl'=>$d->url),1)->str;
			$toplevel[]=$d;
		}
		$samelevel=null;
		if($category && $parentId){
			$baseCat=new category($parentId,false);
			$samelevel=new \stdClass();
			$samelevel->baseCat=$baseCat;
			$samelevel->current=$category;
			$samelevel->cats=array();
			if(!empty($baseCat->id)){
				$res=db::query("SELECT * FROM `{$tbls->category}` WHERE `parentId`='".db::escape($parentId)."'".(!$accessShowAll?" && `view`='1'":'')." ORDER BY `title` ASC");
				while($d=db::fetch($res)){
					$d->count=getSubCount($d,$this->userControl->access);
					if(!isVisible($d,$this->userControl->access->showAllCat)) continue; # выводим только категории в которыз больше 2х постов.
					$d->title=ucfirst($d->title);
					if(!$accessShowAll&&!$d->count) continue;
					$d->funcPanel=module::exec('category/admin',array('act'=>'funcPanel','itemID'=>$d->id,'itemUrl'=>$d->url),1)->str;
					$samelevel->cats[]=$d;
				}
			}
		}
		return (object)array(
			'cats'=>$toplevel,
			'samelevel'=>$samelevel,
			'edit'=>$this->userControl->access->editCat,
			'prfxtbl'=>$tbls->prfx,
		);
	}
	/*
		1. Вывод текущей категории
		2. Вывод подкатегорий этой категории
	*/
	function subList($url){
		$tbls=\posts\tables::init();
		$accessEdit=$this->userControl->rbac('editCat');
		$accessShowAll=$this->userControl->rbac('showAllCat');
		# получаем текущую категорию
		$sub=array();
		$curItem = new category($url,$accessShowAll);
		$funcPanel='';
		if(!empty($curItem->id)){
			$curItem->title=ucfirst($curItem->title);
			# получаем подкатегории
			$funcPanel=module::exec('category/admin',array('act'=>'funcPanel','itemID'=>$curItem->id,'itemUrl'=>$curItem->url),1)->str;
			# получаем подкатегории
			$res=db::query("SELECT * FROM `{$tbls->category}` WHERE `parentID`='{$curItem->url}'".(!$accessShowAll?" && `view`='1'":'')." ORDER BY `title` ASC limit 30");
			while ($d=db::fetch($res)) {
				$d->count=getSubCount($d,$this->userControl->access);
				if(!isVisible($d,$this->userControl->access->showAllCat)) continue; # выводим только категории в которых больше 2х постов.
				$d->title=ucfirst($d->title);
				if(!$accessShowAll&&!$d->count) continue;
				$d->funcPanel=module::execStr('category/admin',['act'=>'funcPanel','itemID'=>$d->id,'itemUrl'=>$d->url]);
				$sub[]=$d;
			}
		}
		return (object)array(
			'cur'=>$curItem,
			'sub'=>$sub,
			'funcPanel'=>$funcPanel,
			'accessShowAll'=>$accessShowAll,
			'prfxtbl'=>$tbls->prfx,
		);
	}
	//Получить список категорий у которых parentId=$url 
	function ajaxCatsByParent($title,$url,$prfxtbl,$limit){
		$tbls=\posts\tables::init($prfxtbl);
		$cats=array();
		$sql=$url?"parentId='$url'":'';
		if(empty($url)){
			$urls=array();
			db::query("SELECT url FROM `$tbls->category` WHERE title='$title'");
			while($d=db::fetch()){
				$urls[]=$d->url;
			}
			$sql=count($urls)?"parentId in ('".implode("','",$urls)."')":'';
		}
		if($sql){
			db::query("SELECT * FROM `$tbls->category` WHERE $sql ORDER BY `count` DESC LIMIT $limit");
			while($d=db::fetch()){
				$cats[]=strtolower(trim($d->title));
			}
			sort($cats);
		}
		print json_encode($cats);
		exit;
	}
	function ajaxCategories($query,$prfxtbl){
		if (!$query) die;
		$tbls=\posts\tables::init($prfxtbl);
		$this->template='';
		$query = db::escape($query);
		db::query("SELECT url,title,parentId FROM `{$tbls->category}` 
			WHERE url LIKE '{$query}%' OR title LIKE '{$query}%' ORDER BY title LIMIT 10");
		$data = array();
		while($d=db::fetch()){
			$data[] = "$d->parentId=>$d->url";
		}
		return array(
			'categories'=> $data
		);
	}
	/*
	 * Получить список подсказок для категории
	*/ 
	function getSugests($cat,$limit,$approve){
		$this->template='';
		$suggests=array();
		$skip=array("a","about","above","after","again","against","all","am","an","and","any","are","aren't","as","at","be","because","been","before","being","below","between","both","but","by","can't","cannot","could","couldn't","did","didn't","do","does","doesn't","doing","don't","down","during","each","few","for","from","further","had","hadn't","has","hasn't","have","haven't","having","he","he'd","he'll","he's","her","here","here's","hers","herself","him","himself","his","how","how's","i","i'd","i'll","i'm","i've","if","in","into","is","isn't","it","it's","its","itself","let's","me","more","most","mustn't","my","myself","no","nor","not","of","off","on","once","only","or","other","ought","our","ours","ourselves","out","over","own","same","shan't","she","she'd","she'll","she's","should","shouldn't","so","some","such","than","that","that's","the","their","theirs","them","themselves","then","there","there's","these","they","they'd","they'll","they're","they've","this","those","through","to","too","under","until","up","very","was","wasn't","we","we'd","we'll","we're","we've","were","weren't","what","what's","when","when's","where","where's","which","while","who","who's","whom","why","why's","with","won't","would","wouldn't","you","you'd","you'll","you're","you've","your","yours","yourself","yourselves");
		if(!empty($cat)){
			$e=explode(" ",$cat);
			$sql=array();
			foreach($e as $v){
				$v=trim($v);
				if($v=='' or in_array($v,$skip))continue;
				$sql[]="`category`.title like '$v %' or 
						`category`.title like '% $v' or 
						`category`.title like '% $v %'
				";
			}
			if($approve==1){
				$msql="SELECT distinct category.title, category.* FROM `category` 
					LEFT JOIN `".PREFIX_SPEC."category2post` as c2p 
						ON c2p.cid=category.url
					LEFT JOIN `post`
						ON post.url=c2p.pid
					LEFT JOIN 
						`".PREFIX_SPEC."imgs` as imgs
						ON imgs.pid=post.id
					WHERE (".implode(" or ",$sql).")
					and approve=1
					order by `count` 
					LIMIT $limit
				";
			}else{
				$msql="SELECT distinct category.title, category.* FROM `category` 
					WHERE ".implode(" or ",$sql)." 
					order by `count` 
					LIMIT $limit
				";
			}
			db::query($msql);
			while($d=db::fetch()){
				$suggests[]=$d;
			}
		}
		return [
			'suggests'=>$suggests,
		];
	}
	function updateCount($cats,$prfxtbl){
		$tbls=\posts\tables::init($prfxtbl);
		if(is_array($cats)) $sqlin=" && cat.url IN('".implode("','", $cats)."')";
		elseif($cats=='all'){
			db::query("UPDATE `{$tbls->category}` SET `count`=0, `countAdmin`=0");
			$sqlin='';
			#добавляем посты в uncategorized, если они не были добавлены
			list($count)=db::qrow("
				SELECT COUNT(*) FROM `{$tbls->post}` post 
				LEFT OUTER JOIN `{$tbls->category2post}` rel ON rel.pid=post.url
				WHERE rel.cid IS NULL");
			if($count){
				db::query("
					INSERT INTO {$tbls->category2post} (cid,pid,datePublish,published,uid)
					VALUES(
						SELECT 'no-category',post.id,post.datePublish,post.published,post.user FROM `{$tbls->post}` post 
						LEFT OUTER JOIN `{$tbls->category2post}` rel ON rel.pid=post.url
						WHERE rel.cid IS NULL
					)");
			}
		}else return;

		#получачем количество опубликованных постов
		$sql="SELECT cat.url as cid,SUM(IF(post.id IS NULL,0,1)) AS `count` FROM `{$tbls->category}` cat
				LEFT JOIN `{$tbls->category2post}` rel ON rel.cid=cat.url
				LEFT JOIN `{$tbls->post}` post ON rel.pid=post.url %s
			WHERE 1 {$sqlin} GROUP BY cat.url";
		$res=db::query(sprintf($sql,"&& post.published=1"));
		while ($d=db::fetch($res)) {
			# Обновляем количество постов count в БД.
			db::query("UPDATE `{$tbls->category}` SET `count`='{$d->count}' WHERE `url`='{$d->cid}' LIMIT 1");
		}
		#получачем количество всех постов
		$res=db::query(sprintf($sql,''));
		while ($d=db::fetch($res)) {
			# Обновляем количество постов count в БД.
			db::query("UPDATE `{$tbls->category}` SET `countAdmin`='{$d->count}' WHERE `url`='{$d->cid}' LIMIT 1");
		}
	
		$this->template='';
		return;
	}
}
