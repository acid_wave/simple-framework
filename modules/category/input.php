<?php
/*
* Должен возвращать
* $this->data - объект обработанных входных переменных
* $this->act - какую функцию обработки используем
*/
class category extends control{
	function __construct($input=''){ # $input - объект входных переменных от других модулей
		$this->data=(object)array();
		if(@$input->act=='')$input->act='index';
		$this->act=$input->act;
		
		if($input->act=='getSugests'){
			$this->data=(object)array(
				'cat'=>@$input->cat,
				'limit'=>empty($input->limit)?15:$input->limit,
				'approve'=>@$input->approve,
			);
		}elseif($input->act=='browse'){
			$this->data=['epcat'=>!empty($input->cat)?$input->cat:''];
		}elseif($input->act=='mlist'){
			$this->data=(object)array(
				'category'=>empty($input->category)?null:$input->category,
				'parentId'=>empty($input->parentId)?null:$input->parentId,
			);
		}elseif($input->act=='subList'){
			$this->data=(object)array(
				'url'=>empty($input->url)?'':$input->url,
			);
		}elseif($input->act=='ajaxCatsByParent'){
			$this->data=(object)[
				'title'=>db::escape(@$input->title),
				'url'=>db::escape(@$input->url),
				'prfxtbl'=>!empty($_GET['prfxtbl'])?$_GET['prfxtbl']:'',
				'limit'=>25,
			];
		}elseif($input->act=='ajaxCategories'){
			$this->data=(object)array(
				'query'=>!empty($_GET['query'])?$_GET['query']:false,
				'prfxtbl'=>!empty($_GET['prfxtbl'])?$_GET['prfxtbl']:''
			);
		}elseif($input->act=='updateCount'&&($input->easy||$input->act==$_POST['act'])){
			$this->data=(object)array(
				'cats'=>empty($input->cats)?false:$input->cats,
				'prfxtbl'=>empty($input->prfxtbl)?'':$input->prfxtbl,
			);
		}
	}
}
