<?
namespace category;
use module,db,url;

/*
	принимает: массив ID категорий
	возвращает: массив категорий с данными
*/
function getCategoryData($cids,$view=1){
	if(!is_array($cids)) return;
	$tbls=\posts\tables::init();
	$cats=array();
	if($cids[0]=='no-category'){
		$cats[]=(object)array('url'=>'no-category','title'=>'Uncategorized');
	}else{
		db::query("SELECT * FROM `{$tbls->category}` WHERE `url` IN('".implode("','", $cids)."')".(!$view?" && `view`='1' && `count` > 2 ":''));
		while($d=db::fetch()){
			$cats[]=$d;
		}
	}
	return $cats;
}
/*
	Получает количество постов в категории
*/
function getSubCount(&$category,$access){
	$count=($access->showAllCat)?$category->countAdmin:$category->count;
	return (int)$count;
}
/*
 * Получить весь список урлов дочерних категорий для категории с урлом = $cat
*/ 
function getLeafCats($cat,&$leafs=array()){
	$q=db::query("SELECT url FROM category WHERE parentId='$cat'");
	while($leaf=@db::fetch($q)->url){
		$leafs[]=$leaf;
		getLeafCats($leaf,$leafs);
	}
	return $leafs;
}
class category{
	function __construct($url,$view=1,$prfxtbl=null){
		if(($empty=empty($url))||$url=='no-category'){
			$this->title=$empty?'':'Uncategorized';
			$this->url=$empty?'':'no-category';
			$this->parentId='';
			$this->count=0;
			$this->id=0;
		}else{
			$tbls=\posts\tables::init($prfxtbl);
			$q="SELECT * FROM `{$tbls->category}` WHERE `url`='{$url}'".(!$view?" && `view`='1'":'')." LIMIT 1";
			$obj=db::qfetch($q);
			if($obj)foreach($obj as $k=>$v){
				$this->{$k}=$v;
			};
		}
	}
}
/*
 * Получаем все категории по их parentId
*/ 
function getCatsByParent($parent=''){
	db::query("SELECT * FROM `category` WHERE `parentId`='$parent' and `view`=1");
	while($d=db::fetch()){
		$cats[]=$d;
	}
	usort($cats,'\category\sortByTitle');
	return $cats;
}
function sortByTitle($a,$b){
	return strcmp($a->title,$b->title);
}
/*
	Определяет показывать или не показывать категорию в зависимоти от:
	- количества постов
	- настройки категории
	- прав для текущей роли пользователя
*/
function isVisible($cat,$access){
	return !(($cat->count<3&&!$cat->disableLimit)&&(!$access));
}