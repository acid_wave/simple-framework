<?php
$tpl->title='Browse all categories on '.NAME;
?>
<div class="row">
<?
$j=-1;
foreach ($data->cats as $c) {?>
	<?=!(++$j%4)&&$j?'</div><div class="row">':''?>
	<div class="col-md-3 col-sm-6">
		<div class="browse-cat-container">
			<?=$c->name->funcPanel?>
			<a href="<?=!empty($c->sub)?url::categoryBrowse($c->name->url):url::category($c->name->url)?>">
				<i class="fa fa-book fa-lg" aria-hidden="true"></i><h4><?=$c->name->title?></h4>
				<small style="float: right;"><?=$c->name->count?> Results</small>
			</a>
			<?if(!empty($c->sub)){?>
			<div class="row">
				<div class="col-md-6">
				<?
				$halfc=ceil(count($c->sub)/2);
				$i=0;
				foreach ($c->sub as $k=>$cs) {
					if(++$i>$halfc) break;
					unset($c->sub[$k]);?>
					<div>
						<a href="<?=!empty($cs->sub)?url::categoryBrowse($cs->url):url::category($cs->url)?>"><?=$cs->title?></a>
						<?=$cs->funcPanel?>
					</div>
				<?}?>
				</div>
				<div class="col-md-6">
				<?
				foreach ($c->sub as $cs) {?>
					<div>
						<a href="<?=!empty($cs->sub)?url::categoryBrowse($cs->url):url::category($cs->url)?>"><?=$cs->title?></a>
						<?=$cs->funcPanel?>
					</div>
				<?}?>
				</div>
			</div>
			<?}?>
		</div>
	</div>
<?}?>
</div>