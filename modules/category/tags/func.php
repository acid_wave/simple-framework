<?
namespace category\tags;
use module,db,url,posts\tables;

include_once module::$path.'/posts/func.php';
include_once module::$path.'/category/admin/func.php';

/**
* Методы для управления тэгами
*/
class tags{

	/**
	 * Получает тэги
	 * @param array $tags 
	 * @return array
	 */
	static function get(array $tags){
		$tbls=tables::init();
		$T=[];
		db::query("SELECT * FROM {$tbls->category} WHERE `title` IN('".implode("','",$tags)."') && parentID='tags'");
		while ($d=db::fetch()) {
			$T[$d->title]=$d;
		}
		return $T;
	}
	/**
	 * Записывает новые тэги в БД
	 * @param array $tags 
	 * @return void
	 */
	static function insert(array $tags){
		$tbls=tables::init();
		$sql="INSERT INTO `{$tbls->category}` (title, url, parentId) VALUES ";
		$dumpUrls=[];
		$sqlVals=[];
		#если массив ассоциативный, то достаем значения изщ ключей
		$isassoc=(array_keys($tags)!==range(0, count($tags)-1));
		foreach ($tags as $key => $val) {
			$title=$isassoc?db::escape($key):$val;
			$url=\category_admin\getUrl($tbls->category,$title);
			$dumpUrls[]=$url;
			$sqlVals[]="('{$title}', '{$url}', 'tags')";
			if(count($sqlVals)>=50){
				db::query($sql.implode(',',$sqlVals),1);
				$sqlVals=[];
			}
		}
		if(count($sqlVals)) db::query($sql.implode(',',$sqlVals),1);
		return $dumpUrls;
	}
}
