<?php
class admin_themes extends control {
	function __construct($input = '') { # $input - объект входных переменных от других модулей
		$this->data = new stdClass;
		if (empty($input->act))	$input->act = 'index';
		/*
		  определяет список доступных действий (act)
		  если запрошенного act нет в спсике, то будет вызван метод совпадающий с названием данного класса
		 */
		switch ($input->act) {
			case 'index':
				$this->data = (object) array(
					'test' => '',
				);
				break;
			case 'set':
				$this->data = (object) array(
					'settheme' => isset($_POST['settheme']) ? $_POST['settheme'] : false,
				);
				break;
			case 'usePreTheme':
				$this->data = (object) array(
					'prethemes' => isset($_POST['prethemes']) ? $_POST['prethemes'] : false,
				);
				break;
			case 'edit':
				$this->data = (object) array(
					'theme' => empty($input->theme) ? false : $input->theme,
					'ext' => empty($_COOKIE['edit_ext']) ? false : (bool) $_COOKIE['edit_ext'],
					'ext_history' => empty($_COOKIE['ext_history']) ? false : (bool) $_COOKIE['ext_history'],
					'clone' => empty($input->clone) ? false : $input->clone,
				);
				break;
			case 'del':
				$this->data = (object) array(
					'theme' => empty($input->theme) ? false : $input->theme,
				);
				break;
			case 'openFile':
				$this->data = (object) array(
					'theme' => empty($_POST['theme']) ? false : $_POST['theme'],
					'path' => empty($_POST['path']) ? false : $_POST['path'],
				);
				break;
			case 'openFileCss':
				$this->data = (object) array(
					'theme' => empty($_POST['theme']) ? false : $_POST['theme'],
					'path' => empty($_POST['path']) ? false : $_POST['path'],
				);
				break;
			case 'openImage';
				$this->data = (object) array(
					'theme' => empty($_POST['theme']) ? false : $_POST['theme'],
					'path' => empty($_POST['path']) ? false : $_POST['path'],
				);
				break;
			case 'saveFile':
				$this->data = (object) array(
					'theme' => empty($_POST['theme']) ? false : $_POST['theme'],
					'path' => empty($_POST['path']) ? false : $_POST['path'],
					'text' => !isset($_POST['text']) ? false : $_POST['text'],
					'css' => !isset($_POST['css']) ? false : $_POST['css'],
				);
				break;
			case 'genBaseTheme':
				$this->data = (object) array(
					'postCall' => empty($_POST['postCall']) ? false : $_POST['postCall'],
					'internalCall' => empty($input->easy) ? 0 : $input->easy,
					'themeName' => empty($input->themeName) ? 'base_theme' : $input->themeName,
				);
				break;
			case 'cloneTheme':
				$this->data = (object) array(
					'postCall' => empty($_POST['postCall']) ? false : $_POST['postCall'],
					'internalCall' => empty($input->easy) ? 0 : $input->easy,
				);
				break;
			case 'newTheme':
				$this->data = (object) array(
					'name' => empty($_POST['name']) ? '' : $_POST['name'],
				);
				break;
			case 'cloneTpl':
				$this->data = (object) array(
					'theme' => empty($_POST['theme']) ? false : $_POST['theme'],
					'path' => empty($_POST['path']) ? false : $_POST['path'],
					'clone' => empty($_POST['clone']) ? false : $_POST['clone'],
					'ext' => empty($_COOKIE['edit_ext']) ? false : (bool) $_COOKIE['edit_ext'],
				);
				break;
			case 'createTpl':
				$this->data = (object) array(
					'theme' => empty($_POST['theme']) ? false : $_POST['theme'],
					'path' => empty($_POST['path']) ? false : $_POST['path'],
				);
				break;
			case 'delTpl':
				$this->data = (object) array(
					'theme' => empty($_POST['theme']) ? false : $_POST['theme'],
					'path' => empty($_POST['path']) ? false : $_POST['path'],
				);
				break;
			case 'uploadFile':
				$this->data = (object) array(
					'file' => empty($_FILES['file']) ? false : $_FILES['file'],
					'theme' => empty($_POST['theme']) ? false : $_POST['theme'],
					'path' => empty($_POST['path']) ? false : $_POST['path'],
				);
				break;
			case 'createDir':
				$this->data = (object) array(
					'theme' => empty($_POST['theme']) ? false : $_POST['theme'],
					'path' => empty($_POST['path']) ? false : $_POST['path'],
				);
				break;
			case 'exportTpl':
				$this->data = (object) array(
					'theme' => empty($input->theme) ? false : $input->theme,
				);
				break;
			case 'importTpl':
				$this->data = (object) array(
					'file' => empty($_FILES['importTheme']) ? false : $_FILES['importTheme'],
				);
				break;
			default:
				$this->act = false;
				break;
		}
	}

}
