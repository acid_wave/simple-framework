<script type="text/javascript">
	$(document).ready(function () {
		$('#newtheme').click(newTheme);
	});
	function newTheme() {
		var name = prompt("Theme name", "lucky star");
		if (name == null)
			return;
		var form = $(this).parent('form');
		form.children('input[name=name]').val(name);
		form.submit();
	}
	function reqGenTheme(el) {
		$(el).val('...');
		$.post(window.location.basepath + '?module=admin/themes&act=genBaseTheme',
			{postCall: 1},
			function (data) {
				$(el).val('done');
				document.location.href = document.location.href;
			}
		);
	}
	function cloneTheme(el) {
		$(el).val('...');
		$.post(window.location.basepath + '?module=admin/themes&act=cloneTheme',
			{postCall: 1},
			function (data) {
				$(el).val('done');
				document.location.href = document.location.href;
			}
		);
	}
</script>
<div class="row">
	<div class="col-md-4 mb-2">
		<form action="<?=url::admin_themesNew()?>" method="post">
			<button id="newtheme" class="btn btn-outline-secondary btn-block" type="submit">New empty theme</button>
			<input type="hidden" value="" name="name"/>
		</form>
	</div>
	<div class="col-md-4 mb-2">
		<button class="btn btn-outline-secondary btn-block" onclick="if (confirm('Unsaved data will be lost, continue?')) reqGenTheme(this);">Update base theme</button>
	</div>
	<div class="col-md-4 mb-2">
		<button class="btn btn-outline-secondary btn-block" onclick="if (confirm('Clone active theme, continue?')) cloneTheme(this);">Clone active theme</button>
	</div>
</div>
<div class="row">
	<div class="col-12">
		<h3>Install preset theme</h3>
	</div>
	<div class="col-12">
		<form method="post" action="<?=url::admin_themes()?>">
			<?if (!empty($data->prethemes)) {?>
			<div class="row no-gutters">
				<div class="col">
			<select class="custom-select" name="prethemes">
				<?foreach ($data->prethemes as $v) {?>
				<option value="<?=$v?>"><?=$v?></option>
				<? }?>
			</select>
				</div>
				<div class="col-3">
			<input type="hidden" name="act" value="usePreTheme"/>
			<button class="btn btn-outline-secondary float-right" type="submit" id="use">Create theme</button>
				</div>
			</div>
			<? } else {?>none :(<? }?>
		</form>
	</div>
</div>

