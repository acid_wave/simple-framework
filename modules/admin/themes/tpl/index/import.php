<div class="themes-import">
	<h3>Import theme from file</h3>
	<form name="themeimport" enctype="multipart/form-data" method="post" action="<?=url::admin_themes()?>">
		<div id="themeimport-log"></div>
		<div class="row no-gutters">
			<div class="col">
				<div class="custom-file">
					<input type="file" class="custom-file-input" name="importTheme">
					<label class="custom-file-label" for="customFile">Choose file</label>
				</div>
			</div>
			<div class="col-3">
				<button class="btn btn-outline-secondary float-right" type="submit">Import theme</button>
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
	(function(){
		var form=document.forms.themeimport;
		var log=document.getElementById('themeimport-log');
		form.onsubmit=function(){
			var formData=new FormData(form);
			formData.append('act','importTpl');

			var xhr = new XMLHttpRequest();
			xhr.onload = xhr.onerror = function() {
				if(this.status==200){
					if(xhr.responseText=='done'){
						log.innerHTML='<span class="success">success</span>';
						setTimeout(function(){
							document.location.reload();
						},300);
					}else
						log.innerHTML=log.innerHTML='<span class="success">'+xhr.responseText+'</span>';
				}else
					log.innerHTML="error "+this.status;
			};
			xhr.upload.onprogress = function(event) {
				log.innerHTML=event.loaded+' / '+event.total;
			}
			xhr.open("POST", this.action, true);
			xhr.send(formData);
			return false;
		}
	})();
</script>
