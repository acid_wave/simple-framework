<div id="EventNew" class="modal" tabindex="-1" role="dialog" aria-labelledby="SmallModal" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" style="float:left;">New file / directory</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="dialog-file" title="Add file">
				<div>
					<input type="text" placeholder="name file...">
					<input type="button" value="new file" class="newfile-tpl">
				</div>
					<b>or</b>
				</div>
				<div id="dialog-fileupload">
					<h5>Upload file <small>max: <?=ini_get('upload_max_filesize');?></small></h5>
					<input type="file"/>
				</div>
				<br><hr><hr><br>
				<div id="dialog-dir" title="Add directory">
					<div>
						<input type="text" placeholder="name directory...">
						<input type="button" value="new dir" class="newdir-tpl">
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>