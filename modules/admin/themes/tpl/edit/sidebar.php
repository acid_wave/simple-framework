<div id="themefiles">
	<h3 style="float:left;margin-right:5px;">Theme</h3>
	<form method="GET" action="<?=HREF?>">
		<select name="theme" onchange="if(this.value) this.form.submit();">
			<option value="<?=$data->theme?>" selected=""><?=$data->theme?></option>
			<?if(!empty($data->themesList)){?>
				<?foreach ($data->themesList as $val) {?>
					<option value="<?=$val?>"><?=$val?></option>
				<?}?>
			<?}?>
		</select>
		<input type="hidden" name="module" value="admin/themes"/>
		<input type="hidden" name="act" value="edit"/>
	</form>
	<ul>
        <li>
        	<span>./</span>
        	<a style="cursor:pointer;" link=""></a><small class="new-file-icon new-file-dir" title="new file"></small>
			<? listRec($data->dir, @$data->edit_ShowHistory)?>
		</li>
	</ul>
</div>

<?function listRec($list, $vie){?>
	<ul>
		<?
		ksort($list);
		foreach ($list as $key => $v) {
			if(is_array($v)){?>
				<li<?=!count($v)?' style="color:#CBCBCB;"':''?>>
					<span><?=htmlspecialchars($key,ENT_QUOTES);?></span>
					<small class="del-icon del-tpl" title="delete"></small>
					<small class="new-file-icon new-file-dir" title="new file"></small>
					<?listRec($v, $vie)?>
				</li>
			<?}
		}
		ksort($list);
		foreach ($list as $key => $v) {
			if(!is_array($v)){?>
				<li><a style="cursor:pointer;"><?=$key?></a>
				<small class="del-icon del-tpl" title="delete"></small></li>
				<?
			};
		}?>
	</ul>
<?}?>
