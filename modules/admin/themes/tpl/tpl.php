<?php
$tpl->title = NAME;
$tpl->desc = "";
?>
<script type="text/javascript">
	$(document).ready(function () {
		//save form preprocessing
		$('#save').click(function () {
			var jqform = $(this).parent('form');
			var val = $('#themes input:radio:checked').val();
			jqform.children('input[name="settheme"]').val(val);
			jqform.submit();
		});
		//use prethemes form preprocessing
		$('#use').click(function () {
			var jqform = $(this).parent('form');
			var chosen = jqform.children('select').children(':selected').val();
			$('#themes input:radio').each(function (indx, el) {
				if ($(el).val() == chosen) {
					if (!confirm('Theme with same name exists. Set and rename as "' + chosen + '_<?=date('Y-m-d')?>"?'))
						return;
				}
			});
			jqform.submit();
		});
	});
</script>
<div class="container">
	<div class="row">
		<?if (isset($data->error)) {?>
			<div class="col-12 alert alert-danger">
				<?=$data->error?>	
			</div>
		<? }?>
		<div class="col-md-6">
			<h3>Available themes</h3>
			<table class="table table-striped table-sm" id="themes">
				<tr>
					<td><?=$data->current?><?=!$data->found ? '<small style="color:red"> not found</small>' : ''?></td>
					<?if ($data->current != 'default') {?>
						<td style="width:20px">
							<a class="text-dark fa fa-edit" href="<?=url::admin_themesEdit($data->current)?>"></a>					
						</td>
						<td style="width:20px"></td>
						<td style="width:20px">
							<a class="text-dark fa fa-save" href="<?=url::admin_themesExport($data->current)?>" title="export"></a>
						</td>
					<? } else {?>
						<td style="width:20px"></td>
						<td style="width:20px"></td>
						<td style="width:20px"></td>
					<? }?>
					<td style="width:20px">
						<?if ($data->whereset != 'conf') {?>
							<input type="radio" value="<?=($data->current == 'default') ? '' : $data->current?>" name="t" checked=""/>
						<? } else {?>
							<small>checkout config.php</small>
						<? }?>
					</td>
				</tr>
				<?foreach ($data->themes as $v) {?>
					<tr>
						<td><?=$v?></td>
						<?if ($v == 'default') {?>
							<td style="width:20px"></td>
							<td style="width:20px"></td>
							<td style="width:20px"></td>
						<? } else {?>
							<td style="width:20px">				
								<a class="text-dark fa fa-edit" href="<?=url::admin_themesEdit($v)?>"></a>
							</td>
							<td style="width:20px">
								<a class="text-danger fa fa-close" href="<?=url::admin_themesDel($v)?>" onclick="return confirm('Are you sure?');"></a>
							</td>
							<td style="width:20px">
								<a class="text-dark fa fa-save" href="<?=url::admin_themesExport($v)?>"title="export"></a>
							</td>
						<? }?>
						<td style="width:20px">
							<?if ($data->whereset != 'conf') {?>
								<input type="radio" value="<?=$v == 'default' ? '' : $v?>" name="t"/>
							<? }?>
						</td>
					</tr>
				<? }?>
			</table>
			<?if ($data->whereset != 'conf') {?>
				<form method="post" action="<?=url::admin_themes()?>">
					<input type="hidden" name="act" value="set"/>
					<input type="hidden" name="settheme" value=""/>
					<input class="btn btn-outline-secondary float-right" type="button" value="use theme" id="save"/>
				</form>
			<? }?>
		</div>
		<div class="col-md-6">
			<h3>Create theme</h3>
			<?include $template->inc('index/newtheme.php');?>
			<?include $template->inc('index/import.php');?>
		</div>
	</div>
</div>