<?
/*
	using editor "Ace"
		- How to https://ace.c9.io/#nav=howto
		- js plugin dir /modules/admin/themes/tpl/files/ace

*/
$tpl->title="Edit theme: {$data->theme}";
?>
<script src="<?=HREF?>/modules/admin/themes/tpl/files/ace/min/ace.js"></script>
<script type="text/javascript">
	(function(){
		//init empty editor
		var editor={}, styleeditor={}, theme='';
		var editorundo={}, styleeditorundo={};
		
		//объект для управления отображением блоков редактора
		var controlEditor={
			styleEditorControlView:function (){
				var editor=$('#style-editor');
				if(getCookie('style-editor')=='true')
					editor.css({'display':'block'});
				else
					editor.css({'display':'none'});

				if($('a.style-editor-view').length > 0) return;

				editor.before('<a class="style-editor-view">styles <span></span></a>');
				var editorview=$('a.style-editor-view');
				editorview.css({'cursor':'pointer','text-decoration':'none'});
				var detectView=function(){
					var view=editor.css('display')!='none';
					editorview.children('span').html(view?'&#9650;':'&#9660;');
					return view;
				};
				detectView();
				
				editorview.click(function(){
					editor.slideToggle(200,function(){
						setCookie('style-editor',detectView(),'365');
						styleeditor.resize();
					});
				});
			},
			headline:function(str,type){
				var head=$('#edittheme-head');
				if(type=='after')
					head.after(str);
				else
					head.html(str);
			},
			detach:function(){
				var editor=$('#style-editor');
				editor.css({'display':'none'});
				var editorview=$('a.style-editor-view');
				editorview.detach();
			},
			hideall:function(){//крывает все блоки редактора
				$('#editor').css('display','none');
				$('#style-editor').css('display','none');
				$('#image-viewer').css('display','none');
				$('a.style-editor-view').detach();
			},
			showImage:function(src){//показывает картинку
				this.hideall();
				var img=$('#image-viewer');
				img.css('display','block');
				$('input[name=save]').css('display','none');
				img.html('<img style="max-width:100%;display:block;margin:auto;" src="'+src+'"/>');
			},
			showEditor:function(){
				$('input[name=save]').css('display','block');
				$('#editor').css('display','block');
				this.styleEditorControlView();
				$('#image-viewer').html('');
				$('#image-viewer').css('display','none');
			}
		};

		$(document).ready(function(){
			//set current theme
			theme=switchTheme();
			//устанавливаем события
			//события дерева файлов
			setEvents();
			setEventsClone();
			//кнопки save
			$('.content input[name="save"]').click(save);
			var isCtrl = false;
			document.onkeyup=function(e){
				if(e.keyCode == 17) isCtrl=false;
			}
			document.onkeydown=function(e){
				if(e.keyCode == 17) isCtrl=true;
				if(e.keyCode == 83 && isCtrl == true) {
					save();
					return false;
				}
			}
			//start editor
			editor=ace.edit("editor");
			editor.setTheme("ace/theme/chrome");
			editor.getSession().setMode("ace/mode/php");
			editorundo = editor.getSession().getUndoManager();
			//start style-editor
			styleeditor=ace.edit("style-editor");
			styleeditor.setTheme("ace/theme/chrome");
			styleeditor.getSession().setMode("ace/mode/css");
			styleeditorundo = styleeditor.getSession().getUndoManager();
			
			//is not save
			window.onbeforeunload = function() {
				if(editorundo.dirtyCounter > 1 || styleeditorundo.dirtyCounter > 1){
					return "The changes are not saved. Do you want to continue?";
				}
			};
			
			//extedit button
			$('#extedit input').change(handlerExt);
			$('#vieedit input').change(handlerShowHistory);
		});

		var eventClone=function(){
			theme=switchTheme();
			var path=buildPath($(this));
			var li=$(this).parent('li');
			if(path) path.reverse();
			else path=new Array();
			if(li.children('i').html()){
				path.push(li.children('i').html());	
			}else if(li.children('span').html()){
				path.push(li.children('span').html());	
			}
			cloneTpl(path);
		};
		var eventOpenToEdit=function(){
			console.log('eventOpenToEdit');
			theme=switchTheme();
			var path=buildPath($(this));
			path.reverse();
			path.push($(this).html());
			clearMessage();
			openToEdit(path);
		}
		var eventOpenToEditClone=function(){
			theme=switchTheme('clone');
			var path=buildPath($(this));
			path.reverse();
			path.push($(this).html());
			clearMessage();
			openToEdit(path);
		}
		var eventDetachFromTree=function(){
			var path=buildPath($(this));
			var li=$(this).parent('li');
			if(path) path.reverse();
			else path=new Array();
			if(li.children('a').html()){
				path.push(li.children('a').html()); 
			}else if(li.children('span').html()){
				path.push(li.children('span').html());  
			}
			detachTpl(path,this);
		};
		/*	Ф-ция для запуска диалога "создание файла/папки", которая определяет путь для создания	*/
		/*	На выходе: запуск ф-ций eventNewFile или eventNewDir	*/
		var eventNew=function(){
			console.log('eventNew');
			element=this;
			$('#EventNew').modal('show');
			var path=buildPath($(this));
			var li=$(this).parent('li');
			if(path) path.reverse();
			else path=new Array();
			if(li.children('a').html()){
				path.push(li.children('a').html()); 
			}else if(li.children('span').html()){
				path.push(li.children('span').html());  
			}
			//new file
			var buttonNew=$('#EventNew #dialog-file input:button');
			buttonNew.unbind('click');
			buttonNew.click(function(){
				eventNewFile(path,element);
			});
			//new dir
			var buttonNewDir=$('#EventNew #dialog-dir input:button');
			buttonNewDir.unbind('click');
			buttonNewDir.click(function(){
				eventNewDir(path,element);
			});
			//browse file
			var buttonBrowseFile=$('#EventNew #dialog-fileupload input:file');
			buttonBrowseFile.unbind('change');
			buttonBrowseFile.change(function(){
				console.log('eventNew buttonBrowseFile');
				eventNewFileUpload(path,this.files[0],element);
			});
		};
		/*	Ф-ция для обработки части(папка) диалога "создание файла/папки", которая читает имя новой папки и закрытия диалога	*/
		/*	На входе: path - путь к папке,в которой будет создаваться новая; element - элемент в структуре DOM, по которому был совершен click*/
		/*	На выходе: запуск ф-ции createDir	*/
		var eventNewDir=function(path,element){
			var dirList=element;
			//new directory
			var dirname=$('#dialog-dir input:text').val();
			if(dirname!=''){
				path.push(dirname);
				createDir(path,dirList);
			}else
				return;
			$('#EventNew').modal('hide');
		}
		/*	Ф-ция для обработки части(файл) диалога "создание файла/папки", которая читает имя нового файла и закрытия диалога	*/
		/*	На входе: path - путь к папке,в которой будет создаваться новый файл; element - элемент в структуре DOM, по которому был совершен click*/
		/*	На выходе: запуск ф-ции createTpl	*/
		var eventNewFile=function(path,element){
			var fileList=element;
			//new file
			var name=$('#dialog-file input:text').val();
			if(name!=''){
				path.push(name);
				createTpl(path,fileList);
			}else
				return;
			$('#EventNew').dialog('hide');
		};

		var eventNewFileUpload=function(path,file,element){
			uploadFileTpl(path,file,element);
			$('#EventNew').dialog('hide');
		}

		//functions
		function uploadFileTpl(path,file,el){
			var data=new FormData();
			data.append('act','uploadFile');
			data.append('file',file);
			if(path!=''){
				$(path).each(function(indx,itm){
					if(itm)
						data.append('path[]',itm);
				});
			}else
				data.append('path[]','');
			data.append('theme',$('input[name="theme"]').val());
			$.ajax({
				url: '?module=admin/themes',
				type: 'POST',
				data: data,
				cache: false,
				processData: false,
				contentType: false,
				success:function(respond){
					var treeTargetEl=$(el).parent('li').children('ul');
					treeTargetEl.append(respond);
					renewEvents();
				},
			});
		}
		function openToEdit(path){
			//проверяем наличие правок
			if(editorundo.dirtyCounter > 1 || styleeditorundo.dirtyCounter > 1){
				if(confirm("The changes are not saved . Do you want to continue?") !== true){
					return false;
				} else {
					editorundo.reset();
					styleeditorundo.reset();
				}
			}
			//сохраняем имя файла в форме
			$('input[name="path"]').val(path);
			//
			var filename=path[path.length-1];
			var regexp=new RegExp("^.+\.(png|jpg|gif|ico)$","i");

			var pathto='?module=admin/themes';
			if(regexp.exec(filename)){
				$.post(
					pathto,
					{act:'openImage',path:path,theme:theme},
					function (answer){
						controlEditor.headline('['+theme+'] '+path.join('/'));
						controlEditor.showImage(answer);
					}
				);
			}else{
				controlEditor.showEditor();
				$.post(
					pathto,
					{act:'openFile',path:path,theme:theme},
					function (answer){
						controlEditor.headline('['+theme+'] '+path.join('/'));
						editor.setValue(answer);
						editor.gotoLine(0);
						editorundo.reset();
					}
				);
				$.post(
					pathto,
					{act:'openFileCss',path:path,theme:theme},
					function (answer){
						if(answer=='nocss'){
							styleeditor.setValue('');
							controlEditor.detach();
						}else{
							styleeditor.setValue(answer);
							styleeditor.gotoLine(0);
							controlEditor.styleEditorControlView();
						}
						styleeditorundo.reset(); 
					}
				);	
			}
		}
		function buildPath(el){
			var p=[];
			var dir=el.parent('li').parent('ul').parent('li').children('span');
			if(dir.html()){
				var name=dir.html();
				p.push(name);
				path=p.concat(buildPath(dir));
				return path;
			}else
				return [];
		}
		function save(){
			var path=$('input[name="path"]').val();
			if(!theme||!path) return;
			if(!$.isArray(path)){
				path=path.split(',');
			}
			$.post(
				'?module=admin/themes',
				{act:'saveFile',path:path,theme:theme,text:editor.getValue(),css:styleeditor.getValue()},
				function (answer){
					clearMessage();
					if(answer=='done'){
						editorundo.reset();
						styleeditorundo.reset();
						controlEditor.headline('<span class="success">saved</span>','after');
					}else{
						controlEditor.headline('<span class="warning">'+answer+'</span>','after');
					}
					clearMessage(1);
				}
			);
		}
		function detachTpl(path,el){
			if(!confirm("Are you sure?")) return;
			$.post(
				'?module=admin/themes',
				{act:'delTpl',path:path,theme:$('input[name="theme"]').val()},
				function (answer){
					clearMessage();
					if(answer=='done'){
						controlEditor.headline('<span class="success">saved</span>','after');
						$(el).parent('li').remove();
					}else{
						controlEditor.headline('<span class="warning">'+answer+'</span>','after');
					}
				}
			);
		}
		//request to create file
		function createTpl(path,el){
			$.post(
				'?module=admin/themes',
				{act:'createTpl',path:path,theme:theme},
				function (answer){
					openToEdit(path,theme);
					var treeTargetEl=$(el).parent('li').children('ul');
					treeTargetEl.append(answer);
					renewEvents();
				}
			);
		}
		/*	Ф-ция post-запроса(ajax) на создание новой папки	*/
		/*	На входе: path - путь к папке,в которой будет создаваться новая; el - элемент в структуре DOM, по которому был совершен click*/
		/*	На выходе: добавление папки в сайдбаре слева	*/
		function createDir(path,el){
			$.post(
				'?module=admin/themes',
				{act:'createDir',path:path,theme:theme},
				function (answer){
					var treeTargetEl=$(el).parent('li').children('ul');
					if ($('body').is(treeTargetEl)==true){
						treeTargetEl=$(el).parent('li');
					}
					treeTargetEl.append(answer);
					renewEvents();
				}
			);
		}
		function clearMessage(delay){
			var suc=$('span.success');
			var war=$('span.warning');
			if(!delay){
				suc.detach();
				war.detach();
			}else{
				suc.delay(1200).fadeOut(500,function(){
					suc.detach();
				});
				war.delay(1200).fadeOut(500,function(){
					war.detach();
				});
			}
		}
		function cloneTpl(path){
			var cloneTheme=$('select[name=clone] option:checked').val();
			if(!cloneTheme) {
				alert('clone theme is empty'); return;
			}
			$.post(
				'?module=admin/themes',
				{act:'cloneTpl',path:path,theme:theme,clone:cloneTheme},
				function (answer){
					//reload sidebar
					$('#themefiles').html('...');
					$('#themefiles').html(answer);
					renewEvents();
				}
			);
		}
		//устанавливает необходимые события на блоки
		function setEvents(){
			$('#themefiles li span').click(function(){
				$(this).parent('li').children('ul').slideToggle(200);
			});
			$('#themefiles ul li a').click(eventOpenToEdit);
			$('#themefiles .del-tpl').click(eventDetachFromTree);
			/*	Используется jQuery ф-ция click() для открытия диалога "создание файла/папки" (ф-ция eventNew)	*/
			$('#themefiles .new-file-dir').click(eventNew);
		};
		// установленные события с блоков
		function renewEvents(){
			$('#themefiles li span').unbind("click");
			$('#themefiles ul li a').unbind("click");
			$('#themefiles .del-tpl').unbind("click");
			$('#themefiles .new-file-dir').unbind("click");
			setEvents();
		}
		//устанавливает необходимые события на блоки "клонировать тему"
		function setEventsClone(){
			$('#clonefiles ul li i').click(eventOpenToEditClone);
			$('.right-sidebar li span').click(function(){
				$(this).parent('li').children('ul').slideToggle(200);
			});
			$('.right-sidebar .clone-tpl').click(eventClone);
		}
		function switchTheme(t){
			var theme;
			if(t=='clone')
				theme=$('select[name=clone] option:checked').val();
			else
				theme=$('input[name="theme"]').val();
			return theme;
		}
		var handlerExt=function(){
			var chk=$(this).prop('checked');
			var val=0;
			if(chk) val=1; 
			setCookie('edit_ext',val,'365');
			document.location.href=document.location.href;
		};
		// Показывать файлы все (с историей) или без истории
		var handlerShowHistory=function(){
			var chk=$(this).prop('checked');
			var val=0;
			if(chk) val=1; 
			setCookie('ext_history',val,'365');
			document.location.href=document.location.href;
		};
	})();
</script>
<?include $template->inc('edit/dialog.php');?>
<div class="container-fluid">
	<div class="row edittheme">
		<div class="col-sm-2 sidebar">
			<?if($data->access->themesSetHandler){?>
				<label id="extedit">расширенный <input type="checkbox"<?=$data->ext?' checked=""':''?>/></label>  <br>
				<label id="vieedit">история <input type="checkbox"<?=$data->ext_history == 1?' checked=""':''?>/></label>
			<?}?>
			<?include $template->inc('edit/sidebar.php');?>
		</div>
		<div class="col-sm content">
			<div>
				<input type="button" value="save" name="save"/>
				<h4 id="edittheme-head"></h4>
				<div id="style-editor"></div><br/>
				<div id="editor"></div>
				<div id="image-viewer"></div>
				<input type="hidden" value="<?=$data->theme?>" name="theme"/>
				<input type="hidden" value="" name="path"/>
				<input type="button" value="save" name="save"/>
			</div>
		</div>
		<div class="col-sm-2 right-sidebar"><?include $template->inc('edit/right-sidebar.php');?></div>
	</div>
</div>
