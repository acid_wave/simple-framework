<?php
namespace admin_install;
use module, db, url;

require_once __DIR__ . '/func.php';

/*
 * Должен возвращать:
 * $this->data - объект переменных для вывода в шаблоне
 * $this->headers - объект для изменения заголовков при отдаче
 * $this->act - если есть несколько вариантов ответов
 */

class handler {

	function __construct() {
		$this->template = 'template'; #Определяем в какой шаблон будем вписывать
		$this->headers = (object) array();
		$this->uhandler = module::exec('user', array(), 1)->handler;
	}

	/*
	 * Выполняет функции install нужных модулей
	 */

	function index($installModules, $fullname, $email, $password) {
		$ready = $check = array();
		$message = array();
		$tables = db::qall("SHOW TABLES");
		if ($installModules) {
			if (!$tables) {
				foreach ($installModules as $name) {
					module::exec($name, array('act' => 'install'), 1);
					if ($name == 'user/admin') {
						db::query("INSERT IGNORE INTO `" . PREFIX_SPEC . "users` (`id`, `name`, `mail`, `pas`, `regdate`, `visit`, `code`, `hash`, `rbac`, `rating`) VALUES
								(1, '{$fullname}', '{$email}', '{$password}', '" . date("Y-m-d") . "', '" . date("Y-m-d") . "', '" . mt_rand() . "', '" . db::escape(hash('md5', $password)) . "', 1, 0)", 1);
					}
				}
				$message[] = 'Installed';
				#устанавливаем базовую тему
				$m = module::exec('admin/themes', array('act' => 'genBaseTheme'), 'data')->data->message;
				#записываем последнюю версию
				$updates = module::exec('admin/update', array('act' => 'index'), 'data')->data;
				$updatesLists = \admin_update\getUpdateList(0);
				db::query("INSERT INTO `" . PREFIX_SPEC . "config`(`key`, `value`) VALUES ('db_version','" . end($updatesLists) . "')");
				$message[] = $m == 'done' ? "Theme Installed" : "Theme - {$m}";
			} else {
				$message[] = 'Error: DB is not empty';
			}
		} else
		if ($tables)
			$this->headers->location = HREF;
		else {
			$ready = readyToInstall(module::$path);
			$check = checkModules();
		}

		return (object) array(
			'ready' => $ready,
			'check' => $check,
			'message' => $message,
			'status' => module::exec('admin/status', [], 1)->str,
		);
	}

}
