<?php
namespace admin_install;
use module,db,url;
/*
	получает список готовых к установке модулей
*/
function readyToInstall($basedir){
	static $ready;
	if(!isset($ready)) $ready=array();
	foreach (scandir($basedir) as $dirName) {
		if(substr($dirName,0,1)=='.'||in_array($dirName,array('files','tmp','tpl'))) continue;
		if(!is_dir($tdir="$basedir/$dirName")) continue;
		#собираем все admin/handler.php
		if($dirName=='admin'){
			if(file_exists("{$tdir}/handler.php")){
				$ready[]=str_replace(module::$path.'/','',$tdir);
				continue;
			}
		}
		readyToInstall($tdir,true);
	}
	return $ready;
}
/*
 * Check for required modules
 */
function checkModules() {
	$requered_php = ['mbstring','gd','xml','imagick'];
	$requered_package = ['jpegoptim','unzip'];
	$result = [];
	$apt = [];
	foreach ($requered_php as $value) {
		if (!extension_loaded($value)) {
			$result['php-'.$value] = false;
			$apt[] = 'php-'.$value;
		} else {
			$result['php-'.$value] = true;
		}
	}
	foreach ($requered_package as $value) {
		if (empty(shell_exec('which '.$value))){
			$result[$value] = false;
			$apt[] = $value;
		} else {
			$result[$value] = true;
		}
	}
	if ($result['php-imagick']){
		if (!in_array('SVG', \Imagick::queryformats())) {
			$result['libmagickcore-6.q16-3-extra']=false;
			$apt[] = 'libmagickcore-6.q16-3-extra';
		}
	}
	return (object)['apt'=>$apt, 'check'=>$result];
}
?>