<?php
$tpl->title = 'Install - ' . NAME;
$tpl->desc = "";
?>
<script type="text/javascript">
$(document).ready(function() {
	$("#user_admin").click(function(){
		if ($(this).is(':checked')) {
			$('[id^="input"]').each(function(){$(this).removeAttr('disabled');});
		}else{
			$('[id^="input"]').each(function(){$(this).attr('disabled', true);});
		}
	});
});
</script>
<form method="post" class="form" action="<?=url::install()?>">
	<div class="row justify-content-center mt-5">
		<?=$data->status?>
		<?if(!empty($data->message)){?>
		<div class="col-12">
			<?foreach ($data->message as $m) {?>
			<div class="alert alert-<?=substr_count($m, 'Installed') ? 'success' : 'danger'?>">
				<?=$m?>
			</div>
			<?}?>
		</div>
		<?}?>
		<?if(!empty($data->check)){?>
		<div class="col-12 col-md-10">
			<div class="row justify-content-center mb-2">
				<div class="col-12 col-md-5">
					<table class="table table-sm table-striped">
						<?foreach ($data->check->check as $key => $value) {?>
						<tr>
							<td><?=$key?></td>
							<td class="text-right <?=$value?'text-success':'text-danger'?>"><?=$value?'Installed':'Not installed'?></td>
						</tr>
						<?}?>
					</table>
				</div>
			</div>
			<?if(!empty($data->check->apt)){?>
			<div class="alert alert-danger">
				Please install required modules and program's.
				On Debian system you can execute<br><span class="font-weight-bold font-italic">apt-get install <?=implode(' ', $data->check->apt)?></span><br>with administrator's rights
			</div>
			<?}?>
		</div>
		<?}?>
		<?if(!empty($data->ready)){?>
		<div class="col-12 col-md-4">
			<table class="table table-striped table-sm">
				<?foreach ($data->ready as $v) {?>
				<tr>
					<td><?=str_replace('/admin', '', $v)?></td>
					<td><input id="<?=str_replace('/', '_', $v)?>" type="checkbox" name="installModules[]" value="<?=$v?>" checked <?=substr_count($v, 'admin/install') ? 'disabled' : ''?>/>
						<?=substr_count($v, 'admin/install') ? '<input type="hidden" name="installModules[]" value="' . $v . '">' : ''?></td>
				</tr>
				<?}?>
			</table>
		</div>
		<div class="col-12 col-md-6">
			<div class="form-group row">
				<label for="inputName" class="col-sm-2 col-form-label">Name</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="inputName" placeholder="Full Name" name="fullname" required>
				</div>
			</div>
			<div class="form-group row">
				<label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
				<div class="col-sm-10">
					<input type="email" class="form-control" id="inputEmail" placeholder="Email" name="email" required>
				</div>
			</div>
			<div class="form-group row">
				<label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
				<div class="col-sm-10">
					<input type="password" class="form-control" id="inputPassword" placeholder="Password" name="password" required>
				</div>
			</div>
		</div>
		<div class="col-12 text-center">
			<input class="btn btn-outline-secondary" type="submit" name="install" value="Install"/>
		</div>
		<?}?>
	</div>
</form>
