<?php
/*
 * Должен возвращать
 * $this->data - объект обработанных входных переменных
 * $this->act - какую функцию обработки используем
 */

class admin_install extends control{
	function __construct($input=''){ # $input - объект входных переменных от других модулей
		$this->data=(object)array();
		if(@$input->act=='') $input->act='index';

		#Передаваемые переменные для каждого действия
		if($input->act=='index'){
			$this->data=(object)array(
				'installModules'=>empty($_POST['installModules'])?false:$_POST['installModules'],
				'fullname'=>empty($_POST['fullname'])?false:$_POST['fullname'],
				'email'=>empty($_POST['email'])?false:$_POST['email'],
				'password'=>empty($_POST['password'])?false:$_POST['password'],
			);
		}else
			$input->act=false;
	}
}