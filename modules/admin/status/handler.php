<?php
namespace admin_status;
use module,db,url,cache;

class handler{
	function __construct(){
		$this->template='';
		$this->headers=new \stdClass;
	}
	function index(){
		$status=[
			'writable'=>[			
				'tmp'=>[
					'status'=>is_writable(TMP),
					'msg'=>TMP
				],
				'module/themes'=>[
					'status'=>is_writable($dir=PATH.'themes'),
					'msg'=>$dir
				],
				'module/images'=>[
					'status'=>is_writable($dir=PATH.'modules/images/files'),
					'msg'=>$dir
				],
				'module/user'=>[
					'status'=>is_writable($dir=PATH.'modules/user/files/avatar'),
					'msg'=>$dir
				],
			],
			'install'=>[
				'geoip'=>[
					'status'=>extension_loaded('geoip')||file_exists("/usr/local/share/GeoIP/GeoIP.dat"),
					'msg'=>"install geoip as root:<br>
							wget -N http://geolite.maxmind.com/download/geoip/database/GeoLiteCountry/GeoIP.dat.gz;
							gunzip GeoIP.dat.gz;
							mkdir /usr/local/share/GeoIP/;
							mv GeoIP.dat /usr/local/share/GeoIP/;"
				]
			]
		];
		return (object)array(
			'status'=>$status,
		);
	}
}
