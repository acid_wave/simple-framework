<?php
/*
	дополняет структуру таблицы post
*/
function update1(){
	list($check)=db::qrow("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='".DB_NAME."' && TABLE_NAME='post' && COLUMN_NAME='theme'");
	if(empty($check)){
		db::query("ALTER TABLE `post` ADD `theme` VARCHAR(255) NOT NULL DEFAULT '' AFTER `kid`",1);
	}
}
/*
	дополняет структуру таблицы category
*/
function update2(){
	list($check)=db::qrow("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='".DB_NAME."' && TABLE_NAME='category' && COLUMN_NAME='subCatList'");
	if(empty($check)){
		db::query("ALTER TABLE `category` ADD `subCatList` int(11) NOT NULL AFTER `view`",1);
	}
}
/*
	улучшение производиьельности запросов к таблице zspec_imgs
*/
function update3(){
	list($check)=db::qrow("SHOW INDEX FROM `".PREFIX_SPEC."imgs` WHERE key_name='tbl_priority'");
	if(empty($check))
		db::query("ALTER TABLE `".PREFIX_SPEC."imgs` ADD INDEX `tbl_priority` (`tbl`,`priority`)",1);
}
/*
	дополняет структуру таблицы category
*/
function update4(){
	list($check)=db::qrow("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='".DB_NAME."' && TABLE_NAME='category' && COLUMN_NAME='theme'");
	if(empty($check)){
		db::query("ALTER TABLE `category` ADD `theme` VARCHAR(255) NOT NULL DEFAULT '' AFTER `view`",1);
	}
}
/*
	дополняет структуру таблицы post
*/
function update5(){
	list($check)=db::qrow("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='".DB_NAME."' && TABLE_NAME='post' && COLUMN_NAME='data'");
	if(empty($check)){
		db::query("ALTER TABLE `post` ADD `data` TEXT NOT NULL AFTER `txt`",1);
	}
}
/*
	дополняет структуру таблицы keyword2post
*/
function update6(){
	list($check)=db::qrow("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='".DB_NAME."' && TABLE_NAME='".PREFIX_SPEC."keyword2post' && COLUMN_NAME='tbl'");
	if(empty($check)){
		db::query("ALTER TABLE `".PREFIX_SPEC."keyword2post` DROP INDEX kid2pid");
		db::query("ALTER TABLE `".PREFIX_SPEC."keyword2post` ADD `tbl` VARCHAR(255) NOT NULL AFTER `pid`",1);
		db::query("ALTER TABLE `".PREFIX_SPEC."keyword2post` ADD UNIQUE KEY `kid2pid` (`kid`,`pid`,`tbl`), ADD INDEX(`pid`, `tbl`)",1);
		db::query("UPDATE `".PREFIX_SPEC."keyword2post` SET `tbl`='post'");
	}
}
/*
	добавляем поле phone к пользователя
*/
function update7(){
	list($check)=db::qrow("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='".DB_NAME."' && TABLE_NAME='".PREFIX_SPEC."users' && COLUMN_NAME='phone'");
	if(empty($check)){
		db::query("ALTER TABLE `".PREFIX_SPEC."users` ADD `phone` int(11) NOT NULL AFTER `mail`",1);
	}
}
/*
	добавляем поле adress,comment к пользователям
*/
function update8(){
	list($check)=db::qrow("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='".DB_NAME."' && TABLE_NAME='".PREFIX_SPEC."users' && COLUMN_NAME='address'");
	if(empty($check)){
		db::query("ALTER TABLE `".PREFIX_SPEC."users` ADD `address` TEXT NOT NULL AFTER `phone`",1);
	}
	list($check)=db::qrow("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='".DB_NAME."' && TABLE_NAME='".PREFIX_SPEC."users' && COLUMN_NAME='comment'");
	if(empty($check)){
		db::query("ALTER TABLE `".PREFIX_SPEC."users` ADD `comment` TEXT NOT NULL AFTER `address`",1);
	}
}
/*
	добавляет поле к таблицам category
*/
function update9(){
	db::query("SHOW TABLES WHERE `tables_in_".DB_NAME."` REGEXP '^[^\_]+\_category$'");
	while ($d=db::fetchRow()) {
		$prfx=preg_replace('!\_category$!i', '', $d);
		$tbl="{$prfx}_category";
		list($check)=db::qrow("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='".DB_NAME."' && TABLE_NAME='{$tbl}' && COLUMN_NAME='disableLimit'");
		if(empty($check)){
			db::query("ALTER TABLE `{$tbl}` ADD `disableLimit` INT NOT NULL AFTER `subCatList`",1);
		}
	}
	list($check)=db::qrow("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='".DB_NAME."' && TABLE_NAME='category' && COLUMN_NAME='disableLimit'");
	if(empty($check)){
		db::query("ALTER TABLE `category` ADD `disableLimit` INT NOT NULL AFTER `subCatList`",1);
	}
}
/*
	добавляет доп. индекс для таблицы post
*/
function update10(){
	list($check)=db::qrow("SHOW INDEX FROM `post` WHERE key_name='pincid'",1);
	if(empty($check))
		db::query("ALTER TABLE `post` ADD INDEX(`pincid`)",1);
	$qid=db::query("SHOW TABLES LIKE '%\_post'",1);
	while ($d=db::fetchRow($qid)) {
		list($check)=db::qrow("SHOW INDEX FROM `{$d}` WHERE key_name='pincid'",1);
		if(empty($check))
			db::query("ALTER TABLE `{$d}` ADD INDEX(`pincid`)",1);
	}
}
/*
	изменяется тип данных для mysql таблиц `post`, `postHistory` и их производных с префиксами
*/
function update11(){
	list($check)=db::qrow("SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='".DB_NAME."' && TABLE_NAME='post' && COLUMN_NAME='txt'",1);
	if(strtolower($check)=='mediumtext') return;

	db::query("ALTER TABLE `post` CHANGE `txt` `txt` MEDIUMTEXT",1);
	db::query("ALTER TABLE `".PREFIX_SPEC."postHistory` CHANGE `txt` `txt` MEDIUMTEXT",1);

	$qid=db::query("SHOW TABLES LIKE '%\_post'",1);
	while ($d=db::fetchRow($qid)) {
		db::query("ALTER TABLE `{$d}` CHANGE `txt` `txt` MEDIUMTEXT",1);
		db::query("ALTER TABLE `".PREFIX_SPEC."{$d}History` CHANGE `txt` `txt` MEDIUMTEXT",1);
	}
}
/*
 * Создаем таблицу лайков пользователей
*/ 
function update12(){
	list($check)=db::qrow("SHOW TABLES WHERE `tables_in_".DB_NAME."` REGEXP '^[^\_]+\_like2img$'");
	if(empty($check)){
		db::query("CREATE TABLE IF NOT EXISTS `".PREFIX_SPEC."like2img` (
			`uid` int(11) NOT NULL,
			`imid` int(11) NOT NULL,
			UNIQUE KEY `uid` (`uid`,`imid`)
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;
		");
	}
}
/*
 * Добавляем таблицу ролей
*/ 
function update13(){
	list($check)=db::qrow("SHOW TABLES LIKE '".PREFIX_SPEC."roles%'");
	if(empty($check)){
		module::execData('user/admin',['act'=>'install']);
	}
}
/*
 * Поле date в таблицу zspec_like2img
*/ 
function update14(){
	list($check)=db::qrow("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='".DB_NAME."' && TABLE_NAME='".PREFIX_SPEC."like2img' && COLUMN_NAME='date'");
	if(empty($check)){
		db::query("ALTER TABLE `".PREFIX_SPEC."like2img` ADD `date` DATE NOT NULL AFTER `imid`, ADD INDEX (`date`) ",1);
	}
}
/*
	добавляет доп. индекс для таблицы zspec_imgs
*/
function update15(){
	list($check)=db::qrow("SHOW INDEX FROM `".PREFIX_SPEC."imgs` WHERE key_name='tbl_pid_priority'",1);
	if(empty($check))
		db::query("ALTER TABLE `".PREFIX_SPEC."imgs` ADD INDEX `tbl_pid_priority` (`tbl`, `pid`, `priority`)",1);
}
/*
	добавляет доп. индекс для таблицы zspec_imgs и post
*/
function update16(){
	#imgs
	list($check)=db::qrow("SHOW INDEX FROM `".PREFIX_SPEC."imgs` WHERE key_name='statViews'",1);
	if(empty($check)){
		db::query("ALTER TABLE `".PREFIX_SPEC."imgs` ADD INDEX `statViews` (`statViews`)",1);
		db::query("ALTER TABLE `".PREFIX_SPEC."imgs` ADD INDEX `statViewsShort` (`statViewsShort`)",1);
	}
	#post
	list($check)=db::qrow("SHOW INDEX FROM `post` WHERE key_name='statViews'",1);
	if(empty($check)){
		db::query("ALTER TABLE `post` ADD INDEX `statViews` (`statViews`)",1);
		db::query("ALTER TABLE `post` ADD INDEX `statViewsShort` (`statViewsShort`)",1);
	}
	$qid=db::query("SHOW TABLES LIKE '%\_post'",1);
	while ($d=db::fetchRow($qid)) {
		list($check)=db::qrow("SHOW INDEX FROM `{$d}` WHERE key_name='statViews'",1);
		if(empty($check)){
			db::query("ALTER TABLE `{$d}` ADD INDEX `statViews` (`statViews`)",1);
			db::query("ALTER TABLE `{$d}` ADD INDEX `statViewsShort` (`statViewsShort`)",1);
		}
	}
}
function update17(){
	list($check)=db::qrow("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='".DB_NAME."' && TABLE_NAME='".PREFIX_SPEC."imgs' && COLUMN_NAME='statDownload'");
	if(empty($check)){	
		db::query("ALTER TABLE `".PREFIX_SPEC."imgs` ADD `statDownload` INT NOT NULL AFTER `statShortFlag`;");
		db::query("ALTER TABLE `".PREFIX_SPEC."imgs` ADD `licence` varchar(255) NOT NULL AFTER `approve`;");
	}
}
function update18(){
	list($check)=db::qrow("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='".DB_NAME."' && TABLE_NAME='".PREFIX_SPEC."users' && COLUMN_NAME='link'");
	if(empty($check)){	
		db::query("ALTER TABLE `".PREFIX_SPEC."users` ADD `link` varchar(255) NOT NULL AFTER `phone`;");
	}
}
/*
	добавляет индекс по `user` для таблицы post
	добавляет индекс по `user`,`statViews` для таблицы post
*/
function update19(){
	list($check)=db::qrow("SHOW INDEX FROM `post` WHERE key_name='user'",1);
	if(empty($check)){
		db::query("ALTER TABLE `post` ADD INDEX `user` (`user`)",1);
	}
	$qid=db::query("SHOW TABLES LIKE '%\_post'",1);
	while ($d=db::fetchRow($qid)) {
		list($check)=db::qrow("SHOW INDEX FROM `{$d}` WHERE key_name='user'",1);
		if(empty($check)){
			db::query("ALTER TABLE `{$d}` ADD INDEX `user` (`user`)",1);
		}
	}
	list($check)=db::qrow("SHOW INDEX FROM `post` WHERE key_name='user_statViews'",1);
	if(empty($check)){
		db::query("ALTER TABLE `post` ADD INDEX `user_statViews` (`user`,`statViews`)",1);
	}
	$qid=db::query("SHOW TABLES LIKE '%\_post'",1);
	while ($d=db::fetchRow($qid)) {
		list($check)=db::qrow("SHOW INDEX FROM `{$d}` WHERE key_name='user_statViews'",1);
		if(empty($check)){
			db::query("ALTER TABLE `{$d}` ADD INDEX `user_statViews` (`user`,`statViews`)",1);
		}
	}
}
/*
	добавляет индекс по `uid` для таблицы zspec_imgs
	добавляет индекс по `uid`, `statDownload` для таблицы zspec_imgs
*/
function update20(){
	list($check)=db::qrow("SHOW INDEX FROM `".PREFIX_SPEC."imgs` WHERE key_name='uid'",1);
	if(empty($check)){
		db::query("ALTER TABLE `".PREFIX_SPEC."imgs` ADD INDEX `uid` (`uid`)",1);
	}
	list($check)=db::qrow("SHOW INDEX FROM `".PREFIX_SPEC."imgs` WHERE key_name='uid_statDownload'",1);
	if(empty($check)){
		db::query("ALTER TABLE `".PREFIX_SPEC."imgs` ADD INDEX `uid_statDownload` (`uid`, `statDownload`)",1);
	}
}
/*
	добавляет индекс по `title` для таблицы category
*/
function update21(){
	list($check)=db::qrow("SHOW INDEX FROM `category` WHERE key_name='title'",1);
	if(empty($check)){
		db::query("ALTER TABLE `category` ADD INDEX `title` (`title`)",1);
	}
	$qid=db::query("SHOW TABLES LIKE '%\_category'",1);
	while ($d=db::fetchRow($qid)) {
		list($check)=db::qrow("SHOW INDEX FROM `{$d}` WHERE key_name='title'",1);
		if(empty($check)){
			db::query("ALTER TABLE `{$d}` ADD INDEX `title` (`title`)",1);
		}
	}
}
/*
	изменяет тип данных published в post
	! оставляет старые талицы с префиксом "old" (удалить старые таблицы лучше вручную)
*/
function update22(){
	list($check)=db::qrow("SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='".DB_NAME."' && TABLE_NAME='post' && COLUMN_NAME='published'",1);
	if(strtolower($check)=='int') return;
	include_once module::$path.'/posts/handler.php';
	\posts\tables::$instance=null;
	$tbl=\posts\tables::init();
	db::query("RENAME TABLE {$tbl->post} TO old_{$tbl->post}",1);
	module::execData('posts/admin',['act'=>'install']);
	db::query("
		INSERT INTO {$tbl->post} (`id`, `date`, `datePublish`, `url`, `title`, `txt`, `data`, `sources`, `user`, `published`, `site`, `statViews`, `statViewsShort`, `statShortFlag`, `countPhoto`, `FBpublished`, `like`, `dislike`, `pincid`, `kid`, `theme`)
		(SELECT `id`, `date`, `datePublish`, `url`, `title`, `txt`, `data`, `sources`, `user`, 1 , `site`, `statViews`, `statViewsShort`, `statShortFlag`, `countPhoto`, `FBpublished`, `like`, `dislike`, `pincid`, `kid`, `theme` FROM old_{$tbl->post} WHERE `published`='published')
	",1);
	db::query("
		INSERT INTO {$tbl->post} (`id`, `date`, `datePublish`, `url`, `title`, `txt`, `data`, `sources`, `user`, `published`, `site`, `statViews`, `statViewsShort`, `statShortFlag`, `countPhoto`, `FBpublished`, `like`, `dislike`, `pincid`, `kid`, `theme`)
		(SELECT `id`, `date`, `datePublish`, `url`, `title`, `txt`, `data`, `sources`, `user`, -1 , `site`, `statViews`, `statViewsShort`, `statShortFlag`, `countPhoto`, `FBpublished`, `like`, `dislike`, `pincid`, `kid`, `theme` FROM old_{$tbl->post} WHERE `published`='autoposting')
	",1);
	db::query("
		INSERT INTO {$tbl->post} (`id`, `date`, `datePublish`, `url`, `title`, `txt`, `data`, `sources`, `user`, `published`, `site`, `statViews`, `statViewsShort`, `statShortFlag`, `countPhoto`, `FBpublished`, `like`, `dislike`, `pincid`, `kid`, `theme`)
		(SELECT `id`, `date`, `datePublish`, `url`, `title`, `txt`, `data`, `sources`, `user`, 0 , `site`, `statViews`, `statViewsShort`, `statShortFlag`, `countPhoto`, `FBpublished`, `like`, `dislike`, `pincid`, `kid`, `theme` FROM old_{$tbl->post} WHERE `published`!='autoposting' && `published`!='published')
	",1);

	$qid=db::query("SHOW TABLES LIKE '%\_post'",1);
	while ($d=db::fetchRow($qid)) {
		if($d=='old_post') continue; 
		$tbl=\posts\tables::initByTbl($d);
		db::query("RENAME TABLE {$tbl->post} TO old_{$tbl->post}",1);
		module::execData('posts/admin',['act'=>'install']);
		db::query("
			INSERT INTO {$tbl->post} (`id`, `date`, `datePublish`, `url`, `title`, `txt`, `data`, `sources`, `user`, `published`, `site`, `statViews`, `statViewsShort`, `statShortFlag`, `countPhoto`, `FBpublished`, `like`, `dislike`, `pincid`, `kid`, `theme`)
			(SELECT `id`, `date`, `datePublish`, `url`, `title`, `txt`, `data`, `sources`, `user`, 1 , `site`, `statViews`, `statViewsShort`, `statShortFlag`, `countPhoto`, `FBpublished`, `like`, `dislike`, `pincid`, `kid`, `theme` FROM old_{$tbl->post} WHERE `published`='published')
		",1);
		db::query("
			INSERT INTO {$tbl->post} (`id`, `date`, `datePublish`, `url`, `title`, `txt`, `data`, `sources`, `user`, `published`, `site`, `statViews`, `statViewsShort`, `statShortFlag`, `countPhoto`, `FBpublished`, `like`, `dislike`, `pincid`, `kid`, `theme`)
			(SELECT `id`, `date`, `datePublish`, `url`, `title`, `txt`, `data`, `sources`, `user`, -1 , `site`, `statViews`, `statViewsShort`, `statShortFlag`, `countPhoto`, `FBpublished`, `like`, `dislike`, `pincid`, `kid`, `theme` FROM old_{$tbl->post} WHERE `published`='autoposting')
		",1);
		db::query("
			INSERT INTO {$tbl->post} (`id`, `date`, `datePublish`, `url`, `title`, `txt`, `data`, `sources`, `user`, `published`, `site`, `statViews`, `statViewsShort`, `statShortFlag`, `countPhoto`, `FBpublished`, `like`, `dislike`, `pincid`, `kid`, `theme`)
			(SELECT `id`, `date`, `datePublish`, `url`, `title`, `txt`, `data`, `sources`, `user`, 0 , `site`, `statViews`, `statViewsShort`, `statShortFlag`, `countPhoto`, `FBpublished`, `like`, `dislike`, `pincid`, `kid`, `theme` FROM old_{$tbl->post} WHERE `published`!='autoposting' && `published`!='published')
		",1);
	}
}
/*
	Создает поля published, datePublish в category2post
	! удалить старые лучше вручную
*/
function update23(){
	include_once module::$path.'/posts/handler.php';
	\posts\tables::$instance=null;
	$tbl=\posts\tables::init();
	list($check)=db::qrow("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='".DB_NAME."' && TABLE_NAME='{$tbl->category2post}' && COLUMN_NAME='published'");
	if(!empty($check)) return;

	db::query("RENAME TABLE {$tbl->category2post} TO old_{$tbl->category2post}",1);
	module::execData('category/admin',['act'=>'install']);
	#заполняем таблицы
	db::query("INSERT INTO {$tbl->category2post} (cid, pid, datePublish, published, uid) (
		SELECT cid, pid, datePublish, published, user FROM old_{$tbl->category2post} o INNER JOIN {$tbl->post} p ON p.url=o.pid && p.pincid=''
	)",1);

	#для постов с префиксами, например blog (если есть)
	$qid=db::query("SHOW TABLES LIKE '".PREFIX_SPEC."%\_category2post'",1);
	while ($d=db::fetchRow($qid)) {
		$tbls=\posts\tables::initByTbl($d);
		db::query("RENAME TABLE {$tbl->category2post} TO old_{$tbl->category2post}",1);
		module::execData('category/admin',['act'=>'install']);
		#заполняем таблицы
		db::query("INSERT INTO {$tbl->category2post} (cid, pid, datePublish, published, uid) (
			SELECT cid, pid, datePublish, published, user FROM old_{$tbl->category2post} o INNER JOIN {$tbl->post} p ON p.url=o.pid && p.pincid=''
		)",1);
	}
}
/*
	добавляет индекс по `cid`+`datePublish` для таблицы category2post
	удялает лишний индекс
*/
function update24(){
	list($check)=db::qrow("SHOW INDEX FROM `".PREFIX_SPEC."category2post` WHERE key_name='cid_datePublish'",1);
	if(empty($check)){
		db::query("ALTER TABLE `".PREFIX_SPEC."category2post` ADD INDEX `cid_datePublish` (`cid`,`datePublish`)",1);
		db::query("DROP INDEX `date_cid_published` ON `".PREFIX_SPEC."category2post`",1);
	}
	$qid=db::query("SHOW TABLES LIKE '".PREFIX_SPEC."\_%\_category2post'",1);
	while ($d=db::fetchRow($qid)) {
		list($check)=db::qrow("SHOW INDEX FROM `{$d}` WHERE key_name='cid_datePublish'",1);
		if(empty($check)){
			db::query("ALTER TABLE `{$d}` ADD INDEX `cid_datePublish` (`cid`,`datePublish`)",1);
			db::query("DROP INDEX `date_cid_published` ON `{$d}`",1);
		}
	}
}
/*
	добавляет поле в таблицу zspec_imgs
*/
function update25(){
	list($check)=db::qrow("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='".DB_NAME."' && TABLE_NAME='".PREFIX_SPEC."imgs' && COLUMN_NAME='exif'");
	if(empty($check)){
		db::query("ALTER TABLE `".PREFIX_SPEC."imgs` ADD `exif` TEXT NOT NULL AFTER `sourcePage`",1);
	}
}
/*
	добавляет FULLTEXT INDEX в category
*/
function update26(){
	list($check)=db::qrow("SHOW INDEX FROM `category` WHERE key_name='ft_title'",1);
	if(empty($check)){
		db::query("ALTER TABLE `category` ADD FULLTEXT `ft_title` (`title`)",1);
	}
	$qid=db::query("SHOW TABLES LIKE '%\_category'",1);
	while ($d=db::fetchRow($qid)) {
		list($check)=db::qrow("SHOW INDEX FROM `{$d}` WHERE key_name='ft_title'",1);
		if(empty($check)){
			db::query("ALTER TABLE `{$d}` ADD FULLTEXT `ft_title` (`title`)",1);
		}
	}
}
/*
	изменяет таблицу users
*/
function update27(){
	list($check)=db::qrow("SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='".DB_NAME."' && TABLE_NAME='".PREFIX_SPEC."users' && COLUMN_NAME='phone'",1);
	if(strtolower($check)!='int') return;

	db::query("ALTER TABLE `".PREFIX_SPEC."users` CHANGE `phone` `phone` VARCHAR(128)",1);
}
/*
 * храним версию в таблице БД
 */
function update28() {
	list($lastVer)=db::qrow("SELECT `value` FROM `" . PREFIX_SPEC . "config` WHERE `key`='db_version'");
	if (!empty($lastVer)) return;
	
	db::query("INSERT INTO `" . PREFIX_SPEC . "config`(`key`, `value`) VALUES ('db_version','update28')");
}
/*
 * добавляем поддержку папок для пинов
 */
function update29() {
	list($check) = db::qrow("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='" . DB_NAME . "' && TABLE_NAME='" . PREFIX_SPEC . "like2img' && COLUMN_NAME='fid'");
	if (empty($check)) {
		db::query("ALTER TABLE `" . PREFIX_SPEC . "like2img` ADD `fid` INT NOT NULL AFTER `imid`;");
		db::query("ALTER TABLE `" . PREFIX_SPEC . "like2img` DROP INDEX `uid`, ADD UNIQUE KEY `uid` (`uid`, `imid`, `fid`)");
		db::query("CREATE TABLE `" . PREFIX_SPEC . "likeFolders` (
				`id` int(11) NOT NULL,
				`url` varchar(100) NOT NULL,
				`title` varchar(100) NOT NULL,
				`uid` int(11) NOT NULL,
				`count` int(11) NOT NULL
				) ENGINE=InnoDB DEFAULT CHARSET=utf8");
		db::query("ALTER TABLE `" . PREFIX_SPEC . "likeFolders` ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `url` (`url`,`uid`)");
		db::query("ALTER TABLE `" . PREFIX_SPEC . "likeFolders` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT");
		$ql2i=db::query("SELECT uid, count(uid) as count FROM `" . PREFIX_SPEC . "like2img` GROUP BY `uid`");
		while ($l2i = db::fetch($ql2i)){
			db::query("INSERT INTO `" . PREFIX_SPEC . "likeFolders`(`url`, `title`, `uid`, `count`) VALUES ('unsorted','Unsorted',{$l2i->uid},{$l2i->count})");
			db::query("UPDATE `" . PREFIX_SPEC . "like2img` SET `fid`=".db::insert()." WHERE `uid` = {$l2i->uid}");
		}
	}
}
/*
 * добавляем поддержку скачанных пользователем изображений
 */
function update30(){
	list($check)=db::qrow("SHOW TABLES LIKE '".PREFIX_SPEC."userDownloads'");
	if(empty($check)){
		db::query("CREATE TABLE IF NOT EXISTS `".PREFIX_SPEC."userDownloads` (
				`uid` INT NOT NULL ,
				`imid` INT NOT NULL ,
				`data` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
				UNIQUE (`uid`, `imid`)
				) ENGINE = InnoDB;");
	}
}
/*
 * Adding support for user achievements
 */
function update31() {
	require_once(module::$path.'/user/handler.php');
	require_once(module::$path.'/posts/admin/func.php');
	require_once(module::$path.'/images/admin/func.php');
	list($check) = db::qrow("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='" . DB_NAME . "' && TABLE_NAME='" . PREFIX_SPEC . "users' && COLUMN_NAME='achievements'");
	if(empty($check)){
		db::query("ALTER TABLE `".PREFIX_SPEC."users` ADD `achievements` TEXT NOT NULL AFTER `token`");
		#Init table with acievements
		db::query("CREATE TABLE IF NOT EXISTS `".PREFIX_SPEC."achievement` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`name` varchar(20) NOT NULL,
				`title` varchar(100) NOT NULL,
				`description` text NOT NULL,
				PRIMARY KEY (`id`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8");
		db::query("INSERT INTO `".PREFIX_SPEC."achievement` (`id`, `name`, `title`, `description`) VALUES
				(1, 'post', 'Post achievement', 'Achievement for 1 post'),
				(2, 'upload', 'Uploader achievement', 'Achievement for 10 images upload'),
				(3, 'info', 'Info achievement', 'Achievement for completing self information.')");
		$u=db::query("SELECT DISTINCT `user` FROM `post`");
		while ($uid=db::fetch($u)){
			\posts_admin\checkAchievement($uid->user);
			\images_admin\checkAchievement($uid->user);
		}
	}
}