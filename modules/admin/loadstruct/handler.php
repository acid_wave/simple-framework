<?php
namespace admin_loadstruct;
use module,db,url,cache,ZipArchive;
#Используем сторонние модули
require_once(module::$path.'/posts/admin/handler.php');
#подключаем функции
require_once(module::$path.'/admin/loadstruct/func.php');

class handler{
	function __construct(){
		$this->template='template';#Определяем в какой шаблон будем вписывать
		$this->headers=new \stdClass;
		$this->uhandler=module::exec('user',array('easy'=>1),1)->handler;
		$this->user=$this->uhandler->user;
	}
	/*
		Загрузка структуры категорий и постов на базе директорий и файлов
	*/
	function loadStruct($file,$ftpfile,$tblprefix,$repeatPosts,$autoposting,$updateCount){
		if(!$this->uhandler->rbac('loadStruct')) die('forbidden');
		$ftpStructName=PATH.'tmp/ftp/files/struct.zip';
		$log=$msg='';
		if($file!==false||$ftpfile!==false){
			set_time_limit(0);
			ini_set('memory_limit', '512M');
			$fname=$file?$file['tmp_name']:$ftpStructName;
			if(file_exists($fname)){
				if(mkdir($tmpDir=sys_get_temp_dir().'/'.SITE.'_'.time())){
					exec("unzip -UU {$fname} -d {$tmpDir}");
					$tbl=\posts\tables::init($tblprefix);
					#Создаем таблички для новых префиксов
						if(!empty($tblprefix))
							module::exec('posts/admin',array('act'=>'install'),1);
							module::exec('category/admin',array('act'=>'install'),1);
					#Проверяем наличие каталога картинок
						@mkdir(PATH.'modules/images/files/images/');
						@mkdir(PATH.'tmp/loadStruct/');
					#Проверяем записана ли база сразу или только в подпапке
						if(count($base=scandir($baseDir=$tmpDir))==3)
							$baseDir=$tmpDir.'/'.$base[2];
					#запись данных
						$log=(object)array();
						storeStruct($baseDir,$tbl,$repeatPosts,$autoposting,$this->user->id,$log);
						# обновляем количество постов в категориях
						if($updateCount){
							module::execData('category',array('act'=>'updateCount','cats'=>'all','tbl'=>$tbl->post));
						}
					#Удаляем временную разархивированную папку
						exec("rm -rf {$tmpDir}");
				}else $msg=sys_get_temp_dir().' is not writable';
			}else $msg='Archive is not exists';
		}else{
			$msg=(strstr(shell_exec('whereis unzip'), 'unzip: /usr/bin/unzip'))?'':'Warning: unzip is not installed';
		}	
		#print_r($log);exit;
		return array(
			'log'=>@$log,
			'message'=>$msg,
			'sizelimit'=>ini_get('upload_max_filesize'),
			'tblprefix'=>$tblprefix,
			'prefixlist'=>\posts_admin\getPrefixList(),
			'detectFTPupload'=>file_exists($ftpStructName),
			'ftpStructName'=>$ftpStructName,
		);
	}
}
