<?php
$tpl->title="Bulk posts upload from ZIP";
?>
<center>
	<h1>Create or update posts using zip file</h1>
</center>
<script type="text/javascript">
	$(document).ready(function(){
		$('#setprfx input:button').click(function(){
			var name=$('#setprfx input:text').val();
			if(name){
				$('#setprfx select').append('<option value="'+name+'">'+name+'</option>');
				$('#setprfx select option').prop('selected',false);
				$('#setprfx select option:last-child').prop('selected',true);
			}
		});
	});
</script>
<div><?=$data->message?></div>
<div style="float:right;border:1px solid black;padding:5px;">
	<h2>Example of zip arhive stucture:</h2>
site.zip<br>
&nbsp; site<br>
&nbsp; &nbsp; cat1<br>
&nbsp; &nbsp; &nbsp; &nbsp; cat1.1<br>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; postName1<br>
&nbsp; &nbsp; cat2<br>
&nbsp; &nbsp; &nbsp; &nbsp; postName2<br>
&nbsp; &nbsp; cat3<br>
&nbsp; &nbsp; &nbsp; &nbsp; postName3<br>
<h2>Example of postName file structure:<br>you can leave it empty.</h2>
<pre>
#title#
post title
#keys#
key1
key2
#desc#
Это описание поста
Это описание поста
#tags#
tag1
tag2
tag3
#data#
{a: 1}
#imgs#
https://ya.ru/1.jpg|free with attribution
https://ya.ru/2.jpg
#pin#
yes
#user#
user name
#publishDate#
2017-10-24 12:34:15
</pre>
</div>
<form action="" method="post" enctype="multipart/form-data">
	<?if($data->detectFTPupload){?>
		FTP <b>struct.zip</b> file detected
		<input type="hidden" name="ftpstructfile" value="1"/>
	<?}else{?>
		<div style="font-weight:bold">
			Please add zip arhive of posts:<br>
			Or you can upload it by ftp here: <?=$data->ftpStructName?>
		</div><br>
		<input type="file" name="structfile"/>
	<?}?><br>
	<small>max file size:<?=$data->sizelimit?></small><br><br>
	<div id="setprfx">
		<label>
			<b>Prefix table name:<br><small>(example: prefix - blog will create table: blog_post)</small></b><br>
			<select name="tblprefix">
				<option<?=empty($data->tblprefix)?' selected=""':''?> value=''>default</option>
			<?foreach ($data->prefixlist as $v) {?>
				<option value="<?=$v?>"<?=$v==$data->tblprefix?' selected=""':''?>><?=$v?></option>
			<?}?>
			</select>
		</label>
		<input type="text"/>
		<input type="button" value="add"/>
		<br/><br/>
	</div>
	<div style="font-weight:bold">Select what we do with repeating posts:</div>
	<table>
		<tr>
			<td><input name='repeatPosts' value='' type='radio' checked></td>
			<td>Nothing (just don't add and not update)</td>
		</tr>
		<tr>
			<td><input name='repeatPosts' value='create' type='radio'></td>
			<td>Create new posts</td>
		</tr>
		<tr>
			<td><input name='repeatPosts' value='update' type='radio'></td>
			<td>Update all post data</td>
		</tr>
	</table>
		<b>Autoposting</b> <input type="checkbox" name="autoposting" value=1 checked=""/>
		<small>Notice: set autoposting config after struct loading</small>
	<br>
		<b>Update count posts in categories</b> <input type="checkbox" name="updateCount" value=1 checked=""/>
		<small>Notice: unchecked make minimum time execution</small>
	<br/><br/>
	<input class="button" name="submit" type="submit" value="upload"/>
</form>
<?if($data->log){?>
	<div style="display:inline-block;min-width:900px;">
		<div style="float:left;max-width:500px;">
			<label style="font-weight:bold">Posts list:</label><br><br>
			<?=recPosts($data->log,$data->tblprefix)?>
		</div>
		<div style="float:right">
			<label style="font-weight:bold">Category list:</label><br><br>
			<?=recCats($data->log,$data->tblprefix)?>
		</div>
		<div style="clear:both;"></div>
	</div>
<?}?>
<div style="clear:both;"></div>
<?



function recCats($log,$tblprefix){
	if(empty($log->cats)) return;?>
	<ul>
	<?foreach($log->cats as $k=>$v){?>
		<li><a href="<?=url::category($v->url,$tblprefix)?>"><?=$v->name?></a>
			<small><?=$v->status=='insert'?$v->status:"<span style='color:red'>$v->status</span>"?></small>
			<?if(!empty($v->cats)){recCats($v,$tblprefix);}?>
		</li>
	<?}?>
	</ul>
<?}?>
<?function recPosts($log,$tblprefix){?>
	<ul>
		<?if(!empty($log->cats)){
			foreach($log->cats as $k=>$v){?>
			<li><a href="<?=url::category($v->url,$tblprefix)?>"><b><?=$v->name?></b></a>
				<small><?=$v->status=='insert'?$v->status:"<span style='color:red'>$v->status</span>"?></small>
				<?if(!empty($v->cats)){recPosts($v,$tblprefix);}?>
			</li>
			<ul>
			<?if(!empty($v->posts))
				foreach($v->posts as $p){?>
				<li><a href="<?=url::post($p->url,$tblprefix)?>"><?=$p->title?></a>
					<small><?=$p->status=='insert'?$p->status:"<span style='color:red'>$p->status</span>"?>
					<?if(!empty($p->pin)){?><?=$p->pin?', pined ('.$p->pin.')':''?><?}?>
					</small>
				</li>
				<?}?>
			</ul>
			<?}
		}?>
		<?if(!empty($log->posts)){?>
			<li>
				Uncategorized
				<ul>
					<?foreach($log->posts as $p){?>
					<li><a href="<?=url::post($p->url,$tblprefix)?>"><?=$p->title?></a>
						<small><?=$p->status=='insert'?$p->status:"<span style='color:red'>$p->status</span>"?>
						<?if(!empty($p->pin)){?><?=$p->pin?', pined ('.$p->pin.')':''?><?}?>
						</small>
					</li>
					<?}?>
				</ul>
			</li>
		<?}?>
	</ul>
<?}?>
