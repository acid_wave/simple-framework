<?
/*
 * Создает посты и категории используя папку с файлами
*/
include_once(PATH."modules/category/admin/handler.php");
include_once(PATH."modules/images/admin/handler.php");
function storeStruct($path,$tbl,$repeatPosts,$autoposting,$userID,&$log,&$cats=array(),$parent=''){
	static $users;
	if(empty($users)){
		db::query("SELECT id,name FROM `".PREFIX_SPEC."users`");
		while($d=db::fetch()){
			$users[$d->name]=$d->id;
		}
	}
	#Определяем максимальную длину урла
		static $length,$tagCat;
		if(!isset($length)){
			list($length)=db::qrow("SELECT CHARACTER_MAXIMUM_LENGTH FROM information_schema.COLUMNS 
				WHERE TABLE_SCHEMA='".DB_NAME."' && TABLE_NAME='post' && COLUMN_NAME='url'");
			#Получайм id категории с тегами
				$tagCat=saveCat($tbl,'Tags','',0)->url;
		}
	$base=scandir($path);
	foreach ($base as $file){
		if($file=='.'||$file=='..') continue;
		$newpath="$path/$file";
		if(is_dir($newpath)){# сохраняем категории
			#Создаем новую категорию
			$res=saveCat($tbl,$file,$parent,$view=1);
			#Пишем логи
			$log->cats[$file]=(object)array(
				'name'=>$file,
				'url'=>$res->url,
				'status'=>isset($res->exists)?'use exists':'insert',
			);
			$t_cats=array_merge($cats,array($res->url));
			#Рекурсия
				storeStruct($newpath,$tbl,$repeatPosts,$autoposting,$userID,$log->cats[$file],$t_cats,$res->url);
		}else{# сохраняем посты
			$data=getData($newpath);
			#Проверяем наличие поста с выбранным url
				$url_prep=mb_substr($data->title,0,$length-4);
				if($repeatPosts=='create'){
					#получаем уникальный URL
					$url=\posts_admin\getUrl($tbl->post,$url_prep);
				}else{
					$url=\posts_admin\key2url($url_prep);
				}
				$log->posts[$data->title]=db::qfetch("SELECT `url`,title FROM `{$tbl->post}` WHERE `url`='$url' LIMIT 1");
				
			#Если пост существует и не нужно его обновлять то ничего не делаем
				if(!empty($log->posts[$data->title]->url)){
					$log->posts[$data->title]->status='exists';
					if(!in_array($repeatPosts,array('update','create')))continue;
				}
			#Записываем пост
				$data->pin=!empty($data->pin)?$parent:false;
				#Записываем пользовтеля
					$uid=$userID;
					if(!empty($data->user)){
						$sname=db::escape(trim($data->user));
						if(empty($users[$data->user])){
							db::query("INSERT INTO `".PREFIX_SPEC."users` SET name='$sname',mail='$sname@".preg_replace("!https?://!",'',HREF)."'");
							$users[$data->user]=db::insert();
						}
						$uid=$users[$data->user];
					}
				$postData=savePost($url,$data,$tbl,$repeatPosts,$autoposting,$uid);
				$id=$postData->id;
				$log->posts[$data->title]=(object)array(
					'status'=>$id?'insert':'error',
					'url'=>$url,
					'title'=>$data->title,
					'pin'=>$data->pin
				);
				if($id){
					/*
					 * создаем и привязываем новые категории
					 *	 Если $repeatPosts=='update' то отвязываем все старые категории
					*/
						if($repeatPosts=='update')
							db::query("DELETE FROM `{$tbl->category2post}` WHERE pid='$url'");
						saveCat2post($cats,$postData,$tbl);
					#Записываем теги
						$tags=array('tags');
						if(!empty($data->tags))foreach($data->tags as $tag){
							$tags[]=saveCat($tbl,$tag,$tagCat)->url;
							
						}
						saveCat2post($tags,$postData,$tbl);
					#Записываем кейворды
						if($repeatPosts=='update')
							db::query("DELETE FROM `".PREFIX_SPEC."keyword2post` where pid='$id' and tbl='{$tbl->post}'");
						if(!empty($data->keys)){
							foreach($data->keys as $key){
								saveKey($key,$id,$tbl);
							}
						}
					#Сохраняем ссылки на картинки в таблицу
						if(!empty($data->imgs))foreach($data->imgs as $img){
							@list($img,$license)=explode("|",$img);
							$url=testUrl($data->title);
							db::query("INSERT INTO `".PREFIX_SPEC."imgs` SET 
								tbl='$tbl->post',
								pid='$id',
								uid='$uid',
								url='$url',
								source='".db::escape(trim($img))."',
								licence='".trim($license)."'
							");
						}		
				}
		}
	}
}
#Проверяем наличие 
function testUrl($title){
	static $urls;
	if(empty($url)){
		db::query("SELECT url FROM `".PREFIX_SPEC."imgs`");
		while($d=db::fetch()){
			$e=explode(".",$d->url);
			array_pop($e);
			$urls[implode(".",$e)]=1;
		}
	}
	$title=\category_admin\key2url($title);
	do{
		@$i++; 
		$file=($i>1)?"$title-$i":$title;
		if(empty($urls[$file])){
			$urls[$file]=1;
			$stop=true;
		}
	}while(!isset($stop));
	return "$file.";
}
/*
 * Сохраняем пост
*/
function savePost(&$url,$data,$tbl,$repeatPosts,$autoposting,$userID){
	$published=$autoposting?-1:1;
	$sql="
		`title`='".db::escape(stripcslashes(html_entity_decode($data->title)))."',
		`txt`='".db::escape($data->desc)."',
		`user`='{$userID}',
		`pincid`='{$data->pin}',
		`data`='".db::escape($data->data)."',
		`published`='{$published}'
	";
	$updateSql=$repeatPosts=="update"?"ON DUPLICATE KEY UPDATE $sql":'';
	$i=-1;
	do{
		@$i++;
		$turl=($i==0)?$url:"$url-$i";
		$pbdate=(
			$data->publishDate?$data->publishDate:date('Y-m-d H:i:s',
				$autoposting?strtotime('+1 day'):time()
			)
		);
		db::query("INSERT INTO `{$tbl->post}` SET 
			`url`='{$turl}',
			`date`=NOW(),
			`datePublish`='$pbdate',
			$sql
			$updateSql
		");
		$id=db::insert();
	}while(!$id && $repeatPosts=='create' && $i<30);
	$url=$turl;
	if($id==0){
		list($id)=db::qrow("SELECT id from `{$tbl->post}` WHERE url='$url'");
	}
	return (object)['id'=>$id,'url'=>$url,'published'=>$published,'datePublish'=>$pbdate,'uid'=>$userID];
}
/*
 * Создаем категорию
*/ 
function saveCat($tbl,$title,$parent='',$view=1){
	$titleSQLEscaped=db::escape($title);
	$parentSQLEscaped=db::escape($parent);
	$cat=db::qfetch("SELECT id,url from `{$tbl->category}` WHERE title='$titleSQLEscaped' && parentId='$parentSQLEscaped'");
	if(!isset($cat->id)){
		$url=\category_admin\getUrl($tbl->category,$title);
		db::query("INSERT INTO `{$tbl->category}` (`url`,`title`,`parentId`,`view`) 
			VALUES('$url','$titleSQLEscaped','$parentSQLEscaped','$view')");
		$cat=(object)['id'=>db::insert(),'url'=>$url];
	}else
		$cat->exists=true;
	return $cat;
}
# записываем категории для поста
function saveCat2post($cats,$postData,$tbl){
	if(empty($cats)) return;
	$sqlVal=array();
	foreach ($cats as $cat) {
		$sqlVal[]="('{$cat}','{$postData->url}','$postData->datePublish','$postData->published','$postData->uid')";
	}
	db::query("INSERT IGNORE INTO `{$tbl->category2post}` (`cid`,`pid`,`datePublish`,`published`,`uid`) VALUES".implode(',',$sqlVal));
}
# записывает кейворд
function saveKey($title,$pid,$tbl){
	$titleSQLEscaped=db::escape($title);
	db::query("INSERT IGNORE INTO `keyword` SET title='$titleSQLEscaped'");
	if(!$kid=db::insert()){
		list($kid)=db::qrow("SELECT id FROM `keyword` WHERE `title`='$titleSQLEscaped' LIMIT 1");
	}
	if($kid){
		db::query("INSERT INTO `".PREFIX_SPEC."keyword2post` SET kid='$kid',pid='$pid',tbl='{$tbl->post}'");
	}
}
#Парсим данные из файла и формируем обект с данными
function getData($newpath){
	$fl=file($newpath);
	$types=explode(",","#title#\n,#keys#\n,#desc#\n,#tags#\n,#data#\n,#imgs#\n,#pin#\n,#user#\n,#publishDate#\n");
	$name="#keys#\n";$data=new \stdClass;
	foreach($fl as $v){
		if(in_array($v,$types)){
			$name=substr($v,1,-2);
			continue;
		}
		if(in_array($name,array('keys','tags','imgs'))){
			$v=trim($v);
			if($v!=''){
				@$data->{$name}[]=$v;
			}
		}else{
			@$data->$name.="$v";
		}
	}
	#Убираем лишние enter в $data
		foreach($types as $type){
			$type=trim($type);
			$type=trim($type,"#");
			if(!is_array(@$data->$type)){
				@$data->$type=trim($data->$type);
			}
		}
	#Проверяем корректность заполнения поля pin
		if(in_array($data->pin,array('yes',1)))$data->pin=false;
	#Выбираем title поста из названия файла или из данных в файле если есть
		$data->title=!empty($data->title)?$data->title:str_replace('.txt','',basename($newpath));
	return $data;
}
			
