<?
$img=&$data->img;
$post=&$data->post;
$prev=&$data->prev;
$next=&$data->next;
?>
<script>
	$(document).ready(function(){
		$(window).keyup(function (event) {
			if ( event.keyCode == 37 ) window.location.hash=$('#navleft').attr('href');
			if ( event.keyCode == 39 ) window.location.hash=$('#navright').attr('href');
		});
	});
</script>
<style type="text/css">
	#overlayClose{
		color: #fff;
		display: block;
		font-size: 30px;
		font-weight: bolder;
		line-height: normal;
		position: absolute;
		top: 10px;
		right: 20px;
		cursor: pointer;
		text-decoration: none;
		border: 1px solid #424242;
		padding: 3px 8px 0px 9px;
		z-index: 999;
	}
	#overlayClose:hover{background-color: #424242;}
	#overlayClose:before {
		content: "×";
	}
	.overlay{
		-moz-user-select: none;
		left: 0;
		overflow-x: hidden;
		overflow-y: scroll;
		position: fixed;
		top: 0;
		width: 100%;
		z-index: 999999 !important;
		background: none repeat scroll 0 0 #000;
		color:#fff;
	}
	.overlay .container{margin-top: 35px;}
	.overlay h1{color:#fff;margin: 3px 0;padding: 0;font-size: 1em;}
	.overlay a{color:#fff;}
	.overlay .fa-arrow-circle-left:hover, .overlay .fa-arrow-circle-right:hover{
		color: #37B641;
	}
</style>
<?if($data->del==1){?>
<div class="row justify-content-center text-center mt-3">
	<button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteDialog" data-imid="<?=$img->id?>">
		<b>DELETE IMAGE</b>
	</button>
</div>
<?include $template->inc('../../images/admin/tpl/delRequest/delForm.php');?>
<?}?>
<a id="overlayClose" title="close gallery" onclick="closeOverlay();"></a>
<div class="container">
	<div class="row justify-content-center">
		<div class="col-lg-6 col-md-8 text-center pb-2">
			<?=isset($data->ads->top)?$data->ads->top:''?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-8 offset-md-2 offset-lg-3">
			<div class="row">
				<div class="col-12">
					<img class="img-fluid bg-black" style="width: 100%" src="<?=url::image($img->url)?>" alt="<?=$img->title." #".$img->num?>" title="<?=$img->title." #".$img->num?>" />
				</div>
			</div>
			<div class="row">
				<div class="col-6 text-left">
					<h1><?=$img->title?> #<?=$img->num?></h1>
				</div>
				<div class="col-6 text-right">
					<a href="<?=url::image($img->url)?>" target="_blank" title="<?=$img->url?>">open in new tab</a>
				</div>
			</div>
			<div class="row">
				<div class="col-12 text-center my-2 mx-0">
					<?=isset($data->ads->bottom)?$data->ads->bottom:''?>
				</div>
			</div>
			<div class="row">
				<div class="col-5 text-left">
					<a id="navleft" href="<?=url::imgOverlay($data->tbl,$post->id,$prev->url)?>" title="<?=$prev->title?>"><i class="fa fa-arrow-circle-left fa-4x" aria-hidden="true"></i></a>
				</div>
				<div class="col-2 text-center">
					<?="{$img->num}&nbsp;of&nbsp;{$data->count}"?>
				</div>
				<div class="col-5 text-right">
					<a id="navright" href="<?=url::imgOverlay($data->tbl,$post->id,$next->url)?>" title="<?=$next->title?>"><i class="fa fa-arrow-circle-right fa-4x" aria-hidden="true"></i></a>
				</div>
			</div>	
		</div>
		<div class="col-lg-3 col-md-2">
			<?=isset($data->ads->right)?$data->ads->right:''?>
		</div>
	</div>
</div>
