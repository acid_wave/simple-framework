<?
$img=&$data->img;
$post=&$data->post;
$prev=&$data->prev;
$next=&$data->next;

$titleUpper=strtoupper($post->title);
$tpl->title="$titleUpper - ".(!empty($data->size)?"{$data->size}px ":'')."Image #{$img->num}";
$tpl->desc="Images gallery of $titleUpper. Image #{$img->num} and navigation by next or previous images.";
$imageTitle = "{$post->title} #{$img->num}";
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<meta name="description" content="<?=$tpl->desc?>" />
	<link href="favicon.ico" rel="shortcut icon">
	<title><?=$tpl->title?></title>  
	<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<script type="application/javascript" src="/files/js/jquery.js"></script>
	<style>
		body{width:100%;margin:0;background-color: #000000;}
		a{text-decoration:none;color:white;}
		a:hover{text-decoration:none;color:white;opacity:0.6}
		a:visited{text-decoration:none;color:white;}
		small{color:white;}
		.breadcrumbs{top:0;margin: 0}
		.breadcrumbs a{font-size: 0.9em}
		h1{font-size: 16px;margin: 4px 0;}
		table.gl-nav{border: 0;width: 100%}
		table.gl-nav .gl-nav-btn{font-weight: bold;font-size: 0.7em;width:100px;}
		table.gl-nav .gl-nav-text{font-size: 0.7em;text-align: center;}
		.prev-next img{width:100%;float:left;}
	</style>
</head>
<body>
<div>
	<div class="breadcrumbs">
		<h1 style="color:#555555;display:inline-block;font-size:0.9em;font-weight:normal;margin:0;"><?=$titleUpper?> - Image #<?=$img->num?></h1>
	</div>
	<div style="text-align:center">
		<?=$data->ads->mobileTop?>
		<br/><br/>
	</div>
	<div style="text-align:center;">
		<img style="width:98%;" src="<?=url::image($img->url)?>" alt="<?=$imageTitle?>" title="<?=$imageTitle?>"/>
	</div>
	<br/><center>
	<?=$data->ads->mobileBottom?>
	</center>
	<br/><br/>
	<table class="gl-nav">
	<tr>
		<td class="gl-nav-btn">
			<div class="prev-next"><a href="<?=url::img($data->tbl,$post->id,$prev->url)?>" title="<?=$prev->title?>" rel="prev"><img src="/files/icons/arrow-left.png"/></a></div>
		</td>
		<td class="gl-nav-text">
			<div class="g-nav-count"><?="{$img->num}&nbsp;of&nbsp;{$data->count}"?></div>
		</td>
		<td class="gl-nav-btn">
			<div class="prev-next"><a href="<?=url::img($data->tbl,$post->id,$next->url)?>" title="<?=$next->title?>" rel="next"><img src="/files/icons/arrow-right.png"/></a></div>
		</td>
	</tr>
	</table>
</div>
</body>
</html>
 
 
