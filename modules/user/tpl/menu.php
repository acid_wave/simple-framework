<ul class="nav navbar-nav navbar-user justify-content-end">
	<?if(@$data->user->mail){?>	
		<? /* Использовать для сообщений пользователю 
		<ul class="nav navbar-nav navbar-right navbar-user">
		<li class="dropdown messages-dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> Messages <span class="label label-danger">2</span> <b class="caret"></b></a>
		<ul class="dropdown-menu">
			<li class="dropdown-header">2 New Messages</li>
			<li class="message-preview">
				<a href="#">
					<span class="avatar"><i class="fa fa-bell"></i></span>
					<span class="message">Security alert</span>
				</a>
			</li>
			<li class="divider"></li>
			<li class="message-preview">
				<a href="#">
					<span class="avatar"><i class="fa fa-bell"></i></span>
					<span class="message">Security alert</span>
				</a>
			</li>
			<li class="divider"></li>
			<li><a href="#">Go to Inbox <span class="badge">2</span></a></li>
		</ul>
		*/?>
		<li class="nav-item dropdown user-dropdown">
		   <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
				<?if($data->user->avatar){?>
					<img src="<?=$data->user->avatar?>" class="rounded-circle" style="width: 20px;height: 20px;">
				<?}else{?>
					<i class="fa fa-user"></i>
				<?}?>
				<?=$data->user->altName?><b class="caret"></b>
			</a>
		   <div class="dropdown-menu dropdown-menu-right">
			   <a class="dropdown-item" href="<?=url::userSettings()?>"><i class="fa fa-user"></i> Profile</a>
			   <a class="dropdown-item" href="<?=url::userChagePassword()?>"><i class="fa fa-gear"></i> Change Password</a>
			   <a class="dropdown-item" href="<?=url::userPhotos($data->user->id)?>"><i class="fa fa-heart-o"></i> Pins</a>
			   <div class="dropdown-divider"></div>
			   <a class="dropdown-item" href="<?=url::userLogout()?>"><i class="fa fa-power-off"></i> Log Out</a>
		   </div>
	   </li>
	<?}else{?>
		<li class="nav-item"><a class="nav-link active" href="<?=url::userRegister()?>">Join <?=NAME?>!</a></li>
		<li class="nav-item"><a class="nav-link" href="<?=url::userLogin()?>">Login</a></li>
	<?}?>
</ul>