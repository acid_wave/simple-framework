<script type="text/javascript">
	$(document).ready(function(){
		var uploadInput=$('input[name=avatar]');
		uploadInput.change(function(){
			var data=new FormData();
			data.append('act','avatarUpload');
			data.append('avatar',this.files[0]);
			$.ajax({
				url: '<?=HREF?>/?module=user',
				type: 'POST',
				data: data,
				cache: false,
				processData: false,
				contentType: false,
				dataType: 'json',
				success: function(respond, textStatus, jqXHR){
					if(respond.avatar!=undefined){
						window.location.reload();
					}
				},
				error: function(jqXHR, textStatus, errorThrown){
					console.log('Error request: ' + textStatus);
				}
			});
		});
	});
</script>
<div class="btn btn-primary btn-file">
	Browse
	<input type="file" name="avatar" value="upload">
</div>