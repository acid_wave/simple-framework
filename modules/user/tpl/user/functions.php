<?
function userErr($v){#Все ошибки пользователя
	static $err;
	if(!isset($err)){
		$err=array(
			'badName'=>"Incorrect username. You can use only letters and numbers.<br>User name length must be grater then 4 and less then 30.",
			'badMail'=>"Please, enter a valid email",
			'badPhone'=>"Incorrect phone",
			'badPas'=>"You entered an incorrect password! You can use only letters and numbers!<br>Password length must be grater then 4 and less then 30!",
			'mailExists'=>"Another user already use this email or name!",
			'dbError'=>"Database error. Try to register later.",
			'badActivate'=>"Your activation code or login is wrong.",
			'badLogin'=>"Wrong login or password!",
			'badSignup'=>"Registration error.",
			'badPasSet'=>"Wrong password!"
		);
	}
	return "<div class='error'>{$err[$v]}</div>";
}
