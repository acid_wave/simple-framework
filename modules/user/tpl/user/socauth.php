<style type="text/css">
	.socauth{display: inline-flex;vertical-align: middle;}
	.socauth a{
		display: inline-block;
		width: 50px;
		height: 50px;
		background-image: url('<?=HREF?>/modules/user/tpl/files/icons/socauth.png');
		margin: 0 2px 0 0;
	}
	.socauth a:hover{opacity: 0.8;}
	.socauth .mailru{background-position: -97px 0;}
	.socauth .facebook{background-position: -147px 0;}
	.socauth .vk{background-position: -194px 0;}
	.socauth .ok{background-position: -47px 0;}
	.socauth .google{background-position: -243px 0;}
</style>
<div class="socauth">
<?foreach($data->socauthlinks as $soc=>$link){?>
	<a href="<?=$link?>" class="<?=$soc?>" title="Авторизация через <?=$soc?>"></a>
<?}?>
</div>