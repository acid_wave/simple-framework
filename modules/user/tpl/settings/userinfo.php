<div class="row wrapper-user-info">
	<div class="col-12 mt-3 mb-5">
		<div class="container-user-info">
			<div class="row">
				<div class="col-12 col-md-3 col-lg-2 text-center">
					<img style="background-image: url('<?=$data->userinfo->avatar?>'); background-size: cover;" class="rounded-circle avatar">
				</div>
				<div class="col-12 col-md-4 col-lg-5 text-center text-md-left my-auto">
					<h2><?=$data->userinfo->altName?></h2>
					<small><?=$data->userinfo->comment?></small>
					<div>
					<?foreach ($data->userinfo->achievements as $achievement){?>
					<i class="achievement <?=$achievement->name?>" title="Received <?=$achievement->date?>"></i>
					<?}?>
					</div>
				</div>
				<div class="col-12 col-md-5 text-center text-md-left my-auto">
					<div class="row text-center">
						<div class="col-4">
							<i class="fa fa-download fa-lg" title="Downloads received"></i>
							<div><?=$data->userinfo->stat->imgsDownload?></div>
						</div>
						<div class="col-4">
							<i class="fa fa-heart-o fa-lg" title="Likes received"></i>
							<div><?=$data->userinfo->stat->imgsPin?></div>
						</div>
						<div class="col-4">
							<i class="fa fa-eye fa-lg" title="Views received"></i>
							<div><?=$data->userinfo->stat->postsViews?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-12">
		<ul class="nav nav-tabs">
			<li class="nav-item">
				<a class="nav-link<?=($data->act=='showFolders')||($data->act=='showPins')?' active':''?>" href="<?=url::userPhotos($data->userinfo->id)?>">Pinned&nbsp;<span class="badge badge-pill badge-info float-right ml-2"><?=$data->userinfo->stat->userPins?></span></a>
			</li>
			<li class="nav-item">
			  <a class="nav-link<?=$data->act=='listByUser'?' active':''?>" href="<?=url::author($data->userinfo->id)?>">Uploaded&nbsp;<span class="badge badge-pill badge-info float-right ml-2"><?=$data->userinfo->stat->postsCount?></span></a>
			</li>
			<li class="nav-item">
			  <a class="nav-link<?=$data->act=='listDownloadByUser'?' active':''?>" href="<?=url::downloads($data->userinfo->id)?>">Downloaded&nbsp;<span class="badge badge-pill badge-info float-right ml-2"><?=$data->userinfo->stat->downloadCount?></span></a>
			</li>
			<?if (!empty($data->mail)) if ($data->mail==$data->userinfo->mail){?>
			<li class="nav-item">
			  <a class="nav-link<?=$data->act=='settings'?' active':''?>" href="<?=HREF.'?module=user&act=settings'?>">Settings</a>
			</li>
			<?}?>
		</ul>
	</div>
</div>