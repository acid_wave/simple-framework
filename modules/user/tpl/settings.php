<?
$tpl->desc=$tpl->title="{$data->userinfo->altName}`s Profile on ".NAME;
$user=$data->userinfo;
$err=$data->err;
$msg=$data->msg;

include $template->inc('user/functions.php');

foreach($err as $er){?><?=userErr($er)?><br /><?}
$mes=array(
	'register'=>"Thank you for you registration!<br>On you email have been send activation code which is needed to chek your mail.",
	'activate'=>"Activation complite!<br>Thank you for you registration!",
	'newPas'=>"New password was generated and send on your email.",
	'settings'=>"Your settings have been successfully updated!",
);
if(@$mes[$msg]){?>
	<span class='message'><?=$mes[$msg]?></span><?
}?>
<div class="container">
	<?include $template->inc('settings/userinfo.php');?>
	<div class="row justify-content-center">
		<div class="col-12 text-center">
			<h1>Edit profile</h1>	
		</div>
		<div class="col-md-4 col-sm-4">
			<form action="<?=url::userSettings()?>" method="post">
				<div class="form-group">
					<label for="email">Email:</label>
					<div><?=@$user->mail?></div>
				</div>
				<div class="form-group">
					<label for="name">Name:</label>
					<input class="form-control" type="text" name="name" value="<?=@$user->name?>">
				</div>
				<div class="form-group">
					<label for="about">About me:</label>
					<textarea class="form-control" type="text" name="comment"><?=@$user->comment?></textarea>
				</div>
				<div class="form-group text-center">
					<input type="submit" name="submit" value="Save" class="btn btn-success form-control">
				</div>
			</form>
			<div class="form-group">
				<label for="avatar">Change avatar:</label><br>
				<?include $template->inc('user/avatar-upload.php');?>
				<?if($user->avatar!='no-avatar.jpg'){?>
				<a class="btn btn-danger" href="<?=HREF?>/?module=user&act=avatarRemove" onclick="return confirm('Are you sure?')">del</a>
				<?}?>
			</div>
		</div>
	</div>
</div>