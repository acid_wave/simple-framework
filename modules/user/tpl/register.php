<?
$tpl->desc=$tpl->title="Sign up. ".NAME;
$tpl->meta='<meta name="robots" content="noindex,follow" />';

include_once $template->inc('user/functions.php');

@$err=$data->err;
@$mail=$data->mail;
?>
<div class="row">
	<div class="col-md-4 col-sm-4"></div>
	<div class="col-md-4 col-sm-4">
		<h1 class="text-center">Sign up</h1>
	<?if(count($err))foreach($err as $er){?>
		<div class="alert alert-danger"><?=userErr($er)?></div>
	<?}?>
		<form action="<?=url::userRegister()?>" method="post">
			<div class="form-group">
				<label for="mail">Email</label>
				<input type="text" name="mail" class="form-control" placeholder="Email" required>
			</div>
			<div class="form-group">
				<label for="pas">Password</label>
				<input type="password" name="pas" class="form-control" placeholder="Password" required>
			</div>
			<div class="form-group">
				<input type="submit" name="submit" value="Sign up" class="btn btn-primary btn-lg btn-block">
				<?if(!empty($data->socauthlinks)){?>
				<h5 class="text-center">Or use</h5>
				<div class="text-center"><?include $template->inc('user/socauth.php');?></div>
				<?}?>
    		</div>
    		<input type="hidden" name="redir" value="<?=@$data->redir?>">
		</form>
	</div>
	<div class="col-md-4 col-sm-4"></div>
</div>
