<?
$tpl->desc=$tpl->title="Login. ".NAME;
$tpl->meta='<meta name="robots" content="noindex,follow" />';

@$err=$data->err;

include_once $template->inc('user/functions.php');
?>
<div class="row">
	<div class="col-md-4 col-sm-4"></div>
	<div class="col-md-4 col-sm-4">
		<h1 class="text-center">Login</h1>
	<?if(count($err))foreach($err as $er){?>
		<div class="alert alert-danger"><?=userErr($er)?></div>
	<?}?>
		<form action="<?=url::userLogin()?>" method="post" id="regFrom">
			<div class="form-group">
				<label for="mail">Email</label>
				<input type="text" name="mail" class="form-control" placeholder="Email">
			</div>
			<div class="form-group">
				<label for="pas">Password</label>
				<input type="password" name="pas" class="form-control" placeholder="Password">
			</div>
			<div class="form-group">
				<div class="checkbox">
					<label>
						<input type="checkbox" name="remember" value="1" checked=""> Remember me
					</label>
				</div>
			</div>
			<div class="form-group">
				<input type="submit" name="submit" value="Login" class="btn btn-success btn-lg btn-block">
				<h5 class="text-center">or</h5>
				<input type="submit" name="signup" value="Sign up" class="btn btn-primary btn-lg btn-block">
			</div>
			<div class="form-group">
				<a href="<?=url::userRestore()?>" rel="nofollow">restore password</a>
				<?include $template->inc('user/socauth.php');?>
			</div>
			<input type="hidden" name="redir" value="<?=@$data->redir?>">
		</form>
	</div>
	<div class="col-md-4 col-sm-4"></div>
</div>
