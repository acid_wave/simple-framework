<?
/*
 * Special panel
*/
if($data->access->specPanel){
	ob_start();
	?>
	<?
	if($data->access->addNews){?>
		<li>
			<?if(count($data->tbls) && count($data->tbls)>1){?>
				<a href="<?=url::post_adminAdd('',current($data->tbls))?>">Add article</a>
				<ul>
					<?foreach ($data->tbls as $tbl) {?>
					<li>
						<a href="<?=url::post_adminAdd('',$tbl)?>"><?=!$tbl?'post':$tbl?></a>	
					</li>
					<?}?>
				</ul>
			<?}else{?>
				<a href="<?=url::post_adminAdd()?>">Add article</a>
			<?}?>
		</li>
	<?}?>
	<?if($data->access->editCat){?>
		<li>
			<a href="<?=url::category_edit()?>">Add category</a>
			<?if(count($data->tbls)>1){?>
			<ul>
				<?
				foreach($data->tbls as $tbl){
					?><li><a href="<?=url::category_edit('','',$tbl?$tbl:'')?>"><?=$tbl?$tbl:'post'?></a></li><?
				}?>
			</ul>
			<?}?>
		</li>
	<?}?>
	<li>
		<a href="<?=url::author($data->user->id)?>">Articles stat</a>
		<ul>
			<li><a href="<?=url::author($data->user->id)?>">My articles</a></li>
			<?if($data->access->listOfArticles){?>
				<li><a href="<?=url::post_byUser()?>">Management of articles</a></li>
			<?}?>
			<?if($data->access->statPost){?>
				<li><a href="<?=url::post_stat()?>">Statistic of articles</a></li>
			<?}?>
		</ul>
	</li>
	<?if($data->access->editNews && count($data->tbls)>1){?>
		<li>
			<a href="#">List of posts</a>
			<ul>
			<?
			foreach($data->tbls as $tbl){
				?><li><a href="<?=url::postList_admin_viewer('',$tbl?$tbl:'')?>"><?=$tbl?$tbl:'post'?></a></li><?
			}?>
			</ul>
		</li>
	<?}?>
	<?if($data->access->moderateComments or $data->access->moderImage){?>
		<li>
		<a href="<?=url::imagesModerate()?>">Moderate</a>
		<ul>
			<?if($data->access->moderImage){?>
				<li><a href="<?=url::imagesModerate()?>">Post Moderation</a></li>
				<li><a href="<?=url::imagesModerateByCat()?>">Bulk Cat Moderation</a></li>
				<li><a href="<?=url::imagesBulkDell()?>">Bulk image Moderation</a></li>
			<?}?>
			<?if($data->user->rbac==1){?>
				<li><a onclick="return confirm()" href="<?=url::imagesDellNotApproved()?>">Delete not approved image</a></li>
				<li><a href="<?=url::images_dellWithoutFile()?>">Delete images from base without files on disk</a></li>
			<?}?>
			<?if($data->access->delNews){?>
				<li><a onclick="return confirm()" href="<?=url::postNoImageDell()?>">Delete post without image</a></li>
			<?}?>
			<?if($data->access->moderateComments){?>
				<li><a href="<?=url::comments_admin()?>">Moderate comments</a></li>
			<?}?>
		</ul>
		</li>
	<?}?>
	<?if($data->access->downloadImages){?>
		<li>
			<a href="<?=url::downloadImages()?>">Download</a>
			<ul>
				<?if($data->access->downloadImages){?>
					<li><a href="<?=url::downloadImages()?>">Download Images</a></li>
					<li><a href="<?=url::redownloadImages()?>">reDownload Images</a></li>
				<?}?>
				<?if($data->access->parseKeywords){?>
					<li><a href="<?=url::keywords_PostList()?>">Parse keywords</a></li>
				<?}?>
				<?if($data->access->parseRepl){?>
					<li><a href="<?=url::admin_traffic()?>">Repl stat</a></li>
				<?}?>
			</ul>
		</li>
	<?}?>
	<?if($data->access->loadStruct or $data->access->themesSet or $data->access->update or $data->access->autoPosting or $data->access->voteAccess){?>
		<li>
			<?if($data->access->themesSet){?>
				<a href="<?=url::admin_themes()?>">Edit site</a>
			<?}elseif($data->access->loadStruct){?>
				<a href="<?=url::post_uploadStruct()?>">Edit site</a>
			<?}else{?>
				<a href="#?>">Edit site</a>
			<?}?>
			<ul>
				<?if($data->access->themesSet){?>
					<li><a href="<?=url::admin_themes()?>">Set themes</a></li>
				<?}?>
				<li><a href="<?=url::post_uploadStruct()?>">Load Struct</a></li>
				<li><a href="<?=url::admin_multiPost()?>">Multi Post</a></li>
				<?if($data->access->update){?>
					<li><a href="<?=url::update()?>">Update</a></li>
				<?}?>
				<?if($data->access->autoPosting){?>
					<li><a href="<?=url::autoPosting()?>">AutoPosting</a></li>
				<?}?>
				<?if($data->access->voteAccess){?>
					<li><a href="<?=url::listVote()?>">Vote</a></li>
				<?}?>
				<?if($data->user->rbac==1){?>
					<li><a href="<?=url::plugins_cron()?>">Cron</a></li>
					<li><a href="<?=url::imagesBulkThumbnail()?>">Images Bulk Thumbnail</a></li>
					<li><a href="<?=url::imagesFillFilesize()?>">Images Fill Filesize</a></li>
					<li><a href="<?=url::postСreateTblPosts()?>">Сreate Table posts</a></li>
					<li><a href="<?=HREF?>?module=category/admin&act=createAutoSubCats" onclick="return confirm()">Сreate subCats</a></li>
				<?}?>
			</ul>
		</li>
	<?}?>
	<?if($data->access->privilegeSet){?>
		<li><a href="<?=url::userPrivileges()?>">Privileges</a></li>
	<?}?>
	<li><a href="/?module=user&act=logout">log out</a></li>
    <?if($data->user->rbac==1){?>
        <li><a>Server <?=$data->ip?></a></li>
    <?}?>
	<?
	$panelHtml=ob_get_clean();
	if($panelHtml!=''){?>
		<style type="text/css">
			#admin-panel-nav{background-color:white;width:100%;position: relative;z-index: 999999;opacity: 0.95;}
			#admin-panel-nav ul{list-style:none; display: inline-block; margin:0; padding:0}
			#admin-panel-nav ul a{display:block; color:#333; text-decoration:none; font-weight:700; font-size:12px; line-height:32px; padding:0 15px; font-family:"HelveticaNeue","Helvetica Neue",Helvetica,Arial,sans-serif}
			#admin-panel-nav ul li{position:relative; float:left; margin:0; padding:0;border-right: 1px solid black;}
			#admin-panel-nav ul li.current-menu-item{background:#ddd}
			#admin-panel-nav ul li:hover{background:#f6f6f6}
			#admin-panel-nav ul ul{display:none; position:absolute; top:100%; left:0; background:#fff; padding:0;z-index:99999;}
			#admin-panel-nav ul ul li{float:none; width:200px}
			#admin-panel-nav ul ul a{line-height:120%; padding:10px 15px}
			#admin-panel-nav ul ul ul{top:0; left:100%}
			#admin-panel-nav ul li:hover > ul{display:block}
		</style>
		<script>
		$('body').prepend('<nav id="admin-panel-nav"><ul><?=str_replace("\n",'',$panelHtml)?></ul></nav>');
		</script>
	<?}?>
<?}?>
