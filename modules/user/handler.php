<?php
/*
 * Role Based Access Control
 * */
namespace user;
use module,db,url,StdClass;

#Используем собственные функции
require_once(__DIR__.'/func.php');

require_once(module::$path.'/../core/mail/send.php');

class handler{
	function __construct(){
		$u=user::init();
		$this->user=&$u->user;
		$this->ut=&$u->ut;
		$this->access=&$u->access;
		$this->achievement=&$u->achievement;

		$this->headers=new \stdClass;
		$this->headers->cookie=new \stdClass;
	}
	/*
		авторизирует пользователя
		добавляет объект пользователя в объект класса handler
		! все это происходит в конструкторе, метод индекс ничего не делает
		! оставлена для совместимости, сейчас можно использовать просто user::init()
	*/
	function index($mail,$pas){
		return;
	}
	/*
		оставлена для совместимости
		новый вызов $o=user::init(); $o->rbac();
		описание см. func.php user::rbac()
	*/
	function rbac($role,$type=''){
		$user=user::init();
		return $user->rbac($role,$type='');
	}
	function menu(){
		return array('user'=>$this->user);
	}
	function panel(){
		require_once module::$path.'/posts/admin/func.php';
		$tbls=\posts_admin\getPrefixList();
		array_unshift($tbls, '');
		#возможность сделать update таблицы ролей
		if($this->user->rbac==1&&empty((array)$this->access)) {
			$this->access->specPanel=true;
			$this->access->update=true;	
		}
		return array(
			'user'=>$this->user,
			'access'=>$this->access,
			'tbls'=>$tbls,
			'ip'=>isset($_SERVER['HTTP_X_SERVER_IP'])?$_SERVER['HTTP_X_SERVER_IP']:$_SERVER['SERVER_ADDR'],
		);
	}
	function login($mail,$pas,$remember,$submit,$easy,$redir=false){
		$this->template='template';
		$hashLogin=hash('md5',$pas);
		$er=array();
		if($submit){
			if($mail && $pas){
				$hashLoginE=db::escape($hashLogin);
				$tUser=db::qfetch("SELECT * FROM ".PREFIX_SPEC."users WHERE hash='$hashLoginE' && mail='$mail'");
				if(!@$tUser->id){$er[]='badLogin';}
				else{
					$this->user=$tUser;
					#print "$mail,$pas,$remember,$submit";
					$time=$remember?"+5 year":"";
					@$this->headers->cookie->mail=array($mail,$time);
					$this->headers->cookie->pas=array($hashLogin,$time);
					if(!$easy) $this->headers->location=url::userSettings();
					if($redir) $this->headers->location=$redir;
					@db::query("UPDATE ".PREFIX_SPEC."users SET visit=NOW() WHERE id={$this->user->id} limit 1");
				}
			}else
				$er[]='badLogin';
		}

		return (object)array(
			'err'=>$er,
			'socauthlinks'=>module::execData('user/oauth',array('urlreceiver'=>url::oauthCallback(),'redir'=>HREF))->links,
			'redir'=>urlencode($redir),
		);
	}
	function register($umail,$pas,$name,$submit,$from='',$phone='',$address='',$comment='',$redir=false,$fredir=false){
		$redir=urldecode($redir);
		$this->template='template';
		if($redir==null) $redir=HREF;
		if($pas=='')$pas=mt_rand(50000,10000000);
		$hashLogin=hash('md5',$pas);
		$hashLoginE=db::escape($hashLogin);
		$pas=db::escape($pas);
		$name=db::escape(trim($name));
		$umail=trim($umail);
		$mail=db::escape($umail);
		$phone=preg_replace("![^0-9]!",'',trim($phone));
		$address=db::escape($address);
		$comment=db::escape($comment);
		$comment.=!empty($_COOKIE['utm_store'])?db::escape($_COOKIE['utm_store']):'';
		$er=array();
		if($submit){
			if(!preg_match("!^[^@]+@[^@.]+\.[^@]+$!",$mail)) $er[]='badMail';
			if(!preg_match('!^[\w\d ]{5,30}$!',$pas)) $er[]='badPas';
			if(strlen($phone)>0 && strlen($phone)<10){$er[]='badPhone';}
			list($id,$sameMail)=db::qrow("SELECT id,mail FROM ".PREFIX_SPEC."users WHERE mail='$mail'");
			if($sameMail){
				$er[]='mailExists';
				setcookie('tryUid',$id,strtotime("+1 year"),"/",COOKIE_HOST);
			}elseif(count($er)==0){
				$code=mt_rand();
				db::query("INSERT INTO ".PREFIX_SPEC."users SET hash='$hashLoginE',mail='$mail',`name`='$name',phone='$phone',address='$address',comment='$comment',code='$code',pas='$pas',regdate=NOW(),visit=NOW()");
				list($id)=db::qrow("SELECT id FROM ".PREFIX_SPEC."users WHERE hash='$hashLoginE' && mail='$mail'");
				if(!$id)$er[]='badSignup';
				else{
					@$this->headers->cookie->mail=array($mail,"+5 year");
					$this->headers->cookie->pas=array($hashLogin,"+5 year");
					$txt=strtr(
						module::exec('user',array('act'=>'tplMail','key'=>'activateTxt'),1)->str,
						array('%mail%'=>$umail,'%pas%'=>$pas,'%code%'=>$code)
					);
					set_time_limit(10);
					$subj=module::exec('user',array('act'=>'tplMail','key'=>'activateSubj'),1)->str;
					$sendStatus=mail_utf8(NAME,'noreply@'.SITE,$subj,$txt,$umail);
					set_time_limit(1);
					$this->headers->location=$redir;
				}
			}
			if($fredir){
				$this->headers->location=$redir;
			}
			if(!count($er))
				db::query("UPDATE ".PREFIX_SPEC."users SET visit=NOW() WHERE id=$id");
		}
		return (object)array(
			'err'=>$er,
			'from'=>$from,
			'mail'=>$mail,
			'name'=>$name,
			'phone'=>$phone,
			'address'=>$address,
			'comment'=>$comment,
			'redir'=>urlencode($redir),
			'socauthlinks'=>module::execData('user/oauth',['urlreceiver'=>url::oauthCallback(),'redir'=>$redir])->links,
		);
	}
	function logout(){
		$this->template='template';
		$this->headers->cookie->mail=$this->headers->cookie->pas=['','-1 day'];
		return array();
	}
	function activate($mail,$code){ # Активация
		$er=array();
		list($id)=db::qrow("SELECT id FROM ".PREFIX_SPEC."users WHERE mail='$mail' && code='$code'");
		if($id){
			db::query("UPDATE ".PREFIX_SPEC."users SET code='' WHERE id=$id");
			$this->headers->location=url::userSettings();
		}else
			$er[]='badActivate';
		return (object)array('err'=>$er);
	}
	function settings($pas,$newPas,$name,$oldHash,$comment,$submit){
		$this->template='template';
		$set=$er=$msg=array();
		if($submit){
			if($name){
				if(preg_match('!^[\w\d ]{3,30}$!',$name)){
					$set[]="`name`='$name'";
					$this->user->name=$name;
				}else
					$er[]='badName';
			}
			if($comment){
				$set[]="`comment`='".db::escape(strip_tags($comment,'<b><i><span>'))."'";
				$this->user->comment=$comment;
			}
			if($pas){
				$hash=db::escape(hash('md5',$pas));
				list($id)=db::qrow("SELECT id FROM ".PREFIX_SPEC."users WHERE hash='$hash' && mail='{$this->user->mail}'");
				if(!$id){$er[]='badPas';}
				else{
					if(preg_match('!^[\w\d ]{4,30}$!',$newPas)){
						$this->headers->cookie->pas=array($hash=hash('md5',$newPas),"+5 years");
						$newHash=db::escape($hash);
						$set[]="`pas`='".db::escape($newPas)."',`hash`='$newHash'";
					}else $er[]='badPas';
				}
			}
			if($set){
				if(empty($id)){
					list($id)=db::qrow("SELECT id FROM ".PREFIX_SPEC."users WHERE hash='$oldHash' && mail='{$this->user->mail}'");
				}
				if(!$id){$er[]='badPas';}
				else{
					$set=implode(',',$set);
					db::query("UPDATE ".PREFIX_SPEC."users SET $set WHERE id=$id LIMIT 1");
					$msg[]='settings';
					$this->achievement->checkInfo();
				}
			}
		}
		$this->user->avatar=avatar::exists($this->user->id,true);
		$this->user->stat=stat::get($this->user);
		$this->user->achievements= $this->achievement->getUserAchievements();
		return (object)array('userinfo'=>$this->user,'err'=>$er,'msg'=>$msg);
	}
	function chagePassword($pas,$newPas){
		$msg='';$this->template='template';
		if(!empty($newPas)){
			$hash=db::escape(hash('md5',$pas));
			list($id)=db::qrow("SELECT id FROM ".PREFIX_SPEC."users WHERE hash='$hash' && mail='{$this->user->mail}'");
			if(!$id){$msg='badOldPas';}
			else{
				if(preg_match('!^[\w\d ]{4,30}$!',$newPas)){
					$this->headers->cookie->pas=array($hash=hash('md5',$newPas),"+5 years");
					$newHash=db::escape($hash);
					db::query("UPDATE ".PREFIX_SPEC."users SET 
							`pas`='".db::escape($newPas)."',`hash`='$newHash' 
						WHERE 
							id=$id LIMIT 1
					");
					$msg='passUpdated';
				}else $msg='badPas';
			}
		}
		return (object)array('user'=>$this->user,'msg'=>$msg);
	}
	function restore($mailEsc){
		$this->template='template';
		$err=array();
		if($mailEsc){
			list($pass,$mail)=db::qrow("SELECT `pas`,`mail` FROM `".PREFIX_SPEC."users` WHERE `mail`='$mailEsc' LIMIT 1");
			if($pass){
				$txt=strtr(
					module::exec('user',array('act'=>'tplMail','key'=>'restoreTxt'),1)->str,
					array('$pas'=>$pass,'$mail'=>$mail)
				);
				set_time_limit(10);
				$subj=module::exec('user',array('act'=>'tplMail','key'=>'restoreSubj'),1)->str;
				if(!sendMail($subj,'',$txt,$mail)){
					$err[]='badMail';
				}
				set_time_limit(1);
			}else
				$err[]='badMail';
		}
		return (object)array('err'=>$err,'mail'=>$mailEsc);
	}
	/*
		загружает аватар пользователя
	*/
	function avatarUpload($email,$pas,$file){
		$this->template='';
		if($this->user->id){
			if($this->user->mail==$email&&$this->user->hash==$pas&&!empty($file)){
				$avatar=avatar::save($file,$this->user->id);
				$this->achievement->checkInfo();
				echo json_encode(['avatar'=>$avatar]); die;
			}
		}
		return;
	}
	/*
		удаляет аватар пользователя
	*/
	function avatarRemove($email,$pas){
		$this->template='';
		if($this->user->id){
			if($this->user->mail==$email&&$this->user->hash==$pas){
				avatar::remove($this->user->id);
			}
			$this->achievement->checkInfo();
		}
		$this->headers->location=url::userSettings();
	}
	/*
		обработчик шаблонов писем
	*/
	function tplMail($key){
		$data=new stdClass;
		$data->key=$key;
		return $data;
	}
	/*
		авторизация через соц. сети
	*/
	function socauth($soc,$code,$redir){
		$this->template='';

		if($soc){
			$userinfo=module::execData('user/oauth',array(
				'act'=>'userinfo','soc'=>$soc,'code'=>$code,'pageurl'=>url::oauthCallback())
			);
			if(!empty($userinfo->user->email)){
				authSave($this->headers->cookie,$soc,$userinfo->token,$userinfo->user);
			}
			if(!empty($redir))
				$this->headers->location=$redir;
		}

		return;
	}
}
