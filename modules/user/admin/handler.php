<?php
namespace user_admin;
use module,db,url;

include_once __DIR__.'/func.php';

class handler{
	function __construct(){
		$this->template='template';
		$this->uhandler=module::exec('user',array(),'data')->handler;
		$this->user=$this->uhandler->user;
	}
	function install(){
		sqlTablesInit();
		return array('instaled');
	}
	/*
		Список всех пользователей и установка прав автора
	*/
	function allUsers($users,$page,$order){
		if(!$this->uhandler->rbac('privilegeSet')){
			$this->headers->location=HREF;
			return;
		}
		#Записываем полученные изменения
		if(!empty($users)){
			foreach($users as $userId => $rbac){
				db::query("UPDATE `".PREFIX_SPEC."users` SET rbac=".(int)$rbac." WHERE id=".(int)$userId);
			}
		}
		#Получаем список всех пользователей кроме админа
			$users=array();
			db::query("SELECT * FROM `".PREFIX_SPEC."users` WHERE id!='{$this->user->id}' ORDER BY regdate $order LIMIT 20 OFFSET ".($page-1)*20);
			while($d=db::fetch()){
				$users[]=$d;
			}
		#Получаем типы пользователей
			db::query("SELECT * FROM `".PREFIX_SPEC."roles`");
			while($d=db::fetch()){
				$rolesTypes[$d->id]=$d->name;
			}
		#Получаем все права
			db::query("SELECT * FROM `".PREFIX_SPEC."rolesAlloc`");
			while($d=db::fetch()){
				$roles[$d->roleid][]=$d->actid;
			}
		#Формируем постраничный вывод
			db::query("SELECT id FROM `".PREFIX_SPEC."users` WHERE id!='{$this->user->id}'");
			$count=db::affected();
			$paginator=module::execStr('plugins/paginator',
			[
				'page'=>$page,
				'num'=>20,
				'count'=>$count,
				'uri'=>'?module=user/admin&act=allUsers&order='.$order.'&page=%d',
			]
			);
		return array(
			'users'=>$users,
			'roles'=>$roles,
			'rolesTypes'=>$rolesTypes,
			'paginator'=>$paginator,
			'count'=>$count,
			'order'=>$order,
		);
	}
}
