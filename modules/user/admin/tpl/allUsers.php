<div class="row">
	<div class="col-md-9 col-sm-12">
		<form action='' method='post' class="form">
			<div class="table-responsive">
				<table class="table table-bordered">
					<thead>
						<tr>
							<td>Name</td>
							<td>Email</td>
							<td>Registration Date
							<? if ($data->order == 'asc') {?>
							<a class="btn btn-default btn-xs" href="/?module=user/admin&act=allUsers&order=desc" role="button"><i class="fa fa-arrow-up"></i></a>
							<? }
							else {?>
							<a class="btn btn-default btn-xs" href="/?module=user/admin&act=allUsers&order=asc" role="button"><i class="fa fa-arrow-down"></i></a>
							<? }?>
							</td>
       						<? foreach ($data->roles as $k => $v) {
                             print "<td>$k {$data->rolesTypes[$k]}</td>";
                             } ?>
    						</tr>
					</thead>
					<tbody>
					<?foreach($data->users as $user){?>
						<tr>
							<td><?= $user->name?></td>
							<td><?= $user->mail?></td>
							<td><?= $user->regdate?></td>
            					<?foreach($data->roles as $k=>$v){?>
							<td><input type="radio" name='users[<?=$user->id?>]' <?=$user->rbac==$k?"checked":''?> value="<?=$k?>"></td>
							<?}?>
        					</tr>
					<?}?>
					</tbody>
				</table>
			</div>
			<input class="button" type="submit" value="save">
		</form>
		<?=@$data->paginator?>
		<?if($data->count){?>
			<small>items:&nbsp<i><?= $data->count?></i></small>
		<?}?>
	</div>
	<div class="col-md-3 col-sm-12">
	<? foreach ($data->roles as $role => $acts) {
    print "$role => " . implode(", ", $acts) . "<hr>";
	}?>
	</div>
</div>