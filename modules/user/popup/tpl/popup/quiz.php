<script type="text/javascript">
	$(document).ready(function(){
		$('input.quiz-button').parents('.choose').click(function(){
			var button=$(this).find('input.quiz-button');
			if(button.attr('data-location')=='/'){
				$('#myModal').trigger('reveal:close');
			}else{
				document.location="<?=url::user_popup_reg()?>";
				return false;
			}
		});
		$('input.quiz-button').click(function(){
			var button=$(this);
			if(button.attr('data-location')=='/'){
				$('#myModal').trigger('reveal:close');
			}else{
				document.location="<?=url::user_popup_reg()?>";
				return false;
			}
		});
		
	});
</script>
<style>
.marginRight{
	margin:0 1% 0 0;
}
.cell{
	width:50%;
	float:left;
}
.cell h2{
	font-size: 2.25em;
	font-family: 'Open Sans', sans-serif;
	font-weight: 900 !important;
}
.choose{
	width:90%;
	margin:0 5%;
	cursor: pointer;
	background-color: #f7f7f7;
	border-radius: 5px;
	text-align:center;
	box-shadow: 0px 3px 0px 0px rgba(0, 0, 0, 0.05);
}
.hover-border{
	height:300px;
	border-radius: 5px;
	cursor: pointer;
	border: 0.3em solid transparent;
}
.hover-border:hover{
	border: 0.3em solid #EF7B7B;
}
.quizButton{
	color: #fff;
	text-decoration: none;
	border-radius: 6px;
	font-weight: 800;
	outline: 0;
	border: 0;
	cursor: pointer;
	font-size: 1.35em;
	padding: 0.5em 0.7em;
	vertical-align: middle;
	min-width: 6em;
	background: #e1525c !important;
}
.user_popup h1, h2{
	margin:30px !important;
}
.user_popup h2{
	height:150px;
}
</style>
<div class="user_popup">
	<h1>Do you want to get 30 free images every week?</h1>
		<div class="cell">
			<section class="choose hover-border">
				<h2>Yes</h2>
				<input class="quizButton quiz-button" 
					data-quizid="1" data-result="yes" data-location="<?=url::user_popup_reg()?>"
					type="button" value="Choose">
			</section>
		</div>
		<div class="cell">
			<section class="choose hover-border">
				<h2>No</h2>
				<input class="quizButton quiz-button" 
					data-quizid="1" data-result="no" data-location="/"
					type="button" value="Choose">
			</section>
		</div>

	<div style="clear:both;"></div>	
</div> 
