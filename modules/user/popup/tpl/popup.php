<?
if($data->geoCheck)return;
?>
<style>
	.reveal-modal-bg { 
		position: fixed; 
		height: 100%;
		width: 100%;
		background: #000;
		background: rgba(0,0,0,.8);
		z-index: 100;
		display: none;
		top: 0;
		left: 0; 
		}
	
	.reveal-modal {
		visibility: hidden;
		top: 100px; 
		left: 50%;
		margin-left: -460px;
		width: 925px;
		background: #eee;
		position: absolute;
		z-index: 100501;
		padding: 30px 40px 34px;
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
		border-radius: 5px;
		-moz-box-shadow: 0 0 10px rgba(0,0,0,.4);
		-webkit-box-shadow: 0 0 10px rgba(0,0,0,.4);
		-box-shadow: 0 0 10px rgba(0,0,0,.4);
		}
		

	.reveal-modal .close-reveal-modal {
		font-size: 22px;
		line-height: .5;
		position: absolute;
		top: 8px;
		right: 11px;
		color: #aaa;
		text-shadow: 0 -1px 1px rbga(0,0,0,.6);
		font-weight: bold;
		cursor: pointer;
		}
</style>
<script type="text/javascript" src="<?=HREF."/".str_replace(PATH,'',__DIR__)?>/files/jquery.reveal.js"></script>
<div id="myModal" class="reveal-modal">
	<?
	include $template->inc('popup/quiz.php');
	?>
	<a class="close-reveal-modal">&#215;</a>
</div>
<script>
    // set/get cookies
	function setCookie(cname,cvalue,exdays){
		var d = new Date();
		d.setTime(d.getTime()+(exdays*24*60*60*1000));
		var expires = "expires="+d.toGMTString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	}
	function getCookie(cname){
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++){
			var c = ca[i].trim();
			if (c.indexOf(name)==0) return c.substring(name.length,c.length);
		}
		return "";
	}
    //popup survey
	if(!getCookie('hideModal'))
    	$('#myModal').reveal($(this).data());
    setCookie('hideModal',1,360);
</script> 
