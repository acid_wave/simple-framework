<?php
namespace user_popup;
use module,db,url,StdClass; 
include_once __DIR__.'/../func.php';
class handler{
	function __construct(){
		$this->template='';
		$quest=array(
			'reciveFreePhoto'=>array(
				"Do you want to get 30 free images every week?",
				array(
					'yes'=>array('Yes','quiz_email.php'),
					'no'=>array('No','quiz.php?quiz=1'),
				),
			),
			'clipartUsage'=>array(
				"You need clip art for:",
				array(
					'presentation'=>"Presentation",
					'website'=>"Web site article",
					"photo"=>"Draw new image",
					"other"=>"Other",
				),
			),
		);
		$quizId=array_keys($quest);
		$this->quest=array_values($quest);
	}
	function popup($questName){
		return [
			'geoCheck'=>in_array(\user\geoip(),explode(",","us,uk,ca,de,au")),
			'quest'=>$this->quest,
			'questName'=>$questName,
		];
	}
	function email(){
		return [
			
		];
	}
}
