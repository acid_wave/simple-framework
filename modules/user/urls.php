<?php
route::set('^/register.html',array('module'=>'user','act'=>'register'),999);
route::set('^/login.html',array('module'=>'user','act'=>'login'),999);
route::set('^/restore.html',array('module'=>'user','act'=>'restore'),999);
route::set('^/oauth/([^\/\?]*)/?',array('module'=>'user','act'=>'socauth','soc'),999);

#url модуля
$urls->userRegister=HREF.'/register.html';
$urls->userRestore=HREF.'/restore.html';
$urls->userSettings=HREF.'?module=user&act=settings';
$urls->userChagePassword=HREF.'?module=user&act=chagePassword';
$urls->userLogout=HREF.'?module=user&act=logout';
$urls->userActivate=HREF.'?module=user&act=activate&mail=%mail%&code=%code%';
$urls->oauthCallback=HREF.'/oauth';

function url_userLogin($redir=''){
	if($redir===true) $r=$_SERVER['REQUEST_URI'];
	elseif(is_string($redir)&&$redir!='') $r=$redir;

	return HREF.'/login.html'.(isset($r)?'?redir='.urlencode($r):'');
}
