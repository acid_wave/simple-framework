<?php
namespace user_oauth;
use module,db,url;

function buildRedirectUrl($url,$soc){
	return $url.'/'.$soc;
}

function curlRequest($fields,$url,$httpType="POST"){
	if(empty($url)) return;
	$curl=curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER=>true,
		CURLOPT_HEADER=>false,
		CURLOPT_SSL_VERIFYPEER=>false
	));
	if($httpType=='POST'){
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($fields));
	}else{
		curl_setopt($curl, CURLOPT_URL, $url.(!empty($fields)?'?'.http_build_query($fields):''));
	}
	
	$resp=json_decode(curl_exec($curl));
	curl_close($curl);
	return $resp;
}

?>