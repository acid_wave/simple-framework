<?php
/*
	примеры констант для config.php
*/
# mail.ru
define('AUTH_MAILRU_CLIENT_ID','');
define('AUTH_MAILRU_PRIVATE_KEY','');
define('AUTH_MAILRU_SECRET_KEY','');

# facebook
define('AUTH_FACEBOOK_CLIENT_ID', '');
define('AUTH_FACEBOOK_CLIENT_SECRET', '');

# vk
define('AUTH_VK_CLIENT_ID', '');
define('AUTH_VK_CLIENT_SECRET', '');

# ok.ru
define('AUTH_OK_CLIENT_ID','');
define('AUTH_OK_SECRET_KEY','');
define('AUTH_OK_PUBLIC_KEY','');

# google
define('AUTH_GOOGLE_CLIENT_ID','');
define('AUTH_GOOGLE_CLIENT_SECRET','');
?>