<?php
namespace user_oauth;
use module,db,url;
/*
	используемые константы (должны быть определены в config.php):
*/

include __DIR__.'/func.php';

class handler{
	function __construct(){
		$this->template='';#Определяем в какой шаблон будем вписывать
		$this->headers=new \stdClass;

		$this->links=array(
			'mailru'=>array(
				'check_constant'=>'AUTH_MAILRU_CLIENT_ID',
				'code'=>array(
					'link'=>'https://connect.mail.ru/oauth/authorize',
					'params'=>array(
						'client_id'=>defined('AUTH_MAILRU_CLIENT_ID')?AUTH_MAILRU_CLIENT_ID:'',
						'response_type'=>'code',
						'redirect_uri'=>''
					)
				),
				'token'=>array(
					'link'=>'https://connect.mail.ru/oauth/token/',
					'params'=>array(
						'client_id'=>defined('AUTH_MAILRU_CLIENT_ID')?AUTH_MAILRU_CLIENT_ID:'',
						'client_secret'=>defined('AUTH_MAILRU_SECRET_KEY')?AUTH_MAILRU_SECRET_KEY:'',
						'grant_type'=>'authorization_code',
						'code'=>'',
						'redirect_uri'=>''
					)
				),
				'func_api_userinfo'=>function($response){
					$params=array(
						'method'=>'users.getInfo',
						'app_id'=>defined('AUTH_MAILRU_CLIENT_ID')?AUTH_MAILRU_CLIENT_ID:'',
						'session_key'=>$response->access_token,
						'secure'=>1,
					);
					//generate sig
					$sig='';
					ksort($params);
					foreach ($params as $key => $v) {
						$sig.="{$key}={$v}";
					}
					$params['sig']=md5($sig.(defined('AUTH_MAILRU_SECRET_KEY')?AUTH_MAILRU_SECRET_KEY:''));
					$resp=curlRequest($params,'http://www.appsmail.ru/platform/api');
					return $resp[0];
				}
			),
			'ok'=>array(
				'check_constant'=>'AUTH_OK_CLIENT_ID',
				'code'=>array(
					'link'=>'https://connect.ok.ru/oauth/authorize',
					'params'=>array(
						'client_id'=>defined('AUTH_OK_CLIENT_ID')?AUTH_OK_CLIENT_ID:'',
						'response_type'=>'code',
						'redirect_uri'=>'',
						'scope'=>'VALUABLE_ACCESS,GET_EMAIL'
					)
				),
				'token'=>array(
					'link'=>'https://api.ok.ru/oauth/token.do',
					'params'=>array(
						'code'=>'',
						'client_id'=>defined('AUTH_OK_CLIENT_ID')?AUTH_OK_CLIENT_ID:'',
						'client_secret'=>defined('AUTH_OK_SECRET_KEY')?AUTH_OK_SECRET_KEY:'',
						'redirect_uri'=>'',
						'grant_type'=>'authorization_code'
					)
				),
				'func_api_userinfo'=>function($response){		
					$params=array(
						'application_key'=>defined('AUTH_OK_PUBLIC_KEY')?AUTH_OK_PUBLIC_KEY:'',
						'fields'=>'uid,name,email,url_profile',
						'format'=>'json',
						'uids'=>''
					);
					//generate sig
					$sig='';
					ksort($params);
					foreach ($params as $key => $v) {
						$sig.="{$key}={$v}";
					}
					$params['sig']=strtolower(md5($sig.md5($response->access_token.(defined('AUTH_OK_SECRET_KEY')?AUTH_OK_SECRET_KEY:''))));
					$params['access_token']=$response->access_token;
					$resp=curlRequest($params,'https://api.ok.ru/api/users/getCurrentUser','GET');

					return $resp;
				}
			),
			'vk'=>array(
				'check_constant'=>'AUTH_VK_CLIENT_ID',
				'code'=>array(
					'link'=>'https://oauth.vk.com/authorize',
					'params'=>array(
						'client_id'=>defined('AUTH_VK_CLIENT_ID')?AUTH_VK_CLIENT_ID:'',
						'response_type'=>'code',
						'redirect_uri'=>'',
						'scope'=>'email',
						'v'=>'5.60'
					)
				),
				'token'=>array(
					'link'=>'https://oauth.vk.com/access_token',
					'params'=>array(
						'client_id'=>defined('AUTH_VK_CLIENT_ID')?AUTH_VK_CLIENT_ID:'',
						'client_secret'=>defined('AUTH_VK_CLIENT_SECRET')?AUTH_VK_CLIENT_SECRET:'',
						'code'=>'',
						'redirect_uri'=>'',
					)
				),
				'func_api_userinfo'=>function($response){
					/*
					$params=array(
						'access_token'=>$response->access_token,
						'v'=>'5.60',
						'fields'=>'contacts'
					);
					$resp=curlRequest($params,'https://api.vk.com/method/users.get');
					*/
					
					return !empty($response->email)?$response:'';
				}
			),
			'facebook'=>array(
				'check_constant'=>'AUTH_FACEBOOK_CLIENT_ID',
				'code'=>array(
					'link'=>'https://www.facebook.com/dialog/oauth',
					'params'=>array(
						'client_id'=>defined('AUTH_FACEBOOK_CLIENT_ID')?AUTH_FACEBOOK_CLIENT_ID:'',
						'response_type'=>'code',
						'redirect_uri'=>'',
						'scope'=>'email,public_profile'
					)
				),
				'token'=>array(
					'link'=>'https://graph.facebook.com/v2.8/oauth/access_token',
					'params'=>array(
						'client_id'=>defined('AUTH_FACEBOOK_CLIENT_ID')?AUTH_FACEBOOK_CLIENT_ID:'',
						'client_secret'=>defined('AUTH_FACEBOOK_CLIENT_SECRET')?AUTH_FACEBOOK_CLIENT_SECRET:'',
						'code'=>'',
						'redirect_uri'=>'',
					)
				),
				'func_api_userinfo'=>function($response){
					$params=array(
						'access_token'=>$response->access_token,
						'fields'=>'id,name,email'
					);
					$resp=curlRequest($params,'https://graph.facebook.com/me','GET');
					if(isset($resp->id))
						$resp->avatar='http://graph.facebook.com/'.$resp->id.'/picture?type=large';
					return $resp;
				}
			),
			'google'=>array(
				'check_constant'=>'AUTH_GOOGLE_CLIENT_ID',
				'code'=>array(
					'link'=>'https://accounts.google.com/o/oauth2/auth',
					'params'=>array(
						'client_id'=>defined('AUTH_GOOGLE_CLIENT_ID')?AUTH_GOOGLE_CLIENT_ID:'',
						'response_type'=>'code',
						'redirect_uri'=>'',
						'scope'=>'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile'
					)
				),
				'token'=>array(
					'link'=>'https://accounts.google.com/o/oauth2/token',
					'params'=>array(
						'client_id'=>defined('AUTH_GOOGLE_CLIENT_ID')?AUTH_GOOGLE_CLIENT_ID:'',
						'client_secret'=>defined('AUTH_GOOGLE_CLIENT_SECRET')?AUTH_GOOGLE_CLIENT_SECRET:'',
						'code'=>'',
						'redirect_uri'=>'',
						'grant_type'=>'authorization_code'
					)
				),
				'func_api_userinfo'=>function($response){
					$params=array(
						'access_token'=>$response->access_token,
					);
					$resp=curlRequest($params,'https://www.googleapis.com/oauth2/v1/userinfo','GET');
					return $resp;
				}
			),
		);
	}
	/*
		генерирует список ссылок для авторизации через соц. сети
	*/
	function index($urlreceiver,$redir){#Делаем все обработки для вывода данных
		if($urlreceiver){
			@session_start();
			$_SESSION['oauth_after_redir']=$redir;
		}
		$links=array();
		foreach ($this->links as $key => $v) {
			if(!@constant($v['check_constant'])) continue;
			$v['code']['params']['redirect_uri']=buildRedirectUrl($urlreceiver,$key);
			if(!isset($v['code']['link'])) continue;
			$links[$key]=$v['code']['link'].'?'.http_build_query($v['code']['params']);
		}

		return (object)array('links'=>$links);
	}
	/*
		1. делает запрос для получения ключа доступа (access_token)
		2. далает запрос к API для получения данных польззователя
	*/
	function userinfo($soc,$code,$page){
		if(!isset($this->links[$soc]['token'])||!$code) return;

		$this->links[$soc]['token']['params']['code']=$code;
		$this->links[$soc]['token']['params']['redirect_uri']=buildRedirectUrl($page,$soc);

		$resp=curlRequest($this->links[$soc]['token']['params'],$this->links[$soc]['token']['link']);
		
		if(!isset($resp->access_token))
			return false;

		return array(
			'user'=>$this->links[$soc]['func_api_userinfo']($resp),
			'token'=>$resp->access_token,
		);
	}
}
