<?php
/*
* Должен возвращать
* $this->data - объект обработанных входных переменных
* $this->act - какую функцию обработки используем
*/
class user_oauth extends control{
	function __construct($input){# $input - объект входных переменных от других модулей
		$this->data=new stdClass;
		if(empty($input->act)) $input->act='index';
		
		/*
			определяет список доступных действий (act)
			если запрошенного act нет в спсике, то будет вызван метод совпадающий с названием данного класса
		*/
		if($input->act=='index'){
			$this->data=(object)array(
				'urlreceiver'=>!empty($input->urlreceiver)?$input->urlreceiver:false,
				'redir'=>!empty($input->redir)?$input->redir:false,
			);
		}elseif($input->act=='auth'){
			$this->data=(object)array(
				'soc'=>!empty($input->soc)?$input->soc:false,
			);
		}elseif($input->act=='userinfo'){
			$this->data=(object)array(
				'soc'=>!empty($input->soc)?$input->soc:false,
				'code'=>!empty($input->code)?$input->code:false,
				'page'=>!empty($input->pageurl)?$input->pageurl:'',
			);
		}else
			$this->act=false;
	}
}