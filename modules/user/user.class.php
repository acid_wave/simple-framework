<?php
namespace user;
use db,StdClass;

require_once __DIR__.'/func.php';

/*
* Управление пользователем
* - получение данных об авторизированном пользователе
* - получение прав доступа пользователя
*/
class user
{
	static $instance;
	
	function __construct($mail='',$pas=''){
		#начальные значения для незарегистрированных пользователей
		$this->user=new StdClass;
		$this->user->id=0;
		$this->user->rbac=-1;
		$this->user->avatar='';
		$this->ut=[];
		#Проверка пользователя на логин
		$authData=$this->_readAuth($mail,$pas);
		if(!empty($authData->email) && !empty($authData->pas)){
			$d=db::qfetch("SELECT * FROM ".PREFIX_SPEC."users WHERE hash='{$authData->pas}' && mail='{$authData->email}' LIMIT 1");
			if(isset($d->id)){
				db::query("UPDATE ".PREFIX_SPEC."users SET visit=NOW() WHERE id='{$d->id}' LIMIT 1");
				$this->user=$d;
				$this->user->avatar=avatar::exists($this->user->id,true);
				$this->ut=self::loadUt($this->user->rbac);
				$this->user->altName=self::setAltName($this->user);
			}
		}
		$this->access=new \user\_access($this->ut);
		$this->achievement=new \user\achievement($this->user->id);
		@setcookie('visit',time(),strtotime('+1 year'),"/",COOKIE_HOST);
	}
	/*
		Инициализирует объект пользователя или возвращает уже существующий
	*/
	static function init($mail='',$pas=''){
		if(self::$instance===null)
			self::$instance=new user($mail,$pas);
		return self::$instance;
	}
	/*
		получение доступных действий для роли пользователя
	*/
	static function loadUt($rbac){
		$ut=[];
		db::query("SELECT `actid` FROM `".PREFIX_SPEC."rolesAlloc` WHERE `roleid`='{$rbac}'");
		while ($d=db::fetch()) {
			$ut[$rbac][]=$d->actid;
		}
		return $ut;
	}
	function _readAuth($mail,$pas){
		return (object)[
			'email'=>empty($mail)?(empty($_COOKIE['mail'])?'':$_COOKIE['mail']):$mail,
			'pas'=>empty($pas)?(empty($_COOKIE['pas'])?'':$_COOKIE['pas']):$pas,
		];
	}
	/*
		Проверяет разрешение для указанного действия
			- можно указать ID роли [опционально]
	*/
	function rbac($role,$type=''){
		if($type==''){
			$type=$role;
			$role=$this->user->rbac;
		}
		foreach($this->ut as $rbac=>$ar){
			foreach($ar as $access){
				$types[$access][$rbac]=1;
			}
		}
		# проверка списка прав
		if(is_array($type))
			foreach ($type as $t) {
				if($res=isset($types[$t][$role])) break;
			}
		else
			$res=isset($types[$type][$role]);

		#для действия "update" = true, если не существует ни одной роли
		if(empty($this->ut)&&$type=='update') return true;

		return $res;
	}
	/*
		получает список ролей для которых доступен тип действий: $type
	*/
	function rbacByUT($type){
		$result=[];
		db::query("SELECT roleid FROM `".PREFIX_SPEC."rolesAlloc` WHERE actid='{$type}'");
		while ($d=db::fetch()) {
			$result[$d->roleid]=$d->roleid;
		}
		return $result;
	}
	/*
		Генерирует Alt Name из Email
		например myname@example.com -> altName = myname
	*/
	static function setAltName($u){
		if(is_object($u)){
			if(isset($u->name)) return $u->name;
			if(isset($u->mail)) $email=$u->mail;
			else return 'Anonym';
		}else
			$email=$u;
		return preg_replace('!@.+?$!i', '', $email);
	}
}
/**
* Класс для формирования объекта со списком доступов пользователя
*/
class _access
{
	function __construct($ut){
		if($_ut=current($ut))
		foreach ($_ut as $act) {
			$this->{$act}=true;
		}
	}
	function __get($name){
		return false;
	}
}
/**
* Собирает статистику пользователя
* - кол-во скачиваний картинки
* - кол-во просмотров постов
* - кол-во постов пользователя
* - кол-во картинок пользователя
* - кол-во pin картинок
*/
class stat{
	/**
	 * Получает статистику просмотров и скачиваний постов и картинок пользователя
	 * @param user object &$u 
	 * @return object
	 */
	static function get(&$u){
		if(!$u->id) return;
		if(!isset($u->stat)) $u->stat=new StdClass;
		$u->stat->postsCount=self::postsCount($u);
		$u->stat->imgsDownload=self::imgsDownloadCount($u);
		$u->stat->postsViews=self::postsViewsCount($u);
		$u->stat->imgsPin=self::imgsPinCount($u);
		$u->stat->userPins=self::userPinCount($u);
		$u->stat->downloadCount=self::userDownloadCount($u);
		return $u->stat;
	}
	static function imgsCount($u){
		list($c)=db::qrow("SELECT COUNT(id) FROM ".PREFIX_SPEC."imgs WHERE `uid`='{$u->id}'");
		return (int)$c;
	}
	static function postsCount($u){
		list($c)=db::qrow("SELECT COUNT(id) FROM `post` WHERE `published`=1 && `user`='{$u->id}'");
		return (int)$c;
	}
	static function imgsDownloadCount($u){
		list($c)=db::qrow("SELECT SUM(statDownload) FROM ".PREFIX_SPEC."imgs WHERE `uid`='{$u->id}'");
		return (int)$c;
	}
	static function postsViewsCount($u){
		list($c)=db::qrow("SELECT SUM(statViews) FROM `post` WHERE `published`=1 && `user`='{$u->id}'");
		return (int)$c;
	}
	static function imgsPinCount($u){
		list($c)=db::qrow("
			SELECT COUNT(i.id) FROM ".PREFIX_SPEC."imgs i
			INNER JOIN ".PREFIX_SPEC."like2img li ON li.imid=i.id && i.uid='{$u->id}'");
		return (int)$c;
	}
	static function userPinCount($u){
		list($c)=db::qrow("SELECT COUNT(imid) FROM `".PREFIX_SPEC."like2img` WHERE `uid`='{$u->id}'");
		return (int)$c;
	}
	static function userDownloadCount($u){
		list($c)=db::qrow("SELECT COUNT(imid) FROM `".PREFIX_SPEC."userDownloads` WHERE `uid`='{$u->id}'");
		return (int)$c;
	}
}