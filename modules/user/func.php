<?php
namespace user;
use module,db,url,StdClass;

require_once __DIR__.'/user.class.php';
require_once __DIR__.'/achievement.class.php';

/**
 * Авторизация на сайте после прихода от соц сети
 */
function authSave(&$cookie,$service,$token,$userinfo){
	if(empty($userinfo->email)) return;
	$hash=md5($service.$userinfo->email.$token);

	$cookie->mail=array($userinfo->email,"");
	$cookie->pas=array($hash,"");

	$userinfo->email=db::escape($userinfo->email);
	$userinfo->name=db::escape($userinfo->name);
	$tbl=PREFIX_SPEC."users";
	$row=db::qfetch("SELECT id FROM `{$tbl}` WHERE mail='".$userinfo->email."'");
	if(isset($row->id)){
		$id=$row->id;
		db::query(sprintf("UPDATE `{$tbl}` SET name='%s',visit=NOW(),hash='%s' WHERE id=%d",$userinfo->name,$hash,$row->id));
	}else{
		db::query(sprintf("INSERT INTO `{$tbl}` (name, mail, regdate, visit, rbac, hash, service) VALUES ('%s','%s',NOW(),NOW(),%d,'%s','%s')", $userinfo->name,$userinfo->email,0,$hash,$service));
		$id=db::insert();
	}
	if(!empty($id)) avatar::save($userinfo->avatar,$id);
}

class avatar{
	static $storage=PATH.'modules/user/files/avatar/';
	static function save($file,$uid){
		include module::$path.'/images/func.php';
		\images\resize($file,$avatar=self::$storage.$uid.'.jpg',200,0);
		return $avatar;
	}
	static function exists($uid,$noavatar=false){
		$avatarFilename=$uid.'.jpg';
		if(file_exists($file=self::$storage.$avatarFilename))
			return HREF.'/modules/user/files/avatar/'.$avatarFilename;
		else{
			if(!$noavatar)
				return false;
			else
				return HREF.'/modules/user/files/avatar/no-avatar.jpg';
		}
	}
	static function remove($uid){
		@unlink(self::$storage.$uid.'.jpg');
	}
}

/**
 * получает данные 1 -го или нескольких пользователей
 * @param array|int $uid 
 * @param none|array &$users [optional]
 * @return array
 */
function getUser($uid,&$users=false){
	if(empty($uid)) return [];
	if($users===false) $users=[];
	$sqlWhere=is_array($uid)?"id IN (".implode(",",$uid).")":"id='$uid'";
	db::query("SELECT `id`,`name`,`mail`,`visit`,`rating`,`comment` FROM `".PREFIX_SPEC."users` WHERE {$sqlWhere}");
	while($d=db::fetch()){
		$d->avatar=avatar::exists($d->id,true);
		$d->altName=user::setAltName($d);
		$d->stat=stat::get($d);
		$ua = new achievement($d->id);
		$d->achievements = $ua->getUserAchievements();
		$ua->destroy();
		$users[$d->id]=$d;
	}

	return is_array($uid)?$users:current($users);
}

function geoip($ip=''){
	/*
	 * install/update as root:
	 * wget -N http://geolite.maxmind.com/download/geoip/database/GeoLiteCountry/GeoIP.dat.gz
	 * gunzip GeoIP.dat.gz
	 * mv GeoIP.dat /usr/local/share/GeoIP/
	 * 
	 * usage:
	 *	print \user\geoip();
	*/
	if($ip=='') $ip=$_SERVER['REMOTE_ADDR'];
	if(!extension_loaded('geoip')){
		include_once(__DIR__."/geoip.inc");	
		$gi=geoip_open("/usr/local/share/GeoIP/GeoIP.dat", GEOIP_STANDARD);
		$code=geoip_country_code_by_addr($gi, $ip);
		geoip_close($gi);
	}else{
		$code=geoip_country_code_by_name($ip);
	}
	return strtolower($code);
}
