<?php
namespace user;
use db,StdClass;

require_once __DIR__.'/func.php';
/*
 * Класс ачивок
 */
class achievement {

	private $achievement;
	private $uid;
	
	function __construct($user=0) {
		$this->achievement = new StdClass;
		$this->uid = isset($user)?$user:0;
		db::query("SELECT * FROM `".PREFIX_SPEC."achievement`");
		while ($achieve=db::fetch()){
		$this->achievement->{$achieve->name}=$achieve;
		}
		$this->initUA();
	}
	function __destruct() {
		unset($this->uid);
		unset($this->achievement);
	}
	public function destroy() {
		$this->__destruct();
	}
	public function initUA() {
		if (!$this->uid) return false;
		list($ua)=db::qrow("SELECT `achievements` FROM `".PREFIX_SPEC."users` WHERE `id`={$this->uid}");
		$ua = json_decode($ua);
		if ($ua) {
			foreach ($ua as $name => $date) {
				$this->achievement->{$name}->date=$date;
			}
		}
	}
	public function getAchievement($achievement) {
	if (isset($this->achievement->{$achievement}->date)){
			return $this->achievement->{$achievement}->date;
		}
		return false;
	}
	public function setAchievement($achievement) {
		if (!$this->uid) return false;
		if (!empty($this->achievement->{$achievement}->date)) return $this->achievement->{$achievement}->date;
		$this->achievement->{$achievement}->date = date('Y-m-d H:i:s');
		$ua=[];
		foreach ($this->achievement as $name => $achieve) {
			if (isset($achieve->date)){
				$ua[$name]=$achieve->date;
			}
		}
		db::query("UPDATE `".PREFIX_SPEC."users` SET `achievements`='". json_encode($ua)."' WHERE `id`={$this->uid}");
		return $this->achievement->{$achievement}->date;
	}
	public function unsetAchievement($achievement) {
		if (!$this->uid) return false;
		if (empty($this->achievement->{$achievement}->date)) return true;
		unset($this->achievement->{$achievement}->date);
		$ua=[];
		foreach ($this->achievement as $name => $achieve) {
			if (isset($achieve->date)){
				$ua[$name]=$achieve->date;
			}
		}
		db::query("UPDATE `".PREFIX_SPEC."users` SET `achievements`='". json_encode($ua)."' WHERE `id`={$this->uid}");
		return true;
	}
	public function getAllAchievements() {
		$ua=[];
		foreach ($this->achievement as $name => $achieve) {
			$ua[$name]=$achieve;
			unset($ua[$name]->date);
		}
		return $ua;
	}
	public function getUserAchievements() {
		if (!$this->uid) return false;
		$ua=[];
		foreach ($this->achievement as $name => $achieve) {
			if (isset($achieve->date)){
				$ua[$name]=$achieve;
			}
		}
		return $ua;
	}
	public function checkInfo() {
		if (!$this->uid) return false;
		list($about)=db::qrow("SELECT `comment` FROM `".PREFIX_SPEC."users` WHERE `id`={$this->uid}");
		if (!empty($about) && avatar::exists($this->uid))
			return $this->setAchievement('info');
		else
			return $this->unsetAchievement('info');
	}
}
