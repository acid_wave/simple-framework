<?php
namespace example;
use control;
/*
* В этом файле перечисляется список разрешенных методов контроллера для вызова с помощью module::exec
* Требуется заполнить следующие свойства объекта input:
* $this->data - объект обработанных входных переменных
* $this->act - какую функцию обработки используем
*/
class input extends control{
	function __construct($input){# $input - объект входных переменных от других модулей
		if(empty($input->act)) $input->act='index';
		
		/*
			определяет список доступных действий (act)
			если запрошенного act нет в спсике, то будет вызван метод совпадающий с названием данного класса
		*/
		if($input->act=='index'){
			$this->data=[
				'test'=>'',
			];
		}else
			$this->act=false;
	}
}
