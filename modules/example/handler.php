<?php
/*
	Пример описания контроллера
*/
namespace example; #совпадает с именем модуля
use module,db,url,cache;
#Используем сторонние модули
#require_once(module::$path.'/[module_name]/[file_name].php');

/*
	Доступные настройки в методах контроллера:

	$this->headers - объект для изменения заголовков при отдаче
		$this->headers->cookie - установка cookies после завершения работы метода
		$this->headers->location - HTTP Location после завершения работы метода
	$this->cache - array(
 		[methodName1]=>array(
 			'id', #uniq key - (int), (string) or as array of (int), (string)
 			'10 min' #expire
 		)
 		...
 	) - определяет список методов для кэширования
 	$this->view - для явного указания шаблона вывода

	Как передать данные в шаблон:
 		В шаблон передается объект, возвращаемый методом контроллера. Доступен в шаблоне в свойстве $this->data.
 */
class handler{
	#enable cache for selected methods
	public $cache=array(
		#'method'=>array(array(id1,id2),expire)
		'post'=>array(array('url','another-input-param'),'10 min'),
		#'method'=>array(id,expire)
		'index'=>array('url','1 day'),
	);
	function __construct(){
		$this->template='template';#Определяем какой модуль использовать как основной шаблон вывода
		$this->headers=new \stdClass;
	}
	function index(){
		return [
			'test'=>1
		];
	}
	function example(){
		$this->view='name_of_view';# меняет шаблон вывода, по умолчанию совпадает с именем метода
	}
}
