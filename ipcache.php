<?php
error_reporting(0);
if(!isset($_GET['file'])) return;
	$filename=$_GET['file'];

$imgdir='modules/images/files/images/';
$file=$imgdir.$filename;
if(!$len=filesize($file)) $len=0;

$file_extension=strtolower(substr(strrchr($filename,"."),1));
switch($file_extension){
	case "gif": $ctype="image/gif"; break;
	case "png": $ctype="image/png"; break;
	case "jpeg":
	case "jpg": $ctype="image/jpeg"; break;
	case "ico": $ctype="image/x-icon"; break;
	case "bmp": $ctype="image/bmp"; break;
	default: $ctype=false;
}
header("Content-Length: $len");
if($ctype) header("Content-Type: $ctype");

echo file_get_contents($file);

if(isset($_SERVER['HTTP_X_REAL_IP']))
	$ip=$_SERVER['HTTP_X_REAL_IP'];
elseif(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
elseif(isset($_SERVER['REMOTE_ADDR']))
	$ip=$_SERVER['REMOTE_ADDR'];
else $ip='';

if(!empty($ip))
	memcachedStore($ip,$filename);

function memcachedStore($ip,$filename){
	if(!class_exists('Memcached')) return;
	$m=new Memcached();
	$m->addServer('localhost', 11211);
	$m->set($ip,$filename,600);#expire 10 min
}
/*
	Тестирование
	- установить libmemcached-tools

	- получить все ключи:
		memcdump --servers=localhost
	- получить значение по ключу:
		telnet localhost 11211
			get [key]

*/
?>